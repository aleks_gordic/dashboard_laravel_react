import { toast ,Zoom} from 'react-toastify';



const config = {
    position: "top-right",
    autoClose: 2000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    transition:Zoom

};



export function success(message) {
    toast.success(message, config);
}
  
export function error(message) {
    toast.error(message, config);
}
  