import React, { Component } from "react";
import ReactCodeInput from "react-code-input";
import API from "@services/APIs";
import { API_URL } from "@js/config";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const codeInputProps = {
  inputStyle: {
    fontFamily: "monospace",
    margin: "4px",
    MozAppearance: "textfield",
    width: "60px",
    borderRadius: "3px",
    fontSize: "32px",
    height: "80px",
    paddingLeft: "7px",
    backgroundColor: "white",
    border: "1px solid #959595",
    textAlign: "center"
  },
  inputStyleInvalid: {
    fontFamily: "monospace",
    margin: "4px",
    MozAppearance: "textfield",
    width: "60px",
    borderRadius: "3px",
    fontSize: "32px",
    height: "80px",
    paddingLeft: "7px",
    backgroundColor: "white",
    border: "1px solid red"
  }
};
class SmsVerification extends Component {
  constructor(props) {
    super(props);

    this.state = {
      warn: false,
      color: "#959595",
      code: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    let headers = {
      Authorization: "Bearer " + this.props.session
    };

    API.get(`/sms/sendAuth`, {}, headers)
      .then(({ data }) => {})
      .catch(err => {
        console.warn(err);
      });
  }

  checkAuth = code => {
    let headers = {
      Authorization: "Bearer " + this.props.session
    };
    API.post("/user/sms-verify", { code: code, register: false }, headers)
      .then(({ verify }) => {
        if (!verify) this.setState({ warn: true });
        else this.props.handleClick();
      })
      .catch(err => {
        console.log(err);
      });
  };
  handleChange = code => {
    this.setState({ code: code, warn: false });
    if (code.length >= 6) this.checkAuth(code);
  };
  render() {
    const { warn, code } = this.state;
    let color = warn ? "red" : "#959595";
    return (
      <div className="bg-white mt-5 p-5">
        <h1 className="two--step__header">
          Complete log in with SMS authentication code
        </h1>
        <hr />
        <br />
        <p className="text-center">
          Enter the 6-digit code shown in the SMS sent to
        </p>
        <br />
        <div className="d-flex justify-content-center align-items-center">
          <ReactCodeInput
            fields={6}
            value={code}
            {...codeInputProps}
            inputStyle={{
              ...codeInputProps.inputStyle,
              color: color,
              borderColor: color,
              border: "none",
              borderBottom: `1px solid ${color}`
            }}
            onChange={this.handleChange}
          />
        </div>
        {color == "red" && (
          <p className="d-table ml-auto mr-auto" style={{ color: "red" }}>
            <FontAwesomeIcon
              icon="exclamation-circle"
              size="lg"
              className="mr-4"
              color="red"
            />
            You six digit verification is not correct/expired
          </p>
        )}
        <div className="two--step__footer d-flex align-items-center justify-content-center">
          <label onClick={this.props.changeMethod} className="btn">
            Use another authentication method
          </label>
        </div>
      </div>
    );
  }
}
export default SmsVerification;
