import { Button } from "reactstrap";
import React, { Component } from "react";
class ChooseAuthenticator extends Component {
  render() {
    return (
      <div className="vh-100 vw-100 form--choose-authenticatior">
        <div className="w-50 ml-auto mr-auto mt-4 d-flex flex-column align-items-center justify-content-center">
          <h2> How would you like to authenticate your log in?</h2>
          <div
            className="mt-5 btn--authenticator-app"
            onClick={this.props.chooseApp}
          >
            Authenticator app
          </div>
          <div
            className="mt-5 btn--authenticator-app"
            onClick={this.props.chooseSms}
          >
            SMS authentication
          </div>
        </div>
      </div>
    );
  }
}

export default ChooseAuthenticator;
