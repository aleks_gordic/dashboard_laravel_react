import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Form, FormGroup } from "reactstrap";
import API from "@services/APIs";
import {connect} from "react-redux";
import UserAction from "@js/store/actions/user"
import { withRouter } from "react-router-dom";
import { ASSETS_URL, API_URL } from "@js/config";

class PasswordSet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      passwordAgain: "",
      moreCharacter: false,
      fullCharacter: false,
      oneNumber: false,
      passwordMismatch: false
    };
  }

  handlePassword = e => {
    const { value } = e.target;
    this.setState({ password: value });

    if (this.state.passwordAgain) {
      if (value != this.state.passwordAgain)
        this.setState({ passwordMismatch: true });
      else this.setState({ passwordMismatch: false });
    }

    if (value.length > 7) this.setState({ moreCharacter: true });
    else this.setState({ moreCharacter: false });

    if (/[a-z]/.test(value) && /[A-Z]/.test(value)) {
      this.setState({ fullCharacter: true });
    } else this.setState({ fullCharacter: false });

    if (/[0-9]/.test(value)) this.setState({ oneNumber: true });
    else this.setState({ oneNumber: false });
  }

  handlePasswordAgain = e => {
    const { value } = e.target;
    this.setState({ passwordAgain: value });
    if (value != this.state.password) this.setState({ passwordMismatch: true });
    else this.setState({ passwordMismatch: false });
  }

  handleCancel = e => {
    this.props.history.push("/portal");
  }

  handleClick = e => {
    const {
      passwordMismatch,
      moreCharacter,
      oneNumber,
      fullCharacter,
      password,
      passwordAgain
    } = this.state;

    const { MIX_ENABLE_2FA =  true } = process.env
    const enable2FA = (MIX_ENABLE_2FA === 'true')

    console.log({passwordMismatch,
      moreCharacter,
      oneNumber,
      fullCharacter,
      password,
      passwordAgain})


    if (
      !passwordMismatch &&
      passwordAgain &&
      moreCharacter &&
      oneNumber &&
      fullCharacter
    ) {
      let data = {
        password: password
      };

      API.post("/password/set", data).then(res => {
        if (res.success) {
          this.props.setCurrentUser(res.user);
          if(!enable2FA){
            this.props.history.push("/portal");
          }
          // this.props.setSubTitleHeader(false);
        }
      });
    }
  }

  render() {
    const {
      moreCharacter,
      fullCharacter,
      oneNumber,
      passwordMismatch
    } = this.state;
    return (
      <div className="password-set d-flex justify-content-center">
        <Form onSubmit={this.handleClick} className="bg-white mt-5 p-5 w-100">
          <h4 className="form-title text-center border-primary pb-2">
            Choose Your Password
          </h4>
          <br />
          <p className="text-center description">Your password must have:</p>
          <div className="d-flex justify-content-center align-item-center description">
            <div>
              <div className="mb-1 d-block">
                <img
                  src={`${ASSETS_URL}/icons/${
                    moreCharacter ? "Icon-Check" : "Icon-Uncheck"
                  }.png`}
                  alt=""
                  className="check mr-1 align-middle"
                  width="20"
                />
                <span className="d-inline-block align-middle">
                  8 or more characters
                </span>
              </div>
              <div className="mb-1 d-block">
                <img
                  src={`${ASSETS_URL}/icons/${
                    fullCharacter ? "Icon-Check" : "Icon-Uncheck"
                  }.png`}
                  alt=""
                  className="check mr-1"
                  width="20"
                />
                <span className="d-inline-block align-middle">
                  Upper & Lowercase letters
                </span>
              </div>
              <div className="mb-4 d-block">
                <img
                  src={`${ASSETS_URL}/icons/${
                    oneNumber ? "Icon-Check" : "Icon-Uncheck"
                  }.png`}
                  alt=""
                  className="check mr-1 align-middle"
                  width="20"
                />
                <span className="d-inline-block align-middle">
                  At least one number
                </span>
              </div>
            </div>
          </div>
          <FormGroup>
            <input
              type="password"
              className="form-control"
              name="username"
              id="username"
              placeholder="Enter password"
              onChange={this.handlePassword}
              autoComplete="off"
            />
          </FormGroup>
          <FormGroup>
            <input
              type="password"
              className={`form-control ${passwordMismatch ? "border-red" : ""}`}
              name="password"
              id="password"
              placeholder="Enter password again"
              onChange={this.handlePasswordAgain}
            />
          </FormGroup>
          <div className="d-flex justify-content-between pt-2">
            <Button
              onClick={this.handleCancel}
              className="login-cta mr-5"
              color="primary"
              outline
              block
            >
              Cancel
            </Button>

            <Button
              onClick={this.handleClick}
              className="login-cta ml-5 d-block w-100"
              color="primary"
            >
              Next
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(PasswordSet)
)

function mapDispatchToProps(dispatch) {
  return {
    setCurrentUser: data => dispatch(UserAction.setCurrentUser(data)),
  }
}

function mapStateToProps({ user }) {
  return { user }
}

