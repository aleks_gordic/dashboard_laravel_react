import React, { Component } from "react";
import { Button } from "reactstrap";
import ReactCodeInput from "react-code-input";
import API from "@services/APIs";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const codeInputProps = {
  inputStyle: {
    fontFamily: "monospace",
    margin: "4px",
    MozAppearance: "textfield",
    width: "60px",
    borderRadius: "3px",
    fontSize: "32px",
    height: "80px",
    paddingLeft: "7px",
    backgroundColor: "white",
    border: "1px solid #959595",
    textAlign: "center"
  },
  inputStyleInvalid: {
    fontFamily: "monospace",
    margin: "4px",
    MozAppearance: "textfield",
    width: "60px",
    borderRadius: "3px",
    fontSize: "32px",
    height: "80px",
    paddingLeft: "7px",
    backgroundColor: "white",
    border: "1px solid red"
  }
};
class AuthenticationCode extends Component {
  constructor(props) {
    super(props);

    this.state = {
      warn: false,
      color: "#959595",
      code: ""
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleClick = () => {
    const { code } = this.state;
    const { phoneNumber } = this.props;
    API.post("/user/sms-verify", {
      code: code,
      phoneNumber: phoneNumber,
      register: "register"
    })
      .then(({ verify, user }) => {
        if (!verify) this.setState({ warn: true });
        else this.props.setCurrentUser(user);
      })
      .catch(err => {
        console.log(err);
      });
  };
  handleChange = code => {
    this.setState({ code });
  };
  render() {
    const { warn, code } = this.state;
    let color = warn ? "red" : "#959595";
    return (
      <div className="two--step__content bg-white mt-5 p-5">
        <h1 className="two--step__header">Mobile Number Verification</h1>
        <hr />
        <br />
        <p className="description">
          Please check your messages and enter in the six digit verification
          code below sent to {this.props.phoneNumber}
        </p>
        <br />
        <div className="d-flex justify-content-center align-items-center mb-3">
          <ReactCodeInput
            fields={6}
            value={code}
            {...codeInputProps}
            inputStyle={{
              ...codeInputProps.inputStyle,
              color: color,
              borderColor: color,
              border: "none",
              borderBottom: `1px solid ${color}`
            }}
            onChange={this.handleChange}
          />
        </div>
        {color == "red" && (
          <p className="d-table" style={{ color: "red" }}>
            <FontAwesomeIcon
              icon="exclamation-circle"
              size="lg"
              className="mr-4"
              color="red"
            />
            You six digit verification is not correct/expired
          </p>
        )}
        <div className="d-flex justify-content-between phone--verify__submit">
          <Button
            onClick={this.props.handleBack}
            color="primary"
            outline
            className="verification--button__back"
          >
            Back
          </Button>

          <Button
            onClick={this.handleClick}
            color="primary"
            className="verification--button__next"
          >
            Next
          </Button>
        </div>
      </div>
    );
  }
}
export default AuthenticationCode;
