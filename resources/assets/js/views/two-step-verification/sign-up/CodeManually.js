import React, { Component } from "react";
import { Button, Input } from "reactstrap";
import API from "@services/APIs";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class CodeManually extends Component {
  constructor(props) {
    super(props);

    this.state = {
      warn: false,
      color: "#959595",
      code: ""
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleClick = () => {
    this.props.handleClick();
  };
  handleChange = code => {
    this.setState({ code: code, warn: false });
  };
  render() {
    const { qr_code_key, email } = this.props;
    return (
      <div className="two--step__content bg-white mt-5 p-5">
        <h1 className="two--step__header">Enter Manually</h1>
        <hr />
        <br />
        <span>Account Name</span>
        <Input readOnly defaultValue={email} className="text-center" />
        <span>Key</span>
        <Input readOnly defaultValue={qr_code_key} className="text-center" />
        <br />
        <br />
        <div className="d-flex justify-content-between">
          <Button
            onClick={this.props.handleBack}
            color="primary"
            outline
            className="verification--button__back"
          >
            Back
          </Button>

          <Button
            onClick={this.handleClick}
            color="primary"
            className="verification--button__next"
          >
            Next
          </Button>
        </div>
      </div>
    );
  }
}
export default CodeManually;
