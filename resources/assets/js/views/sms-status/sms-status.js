import React, { Component } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import { connect } from 'react-redux';
import App from '@components/App';
import {withRouter} from 'react-router-dom'
import { API_URL} from '@js/config';

import Transaction from '@services/Transaction';
import Invitation from '@services/Invitation';

import {success, error} from '@js/lib/Tostify'
import AdditionalApplicants from '@components/Modals/AdditionalApplicants';
//import RequestSent from '@components/Modals/RequestSent';

import ReviewCustomer from '@components/Modals/ReviewCustomer';

import AddressReview from '@components/Modals/AddressReview';
import AwaitingSMS from '@components/Modals/AwaitingSMS';
import CaptureID from '@components/Modals/CaptureID';
import CaptureInfoScreen from '@components/Modals/CaptureInfoScreen';

import CustomerInfoReview from '@components/Modals/CustomerInfoReview';
import DeclarationSMS from '@components/Modals/DeclarationSMS';
import TermAndCondition from '@components/Modals/TermAndCondition';
import BankDetails from '@components/Modals/BankDetails';
import AdditionalDetails from '@components/Modals/AdditionalDetails';
import FaceCapture from '@components/Modals/FaceCapture';
import FaceCaptureFail from '@components/Modals/FaceCaptureFail';
import SessionCanceled from '@components/Modals/SessionCanceled';

import SelectID from '@components/Modals/SelectID';
import SmsActivated from '@components/Modals/SmsActivated';

import VerificationResult from '@components/VerificationResult';
import NoRecordFoundGeneric from '@components/NoRecordFoundGeneric';

import './sms-status.style.scss'
import PageLoader from "../../components/PageLoader";



/*Call
/api/mobile/update/{transactionId}

for changing the status to the following

-waiting
-activated
-reviewPrivacy
-reviewInvestment(in case of investment)
-selectId
-captureId
-capturing
-approveId
----
A this point mobile need to call
/api/mobile/approval/{transactionId}
until it gets the following response

{
  status:true
}

--------
-reviewInfo
-reviewAddress
-reviewFrInfo
-faceCapture
-frLivenessFail(in case of liveness fail)
--------------------------
bankDetails
investmentDetails
additionalDetails
--------------------------*/


class SmsStatus extends Component {

    /** The component's constructor */
    constructor(props) {
        super(props);

        this.state = {
            status:'',
            handle:null,
            id:'',
            valid:false,
            loading:true,
            showAdditional:false,
            additionalApplicants:{}
        };

    }

    validate = () => {

        let id = this.state.id;
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.validateGuided(id, headers )
            .then(({status}) => {
                let valid;
                if (status === 'success') {
                    valid = true;

                    let handle = setInterval(()=> {
                        this.checkStatus()
                    },2500)

                    this.setState({
                        handle: handle
                    })

                }else{
                    //error(data.message);
                    valid=false;
                }
                this.setState({
                    valid,
                    loading:false
                })
            })
            .catch(err => {
                console.warn(err);
            });
    }

    handleBack = () => {
        this.props.history.goBack()
    }

    checkStatus = () => {

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        Transaction.getUserStatus(this.state.id, headers)
            .then(({status, data, message}) => {
                if (status === 'success') {

                    if(data === 'completed' || data === 'cancelled'){
                        clearInterval(this.state.handle)
                    }

                    this.setState({
                        status: data
                    })

                }
            })
            .catch(err => {
                console.warn(err);
            });

    }

    componentWillMount(){
        this.setState({
            id: this.props.location.hash.replace('#',''),
        })

    }

    componentDidMount(){
        this.validate();
    }

    sendToAdditional = () => {

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        Invitation.sendAdditional(this.state.id, headers)
            .then(({status, data, showPopup, message}) => {

                if (status === 'success') {

                    if(showPopup){
                        this.setState({
                            showAdditional: showPopup,
                            additionalApplicants: data,
                            status:''
                        })
                    }else{
                        this.props.history.push(`/portal`);
                    }
                }
            })
            .catch(err => {
                console.warn(err);
            });

    }

    handleVerification = async (action)=>{
        if(action === 'submit'){

            //----Send to additional applicants if here is any
            await this.sendToAdditional();

        }else if(action === 'redo'){
            //restart the timer
            //Redo sms status changed to waiting
            //Redo transaction status changed to Initiated
            let handle = setInterval(()=> {
                this.checkStatus()
            },2500)

            this.setState({
                showAdditional:false,
                status:'',
                handle:handle

            })
            //this.props.history.push(`/portal`);
        }else if(action === 'cancel'){
            //Redo sms status changed to expired
            //Redo transaction status changed to Terminated
            this.setState({
                showAdditional:false,
                status:''
            })
            this.props.history.push(`/portal`);
        }

    }

    renderStatusModal = () => {
        if(!this.state.valid) return ''

        let component = '';

        switch(this.state.status) {
            case 'address': {
                component = <AddressReview/>
                break;
            }
            case 'waiting': {
                component = <AwaitingSMS/>
                break;
            }
            case 'capturingId': {
                component = <CaptureID/>
                break;
            }
            case 'faceScanTips': {
                component = <CaptureInfoScreen/>
                break;
            }
            case 'reviewInfo': {
                component = <CustomerInfoReview/>
                break;
            }
            case 'privacy': {
                component = <DeclarationSMS/>
                break;
            }
            case 'termsAndCondition': {
                //component = <TermAndCondition/>
                component = <DeclarationSMS/>
                break;
            }
            case 'bankDetails': {
                component = <BankDetails/>
                break;
            }
            case 'additionalDetails': {
                component = <AdditionalDetails/>
                break;
            }
            case 'faceCapture': {
                component = <FaceCapture/>
                break;
            }
            case 'livenessFail': {
                component = <FaceCaptureFail/>
                break;
            }
            case 'cancelled': {
                component = <SessionCanceled/>
                break;
            }
            case 'selectId': {
                component = <SelectID/>
                break;
            }
            case 'activated': {
                component = <SmsActivated/>
                break;
            }
            case 'checkDocument': {
                component = <ReviewCustomer/>
                break;
            }
            case 'completed': {
                component = <VerificationResult onActionTaken={this.handleVerification}/>
                break;
            }
            default: {
                component = ''
                break;
            }
        }


        let comp;
        if(this.state.status && this.state.status !== 'completed'){
            comp = (<Modal isOpen={true} >
                <ModalBody>
                    {component}
                </ModalBody>
            </Modal>)

        }else{
            comp = component;
        }

        return comp;

    }

    renderAdditionalApp = () => {
        return <AdditionalApplicants show={this.state.showAdditional} additional={this.state.additionalApplicants}/>
    }

    /**
     * Render the component's markup
     * @return {ReactElement}
     */
    render() {

        let modal = this.renderStatusModal();

        return (
            <App>
                {this.renderAdditionalApp()}
                {modal}
                {!this.state.valid && !this.state.loading && <NoRecordFoundGeneric onClickCta={this.handleBack} message="No Action required for this transaction."/>}
            </App>
        );
    }
}

export default withRouter(connect(mapStateToProps, null)(SmsStatus));
function mapStateToProps({user}) {
    return {session: user.session};
}