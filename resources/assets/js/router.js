import React from "react"
import { connect } from "react-redux"
import {
    BrowserRouter as Router,
    Route,
    Switch,
    withRouter,
    Redirect
} from "react-router-dom"
import Loadable from "react-loadable"
import { getC, setC} from "@js/lib/Cookie";
import PageLoader from "./components/PageLoader"
import UserAction from "@js/store/actions/user"

const Login = Loadable({
    loader: () => import("./views/login"),
    loading: PageLoader
})

const ResetPasswordSendMail = Loadable({
    loader: () => import("./views/reset-password/reset-password-send-mail"),
    loading: PageLoader
})

const ResetPassword = Loadable({
    loader: () => import("./views/reset-password/reset-password"),
    loading: PageLoader
})

const Modules = Loadable({
    loader: () => import("./views/modules"),
    loading: PageLoader
})

const TransSingle = Loadable({
    loader: () => import("./views/transaction-single"),
    loading: PageLoader
})

const SmsStatus = Loadable({
    loader: () => import("./views/sms-status"),
    loading: PageLoader
})

const PageNotFound = Loadable({
    loader: () => import("./views/page-not-found"),
    loading: PageLoader
})
const TwoStepVerification = Loadable({
    loader: () => import("./views/two-step-verification"),
    loading: PageLoader
})

//-------------------------
//check login
function authenticate() {
    let token = document.head.querySelector('meta[name="csrf-token"]')

    let fetchData = {
        method: "GET",
        headers: {
            "X-CSRF-TOKEN": token.content
        }
    }
    return fetch("/portal/check", fetchData).then(function(response) {
        return response.json()
    })
}
//-------------------------

function PrivateRoute({ component: Component, user, isLoggedIn, ...rest }) {
    return (
        <Route
            {...rest}
            render={props =>{
                    if(isLoggedIn){
                        const { role, google_secret, password_status, mobile_verified_at } = user
                        const { MIX_ENABLE_2FA =  true } = process.env
                        const enable2FA = (MIX_ENABLE_2FA === 'true')
                        const twoStepLogin = getC('twoStepLogin') === 'true'

                        //google_secret = is2FASetup
                        //password_status = isPasswordSetup

                        if(parseInt(password_status) && role === 'admin'){
                            // do nothing
                            return <Component {...props} />

                        }else if(!parseInt(password_status) || (enable2FA && (!google_secret || !mobile_verified_at || (google_secret && !twoStepLogin)))){
                            // do nothing
                            return <Redirect
                                to={{
                                    pathname: "/portal/twoStep",
                                    state: { from: props.location }
                                }}
                            />

                        }else{
                            return <Component {...props} />
                        }

                    }else{
                       return <Redirect
                            to={{
                                pathname: "/portal/login",
                                state: { from: props.location }
                            }}
                        />
                    }
                }
            }
        />
    )
}

class router extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loaded: false
        }
    }

    componentWillMount() {
        let isLoggedIn = !_.isEmpty(this.props.user.info)

        if (!isLoggedIn) {
            //console.log('router',isLoggedIn)
            authenticate()
                .then(res => {
                    if (res.status === "success") {
                        //console.log('logged in',res)
                        const data = res.data
                        if(data.role === 'admin') {
                            this.props.setTwoStepStatus(true)
                        }
                        this.props.setCurrentUser(data)
                        this.props.setCurrentSession(res.session)
                    } else {
                        //console.log('not logged in')
                        this.props.setCurrentUser(null)
                    }

                    this.setState({
                        loaded: true
                    })
                })
                .catch(function(e) {
                    console.log('auth check err',e)
                })
        }
    }

    render() {
        if (!this.state.loaded) return false
        const { user } =this.props
        let isLoggedIn = !_.isEmpty(user.info)
        let info = {}
        if(isLoggedIn){
            info = user.info
        }

        return (
            <Router>
                <Switch>
                    <Route path="/portal/login" component={withRouter(Login)} />

                    <Route
                        exact
                        path="/portal/password/reset"
                        component={withRouter(ResetPasswordSendMail)}
                    />
                    <Route
                        exact
                        path="/portal/password/reset/:token/:email"
                        component={withRouter(ResetPassword)}
                    />

                    <Route
                        exact
                        path="/portal/twoStep"
                        component={withRouter(TwoStepVerification)}
                    />

                    <PrivateRoute
                        exact
                        user={info}
                        isLoggedIn={isLoggedIn}
                        path="/portal/transaction/:tId"
                        component={withRouter(TransSingle)}
                    />
                    <PrivateRoute
                        exact
                        user={info}
                        isLoggedIn={isLoggedIn}
                        path="/portal/sms/user-status"
                        component={withRouter(SmsStatus)}
                    />
                    <PrivateRoute
                        user={info}
                        isLoggedIn={isLoggedIn}
                        path="/portal/:module?/:workflow?"
                        component={withRouter(Modules)}
                    />

                    <Route path="*" component={PageNotFound} />
                </Switch>
            </Router>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(router)

function mapStateToProps({ user }) {
    return { user }
}

function mapDispatchToProps(dispatch) {
    return {
        setCurrentUser: data => dispatch(UserAction.setCurrentUser(data)),
        setTwoStepStatus: data => dispatch(UserAction.setTwoStepStatus(data)),
        setCurrentSession: data => dispatch(UserAction.setCurrentSession(data))
    }
}
