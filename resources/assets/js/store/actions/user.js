export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const SET_CURRENT_SESSION = "SET_CURRENT_SESSION";
export const SET_TWO_STEP_STATUS = "SET_TWO_STEP_STATUS";
export const SET_SUB_TITLE_HEADER = "SET_SUB_TITLE_HEADER";
export default class Document {
  /**
   * Return an action to update the document info
   * @param {Object} data The document data extracted
   */
  static setCurrentUser(data) {
    return {
      type: SET_CURRENT_USER,
      data
    };
  }

  static setCurrentSession(data) {
    return {
      type: SET_CURRENT_SESSION,
      data
    };
  }

  static setTwoStepStatus(data) {
    return {
      type: SET_TWO_STEP_STATUS,
      data
    };
  }

  static setSubTitleHeader(data) {
    return {
      type: SET_SUB_TITLE_HEADER,
      data
    };
  }
}
