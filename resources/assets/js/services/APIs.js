import axios from 'axios';
import {API_URL} from '@js/config';

export default class APIs {


    static createUser(params, headers = {}) {
        const data = new FormData();
        Object.keys(params).forEach(field => data.append(field, params[field]));

        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/user/create`, data, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static resetPassword(params, headers = {}) {
        const data = new FormData();
        Object.keys(params).forEach(field => data.append(field, params[field]));

        let url = `${API_URL}/user/resetPassword`;

        return new Promise((resolve, reject) => {
            axios
                .post(url, data, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    console.warn(err);
                    reject("");
                });
        });
    }

    static resetTwoStep(params, headers = {}) {
        const data = new FormData();
        Object.keys(params).forEach(field => data.append(field, params[field]));

        let url = `${API_URL}/user/resetTwoStep`;

        return new Promise((resolve, reject) => {
            axios
                .post(url, data, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    console.warn(err);
                    reject("");
                });
        });
    }

    static getCurrentUser(params, headers = {}) {
        let url = `${API_URL}/user/getCurrentUser`;

        return new Promise((resolve, reject) => {
            axios
                .get(url, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    console.warn(err);
                    reject("");
                });
        });
    }

    static updateUser(params, headers = {}) {
        const data = new FormData();
        Object.keys(params).forEach(field => data.append(field, params[field]));

        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/user/update`, data, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static fetchUser(id = null, headers = {}, params = {}) {

        let url = `${API_URL}/user/read`;
        if (id) {
            url = `${API_URL}/user/read/${id}`
        }

        return new Promise((resolve, reject) => {
            axios
                .get(url, {headers, params})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static deleteUser(pk, headers = {}) {

        let url = `${API_URL}/user/delete`;

        return new Promise((resolve, reject) => {
            axios
                .post(url, {pk}, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static searchUser(query, headers = {}) {

        let url = `${API_URL}/user/search`;

        let params = {
            q: query
        }

        return new Promise((resolve, reject) => {
            axios
                .get(url, {params, headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static country() {

        return new Promise((resolve, reject) => {
            axios
                .get(`/api/portal/country`)
                .then(({ data }) => resolve(data))
                .catch(err => {
                    console.warn(err);
                    reject('');
                });
        });
    }

    //------------------------------
    static post(path, params, headers = {}) {
        const data = new FormData();
        Object.keys(params).forEach(field => data.append(field, params[field]));

        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}${path}`, data, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static get(path, params, headers = {}) {

        let url = `${API_URL}${path}`;
        let input = {
            params: params,
            headers
        }

        return new Promise((resolve, reject) => {
            axios
                .get(url, input)
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }
}
