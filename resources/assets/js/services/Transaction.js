import axios from 'axios';
import {API_URL} from '@js/config';

/**
 * SMS Service
 */
export default class Transaction {
    /**
     * Make a request to verify address.
     * @param {Integer} id
     * @param {Object} headers
     * @return {Promise}
     */
    static getUserStatus(id, headers = {}) {
        let url = `${API_URL}/transaction/guided/status/${id}`;
        let input = {
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url, input)
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static validateGuided(id, headers = {}) {
        let url = `${API_URL}/transaction/guided/validate/${id}`;
        let input = {
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url, input)
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static update(data, headers = {}) {
        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/transaction/guided/update`, data, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static resendOrTerminate(data, headers = {}) {
        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/transaction/update`, data, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static getCards(id, headers = {}) {
        let url = `${API_URL}/transaction/guided/cards/${id}`;
        let input = {
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url, input)
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static approveCards(data, headers = {}) {
        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/transaction/guided/cards/approve`, data, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static delete(id, headers = {}) {
        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/transaction/delete/${id}`, {}, {headers})
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static getCompleted(params = {}, headers = {}) {

        let url = `${API_URL}/transaction/completed`;
        let input = {
            params,
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url, input)
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static getPending(params = {}, headers = {}) {

        let url = `${API_URL}/transaction/pending`;
        let input = {
            params,
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url, input)
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static getDetails(id, headers = {}) {
        let url = `${API_URL}/transaction/${id}`;
        let input = {
            headers
        }
        return new Promise((resolve, reject) => {
            axios
                .get(url, input)
                .then(({data}) => resolve(data))
                .catch(err => {
                    if (err.response.status === 401) {
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

}
