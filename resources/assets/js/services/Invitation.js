import axios from 'axios';
import {API_URL} from '@js/config';
/**
 * SMS Service
 */
export default class Invitation {
    /**
     * Make a request to verify address.
     * @param {Object} data
     * @param {Object} headers
     * @return {Promise}
     */
    static send(data, headers = {}) {
        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/invitation/send`, data, {headers})
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }

    static sendAdditional(id, headers = {}) {
        return new Promise((resolve, reject) => {
            axios
                .post(`${API_URL}/invitation/additional/${id}`, {}, {headers})
                .then(({ data }) => resolve(data))
                .catch(err => {
                    if(err.response.status === 401){
                        location.reload()
                    }
                    reject(err)
                });
        });
    }
}
