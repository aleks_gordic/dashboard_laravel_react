import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { withRouter } from 'react-router-dom'
import './NotFound.style.scss'

class NotFound extends Component {

	/** The component's constructor */
	constructor(props) {
		super(props);
		this.state = {};
	}


    handleBack = () => {
        this.props.history.goBack()
    }


    /**
	 * Render the component's markup
	 * @return {ReactElement}
	 */
	render() {
		return (
            <section className="page-404">
                <div className="page-404-inner">
                    <h2>{this.props.message}</h2>
                    <Button onClick={this.handleBack} color="primary">Go Back</Button>
                </div>
            </section>
			);
	}
}

NotFound.propTypes = {
    message: PropTypes.string.isRequired,
};

NotFound.defaultProps = {
    message: '404 - Page does not exists',
};

export default withRouter(NotFound);
