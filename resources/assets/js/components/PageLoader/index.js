import React, { Component } from 'react';


export default class IndexPage extends Component {

    /** The component's constructor */
    constructor(props) {
        super(props);

        this.state = {

        };

    }

    render() {
        return (
            <div className="page-loader d-flex flex-column align-items-center justify-content-center">
                <div className="donut"></div>
                <h4 className="mt-2">Please wait</h4>
            </div>
        );
    }
}

