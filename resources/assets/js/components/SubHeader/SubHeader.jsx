import React, {Component} from 'react';
import {withRouter} from 'react-router-dom'
import {ASSETS_URL} from '@js/config';
import { getC } from '@js/lib/Cookie';
import './SubHeader.style.scss';


class SubHeader extends Component {

    /** The component's constructor */
    constructor(props) {
        super(props);

        this.state = {
            title: 'Identity Portal - Menu',
            showBack: true,
            showClose: false
        };

    }

    handleBack = (e) => {
        e.preventDefault()

        const {match = {}, location = {}} = this.props;
        const { pathname } = location
        const { params = {} }= match;
        const { module = '', workflow = '', tId ='' } = params
        let path  = 'portal'
        if(module && workflow && workflow !== 'role'){
            //path = `portal/${module}`
            path = `portal`
        } else if (pathname.includes('transaction') && tId ){
            if(getC('page') > 1){
                path = `portal/customer-review/customer-repository#${getC('page')}`
            }else{
                path = `portal/customer-review/customer-repository`
            }

            //this.props.history.goBack()
        } else if (pathname.includes('sms')){
            path = `portal/remote-verification`
        } else if (pathname.includes('role')){
            path = `portal/management/user`
        }
        this.props.history.push(`/${path}`);

    }

    render() {

        const {match = {}, location = {}} = this.props;
        const { pathname } = location
        const { params = {} }= match;
        const { module = '', workflow = '' } = params
        let showBackBtn = (module || workflow || pathname.includes('transaction') || pathname.includes('sms'))
        let title = this.state.title;

        if (module && workflow){
            title = `${module.replace(/-/g, ' ')} - ${workflow.replace(/-/g, ' ')}`;
        }else if( module && workflow.length === 0){
            title = `Identity Portal - ${module.replace(/-/g, ' ')}`;
        }

        let backCtaClass = (pathname === '/portal/login' || (this.props.match && this.props.match.path === '/portal/:module?')) ? 'back-arrow hide' : 'back-arrow'
        let closeCtaClass = (this.state.showClose) ? 'close' : 'close hide'

        return (
            <div
                className="header-strip bg-primary text-white text-center text-capitalize d-flex justify-content-around">
                {showBackBtn && <a href="#" className={backCtaClass} onClick={this.handleBack}>
                    <img src={`${ASSETS_URL}/icons/back-arrow.png`} alt=""/>
                </a>}
                <h4 className="mb-0 page-title">{title}</h4>
                <a href="#" className={closeCtaClass}>
                    <img src={`${ASSETS_URL}/icons/close-cross.png`} alt=""/>
                </a>
            </div>
        );
    }

};
export default withRouter(SubHeader);
