import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import classnames from 'classnames';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { ASSETS_URL } from '@js/config';
import  './Asf.style.scss';

class Asf extends Component  {

    constructor(props) {
        super(props);
        this.state = {};
    }

    toggle = (e) => {
        e.preventDefault()
        this.props.onClose();
    }

    render() {

        let a_failed = `${ASSETS_URL}/app/status-no.png`;
        let a_success = `${ASSETS_URL}/app/status-yes.png`;
        let a_warning = `${ASSETS_URL}/app/warning.png`;

        let { show, asfItems = {} } = this.props;
        let n_asfItems = {...asfItems};
            delete n_asfItems.overall;
        n_asfItems = Object.keys(n_asfItems).map((key, i) => {
            return (
                <div className="asf-item" key={key}>
                    <h5>
                            <span>
                                <img src={n_asfItems[key] ? a_success : a_failed} alt="" />
                            </span>
                        {/*<span>{key} - </span>*/}
                        <span>Check {i+1} - </span>
                        <span>{n_asfItems[key] ? 'Pass' : 'Fail'}</span>
                    </h5>
                </div>
            );
        });

        return (
          <div>
            <Modal size="md" isOpen={show||false} className="modal-class">
                <ModalHeader toggle={this.toggle}>Document Fraud Breakdown</ModalHeader>
                <div className="asf-items">
                    <div className={classnames("asf-item", "overall", { "success" : asfItems.overall },{ "failed" : !asfItems.overall })}>
                        <h5>
                        <span>
                            <img src={asfItems.overall ? a_success : a_failed} alt="" />
                        </span>
                            <span>Overall - </span>
                            <span>{asfItems.overall ? 'True': 'False'}</span>
                        </h5>
                    </div>
                    <div className="">
                        {n_asfItems}
                    </div>

                </div>
            </Modal>
        </div>
    );
  }
}

export default  withRouter(Asf)