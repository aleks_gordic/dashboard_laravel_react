import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import classnames from 'classnames';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { ASSETS_URL } from '@js/config';
import  './Features.style.scss';

class Features extends Component  {

    constructor(props) {
        super(props);
        this.state = {};
    }

    toggle = (e) => {
        e.preventDefault()
        this.props.onClose();
    }

    render() {

        let { show, features = {} } = this.props;
        let faceFetatures = Object.keys(features).map((key, i) => {
            let obj = {};
            if (parseFloat(features[key]) > 75) {
                obj = {
                    img: 'success',
                    cls: 'success'
                };
            } else if (parseFloat(features[key]) >= 65 && parseFloat(features[key]) < 75) {
                obj = {
                    img: 'failed',
                    cls: 'orange'
                };
            } else if (parseFloat(features[key]) < 65) {
                obj = {
                    img: 'failed',
                    cls: 'failed'
                };
            } else {
                obj = {
                    img: 'failed',
                    cls: 'failed'
                };
            }

            return (
                <div className="feature" key={i}>
                    <div className={obj.cls + ' inner'}>
                        <div className="icon">
                            <img src={`${ASSETS_URL}/app/${obj.img}.png`} />
                        </div>
                        <div className="result">
                            <h6 className="percent">{features[key]}%</h6>
                            <h6 className="target">{key}</h6>
                        </div>
                    </div>
                </div>
            );
        });

        return (
          <div className="features">
              {faceFetatures}
        </div>
    );
  }
}

export default  withRouter(Features)