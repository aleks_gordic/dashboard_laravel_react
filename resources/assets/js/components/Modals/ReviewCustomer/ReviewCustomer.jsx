import React, {Component} from 'react';
import { Button } from 'reactstrap';
import {ASSETS_URL} from '@js/config';
import  './ReviewCustomer.style.scss';
import {success,error} from '@js/lib/Tostify'
import {withRouter} from 'react-router-dom'
import {connect} from "react-redux";

import Transaction from '@services/Transaction'

class ReviewCustomer extends Component  {

    /** The component's constructor */
    constructor(props) {
        super(props);

        this.state = {
            id: '',
            front: null,
            back: null,
            disableApproveImage:false
        };
    }

    handleClick = () => {

        let params = {
            id: this.state.id,
            action:"cancel"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.history.push(`/portal`);
                    success('Transaction cancelled successfully');
                }else{
                    error(data.message);
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    fetchCards = () => {

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        Transaction.getCards(this.state.id, headers)
            .then(({status, data}) => {
                if (status === 'success') {

                    const { front, back } = data

                    this.setState({
                        front, back
                    })

                }

            })
            .catch(err => {
                console.warn(err);
            });
    }

    sendApproval = (val) => {

        let inputs = {
            status: val,
            id: this.state.id
        };
        this.setState({
            disableApproveImage: true
        })

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        Transaction.approveCards(inputs, headers)
            .then(({ status }) =>{
                if(status === 'success'){
                    success('Action submitted successfully!')
                }else{
                    error('Error while submitting actions!')
                }
            })
            .catch(err => {
                console.warn(err);
            });

    }

    componentWillMount(){
        this.setState({
            id: this.props.location.hash.replace('#',''),
        })
    }

    componentDidMount(){
        this.fetchCards();
    }


    render() {

        let {front, back, disableApproveImage} = this.state;


        let approveClass = (disableApproveImage) ? 'approve_button disabled' : 'approve_button'

        return (
            <div className="custom-modal" >
                <div className="left-side" >

                    <div className="id_Images">

                        <div className="front">
                            <img className="front_card" src={front} />
                            <h5 className="front_name">Front</h5>
                        </div>
                        {back && <div className="back">
                            <img className="back_card" src={back} />
                            <h5 className="back_name">Back</h5>
                        </div>}
                    </div>
                </div>

                <div className="right-side">
                    <div className="right-margin">
                        <h4 className="dialog-title">Review customer ID document</h4>
                        <div className="under-line"></div>
                        <h5 className="dialog-subtitle">Note:</h5>
                        <h5 className="dialog-description">Please check the ID images on the left and make sure they appear correctly and readable.</h5>

                        <div className="button_group" >
                            <div className="approve">
                                <button onClick={()=>this.sendApproval(1)} className={approveClass}>Approve Image</button>
                            </div>
                            <div className="recapture" >
                                <button onClick={()=>this.sendApproval(2)} className="recapture_button">Recapture</button>
                            </div>
                        </div>


                        <div className="cancel-button">
                            <Button onClick={this.handleClick} label='Cancel'  className="bottom-cacel">Cancel</Button>
                        </div>
                    </div>
                </div>
            </div>
    );
  }
}  

export default withRouter(connect(mapStateToProps, null)(ReviewCustomer));
function mapStateToProps({user}) {
    return {session: user.session};
}