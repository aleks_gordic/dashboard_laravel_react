import React, {Component} from 'react';
import { Button } from 'reactstrap';
import {ASSETS_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'
import  './CaptureID.style.scss';
import { withRouter } from 'react-router-dom'
import {connect} from "react-redux";
import Transaction from '@services/Transaction'

class CaptureID extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
        };
    }

    componentWillMount(){
        this.setState({
            id: this.props.location.hash.replace('#',''),
        })
    }

    handleClick = () => {

        let params = {
            id: this.state.id,
            action:"cancel"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.history.push(`/portal`);
                    success('Transaction cancelled successfully');
                }else{
                    error(data.message);
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    render() {
        return (
            <div className="custom-modal" >
                <div className="left-side" >

                    <div className="capturing" >
                        <img className="capture_gif"
                             src={`${ASSETS_URL}/steps/loading.gif`} />
                        <h5>Image is currently</h5>
                        <h5>being captured by the customer</h5>
                    </div>
                </div>

                <div className="right-side">
                    <div className="right-margin">
                        <h4 className="dialog-title">Customer is capturing their ID</h4>
                        <div className="under-line"></div>
                        <h5 className="dialog-subtitle">Instructions to the user:</h5>
                        <h5 className="dialog-description">Please make sure you capture the whole card in the image and follow the instructions.</h5>

                        <h5 className="dialog-descriptiontitle">Notes</h5>
                        <h5 className="dialog-description">Customer will be prompted to grant permission for location. This is needed as part of the ID process.</h5>
                        <h5 className="dialog-description">After capturing the image, the customer will be prompted by their device to use image or recapture.</h5>
                        <div className="cancel-button">
                            <Button onClick={this.handleClick} label='Cancel'  className="bottom-cacel">Cancel</Button>
                        </div>
                    </div>
                </div>
            </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, null)(CaptureID));
function mapStateToProps({user}) {
    return {session: user.session};
}