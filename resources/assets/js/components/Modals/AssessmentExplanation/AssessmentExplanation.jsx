import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import { Button, Modal, FormGroup, Input } from 'reactstrap';
import { ASSETS_URL } from '@js/config';
import  './AssessmentExplanation.style.scss';

class AssessmentExplanation extends Component  {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    image = () => {
        return (
            <div className="assessment-content">
                <h5>Image Composition Explanation</h5>
                <p>We suspect that the document captured may not be live and present.</p>
                <p>Some scenarios that may trigger this flag include:</p>
                <ul className="dashed">
                    <li>The image may be an ID document captured from a screen and not of - the actual ID document.
                        <p>(check for pixellation or computer screen items like application windows, mouse cursors or screen edges)</p>
                    </li>
                    <li>The image may be taken of an ID document photocopy or photo
                        <p>(check to see if the ID is printed on a piece of paper or if it is black and white.)</p>
                    </li>

                </ul>
            </div>
        );
    }

    photo = () => {
        return (
            <div className="assessment-content">
                <h5>Photo Check Explanation</h5>
                <p>We suspect that there may be an issue with the photo of the person on the card.</p>
                <p>Some scenarios that may trigger this flag include:</p>
                <ul className="dashed">
                    <li>Another face image may be placed on top of the original face image on the document.
                        <p>(check for exposed edges or shadows around the face or the face image or a difference in surface texture/glossiness over the face)</p>
                    </li>
                </ul>
            </div>
        );
    }

    doc = () => {
        return (
            <div className="assessment-content">
                <h5>Document Integrity Explanation</h5>
                <p>The physical document itself may not align with expectation.</p>
                <p>Some scenarios that may trigger this flag include:</p>
                <ul className="dashed">
                    <li>The ID Document itself is damaged or not entirely in shot.</li>
                    <li>The image may be taken of an ID document photocopy or photo
                        <p>(check to see if the ID is printed on a piece of paper or if it is black and white.)</p>
                    </li>

                </ul>
            </div>
        );
    }

    detail = () => {
        return (
            <div className="assessment-content">
                <h5>Detail Check Explanation</h5>
                <p>Something about the ID Document captured does not align with expectation.</p>
                <p>Some scenarios that may trigger this flag include:</p>
                <ul className="dashed">
                    <li>The measurements of the overall ID Document and text may not be - exactly the same as normal versions of this ID document.
                        <p>(check for fields that look out of place or if the document may be fake. This could accidentally flag if the user has a very long name or address)</p>
                    </li>
                    <li>The font and text on the card may not be consistent or correct.
                        <p>(check to make sure the font of all text is similar and that the user hasn’t physically changed any text)</p>
                    </li>
                    <li>Symbols and Colours of the card may not be correct.
                        <p>(check that the symbols and colours on the card look real or if the document could be completely fake.)</p>
                    </li>
                </ul>
            </div>
        );
    }

    toggle = (e) => {
        e.preventDefault()
        this.props.onClose();
    }

    render() {

        const { show, type } = this.props

        let contents;
        if(type === 'image'){
            contents = this.image()
        }else if(type === 'photo'){
            contents = this.photo()
        }else if(type === 'doc'){
            contents = this.doc()
        }else if(type === 'detail'){
            contents = this.detail()
        }else{
            contents = this.image()
        }


        return (
          <div>
            <Modal toggle={this.toggle} size="md" isOpen={show||false} className="AssessmentExplanation">
                <div className="inner-record-goto">
                    {contents}
                    <div className="mt-4 text-center"><Button onClick={this.toggle} color="primary">Close</Button></div>
                </div>

            </Modal>
        </div>
    );
  }
}

export default  withRouter(AssessmentExplanation)