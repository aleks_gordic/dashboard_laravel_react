import React, {Component} from 'react';
import { Button } from 'reactstrap';
import {ASSETS_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'
import { withRouter } from 'react-router-dom'
import  './FaceCapture.style.scss';
import {connect} from "react-redux";

import Transaction from '@services/Transaction'

class FaceCapture extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
        };
    }

    componentWillMount(){
        this.setState({
            id: this.props.location.hash.replace('#',''),
        })
    }

    handleClick = () => {

        let params = {
            id: this.state.id,
            action:"cancel"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.history.push(`/portal`);
                    success('Transaction cancelled successfully');
                }else{
                    error(data.message);
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    render() {
        return (
            <div className="custom-modal" >
                <div className="left-side" >

                    <div className="capturing" >
                        <img className="capture_gif"
                             src={`${ASSETS_URL}/steps/loading.gif`} />
                        <h5>Customer is capturing their face.</h5>
                    </div>
                </div>

                <div className="right-side">
                    <div className="right-margin">
                        <h4 className="dialog-title">Facial recognition</h4>
                        <div className="under-line"></div>
                        <h5 className="dialog-subtitle">Notes:</h5>
                        <h5 className="dialog-description">Customer is verifying their face. They will be asked prompts such as to smile or turn their head a certain direction.</h5>
                        <h5>Customer will be prompted to grant permission for the camera. This is needed as
                            part of the ID process</h5>
                        <div className="cancel-button">
                            <Button onClick={this.handleClick} label='Cancel'  className="bottom-cacel">Cancel</Button>
                        </div>
                    </div>
                </div>
            </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, null)(FaceCapture));
function mapStateToProps({user}) {
    return {session: user.session};
}