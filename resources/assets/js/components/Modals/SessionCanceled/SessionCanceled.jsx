import React, {Component} from 'react';
import { Button } from 'reactstrap';
import {ASSETS_URL} from '@js/config';
import  './SessionCanceled.style.scss';

import { withRouter } from 'react-router-dom'

class SessionCanceled extends Component  {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    handleClick = () => {
        this.props.history.push(`/portal`);
    }

    render() {
        return (
            <div className="custom-modal" >
                <div className="left-side" >

                    <div className="outer-box">
                        <div className="inner-box">
                            <img className="image-wating" src={`${ASSETS_URL}/steps/cancelled.png`} />
                        </div>
                    </div>
                </div>

                <div className="right-side">
                    <div className="right-margin">
                        <h4 className="dialog-title">User process cancellation</h4>
                        <div className="under-line"></div>
                        <h5 className="dialog-description">User has cancelled the ID verification process.</h5>
                        <div className="cancel-button">
                            <Button onClick={this.handleClick} label='Cancel'  className="bottom-cacel">Close</Button>
                        </div>
                    </div>
                </div>
            </div>
    );
  }
}

export default withRouter(SessionCanceled)