import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import { Button, Modal, ModalBody } from 'reactstrap';

import  './RequestSent.style.scss';

class RequestSent extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            reference:'NA',
            applicants:[

            ]
        };
    }


    handleClick = () => {
        //this.props.history.push(`/portal`);
        this.props.onClose();
    }


    render() {

        let {additional} = this.props;

        let reference = additional.reference || 'NA';
        let applicants = additional.applicants || [];

        let applicantsRecords = applicants.map((app,i)=>{
            let contact;
            if(app.medium === 'sms'){
                contact = app.country_code+' '+app.mobile;
            }else if(app.medium === 'email'){
                contact = app.email;
            }else{
                contact = 'NA'
            }

            return (
                <div className="applicant-props d-flex w-75" key={i}>
                    <h5 className="one applicant-name">{i+1}. </h5>
                    <h5 className="two applicant-name">{app.full_name || 'Applicant '+(i+1)} </h5>
                    <h5 className="three applicant-phonenumber"> {contact}</h5>
                </div>
            );
        })



        return (
            <div>
                <Modal isOpen={this.props.show||false} className="modal-class" size="lg">
                    <ModalBody className="single-Modal">

                        <div className="applicants-modal">
                            <h4 className="dialog-title">Identification request sent</h4>
                            <div className="under-line"></div>
                            <div className="subtitle-group d-flex w-75">
                                <h5 className="flex-fill subtitle-key">Reference</h5>
                                <h5 className="flex-fill dialog-subtitle subtitle-value">: {reference}  </h5>
                            </div>
                            <h5 className="dialog-description">Identification requests have been sent to the following applicants at:</h5>
                            <div className="applicant-info">
                                {applicantsRecords}
                            </div>
                            <Button onClick={this.handleClick} label='Close'  className="dialog-close">Close</Button>
                        </div>

                    </ModalBody>
                </Modal>
            </div>
        );
    }
}

export default  withRouter(RequestSent)