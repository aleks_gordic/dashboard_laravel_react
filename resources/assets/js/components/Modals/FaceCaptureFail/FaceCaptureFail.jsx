import React, {Component} from 'react';
import { Button } from 'reactstrap';
import { withRouter } from 'react-router-dom'
import {ASSETS_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'
import  './FaceCaptureFail.style.scss';
import {connect} from "react-redux";
import Transaction from '@services/Transaction'

class FaceCaptureFail extends Component  {

    constructor(props) {
        super(props);

        this.state = {
            id: '',
        };
    }

    componentWillMount(){
        this.setState({
            id: this.props.location.hash.replace('#',''),
        })
    }

    handleClick = () => {

        let params = {
            id: this.state.id,
            action:"cancel"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.history.push(`/portal`);
                    success('Transaction cancelled successfully');
                }else{
                    error(data.message);
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    render() {
        return (
            <div className="custom-modal" >
                <div className="left-side" >

                    <div className="outer-box">
                        <div className="inner-box">
                            <img className="image-wating"
                                 src={`${ASSETS_URL}/steps/face_failed.png`} />
                        </div>
                    </div>
                </div>

                <div className="right-side">
                    <div className="right-margin">
                        <h4 className="dialog-title">Face capture-liveness fail</h4>
                        <div className="under-line"></div>
                        <h5 className="dialog-subtitle">Introductions to the user:</h5>
                        <h5 className="dialog-description">Please review the privacy delcarations.I can read this to you or answer any questions if you wish.Tap "Agree" if you accept the terms to continue.</h5>
                        <h5 className="dialog-description"> Please try again but be sure to give a big smile and follow the prompts when it asks youto perofm an action.</h5>

                        <h5 className="dialog-descriptiontitle">Privacy Declaration shown:</h5>
                        <h5 className="dialog-description">-The customer may not have give a big enough smile,perhaps show some teeth when they smile.</h5>
                        <h5 className="dialog-description">-The customer did not follow the prompts exactly when asked</h5>
                        <div className="cancel-button">
                            <Button onClick={this.handleClick} label='Cancel'  className="bottom-cacel">Cancel</Button>
                        </div>
                    </div>
                </div>
            </div>
    );
  }
}


export default withRouter(connect(mapStateToProps, null)(FaceCaptureFail));
function mapStateToProps({user}) {
    return {session: user.session};
}