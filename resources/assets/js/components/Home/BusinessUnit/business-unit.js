import React, { Component } from 'react';
import {FormGroup,Label} from 'reactstrap'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Select, {components} from 'react-select';
import FlowCards from "@components/FlowCards"
import './business-unit.style.scss'


const SingleValue = ({children, ...props}) => {
	return (
		<components.SingleValue className="ocr-single-value" {...props}>
			{props.data.title}
		</components.SingleValue>
	)

};
const Menu = (props) => {
	return (
		<components.Menu className="select-options-container" {...props}>
			<div className="select-options">
				{props.children}
			</div>
		</components.Menu>
	);
};
const Inputs = (props) => {
	return (
		<div {...props} className="select-input">

		</div>
	);
};
const Option = (props) => {
	let existingClasses = props.isSelected ? 'option selected' : 'option';
	return (
		<components.Option {...props} className={existingClasses}>
			{props.children}
		</components.Option>
	);
};

class BusinessUnit extends Component {

	/** The component's constructor */
	constructor(props) {
		super(props);
		this.state = {
			selected:'',
		};
	}

    handleChange = async (obj) => {

		let value = obj.slug;

        await this.setState({
            selected: obj,
		});

		this.props.history.push(`/portal/${value}`);

	}

	componentWillMount() {
		const { match, user = {} } = this.props;
		const { info = {}} = user
		let selectedSlug = ""
		if(match && match.params && match.params.module){
			selectedSlug = match.params.module;
		}

		let permissions = info.permissions || [];
		let found = permissions.filter((perm) => {
			return perm.slug === selectedSlug
		})

		if( found.length > 0 ){
			this.setState({
				selected: found[0]
			})
		}


	}

	formatPermission = () =>{
		let permissions = this.props.user.info.permissions || [];
		let categories = [];

		permissions.forEach(perm => {
			let catExist = categories.findIndex(cat => {
				return cat.id === perm.category.id
			})

			if(catExist === -1){
				categories.push(perm.category)
			}

		});

		permissions.forEach(perm => {
			let catExist = categories.findIndex(cat => {
				return cat.id === perm.category.id
			})

			if(catExist > -1){
				if(!categories[catExist]['modules']){
					categories[catExist]['modules'] = [];
				}

				let modExists = categories[catExist]['modules'].findIndex(mod => {
					return mod.id === perm.id
				});

				if(modExists === -1){
					categories[catExist]['modules'].push({
						id: perm.id,
						title: perm.title,
						slug: perm.slug,
						access: perm.access,
					})
				}

			}

		});

		return categories;
	}

	/**
	 * Render the component's markup
	 * @return {ReactElement}
	 */
	render() {

		let permissions = this.formatPermission() || [];

		let workflows = permissions.map(({id, slug, modules, title}) => {
			return <FlowCards key={slug} module={slug} title={title} workFlows={modules} />
		})
		return (
				<div className="mt-4">
					{workflows}
				</div>
			);
	}
}

export default withRouter(connect(mapStateToProps, null)(BusinessUnit));


function mapStateToProps({user}) {
    return {user};
}
