import React, {useState, useEffect} from 'react';
import {
  Row,
  Col,
} from "reactstrap";
import { connect } from "react-redux";
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import ReactECharts from "@components/ReactECharts";
import echarts from "echarts";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import Loading from "@components/PageLoader";

import Dashboard from "@services/Dashboard";
import "./Usage.style.scss"

library.add(faCalendarAlt);

const hourlyGraph = {
  color: ['#3398DB'],
  tooltip : {
    trigger: 'axis',
    axisPointer : {            // 坐标轴指示器，坐标轴触发有效
      type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
    }
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
  },
  xAxis : [
    {
      type : 'category',
      data : ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      axisTick: {
        alignWithLabel: true
      }
    }
  ],
  yAxis : [
    {
      type : 'value'
    }
  ],
  series: [
    {
      type: 'bar',
      itemStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(
            0, 0, 0, 1,
            [
              {offset: 0, color: '#83bff6'},
              {offset: 0.5, color: '#188df0'},
              {offset: 1, color: '#188df0'}
            ]
          )
        },
        emphasis: {
          color: new echarts.graphic.LinearGradient(
            0, 0, 0, 1,
            [
              {offset: 0, color: '#2378f7'},
              {offset: 0.7, color: '#2378f7'},
              {offset: 1, color: '#83bff6'}
            ]
          )
        }
      },
      data: [10, 52, 200, 334, 390, 330, 220]
    }
  ]
};

const dailyGraph = {
  xAxis: {
    type: 'category',
    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  },
  yAxis: {
    type: 'value'
  },
  series: [{
    data: [820, 932, 901, 934, 1290, 1330, 1320],
    type: 'line'
  }]
};


function Usage({ session }) {
  const [loading, setLoading] = useState(true)
  const [data, setData] = useState(usageSampleData)
  const [startDate, setStartDate] = useState(new Date(Date.now() - 864e5))
  const [endDate, setEndDate] = useState(new Date())

  useEffect(() => {
    setLoading(true)

    let headers = {
      Authorization: "Bearer " + session
    };
    let params = {
      startOfDate: Math.round(startDate.getTime()/1000),
      endOfDate: Math.round(endDate.getTime()/1000)
    };

    Dashboard.getUsageReport(headers, params).then((usageData) => {
      if (typeof(usageData.totalCompleted) !== 'undefined') {
        setData(usageData)
      } else {
        setData(usageSampleData)
      }
      setLoading(false)
    }).catch((e) => {
      setLoading(false)
    })
  }, [startDate, endDate])

  dailyGraph.xAxis.data = data.dailyVisit.data
  dailyGraph.series[0].data = data.dailyVisit.values
  hourlyGraph.xAxis[0].data = data.averageHourlyVisits.data
  hourlyGraph.series[0].data = data.averageHourlyVisits.values

  return (
    <div className="usage-container">
      <Row className="m-0 mt-3 mb-3">
        <div className="date-picker mr-3">
          <FontAwesomeIcon icon="calendar-alt" size="lg"/>
          <DatePicker
            disabled={loading}
            selected={startDate}
            onChange={date => setStartDate(date)}
            dateFormat="MMMM d, yyyy"
            maxDate={endDate}
            placeholderText="From"
            ClassName="rasta-stripes"
          />
        </div>
        <div className="date-picker">
          <FontAwesomeIcon icon="calendar-alt" size="lg"/>
          <DatePicker
            disabled={loading}
            selected={endDate}
            onChange={date => setEndDate(date)}
            minDate={startDate}
            dateFormat="MMMM d, yyyy"
            placeholderText="To"
            ClassName="rasta-stripes"
          />
        </div>
      </Row>
      <div className="content mt-3">
        <Row className="m-0">
          <Col className="header-item">
            <div className="gray-title">Total completed sessions</div>
            <div className="big-number-container">
              <div className="big-number">{data.totalCompleted}</div>
            </div>
          </Col>
          <Col className="header-item">
            <div className="gray-title">Average duration</div>
            <div className="big-number-container">
            <div className="big-number">{data.averageDuration.minutes}</div>
            <div className="small-number">min</div>
            <div className="big-number">{data.averageDuration.seconds}</div>
            <div className="small-number">sec</div>
            </div>
          </Col>
          <Col className="header-item">
            <div className="gray-title">Total completed flagged</div>
            <div className="big-number-container">
              <div className="big-number">{data.totalCompletedFlagged}</div>
            </div>
          </Col>
          <Col className="header-item">
            <div className="gray-title">Total incomplete</div>
            <div className="big-number-container">
              <div className="big-number">{data.totalIncomplete}</div>
            </div>
          </Col>
        </Row>
        <Row className="sub-item-title m-3">Daily Visit</Row>
          <ReactECharts option={dailyGraph} showLoading={false}/>
        <Row className="sub-item-title m-3">Average Hourly Visits</Row>
          <ReactECharts option={hourlyGraph} showLoading={false}/>
      </div>
      {loading && <Loading/>}
    </div>
  )
}

function mapStateToProps({ user }) {
  return { session: user.session };
}

export default connect(mapStateToProps, null)(Usage)

const usageSampleData = {
  totalCompleted: 0,
  averageDuration: {
    minutes: 0,
    seconds: 0,
  },
  totalCompletedFlagged: 0,
  totalIncomplete: 0,
  dailyVisit: {
    data: [
    ],
    values: [
    ]
  },
  averageHourlyVisits: {
    data: [
    ],
    values: [
    ]
  }
}