import React, {useState, useEffect} from "react";
import { connect } from "react-redux";
import { Row, Col, Table } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import DatePicker from "react-datepicker"; 
import Dashboard from "@services/Dashboard";
import Loading from "@components/PageLoader";
import "./Analytics.style.scss";
import "react-datepicker/dist/react-datepicker.css";

library.add(faCalendarAlt);

function Analytics({ session }) {
	const [loading, setLoading] = useState(true)
	const [anlaytics, setAnlaytics] = useState(analyticsReportSample)
	const [startDate, setStartDate] = useState(new Date(Date.now() - 864e5))
	const [endDate, setEndDate] = useState(new Date())
  
	useEffect(() => {
		setLoading(true)
	
		let headers = {
		  	Authorization: "Bearer " + session
		};
		let params = {
			startDate: Math.round(startDate.getTime()/1000),
			endDate: Math.round(endDate.getTime()/1000)
		};
	
		Dashboard.getAnalyticsReport(headers, params).then((analyticsData) => {
			if (typeof(analyticsData.totalSessionsComplete) !== 'undefined') {
				setAnlaytics(analyticsData)
			} else {
				setAnlaytics(analyticsReportSample)
			}
		  	setLoading(false)
		}).catch((e) => {
		  setLoading(false)
		})
	}, [startDate, endDate])
	
	return (
		<div className="anlaytics-container mt-3">
			<Row className="m-0 mb-3">
				<div className="date-picker mr-3">
					<FontAwesomeIcon icon="calendar-alt" size="lg"/>
					<DatePicker
						selected={startDate}
						onChange={date => {setStartDate(date)}}
						maxDate={endDate}
						dateFormat="MMMM d, yyyy"
						placeholderText="From"
						ClassName="rasta-stripes"
					/>
				</div>
				<div className="date-picker">
					<FontAwesomeIcon icon="calendar-alt" size="lg"/>
					<DatePicker
						selected={endDate}
						onChange={date => {setEndDate(date)}}
						minDate={startDate}
						dateFormat="MMMM d, yyyy"
						placeholderText="To"
						ClassName="rasta-stripes"
					/>
				</div>
			</Row>
			<div className="anlaytics">
				<Row className="figures m-0">
					<Col className="header-item">
						<div className="gray-title">Total sessions complete</div>
						<div className="big-number-container">
							<div className="big-number">{anlaytics.totalSessionsComplete}</div>
						</div>
					</Col>
					<Col className="header-item">
						<div className="gray-title">Total sessions flagged</div>
						<div className="big-number-container">
							<div className="big-number">{anlaytics.totalSessionsFlagged}</div>
						</div>
					</Col>
					<Col className="header-item">
						<div className="gray-title">Total sessions incomplete</div>
						<div className="big-number-container">
							<div className="big-number">{anlaytics.totalSessionsIncomplete}</div>
						</div>
					</Col>
				</Row>
				<Row className="status-details m-0">
					<Row className="status-group mb-0">
						<Col className="status-table">
							<div className="group-table">
								<label>Average Session Duration Breakdown</label>
								<Table>
									<tbody>
										<tr>
											<td className="title">Time to activate SMS</td>
											<td className="value">{anlaytics.averageSessionDurationBreakdown.timeToActiveSMS} attempts</td>
										</tr>
										<tr>
											<td className="title">Time to review terms</td>
											<td className="value">{anlaytics.averageSessionDurationBreakdown.timeToReviewTerms} times</td>
										</tr>
										<tr>
											<td className="title">Time to capture ID</td>
											<td className="value">{anlaytics.averageSessionDurationBreakdown.timeToCaptureID} fields</td>
										</tr>
										<tr>
											<td className="title">Time to review data</td>
											<td className="value">{anlaytics.averageSessionDurationBreakdown.timeToReviewData} %</td>
										</tr>
										<tr>
											<td className="title">Time to capture liveness</td>
											<td className="value">{anlaytics.averageSessionDurationBreakdown.timeToCaptureLiveness} %</td>
										</tr>
										<tr>
											<td className="total">Total Process Time (exclude Terms)</td>
											<td className="value">{anlaytics.averageSessionDurationBreakdown.totalProcessTime} %</td>
										</tr>
									</tbody>
								</Table>
							</div>
							<div className="group-table">
								<label>Average Session Duration Breakdown</label>
								<Table>
									<tbody>
										<tr>
											<td className="title">Time to activate SMS</td>
											<td className="value">{anlaytics.otherStatistics.averageLivenessAttempts}</td>
										</tr>
										<tr>
											<td className="title">Time to review terms</td>
											<td className="value">{anlaytics.otherStatistics.averageIDCaptureAttempts}</td>
										</tr>
										<tr>
											<td className="title">Time to capture ID</td>
											<td className="value">{anlaytics.otherStatistics.averageFieldsEdited}</td>
										</tr>
										<tr>
											<td className="title">Time to review data</td>
											<td className="value">{anlaytics.otherStatistics.percentageAndroid}</td>
										</tr>
										<tr>
											<td className="title">Time to capture liveness</td>
											<td className="value">{anlaytics.otherStatistics.percentageIOS}</td>
										</tr>
										<tr>
											<td className="total">Total Process Time (exclude Terms)</td>
											<td className="value">{anlaytics.otherStatistics.percentageOther}</td>
										</tr>
									</tbody>
								</Table>
							</div>
							<div className="group-table">
								<label>Document type Breakdown</label>
								<div className="document"><span>NSW Driver Licence</span><span>{anlaytics.documentTypeBreakdown.NSWDriverLicense}</span></div>
								<div className="document"><span>VIC Driver Licence</span><span>{anlaytics.documentTypeBreakdown.VICDriverLicense}</span></div>
								<div className="document"><span>QLD Driver Licence</span><span>{anlaytics.documentTypeBreakdown.QLDDriverLicense}</span></div>
								<div className="document"><span>WA Driver Licence</span><span>{anlaytics.documentTypeBreakdown.WADriverLicense}</span></div>
								<div className="document"><span>Australia Passport</span><span>{anlaytics.documentTypeBreakdown.australiaPassport}</span></div>
								<div className="document"><span>Medicare Card</span><span>{anlaytics.documentTypeBreakdown.medicareCard}</span></div>
								<div className="document"><span>ACT Driver Licence</span><span>{anlaytics.documentTypeBreakdown.ACTDriverLicense}</span></div>
							</div>
						</Col>
						<Col className="status-figures">
							<div className="group-table">
								<label>Status Breakdown</label>
								<Row className="item m-0">
									<Col>Completed - Passed</Col>
									<Col>{anlaytics.statusBreakdown.completedPassed.times} times</Col>
									<Col>{anlaytics.statusBreakdown.completedPassed.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Completed - Flagged</Col>
									<Col>{anlaytics.statusBreakdown.completedFlagged.times} times</Col>
									<Col>{anlaytics.statusBreakdown.completedFlagged.percentage}%</Col>
								</Row>
							</div>
							<div className="group-table">
								<label>Incomplete</label>
								<Row className="item m-0">
									<Col>Incomplete - Initiated</Col>
									<Col>{anlaytics.incompleteBreakdown.initiated.times} times</Col>
									<Col>{anlaytics.incompleteBreakdown.initiated.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Incomplete - In - Progress</Col>
									<Col>{anlaytics.incompleteBreakdown.inProgress.times} times</Col>
									<Col>{anlaytics.incompleteBreakdown.inProgress.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Incomplete - Expired</Col>
									<Col>{anlaytics.incompleteBreakdown.expired.times} times</Col>
									<Col>{anlaytics.incompleteBreakdown.expired.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Incomplete - Terminated</Col>
									<Col>{anlaytics.incompleteBreakdown.terminated.times} times</Col>
									<Col>{anlaytics.incompleteBreakdown.terminated.percentage}%</Col>
								</Row>
							</div>
							<div className="group-table">
								<label>Flagged Breakdown</label>
								<Row className="item m-0">
									<Col>Document Fraud Analytics</Col>
									<Col>{anlaytics.flaggedBreakdown.documentFraudAnalysis.times} times</Col>
									<Col>{anlaytics.flaggedBreakdown.documentFraudAnalysis.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Liveness Flagged</Col>
									<Col>{anlaytics.flaggedBreakdown.livenessFlagged.times} times</Col>
									<Col>{anlaytics.flaggedBreakdown.livenessFlagged.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Face Flagged</Col>
									<Col>{anlaytics.flaggedBreakdown.faceFlagged.times} times</Col>
									<Col>{anlaytics.flaggedBreakdown.faceFlagged.percentage}%</Col>
								</Row>
								<Row className="item m-0">
									<Col>Data Fail</Col>
									<Col>{anlaytics.flaggedBreakdown.dataFail.times} times</Col>
									<Col>{anlaytics.flaggedBreakdown.dataFail.percentage}%</Col>
								</Row>
							</div>
						</Col>
					</Row>
				</Row>
			</div>
			{loading && <Loading />}
		</div>
	)
}

export default 
  connect(
    mapStateToProps,
    null
  )(Analytics);

function mapStateToProps({ user }) {
  return { session: user.session };
}

const analyticsReportSample={
  totalSessionsComplete: 0,
  totalSessionsFlagged: 0,
  totalSessionsIncomplete: 0,
  averageSessionDurationBreakdown: {
    timeToActiveSMS: 0,
    timeToReviewTerms: 0,
    timeToCaptureID: 0,
    timeToReviewData: 0,
    timeToCaptureLiveness: 0,
    totalProcessTime: 0,  
  },
  otherStatistics: {
    averageLivenessAttempts: 0,
    averageIDCaptureAttempts: 0,
    averageFieldsEdited: 0,
    percentageAndroid: 0,
    percentageIOS: 0,
    percentageOther: 0,  
  },
  documentTypeBreakdown: {
    NSWDriverLicense: 0,
    VICDriverLicense: 0,
    QLDDriverLicense: 0,
    WADriverLicense: 0,
    australiaPassport: 0,
    medicareCard: 0,
    ACTDriverLicense: 0,  
  },
  statusBreakdown: {
    completedPassed: {
      times: 0,
      percentage: 0
    },
    completedFlagged: {
      times: 0,
      percentage: 0
    },
  },
  incompleteBreakdown: {
    initiated: {
      times: 0,
      percentage: 0,
    },
    inProgress: {
      times: 0,
      percentage: 0,
    },
    expired: {
      times: 0,
      percentage: 0,
    },
    terminated: {
      times: 0,
      percentage: 0,
    },
  },
  flaggedBreakdown: {
    documentFraudAnalysis: {
      times: 0,
      percentage: 0,
    },
    livenessFlagged: {
      times: 0,
      percentage: 0,
    },
    faceFlagged: {
      times: 0,
      percentage: 0,
    },
    dataFail: {
      times: 0,
      percentage: 0,
    },
  },
}