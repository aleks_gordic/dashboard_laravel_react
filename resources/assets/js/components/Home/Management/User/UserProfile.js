import React, { Component, Fragment } from "react";
import {
  Row,
  Col,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from "reactstrap";
import RoleAPI from "@services/Role";
import UserAPI from "@services/User";
class UserProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data:{
        username: '',
        mobile: '',
        email: '',
        first_name: '',
        last_name: '',
        role: '',
        password_status: false,
        google_secret: null,
        password_created_at: null,
        created_at : '',
        created_by: '',
      },
      roles: [],
      fields: []
    };
  }

  componentDidMount() {
    const { fields, roles } = this.props
    const {
    data: {
      id,
      username,
      mobile,
      email,
      first_name,
      last_name,
      role,
      password_status,
      google_secret,
      password_created_at,
      created_at,
      created_by
     }
    } = this.props;

    const newPassword = new Date(password_created_at);

    const data = {
      id,
      username,
      mobile,
      email,
      first_name,
      last_name,
      role,
      password_status,
      google_secret,
      created_at,
      created_by,
      password_created_at:
          newPassword.getDate() +
          "/" +
          (newPassword.getMonth() + 1) +
          "/" +
          newPassword.getFullYear()
    };

    fields.forEach( field => {
      data[field.key] = this.props.data[field.key]
    })

    this.setState({data,fields, roles});
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { fields } = this.state
    const {
      data: {
        id,
        username,
        mobile,
        email,
        role,
        first_name,
        last_name,
        password_status,
        google_secret,
        password_created_at,
        created_at,
        created_by
      }
    } = nextProps;

    const newPassword = new Date(password_created_at);

    const data = {
      id,
      username,
      mobile,
      email,
      first_name,
      last_name,
      role,
      password_status,
      google_secret,
      created_at,
      created_by,
      password_created_at:
          newPassword.getDate() +
          "/" +
          (newPassword.getMonth() + 1) +
          "/" +
          newPassword.getFullYear()
    };

    fields.forEach( field => {
      data[field.key] = nextProps.data[field.key]
    })
    this.setState({data});
  }

  handleChange = (e) => {
    let field = $(e.target).attr("name");
    let val = e.target.value;

    this.setState({
      [field] : val
    })

    let inputs = {...this.state}
    delete inputs.password_created_at

    inputs[field] = val
    this.props.onFieldChange(inputs);

  }

  render() {

    const { loggedInUser } = this.props
    const { MIX_ENABLE_2FA } = process.env
    const enable2FA = MIX_ENABLE_2FA === 'true'
    const {
      id,
      username,
      mobile,
      email,
      first_name,
      last_name,
      role,
      resetPassword,
      resetTwoStep,
      password_status,
      google_secret,
      password_created_at,
      created_at,
      created_by
    } = this.state.data;


    const { roles = [], fields = []} = this.state

    const isCurrentUser = loggedInUser.id === id
    const roleOptions = roles.map((role)=> {
        return (
            <option key={role.slug} value={role.slug}>{role.name}</option>
        )
    })

    let custoFields = fields.map(({label, key}) => {
      return (
          <div className="col-6 d-flex" key={key}>
            <Label className="col-form-label mr-2">{label}</Label>
            <Input
                className=""
                name={key}
                defaultValue={this.state.data[key]}
                value={this.state.data[key]}
                onChange={this.handleChange}
            />
          </div>
      );
    })

    return (
      <FormGroup className="profile-section">
        <FormGroup className="username--section row">
          <div className="col-6 d-flex">
            <Label className="col-form-label mr-2">Username</Label>
            <Input
                name="username"
                defaultValue={username}
                readOnly={isCurrentUser}
                onChange={this.handleChange}
            />
          </div>
        </FormGroup>

        <FormGroup className="username--section row">
          <div className="col-8 d-flex align-items-center">
            <Label className="col-form-label mr-2">Password</Label>
            {password_status ? (
                <div className="">
                  <span className="mr-2">{`Last changed at ${password_created_at}`}</span>
                  <Button onClick={resetPassword}>Reset</Button>
                </div>
            ) : (
                <span className="form-control label-only">Has not been set up</span>
            )}
          </div>
          {enable2FA && <div className="col-6 d-flex align-items-center">
            <Label className="col-form-label mr-2">2 Step Authentication</Label>
            {google_secret && mobile ? (
                <div className="pr-0">
                  <span className="mr-2">2 Step Authentication</span>
                  <Button onClick={resetTwoStep}>Reset</Button>
                </div>
            ) : (
                <span className="form-control label-only">Has not been set up</span>
            )}
          </div>}
        </FormGroup>

        <FormGroup className="username--section row">
          <div className="col-6 d-flex">
            <Label className="col-form-label mr-2">Name</Label>
            <Input
                name="first_name"
                defaultValue={first_name}
                onChange={this.handleChange}
            />
          </div>
          <div className="col-6 d-flex">
            <Label className="col-form-label mr-2">&nbsp;</Label>
            <Input
                name="last_name"
                defaultValue={last_name}
                onChange={this.handleChange}
            />
          </div>
        </FormGroup>

        <FormGroup className="username--section row">
          <div className="col-6 d-flex">
            <Label className="col-form-label mr-2">Role</Label>
            <div className=" w-100 selectPendingClass select-container">
              <select
                  onChange={this.handleChange}
                  name="role"
                  value={role}
                  className="selectPendingInput form-control">
                <option value="">Select Role</option>
                {roleOptions}
              </select>
            </div>
          </div>
        </FormGroup>

        <FormGroup className="username--section row">
          <div className="col-6 d-flex">
            <Label className="col-form-label mr-2">eMail</Label>
            <Input
                name="email"
                defaultValue={email}
                readOnly={isCurrentUser}
                onChange={this.handleChange}
            />
          </div>
          <div className="col-6 d-flex">
            <Label className="col-form-label mr-2">Phone</Label>
            <Input
                name="mobile"
                defaultValue={mobile}
                onChange={this.handleChange}
            />
          </div>
        </FormGroup>

        <FormGroup className="username--section row">
          <div className="col-6 d-flex">
            <Label className="col-form-label mr-2">Created At</Label>
            <Input
                readOnly={true}
                name="created_at"
                defaultValue={created_at}
            />
          </div>
          <div className="col-6 d-flex">
            <Label className="col-form-label mr-2">Created By</Label>
            <Input
                readOnly={true}
                name="created_by"
                defaultValue={created_by}
                onChange={this.handleChange}
            />
          </div>
        </FormGroup>

       {/* Custom Fields*/}
        <FormGroup className="username--section row">
          {custoFields}
        </FormGroup>

      </FormGroup>
    );
  }
}

export default UserProfile;
