import React, {Component} from "react";
import {
    Container,
    Row,
    Col,
    Button,
    Form,
    FormGroup,
    Input,
} from "reactstrap";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {library} from "@fortawesome/fontawesome-svg-core";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import Permission from "@components/Home/Management/Permission";
import RoleFormModal from "@components/Home/Management/RoleFormModal";
import RoleAPI from "@services/Role";
import {success, error} from "@js/lib/Tostify";
import {ASSETS_URL, API_URL} from "@js/config";
import NoRecordFoundGeneric from "@components/NoRecordFoundGeneric";
import Pagination from "@components/Pagination";
import RolePermissions from "./RolePermissions";
import DeletePrompt from '@components/Modals/DeletePrompt'
import "./Role.style.scss";

library.add(faPlus);

class Role extends Component {
    /** The component's constructor */
    constructor(props) {
        super(props);

        this.state = {
            roles: {
                data: []
            },
            users: {
                data: []
            },
            selected: {},
            permissions: [],
            modalOpen: false,
            query: "",
            page: 1,
            userPermission: []
        };
    }

    fetchRoles = (setSelected = false) => {
        let headers = {
            Authorization: "Bearer " + this.props.session
        };

        let params = {
            page: this.state.page || 1
        };

        RoleAPI.read(params, headers).then(res => {
            if (res.status === "success") {
                //success

                let data = {
                    roles: res.data
                };

                if (setSelected) {
                    let i;
                    i = data.roles.data.findIndex(role => {
                        return setSelected === role.id;
                    });

                    if (i > -1) {
                        data["selected"] = data.roles.data[i];
                    } else {
                        data["selected"] = data.roles.data[0];
                    }
                }

                this.setState({...data});
            } else {
                //error
                error("Error in fetching roles!");
            }
        });
    }

    componentWillMount() {
        this.fetchRoles()
    }

    componentDidMount() {
        /*const { user } = this.props
        const userPermission = user.other_permissions.map((permission, i) => {
          if(permission.access){
            return permission.slug
          }
        });

        this.setState({
          userPermission
        })*/
    }

    changeCurrentRole = (e, role) => {
        e.preventDefault();

        this.setState({
            selected: role
        });

        this.fetchRolePermission(role.id);
    };

    handleEditRoleChange = (field, value) => {
        let selected = {...this.state.selected};
        selected[field] = value;
        this.setState({
            selected
        });
    };

    formatPermissions = (permissions) => {
        let overall = [];
        permissions.forEach(perm => {
            overall.push({
                module_id: perm.id,
                access: perm.access
            })

            if (perm.sub_permissions) {
                perm.sub_permissions.forEach(sub => {

                    if (!isNaN(sub.id)) {
                        overall.push({
                            module_id: sub.id,
                            access: sub.access
                        })
                    }
                })
            }
        })
        return overall;
    }

    handleSubmitRoleChange = () => {
        let {selected, permissions} = this.state;

        const formattedPermission = this.formatPermissions(permissions)

        let params = {
            id: selected.id,
            name: selected.name,
            permissions: formattedPermission
        };

        let headers = {
            Authorization: "Bearer " + this.props.session,
            'Content-Type': 'application/json',
        };
        RoleAPI.update(params, headers).then(res => {
            if (res.status === "success") {
                //success
                this.fetchRoles(params.id);
                success("Role updated successfully!");
            } else {
                //error
                error("Error in creating role !");
            }
        });
    }

    handleModuleUpdate = (module_id, access) => {
        let permissions = [...this.state.permissions]
        permissions = permissions.map(perm => {
            let temp = {...perm}
            if (perm.id === module_id) {
                temp['access'] = access
            } else if (perm.sub_permissions) {
                let sub_permissions = [...perm.sub_permissions];
                sub_permissions = sub_permissions.map(sub => {
                    let temp1 = {...sub}
                    if (sub.id === module_id) {
                        temp1['access'] = access
                    }
                    return temp1
                })

                temp['sub_permissions'] = sub_permissions
            }

            return temp
        })

        let foundIndex = permissions.findIndex((permission) => {
            return permission.id === module_id
        })

        if (foundIndex > -1 && permissions[foundIndex]['sub_permissions']) {

            let temp_sub = permissions[foundIndex]['sub_permissions'];
            temp_sub = temp_sub.map(sub_perm => {
                let perm = {...sub_perm}
                perm['access'] = access
                return perm
            });

            permissions[foundIndex]['sub_permissions'] = temp_sub
        }

        this.setState({
            permissions
        })
    }

    deleteRole = () => {
        let {selected} = this.state;
        let headers = {
            Authorization: "Bearer " + this.props.session
        };
        RoleAPI.delete(selected.id, headers).then(res => {
            if (res.status === "success") {
                //success
                this.setState({showDeletePrompt: false, selected: {}});
                this.fetchRoles();
                success("Role deleted successfully!");
            } else {
                //error
                error("Error in deleting role !");
            }
        });
    };

    fetchRolePermission = id => {

        RoleAPI.permissions(id, {}, {}).then(({status, data}) => {
            if (status === "success") {
                this.setState({
                    permissions: data
                });
            }
        });

    };

    handleChange = e => {
        let field = $(e.target).attr("name");

        this.setState({
            [field]: e.target.value
        });

        this.debounceHandleUpdate(e.target.value);
    };

    debounceHandleUpdate = _.debounce(input => this.searchRole(input), 500, {
        maxWait: 500
    });

    searchRole = q => {
        let headers = {
            Authorization: "Bearer " + this.props.session
        };
        RoleAPI.search(q, headers).then(res => {
            this.setState({
                roles: res,
                //selected: res.data.length > 0 ? res.data[0] : {}
            });
        });
    };

    clearSearch = () => {
        this.setState({
            query: ""
        });
        this.fetchRoles();
    };
    //--------------
    roleCreated = () => {
        this.setState({
            modalOpen: !this.state.modalOpen
        });
        this.fetchRoles();
    };

    renderNewRoleModal = () => {
        return (
            <RoleFormModal
                modalOpen={this.state.modalOpen}
                onSubmit={this.roleCreated}
                onToggleUserFormModal={this.toggleModal}
            />
        );
    };

    toggleModal = () => {
        this.setState({
            modalOpen: !this.state.modalOpen
        });
    };
    //----------------------------
    handlePageChange = async currentPage => {
        await this.setState({
            page: currentPage || 1
        });

        this.fetchRoles();
    };
    //----------------------------
    handleDelete = (e) => {
        e.preventDefault()
        this.setState({
            showDeletePrompt: true,
        })
    }

    renderDeletePrompt = () => {
        return (
            <DeletePrompt
                show={this.state.showDeletePrompt}
                onClose={() => this.setState({showDeletePrompt: false})}
                onSubmit={this.deleteRole}
            />
        )
    }

    render() {

        const {user} = this.props
        const {selected = {}, permissions = [], userPermission = []} = this.state;

        let hasRoles = Object.keys(selected).length > 0;

        let roleList = this.state.roles.data.map(role => {
            let classname = role.id === this.state.selected.id ? "active" : "";
            return (
                <li key={role.id}>
                    <a
                        href="#"
                        className={classname}
                        onClick={e => this.changeCurrentRole(e, role)}
                    >
                        {role.name}
                    </a>
                </li>
            );
        });

        let Norecords;
        if (this.state.query) {
            let cta = {
                show: true,
                text: "Reset Search"
            };
            Norecords = (
                <NoRecordFoundGeneric
                    message="No records found"
                    onClickCta={this.clearSearch}
                    cta={cta}
                />
            );
        } else {
            let cta = {
                show: false
            };
            Norecords = (
                <NoRecordFoundGeneric message="Please select a record" cta={cta}/>
            );
        }

        //pagination
        let paginate = this.state.roles || {};
        paginate = {
            current_page: paginate.current_page,
            last_page: paginate.last_page
        };

        const isCurrentUser = selected.id === user.id

        return (
            <section className="management-role mt-2 mb-5">
                <Container>
                    <Row className="content-area m-0">
                        <Col sm="3" className="p-0 bg-white">
                            <h4 className="m-0 col-heading d-flex">Roles <span onClick={this.toggleModal}
                                                                               className="ml-auto"><FontAwesomeIcon
                                icon="plus"
                                size="lg"
                                className="mr-4"
                            /></span></h4>
                            <ul className="user-list primary-actions">
                                <li>
                                    <Form>
                                        <FormGroup className="p-0">
                                            <img src={`${ASSETS_URL}/icons/search.png`} alt=""/>
                                            <Input
                                                type="text"
                                                value={this.state.query}
                                                name="query"
                                                onChange={this.handleChange}
                                                placeholder="Search"
                                                className="search-field"
                                            />
                                        </FormGroup>
                                    </Form>
                                </li>
                                {roleList}
                            </ul>
                        </Col>
                        <Col sm="9" className="pl-3 pr-0">
                            <div className="bg-white h-100">
                                <h4 className="m-0 col-heading">Settings & default Permissions</h4>

                                {hasRoles && (
                                    <div>
                                        {!isCurrentUser && <div className="secondary-actions mt-2 d-flex">
                                            <Button
                                                className="btn-management ml-auto"
                                                outline
                                                color="gray"
                                                onClick={this.handleDelete}
                                            >
                                                Delete
                                            </Button>
                                        </div>}
                                        <RolePermissions
                                            onFieldChange={values => this.setState({selected: {...this.state.selected, ...values}})}
                                            resetPassword={this.resetPassword}
                                            resetTwoStep={this.resetTwoStep}
                                            data={selected}
                                            loggedInUser={user}
                                        />
                                        <Permission
                                            onModuleChange={this.handleModuleUpdate}
                                            modules={permissions}/>
                                        <div className="secondary-actions d-flex mb-3">
                                            <Button
                                                className="ml-auto btn-management"
                                                outline
                                                onClick={e => this.setState({selected: {}})}
                                                color="primary"
                                            >
                                                Cancel
                                            </Button>
                                            <Button
                                                className="btn-management ml-2 mr-2"
                                                onClick={this.handleSubmitRoleChange}
                                                color="primary"
                                            >
                                                Save
                                            </Button>
                                        </div>
                                    </div>
                                )}

                                {!hasRoles && Norecords}
                            </div>
                        </Col>
                    </Row>
                    <div className="float-right mt-3">
                        <Pagination onPageChange={this.handlePageChange} data={paginate}/>
                    </div>
                    {this.renderNewRoleModal()}
                    {this.renderDeletePrompt()}
                </Container>
            </section>
        );
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        null
    )(Role)
);

function mapStateToProps({user}) {
    return {session: user.session, user: user.info};
}
