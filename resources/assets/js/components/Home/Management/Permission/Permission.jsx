import React, { Component } from 'react';
import classname from 'classnames'
import { ASSETS_URL } from '@js/config';

import './Permission.style.scss'

class Permission extends Component {

	/** The component's constructor */
	constructor(props) {
		super(props);
		this.state = {
            modules: [],
		    selected: {},
            role_id:null
		};
	}

	componentDidMount() {
        const { role_id, modules } = this.props
        this.setState({
            modules,
            role_id
        })
    }

    handleModuleChange = (module_id, access) => {
        this.props.onModuleChange(module_id, access);
    }

    componentWillReceiveProps(nextProps, nextContext) {
	    const { modules } = nextProps
        const obj = {}
        obj.modules = modules

        let item = modules.find((mod) => {
            return mod.id === this.state.selected.id
        })

        if(item){
            obj.selected = item
        }

        this.setState(obj)
    }

    /**
	 * Render the component's markup
	 * @return {ReactElement}
	 */
	render() {

	    let { modules, selected: { sub_permissions = [] } } = this.state, i=1

        if(!modules) return null

        while (sub_permissions.length < modules.length){
            sub_permissions.push({'id': 'hh'+i})
            ++i
        }

        let items = modules.map((module) => {

	        return (
                <div key={module.id} onClick={() => this.setState({selected: module})} className={classname("perm-item", "d-flex",{ active: this.state.selected.id === module.id})}>
                    <div className="flex-fill">
                        <h4>{module.title}</h4>
                    </div>
                    <div className="flex-fill text-center">
                        <label className="checkbox-container">
                            <input type="checkbox"
                                   checked={module.access}
                                   onChange={()=>this.handleModuleChange(module.id, !module.access)}
                            />
                            <span className="checkmark"></span>
                        </label>
                    </div>
                </div>
            )
        })

        let subItems;
        if(sub_permissions){
            subItems = sub_permissions.map((sub_module) => {
                return (
                    <div key={sub_module.id} className="perm-item d-flex">
                        <div className="flex-fill">
                            <h4>{sub_module.title}</h4>
                        </div>
                        {sub_module.title && <div className="flex-fill text-center">
                            <label className="checkbox-container">
                                <input type="checkbox"
                                       checked={sub_module.access}
                                       onChange={()=>this.handleModuleChange(sub_module.id, !sub_module.access)}
                                />
                                <span className="checkmark"></span>
                            </label>
                        </div>}
                    </div>
                )
            })
        }


		return (
            <div className="permissions pb-3">
                <div className="d-flex permission-heading">
                    <div className="flex-fill">
                        <h4>Module Access</h4>
                    </div>
                    <div className="flex-fill">
                        <h4>Module Specific Access</h4>
                    </div>
                </div>

                <div className="d-flex permission-content">
                    <div className="flex-fill w-50">
                        {items}
                    </div>
                    <div className="flex-fill w-50">
                        {subItems}
                    </div>
                </div>
            </div>
        );
	}
}

export default Permission;

