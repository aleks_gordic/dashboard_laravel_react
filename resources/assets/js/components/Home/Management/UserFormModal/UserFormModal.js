import React from "react";
import { Modal, Col } from "reactstrap";
import UserForm from "@components/Home/Management/UserForm";
import { ASSETS_URL } from "@js/config";

import "./UserFormModal.style.scss";

class UserFormModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        first_name: "",
        last_name: "",
        username: "",
        email: "",
        password: "",
        mobile: "",
        role: "manager"
      },
    };
  }

  toggle = e => {
    e.preventDefault();
    this.props.onToggleUserFormModal();
  };

  handleChange = (field, value) => {
    let user = { ...this.state.user };
    user[field] = value;
    this.setState({
      user
    });
  };

  handleSubmit = () => {
    this.props.onSubmit();
  };

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.modalOpen}
          fade={false}
          toggle={this.toggle}
          className="user-form-modal"
          centered
        >
          <div className="pl-4 pr-4 mt-4">
            <h4 className="">Invite User</h4>
            <p className="">
              Fill in the details below to send an invitation email to the new
              user
            </p>
            <a href="#" onClick={()=>this.props.onClickBatchUser()}>
                Or create multiple user at once via Batch Upload
            </a>
          </div>
          <UserForm
            user={this.state.user}
            disableUsername={false}
            onFieldChange={this.handleChange}
            onSubmit={this.handleSubmit}
            cta={true}
            prefix="create"
            roles={this.props.roles}
            fields={this.props.fields}
          />
        </Modal>
      </div>
    );
  }
}

export default UserFormModal;
