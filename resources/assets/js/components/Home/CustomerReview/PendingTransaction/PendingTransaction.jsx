import React, {Component} from 'react';
import {Table, Input, Container, Button} from 'reactstrap';
import {ASSETS_URL, API_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'
import { setC, getC } from '@js/lib/Cookie';

import {withRouter} from 'react-router-dom'
import moment from 'moment'
import Datetime from 'react-datetime'
import Pagination from '@components/Pagination'
import {connect} from "react-redux";
import User from '@services/User'

import Transaction from '@services/Transaction'
import DeletePrompt from '@components/Modals/DeletePrompt'
import './PendingTransaction.style.scss';
import 'react-datetime/css/react-datetime.css';
/**
 * Customer Review - Pending Transaction
 */
class PendingTransaction extends Component {

    constructor(props) {
        super(props);
        this.state = {
            transactions: [],
            users:[],
            filter: {
                type: '',
                result: '',
                channel: '',
                dateFrom: '',
                dateTo: '',
            },
            query: {
                term: '',
            },
            export:'csv',
            page: 1,
            showDeletePrompt: false,
            toDelete: null,
            userPermission: [],
            autoRefresh: null
        };
    }

    resend = (e, id) => {
        e.preventDefault()
        let params = {
            id: id,
            action: 'resend'
        }
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.resendOrTerminate(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    success('Invitation re-sent successfully.')
                    this.fetchTransactions();
                } else {
                    error('Error in re-sending invitation.')
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    terminate = (e, id) => {
        e.preventDefault()
        let params = {
            id: id,
            action: 'cancel'

        }
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Transaction.resendOrTerminate(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    success('Transaction cancelled successfully.')
                    this.fetchTransactions();
                } else {
                    error('Error in cancelling Transaction.')
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    delete = () => {

        let id = this.state.toDelete
        if(!id) return
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        Transaction.delete( id, headers)
            .then(({ status }) => {
                if ( status === 'success') {
                    this.setState({
                        showDeletePrompt: false,
                        toDelete: null
                    });
                    success('Transaction deleted successfully.');
                    this.fetchTransactions();
                } else {
                    error('Error in deleting transaction.');
                }
            })
            .catch(err => {
                console.warn(err);
            });
    };

    fetchUsers = () => {
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        let params = {
            show: 'all',
        };

        User.read(params, headers)
            .then(({ status, data }) => {
                let users = data.map(user => user.username);
                this.setState({
                    users: users.sort()
                });
            })
            .catch(err => {
                console.warn(err);
            });
    };

    fetchTransactions = (params = null) => {

        let headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.props.session
        }

        if(!params){
            params = {
                page: this.state.page || 1,
                filter: this.state.filter,
                query: this.state.query
            }
        }

        if(params.filter.term){
            delete params.filter.term
        }

        Transaction.getPending( params, headers )
            .then((res) => {

                this.setState({
                    transactions: res
                })
                //console.log(res)

            })
            .catch(err => {
                console.warn(err);
            });
    }

    componentWillMount() {
        const params = {
            filter: getC('p_filter') ? JSON.parse(getC('p_filter')) : {},
            query: getC('p_query') ? JSON.parse(getC('p_query')) : {},
            //page: getC('page') ? getC('page') : 1,
        }

        this.setState({...this.state,...params})

        this.fetchTransactions(params);
        this.fetchUsers()
    }

    formatUserPermission = () =>{
        let permissions = this.props.user.permissions || [];
        let item = permissions.find((perm) => {
            return perm.slug === 'pending-transactions'
        })

        if(item){
            let sub_permissions = item.sub_permissions
            sub_permissions = sub_permissions.filter(perm => {
                return perm.access
            })

            sub_permissions = sub_permissions.map(perm => perm.slug)

            this.setState({
                userPermission: sub_permissions
            })
        }

    }

    componentDidMount() {
        this.setAutoRefresh();
        this.formatUserPermission()
    }

    componentWillUnmount() {
        const { autoRefresh } = this.state
        clearInterval(autoRefresh)
        this.setState({
            autoRefresh: null
        })
    }

    debounceHandleUpdate = _.debounce(() => this.fetchTransactions(), 500, {maxWait: 500});

    validateDate = (field, dateMoment) => {
        const now = moment();
        const { filter: { dateTo, dateFrom } } = this.state

        if(dateMoment > now || (field === 'dateTo' && dateMoment < moment(dateFrom, 'YYYY-MM-DD')) || (field === 'dateFrom' && dateMoment > moment(dateTo, 'YYYY-MM-DD'))){
            return false
        }

        return true
    }

    handleDateChange = (field, val) => {
        const { page, filter, query } = this.state
        let date = val.format('YYYY-MM-DD');

        const newFilter = {...filter}
        newFilter[field] = date

        setC('p_filter', JSON.stringify(newFilter), 1)

        this.setState({
            filter: newFilter
        })

        const params = { page, query, filter: newFilter}
        this.fetchTransactions(params)

    }

    handleChange = async (e) => {

        let field = $(e.target).attr('name');
        let val = e.target.value;

        console.log(field, val)

        if (field === 'term') {
            let query = {...this.state.query};
            query[field] = val;

            setC('p_query', JSON.stringify(query), 1)

            await this.setState({
                query
            })

        } if (field === 'export') {

            await this.setState({
                export:val
            })

            this.handleExport();

        } else {
            let filter = {...this.state.filter};
            filter[field] = val;
            setC('p_filter', JSON.stringify(filter), 1)

            await this.setState({
                filter
            })
        }

        this.debounceHandleUpdate();
    }

    handleExport = () => {

        $('#exportFormComplete').submit();

    }

    //----------------------------
    handlePageChange = async (currentPage) => {

        await this.setState({
            page: currentPage || 1
        })

        this.fetchTransactions()
        console.log('currentPage', currentPage)
    }
    //----------------------------
    handleDelete = (e, id) => {
        e.preventDefault()
        this.setState({
            showDeletePrompt: true,
            toDelete: id
        })
    }

    renderDeletePrompt = () =>{
        return (
            <DeletePrompt
                show={this.state.showDeletePrompt}
                onClose={() => this.setState({showDeletePrompt: false, toDelete: null})}
                onSubmit={this.delete}
            />
        )
    }

    setAutoRefresh = () => {
        const { MIX_AUTOREFRESH, MIX_AUTOREFRESH_INTERVAL } = process.env
        const enableAutorefresh = MIX_AUTOREFRESH === 'true'

        let autoRefresh;
        if(enableAutorefresh){
            autoRefresh = setInterval(() => {
                this.fetchTransactions();
            }, 1000 * parseInt(MIX_AUTOREFRESH_INTERVAL))
        }

        this.setState({
            autoRefresh
        })
    }

    showClear = () => {
        let filter = getC('p_filter') ? JSON.parse(getC('p_filter')) : {}
        let query = getC('p_query') ? JSON.parse(getC('p_query')) : {}

        let combined  = {...filter, ...query}

        let overall = [];
        Object.keys(combined).forEach( key => {
            if(key && combined[key]){
                overall.push(combined[key])
            }
        })

        if(overall.length < 1){
            setC('filter', {}, -1)
            setC('query', {}, -1)
            return false
        }

        return true
    }

    clearSearchnFilter = () => {

        const filter = {...this.state.filter}
        filter['username'] = '';
        filter['dateFrom'] = '';
        filter['dateTo'] = '';
        filter['status'] = '';

        const query = {...this.state.query}
        query['term'] = '';

        this.setState({
            filter,
            query
        })
        setC('p_filter', {}, -1)
        setC('p_query', {}, -1)
        this.fetchTransactions({
            filter: {},
            query: {}
        })
    }

    render() {

        const { userPermission, users, transactions } = this.state
        //----------
        let options = users.map((user) => {
            return (
                <option key={user} value={user}>{user}</option>
            )
        })

        const showClear = this.showClear()
        //----------

        let data = transactions.data || []
        let transactionRows = data.map((trans) => {

            const { contact_details = {}  } = trans;
            const { name, phone, email } = contact_details || {};
            //-------------------------
            let contact = (email) ? email : phone;
            //-------------------------

            return (
                <tr key={trans.id}>
                    <th scope="row">{trans.created_at}</th>
                    <td>{trans.transactionId}</td>
                    <td>{name || 'NA'}</td>
                    <td>{trans.reference || 'NA'}</td>
                    <td>{trans.status || 'NA'}</td>
                    <td>{trans.time_elapsed || 'NA'}</td>
                    <td>{ contact }</td>
                    <td>{ trans.username || 'NA'}</td>
                    <td>
                        <center>
                            { (trans.status !== 'CANCELLED') && contact &&
                            <a href="#" onClick={(e) => this.resend(e, trans.id)}> <img style={{ width: '19px' }}
                                                                                        className="action-imgs"
                                                                                        src={`${ASSETS_URL}/icons/resend.png`}/></a>}
                        </center>
                    </td>
                    <td>
                        <center>
                            {(trans.status !== 'EXPIRED') && (trans.status !== 'CANCELLED') &&
                            <a href="#" onClick={(e) => this.terminate(e, trans.id)}> <img style={{ width: '19px' }}
                                                                                           className="action-imgs"
                                                                                           src={`${ASSETS_URL}/icons/terminate.png`}/>
                            </a>}</center>
                    </td>
                    {userPermission.includes('delete-records') &&<td>
                        <center>
                            <a href="#" onClick={(e) => this.handleDelete(e,trans.id)}> <img className="action-imgs"
                                                                                       style={{ width: '19px' }}
                                                                           src={`${ASSETS_URL}/icons/trash.png`}/></a>
                        </center>
                    </td>}
                </tr>
            )
        });

        //pagination
        let paginate = transactions || {}
        paginate = {current_page: paginate.current_page, last_page: paginate.last_page}

        return (
            <Container>
                <div className="pendingtransaction-body">
                    <div className="inputGroup d-flex">
                        <div className="quick-search">
                            <div className="searchPendingClass">
                                <span className="spanPendingSearch"><img className="searchimage"
                                                                         src={`${ASSETS_URL}/icons/search.png`}/></span>
                                <Input onChange={this.handleChange} type="text" value={this.state.query.term}
                                       className="searcPendinghInput" name="term" id="quicksearch"
                                       placeholder="Quick Search"/>
                            </div>
                        </div>
                        <div className="Status ml-auto">
                            <div className="selectPendingClass select-container">

                                <select onChange={this.handleChange} value={this.state.filter.status} name="status"
                                        className="selectPendingInput form-control">
                                    <option value="">All</option>
                                    <option value="PENDING">Pending</option>
                                    <option value="INPROGRESS">InProgress</option>
                                    <option value="EXPIRED">Expired</option>
                                    <option value="CANCELLED">Cancelled</option>
                                </select>
                            </div>
                        </div>
                        <div className="User">
                            <div className="selectPendingClass select-container">
                                <select onChange={this.handleChange} value={this.state.filter.username} name="username"
                                       className="selectPendingInput form-control">
                                    <option value="">User</option>
                                    {options}
                                </select>
                            </div>
                        </div>
                        <div className="date-pick">
                            <Datetime
                                isValidDate={(moment) => this.validateDate('dateFrom', moment)}
                                onChange={(moment) => this.handleDateChange('dateFrom',moment)}
                                inputProps={{'placeholder': 'Date from',name: 'dateFrom'}}
                                dateFormat="YYYY-MM-DD"
                                value={this.state.filter.dateFrom ? moment(this.state.filter.dateFrom, 'YYYY-MM-DD').toDate() : ''}
                                timeFormat={false} />
                            <img src="/assets-portal/images/icons/calendar.svg" alt=""/>
                        </div>
                        <div className="date-pick">
                            <Datetime
                                isValidDate={(moment) => this.validateDate('dateTo', moment)}
                                onChange={(moment) => this.handleDateChange('dateTo',moment)}
                                inputProps={{'placeholder': 'Date to',name: 'dateTo'}}
                                dateFormat="YYYY-MM-DD"
                                value={ this.state.filter.dateTo ? moment(this.state.filter.dateTo, 'YYYY-MM-DD').toDate() : ''}
                                timeFormat={false} />
                            <img src="/assets-portal/images/icons/calendar.svg" alt=""/>
                        </div>
                        {showClear && <div className="clearCookie mr-2">
                            <Button className="clear-filters" onClick={() => {this.clearSearchnFilter()}} color="primary">Clear Filters</Button>
                        </div>}
                        {userPermission.includes('export-records') && <div className="Type">
                            <form action="/api/portal/transaction/download" id="exportFormComplete" method="post">
                                <div className="selectPendingClass select-container">
                                    <select onChange={this.handleChange} name="export"
                                           className="selectPendingInput form-control">
                                        <option value="">Export</option>
                                        <option value="csv">CSV</option>
                                        <option value="excel">Excel</option>
                                    </select>
                                </div>
                                <input type="hidden" name="from" value="incomplete"/>
                            </form>
                        </div>}
                    </div>
                    <div className="table-part">
                        <Table className="custom-table">
                            <thead>
                            <tr>
                                <th className="th-date">Date</th>
                                <th>Transaction ID</th>
                                <th className="">Name</th>
                                <th className="th-reference">Reference</th>
                                <th className="th-status">Status</th>
                                <th>Time elapsed</th>
                                <th>Contact</th>
                                <th>User</th>
                                <th className="th-resend">Resend</th>
                                <th className="th-terminate">Cancel</th>
                                {userPermission.includes('delete-records') && <th className="th-delete">Delete</th>}
                            </tr>
                            </thead>
                            <tbody>
                            {transactionRows}
                            </tbody>
                        </Table>
                        <div className="float-right">
                            <Pagination onPageChange={this.handlePageChange} data={paginate}/>
                        </div>
                    </div>
                </div>
                {this.renderDeletePrompt()}
            </Container>
        );
    };
}


export default withRouter(connect(mapStateToProps, null)(PendingTransaction));
function mapStateToProps({user}) {
    return { session: user.session, user: user.info };
}
