import React from 'react';
import {
    Container,
    TabContent,
    TabPane,
    Nav,
    NavItem,
    NavLink,
    Button,
    Row,
    Col,
} from 'reactstrap';
import classnames from 'classnames';
import {ASSETS_URL, API_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify';
import {withRouter} from 'react-router-dom';
import './TransactionSingle.style.scss';
import ReactPlayer from 'react-player';
import {findDOMNode} from 'react-dom'
import screenfull from 'screenfull'
import {connect} from "react-redux";
import Carousel, { Modal, ModalGateway } from 'react-images';
import Asf from '@components/Modals/Asf';
import Features from '@components/Modals/Features';
import AssessmentExplanation from '@components/Modals/AssessmentExplanation';
import Trsansaction from '@services/Transaction'
import User from '@services/User'

const icons = {
    success: `${ASSETS_URL}/app/status-yes.png`,
    failed: `${ASSETS_URL}/app/status-no.png`,
    warning: `${ASSETS_URL}/app/warning.png`,
    noStatus: `${ASSETS_URL}/app/no-status.png`
};

class TransactionSingle extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = this.setInitialState();
    }

    setInitialState = () => {
        return {
            idTabs: {
                0: 1,
            },
            activeTab: '1',
            tId: '',
            data: {},
            showMTab: '1',
            loading: false,
            currentVideo: {
                index: 1
            },
            imageLightBox: false,
            imageUrl: '',

            showAsf: false,

            playVideo: false,
            showPlay: true,
            showVideoControl: false,

            showFeature: false,
            assessmentExplanation:{
                show: false,
                type: 'image'
            },
            userPermission: [],
            selfie : 1,
            centrixCountries: ['AU', 'NZ']
        }
    }

    FSstatus = value => {
        if (parseInt(value) >= 65) {
            return 'success';
        } else if (parseInt(value) >= 50) {
            return 'warning';
        } else {
            return 'failed';
        }
    };

    smartIdStatus = value => {
        if (value !== 'NA') {
            if (value === 'Match') {
                return icons.success;
            } else if (value === 'NotPresent') {
                return icons.warning;
            } else if (value === 'NoMatch') {
                return icons.failed;
            } else {
                return icons.noStatus;
            }
        } else {
            return icons.noStatus;
        }
    };

    wordFormat = (string, to) => {
        let newString = '';
        for (let i in string) {
            let s = string[i];
            if (s.match(/([A-Z])/g)) {
                s = (to + s).toLowerCase();
            }
            newString += s;
        }
        newString = newString.replace('n z t a', 'NZTA');
        newString = newString.replace('n z', 'NZ');
        newString = newString.replace('d i a', 'DIA');
        return newString;
    };

    renderMap = () => {

        const {data} = this.state;

        let {
            details = {},
        } = data;

        const {
            location = {}
        } = details;

        const {ip = {}, geolocation = {}} = location

        let styles = [
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e9e9e9"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dedede"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            }
        ];
        let styledMapType = new google.maps.StyledMapType(styles, {name: 'Styled'});

        let ipLocation = ip.coordinates

        let ipLocationParts = ipLocation.split(',')
        let ipLoc = {lat: parseFloat(ipLocationParts[0]), lng: parseFloat(ipLocationParts[1])};

        let map = new google.maps.Map(
            document.getElementById('map1'), {
                zoom: 14,
                center: ipLoc,
                mapTypeId: 'Styled',
                streetViewControl: false,
                mapTypeControl: false,
                controlSize: 22,
            });
        map.mapTypes.set('Styled', styledMapType);
        //let icon_url =  `${ASSETS_URL}/images/marker.png`;;

        let marker = new google.maps.Marker({
            position: ipLoc,
            map: map,
            //icon: icon_url
        });

        let geoLocation = geolocation.coordinates

        if (geolocation) {

            let geoLocationParts = geoLocation.split(',')
            let geoLoc = {lat: parseFloat(geoLocationParts[0]), lng: parseFloat(geoLocationParts[1])};

            let map2 = new google.maps.Map(
                document.getElementById('map2'), {
                    zoom: 14,
                    center: geoLoc,
                    mapTypeId: 'Styled',
                    streetViewControl: false,
                    mapTypeControl: false,
                    controlSize: 22,
                });

            map2.mapTypes.set('Styled', styledMapType);

            let marker2 = new google.maps.Marker({
                position: geoLoc,
                map: map2,
                //icon: icon_url
            });
        }
    }

    componentWillMount() {
        let { tId } = this.props
        if(!tId){
            tId = this.props.match.params.tId
        }
        this.setState({
            tId
        });
    }

    formatUserPermission = () =>{
        let permissions = this.props.user.permissions || [];
        let item = permissions.find((perm) => {
            return perm.slug === 'customer-repository'
        })

        if(item){
            let sub_permissions = item.sub_permissions
            sub_permissions = sub_permissions.filter(perm => {
                return perm.access
            })

            sub_permissions = sub_permissions.map(perm => perm.slug)

            this.setState({
                userPermission: sub_permissions
            })
        }

    }

    componentDidMount() {
        if (screenfull.enabled) {
            screenfull.on('change', () => {
                if (screenfull.isFullscreen) {
                    this.setState({
                        showVideoControl: true
                    })
                } else {
                    this.setState({
                        showVideoControl: false
                    })
                }
            });
        }
        this.loadData();
        this.updateSeenStatus()
        this.formatUserPermission()
    }

    componentWillReceiveProps(nextProps, nextContext) {
        //console.log(nextProps.match.params.tId ,this.props.match.params.tId)
        if (nextProps.match.params.tId !== this.props.match.params.tId) {
            this.loadData(nextProps.match.params.tId)
        }
    }

    loadData = (tId = null) => {
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        let id = (tId) ? tId : this.state.tId

        Trsansaction.getDetails(id, headers)
            .then(({data}) => {

                const {details = {}} = data;
                const {face_scan = {}} = details;
                const {liveness = {}} = face_scan;
                const {attempts = []} = liveness;

                let currentVideo = {...((attempts.length > 0) ? attempts[attempts.length - 1] : {})}
                currentVideo['index'] = (attempts.length > 0) ? attempts.length - 1 : 0

                this.setState({
                    data: data,
                    currentVideo
                });
                this.renderMap()
            })
            .catch(err => {
                console.warn(err);
            });
    }

    updateSeenStatus = () => {
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        User.updateSeen(this.state.tId, headers)
            .then(({status}) => {

            })
            .catch(err => {
                console.warn(err);
            });
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    onClickFullscreen = () => {
        screenfull.request(findDOMNode(this.refs.player))
    }

    isJSON = str => {
        try {
            return JSON.parse(str) && !!str;
        } catch (e) {
            return false;
        }
    };

    renderLightBox = () => {

        const {imageLightBox = false, imageUrl = ''} = this.state

        const images = [
            {source: imageUrl},
        ]

        return (
            <ModalGateway>
                {imageLightBox ? (
                    <Modal onClose={() => this.setState({imageLightBox: false})}>
                        <Carousel
                            views={images} />
                    </Modal>
                ) : null}
            </ModalGateway>
        )
    }

    navigateTo = (e, tId = null) => {
        e.preventDefault()
        if (tId) {
            this.props.history.push(`/portal/transaction/${tId}`);
        }

    }

    changeCurrentVideo = (e, vid, i) => {
        e.preventDefault()
        let currentVideo = {...vid}
        currentVideo['index'] = i

        this.setState({currentVideo});
    }

    assessmentBreakDown = (idType, asf, country = 'AU') => {
        let  result = {
            image: false,
            photo: false,
            doc: false,
            detail: false,
        }

        if(asf){
            if(idType === 'passport'){
                result['image']    = !(!asf['check_3'] || !asf['check_6'] || !asf['check_7'])
                result['photo']    = asf['check_5']
                result['doc']      = asf['check_2']
                result['detail']   = asf['check_4']
            }else{
                result['image'] = !(!asf['check_3'] || !asf['check_4'])
                result['photo'] = !(!asf['check_2'] || !asf['check_5'])
                result['doc']   = asf['check_1']

                if(country === 'NZ') {
                    result['detail']   = !(!asf['check_6'] || !asf['check_7'] || !asf['check_8'] || !asf['check_9'])
                }else{
                    result['detail']   = !(!asf['check_6'] || !asf['check_8'] || !asf['check_9'])
                }
            }
        }

        return result;
    }

    renderAssessmentExplanation = () => {
        const { assessmentExplanation: {show, type} } = this.state
        return (
            <AssessmentExplanation show={show} onClose={() => this.setState({assessmentExplanation: {show:false,type:'image'}})} type={type}/>
        );
    }

    render() {
        const {guided = false} = this.props
        const {data, showVideoControl, currentVideo, userPermission, selfie = 1, centrixCountries} = this.state;

        let {
            token = null,
            status = null,
            id_type = '',
            flow_type = '',
            details = {},
            country = '',
            reference = 'NA',
            created_at ='',
            completed_at= '',
            prevTrans = null,
            nextTrans = null,
        } = data;

        const {
            card = {},
            centrix = {},
            contact = {},
            device = {},
            location = {},
            extracted = {},
            face_scan = {},
            personal = {},
            statistics = {}
        } = details;

        const {
            asf_check = {},
            images = {},
            edited = [],
            id_capture_count = 1
        } = card

        const {
            uncropped = {}
        } = images

        const {
            result = {},
            overall,
            ResponseMeta,
        } = centrix

        const {
            smart_id = {},
            pep_watchlist = {},
            driver_licence = {},
            passport = {},
        } = result

        const {
            liveness = {},
            selfies: { face, turn_head } = {},
            liveness_assessments
        } = face_scan;

        const {
            attempts = []
        } = liveness;

        const spoofRisk = liveness_assessments === 'Low Risk'
        const isAlternate = flow_type === 'ALTERNATE'
        //--------------------------------------------------
        const effective_id_type = (id_type === 'id_card') ? 'passport' : id_type
        const {image, photo, doc, detail} = this.assessmentBreakDown(effective_id_type, asf_check.result, country)
        //--------------------------------------------------
        let videoLinks;

        if(!isAlternate){
            videoLinks = attempts.map((attempt, i) => {
                return (
                    <a href="#" onClick={(e) => this.changeCurrentVideo(e, attempt, i)}
                       className={classnames("attempt-link", {active: currentVideo.index === i})}
                       key={i}>{`Attempt ${i + 1}`}</a>
                );
            })
        }else{
            videoLinks = (
                <div>
                    <a href="#" onClick={() => this.setState({selfie: 1})}
                       className={classnames("attempt-link", {active: selfie === 1})}>{`Picture 1`}</a>
                    <a href="#" onClick={() => this.setState({selfie: 2})}
                       className={classnames("attempt-link", {active: selfie === 2})}>{`Picture 2`}</a>
                </div>
            );
        }


        //--------------------------------------------------
        const {ip = {}, geolocation = {}} = location
        const { MIX_ENABLE_CENTRIX = 'false', MIX_ENABLE_SPOOF_RISK = 'true' } = process.env
        const enableCentrix = MIX_ENABLE_CENTRIX === 'true' && centrixCountries.includes(country)
        const enableSpoof = MIX_ENABLE_SPOOF_RISK === 'true'
        //---------------------------------------------------

        let a_success = `${ASSETS_URL}/app/status-yes.png`;
        let a_warning = `${ASSETS_URL}/app/warning.png`;
        let play = `${ASSETS_URL}/icons/play.png`;
        let maximise = `${ASSETS_URL}/icons/maximise.png`;

        let smart_id_data = result.smart_id_data || []
        let centrixDatas = smart_id_data.map((item, i) => {
            return (
                <tr key={i}>
                    <td>
                        <h4 className="ucfirst">{this.wordFormat(item.name, ' ')}</h4>
                    </td>
                    <td className="text-center">
                        <img src={this.smartIdStatus(item.name_match_status)}/>
                    </td>
                    <td className="text-center">
                        <img src={this.smartIdStatus(item.dateofbirth_match_status)}/>
                    </td>
                    <td className="text-center">
                        <img src={this.smartIdStatus(item.address_match_status)}/>
                    </td>
                </tr>
            );
        });

        return (
            <div className="transaction-single">
                <div className="tab-heading-container">
                    <Container>
                        {this.props.children}
                        <div className="top-info d-flex justify-content-around">
                            <div>
                                <h4>Time</h4>
                                <p>{created_at}</p>
                            </div>
                            <div>
                                <h4>Transaction ID</h4>
                                <p>{token}</p>
                            </div>
                            <div>
                                <h4>Reference</h4>
                                <p>{reference || 'NA'}</p>
                            </div>
                            <div>
                                <h4>Status</h4>
                                <p>{status}</p>
                            </div>
                            <div>
                                <h4>Name</h4>
                                <p>{personal.firstName + ' ' + (personal.lastName) ? personal.lastName : '' }</p>
                            </div>
                            <div>
                                <h4>DOB</h4>
                                <p>{personal.dateOfBirth}</p>
                            </div>
                            <div>
                                <h4>Contact</h4>
                                <p>{(contact.email) ? contact.email : contact.phone}</p>
                            </div>
                        </div>
                        <div className="nav-tabs-container">
                            <Nav className="d-flex justify-content-around" tabs>
                                <NavItem>
                                    <NavLink
                                        className={classnames({active: this.state.activeTab === '1'})}
                                        onClick={() => {
                                            this.toggle('1');
                                        }}
                                    >
                                        Summary
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={classnames({active: this.state.activeTab === '2'})}
                                        onClick={() => {
                                            this.toggle('2');
                                        }}
                                    >
                                        ID Documents
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={classnames({active: this.state.activeTab === '3'})}
                                        onClick={() => {
                                            this.toggle('3');
                                        }}
                                    >
                                        Face & Liveness
                                    </NavLink>
                                </NavItem>
                                {enableCentrix && <NavItem>
                                    <NavLink
                                        className={classnames({active: this.state.activeTab === '4'})}
                                        onClick={() => {
                                            this.toggle('4');
                                        }}
                                    >
                                        Data
                                    </NavLink>
                                </NavItem>}
                            </Nav>
                            {!guided && userPermission.includes('back-forward-buttons') && <div className="navigation">
                                <a onClick={(e) => this.navigateTo(e, nextTrans)} href="#">
                                    {nextTrans && <img src={`${ASSETS_URL}/icons/prev.png`} alt=""/>}
                                    {!nextTrans && <img src={`${ASSETS_URL}/icons/prev-disabled.png`} alt=""/>}
                                </a>
                                <a onClick={(e) => this.navigateTo(e, prevTrans)} href="#">
                                    {prevTrans && <img src={`${ASSETS_URL}/icons/next.png`} alt=""/>}
                                    {!prevTrans && <img src={`${ASSETS_URL}/icons/next-disabled.png`} alt=""/>}
                                </a>
                            </div>}
                        </div>
                    </Container>
                </div>
                <Container>
                    <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                            <div className="tab-1-inner">
                                <Row>
                                    <Col sm={12}>
                                        <div className="content-tab-heading">
                                            <h4>Verification Overview</h4>
                                        </div>
                                        <div className="consumer-verification">
                                            <Row>
                                                <Col sm={4}>
                                                    <div className="block">
                                                        <div className="d-flex">
                                                            <span className="strong">Name</span>
                                                            <span>{personal.firstName + ' ' + (personal.middleName || '') + ' ' + personal.lastName}</span>
                                                        </div>
                                                        <div className="d-flex">
                                                            <span className="strong">DOB</span>
                                                            <span>{personal.dateOfBirth} ({personal.age} yo)</span>
                                                        </div>
                                                        <div className="d-flex">
                                                            <span className="strong">Address</span>
                                                            <span>{contact.residential_address}</span>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col sm={4}>
                                                    <div className="block">
                                                        <div className="d-flex">
                                                            <span className="strong">Time Initiated</span>
                                                            <span>{created_at}</span>
                                                        </div>
                                                        <div className="d-flex">
                                                            <span className="strong">Time Completed</span>
                                                            <span>{completed_at}</span>
                                                        </div>
                                                        <div className="d-flex">
                                                            <span className="strong">Contact</span>
                                                            <span>{(contact.email) ? contact.email : contact.phone}</span>
                                                        </div>
                                                        <div className="d-flex">
                                                            <span className="strong">Status</span>
                                                            <span>{status}</span>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col sm={4}
                                                     className="d-flex align-items-center justify-content-center">
                                                    <div className="flags">
                                                        <div className="flags-inner">
                                                            <div className="d-flex">
                                                                {enableCentrix && <div
                                                                    className={classnames({"success": centrix.overall}, {"failed": !centrix.overall})}>DATA</div>}
                                                                <div
                                                                    className={classnames({"success": asf_check.result && asf_check.result.overall}, {"failed": !(asf_check.result && asf_check.result.overall)})}>DOCS
                                                                </div>
                                                                {!enableCentrix && <div
                                                                    className={classnames({"success": face_scan.confidence}, {"failed": !face_scan.confidence})}>FACE</div>}
                                                            </div>
                                                            <div className="d-flex">
                                                                {enableCentrix && <div
                                                                    className={classnames({"success": face_scan.confidence}, {"failed": !face_scan.confidence})}>FACE</div>}
                                                                <div
                                                                    className={classnames({"success": liveness.result}, {"failed": !liveness.result})}>LIVENESS
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                        <div className="other">
                                            <Row>
                                                <Col sm={4}>
                                                    <div>
                                                        <div className="content-tab-heading">
                                                            <h4>Timing & Statistics</h4>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col sm={5}>
                                                    <div>
                                                        <div className="content-tab-heading">
                                                            <h4>Location</h4>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col sm={3}>
                                                    <div>
                                                        <div className="content-tab-heading">
                                                            <h4>Network</h4>
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col sm={4}>
                                                    <div className="bg-white h-100">
                                                        <div className="content-tab-heading-secondary">
                                                            <div className="d-flex">
                                                                <span>Item</span>
                                                                <span>Time</span>
                                                            </div>
                                                        </div>
                                                        <div className="block bg-white statistics">
                                                            <div className="d-flex">
                                                                <span className="strong">Time to activate SMS</span>
                                                                <span>{statistics.activate_time}</span>
                                                            </div>
                                                            <div className="d-flex">
                                                                <span className="strong">Time to review terms</span>
                                                                <span>{statistics.review_terms}</span>
                                                            </div>
                                                            <div className="d-flex">
                                                                <span className="strong">Time to Capture ID</span>
                                                                <span>{statistics.capture_id}</span>
                                                            </div>
                                                            <div className="d-flex">
                                                                <span className="strong">Time to Review Data</span>
                                                                <span>{statistics.review_data}</span>
                                                            </div>
                                                            <div className="d-flex">
                                                                <span className="strong">Time to Capture Liveness</span>
                                                                <span>{statistics.liveness}</span>
                                                            </div>
                                                            <div className="d-flex">
                                                                <span className="strong">Total process time</span>
                                                                <span>{statistics.total}</span>
                                                            </div>
                                                            <div className="d-flex">
                                                                <span className="strong">Liveness Attempt</span>
                                                                <span>{face_scan.attempts}</span>
                                                            </div>
                                                            <div className="d-flex">
                                                                <span className="strong">ID Capture Attempts</span>
                                                                <span>{id_capture_count}</span>
                                                            </div>
                                                            <div className="d-flex">
                                                                <span className="strong">Fields Changed</span>
                                                                <span>{edited.length}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                                <Col sm={5}>
                                                    <div className="bg-white h-100">
                                                        <div className="locations">
                                                            <div className="content-tab-heading-secondary">
                                                                <div className="d-flex">
                                                                    <span className="text-center">IP Address</span>
                                                                    <span className="text-center">Geolocation</span>
                                                                </div>
                                                            </div>
                                                            <div
                                                                className="d-flex align-items-center justify-content-between">
                                                                <div id="map1" style={{
                                                                    width: '240px',
                                                                    height: '250px',
                                                                    margin: '4px'
                                                                }}></div>
                                                                {geolocation.loc && <div id="map2" style={{
                                                                    width: '240px',
                                                                    height: '250px',
                                                                    margin: '4px'
                                                                }}></div>}
                                                                {!geolocation.loc && <div
                                                                    className="d-flex justify-content-center align-items-center"
                                                                    style={{
                                                                        width: '240px',
                                                                        height: '250px',
                                                                        margin: '4px'
                                                                    }}><span>Not Visible</span></div>}
                                                            </div>
                                                            <div
                                                                className="d-flex align-items-center justify-content-between locText">
                                                                <div className="text-center w-50 p-3">{ip.loc}</div>
                                                                {geolocation.loc && <div
                                                                    className="text-center w-50 p-3">{geolocation.loc}</div>}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </Col>
                                                <Col sm={3}>
                                                    <div className="bg-white h-100">
                                                        <div className="block network">
                                                            <div className="d-flex flex-column">
                                                                <span className="strong">IP Address</span>
                                                                <span>{device.ip}</span>
                                                            </div>
                                                            <div className="d-flex flex-column">
                                                                <span className="strong">Operating System</span>
                                                                <span>{device.device + ' ' + device.browser + ' ' + device.os}</span>
                                                            </div>
                                                            <div className="d-flex flex-column">
                                                                <span className="strong">VPN</span>
                                                                <span>{device.vpn ? 'YES' : 'NO'}</span>
                                                            </div>
                                                            <div className="d-flex flex-column">
                                                                <span className="strong">Network/ISP</span>
                                                                <span>{device.isp}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        </TabPane>

                        <TabPane tabId="2">
                            <Container className="p-0">
                                <div className="tab-2-inner">
                                    <Row>
                                        <Col sm={6}>
                                            <div className="id-repeat">
                                                <div className="content-tab-heading">
                                                    <h4>{id_type.replace(/_/g, " ")}</h4>
                                                </div>
                                                <Row>
                                                    <Col sm={6}>
                                                        <div className="cards">
                                                            <div>
                                                                <img
                                                                    onClick={() => this.setState({
                                                                        imageLightBox: true,
                                                                        imageUrl: uncropped.front,
                                                                    })}
                                                                    src={`${images.front}`} alt=""/>
                                                            </div>
                                                            {images.back && images.back.includes('.') &&
                                                            <div className="mt-2">
                                                                <img
                                                                    onClick={() => this.setState({
                                                                        imageLightBox: true,
                                                                        imageUrl: uncropped.back,
                                                                    })}
                                                                    src={`${images.back}`} alt=""/>
                                                            </div>}
                                                        </div>
                                                    </Col>
                                                    <Col sm={6}>
                                                        <div>
                                                            <div
                                                                className="content-tab-heading-secondary card-info-tabs">
                                                                <div className="d-flex">
                                                                <span
                                                                    className={classnames({"active": this.state.idTabs[0] === 1})}
                                                                    onClick={() => {
                                                                        this.setState({
                                                                            idTabs: {...this.state.idTabs, 0: 1}
                                                                        });
                                                                    }}>
                                                                    Confirmed
                                                                </span>
                                                                    <span
                                                                        className={classnames({"active": this.state.idTabs[0] === 2})}
                                                                        onClick={() => {
                                                                            this.setState({
                                                                                idTabs: {...this.state.idTabs, 0: 2}
                                                                            });
                                                                        }}>
                                                                    Extracted
                                                                </span>
                                                                </div>
                                                            </div>
                                                            <div className="block card-info bg-white">
                                                                {this.state.idTabs[0] === 1 &&
                                                                <div className="confirmed">
                                                                    <div className="d-flex">
                                                                        <span className="strong">Name</span>
                                                                        <span>{personal.firstName}</span>
                                                                    </div>
                                                                    <div className="d-flex">
                                                                        <span className="strong">Middle name</span>
                                                                        <span>{personal.middleName}</span>
                                                                    </div>
                                                                    <div className="d-flex">
                                                                        <span className="strong">Surname</span>
                                                                        <span>{personal.lastName}</span>
                                                                    </div>
                                                                    {id_type !== 'passport' && !id_type.match(/idcard/i) && id_type !== 'id_card' && <div className="d-flex">
                                                                        <span className="strong">Address</span>
                                                                        <span>{contact.residential_address}</span>
                                                                    </div>}
                                                                    {id_type !== 'passport' && !id_type.match(/idcard/i) && id_type !== 'id_card' && <div className="d-flex">
                                                                        <span className="strong">License Number</span>
                                                                        <span>{personal.licenceNumber}</span>
                                                                    </div>}
                                                                    {id_type === 'passport' && <div className="d-flex">
                                                                        <span className="strong">Passport Number</span>
                                                                        <span>{personal.passportNumber}</span>
                                                                    </div>}
                                                                    {id_type === 'id_card' && <div className="d-flex">
                                                                        <span className="strong">Card Number</span>
                                                                        <span>{personal.passportNumber}</span>
                                                                    </div>}
                                                                    {id_type.match(/idcard/i) && <div className="d-flex">
                                                                        <span className="strong">Card Number</span>
                                                                        <span>{personal.cardNumber}</span>
                                                                    </div>}
                                                                    <div className="d-flex">
                                                                        <span className="strong">Date of Birth</span>
                                                                        <span>{personal.dateOfBirth}</span>
                                                                    </div>
                                                                    <div className="d-flex">
                                                                        <span className="strong">Date of Expiry</span>
                                                                        <span>{personal.dateOfExpiry}</span>
                                                                    </div>
                                                                </div>}
                                                                {this.state.idTabs[0] === 2 &&
                                                                <div className="extracted">
                                                                    <div className="d-flex">
                                                                        <span className="strong">Name</span>
                                                                        <span>{extracted.firstName}</span>
                                                                    </div>
                                                                    <div className="d-flex">
                                                                        <span className="strong">Middle name</span>
                                                                        <span>{extracted.middleName}</span>
                                                                    </div>
                                                                    <div className="d-flex">
                                                                        <span className="strong">Surname</span>
                                                                        <span>{extracted.lastName}</span>
                                                                    </div>
                                                                    {id_type !== 'passport' && id_type.match(/idcard/i) && id_type !== 'id_card' && <div className="d-flex">
                                                                        <span className="strong">Address</span>
                                                                        <span>{extracted.address}</span>
                                                                    </div>}
                                                                    {id_type !== 'passport' && !id_type.match(/idcard/i) && id_type !== 'id_card' && <div className="d-flex">
                                                                        <span className="strong">License Number</span>
                                                                        <span>{extracted.licenceNumber}</span>
                                                                    </div>}
                                                                    {id_type === 'passport' && <div className="d-flex">
                                                                        <span className="strong">Passport Number</span>
                                                                        <span>{extracted.cardNumber}</span>
                                                                    </div>}
                                                                    {id_type.match(/idcard/i) && <div className="d-flex">
                                                                        <span className="strong">Card Number</span>
                                                                        <span>{extracted.cardNumber}</span>
                                                                    </div>}
                                                                    <div className="d-flex">
                                                                        <span className="strong">Date of Birth</span>
                                                                        <span>{extracted.dateOfBirth}</span>
                                                                    </div>
                                                                    <div className="d-flex">
                                                                        <span className="strong">Date of Expiry</span>
                                                                        <span>{extracted.expiryDate}</span>
                                                                    </div>
                                                                </div>}
                                                                <p className="strong fraud-assessment-label text-capitalize">Document
                                                                    fraud assessment</p>
                                                                <div onClick={() => this.setState({showAsf: true})}
                                                                     className={classnames("flag", {"success": asf_check.document_authenticity}, {"failed": !asf_check.document_authenticity})}>
                                                                    {(asf_check.document_authenticity) ? 'Low Risk' : 'High Risk'}
                                                                </div>

                                                                <p className="strong assessment-breakdown-label text-capitalize">Assessment Breakdown</p>
                                                                <div className="assessment-breakdown">
                                                                    <div className="d-flex justify-content-between">
                                                                        <div onClick={() => this.setState({assessmentExplanation: {show:true,type:'image'}})} className={image? "success": "failed"}>Image <br/>Composition</div>
                                                                        <div onClick={() => this.setState({assessmentExplanation: {show:true,type:'photo'}})} className={photo? "success": "failed"}>Photo <br/>Check</div>
                                                                    </div>
                                                                    <div className="d-flex justify-content-between">
                                                                        <div onClick={() => this.setState({assessmentExplanation: {show:true,type:'doc'}})} className={doc? "success": "failed"}>Document <br/>Integrity</div>
                                                                        <div onClick={() => this.setState({assessmentExplanation: {show:true,type:'detail'}})} className={detail? "success": "failed"}>Detail <br/>Check</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                        {/*<Col sm={6}>
                                        <div>
                                            <div className="content-tab-heading">
                                                <h4>Medicare card</h4>
                                            </div>
                                        </div>
                                    </Col>*/}
                                    </Row>
                                </div>
                            </Container>
                        </TabPane>

                        <TabPane className="document" tabId="3">
                            <Container className="p-0">
                                <div className="tab-3-inner">
                                    <Row>
                                        <Col sm={4}>
                                            <div className="content-tab-heading">
                                                <h4>{(isAlternate) ? 'Alternate Flow' : 'Liveness Video'}</h4>
                                            </div>
                                            <div className="video-container">
                                                <div className="attempt-links">
                                                    {videoLinks}
                                                </div>
                                                { !isAlternate && <div className="video-inner">
                                                    {this.state.showPlay && <div
                                                        className="play-icon d-flex align-items-center justify-content-center">
                                                        <img onClick={() => this.setState({playVideo: true})} src={play}
                                                             alt=""/>
                                                    </div>}
                                                    <ReactPlayer
                                                        width='100%' height='100%'
                                                        className={classnames({"ocr-react-player": !showVideoControl})}
                                                        ref='player'
                                                        controls={this.state.showVideoControl}
                                                        playing={this.state.playVideo}
                                                        onEnded={() => this.setState({
                                                            showPlay: true,
                                                            playVideo: false
                                                        })}
                                                        onPause={() => this.setState({
                                                            showPlay: true,
                                                            playVideo: false
                                                        })}
                                                        onPlay={() => this.setState({showPlay: false})}
                                                        url={currentVideo.video}/>
                                                </div>}
                                                { !isAlternate && <div className="btn-container">
                                                    <Button
                                                        outline={true}
                                                        size="md"
                                                        color="secondary"
                                                        block={true}
                                                        onClick={this.onClickFullscreen}
                                                        className="enlarge"> Enlarge &nbsp; &nbsp; &nbsp;<span><img
                                                        src={maximise} alt=""/></span></Button>
                                                </div>}

                                                { isAlternate && <div className="selfie-inner">
                                                    {selfie === 1 && <img src={face} alt="Face image"/>}
                                                    {selfie === 2 && <img src={turn_head} alt="Turn head"/>}
                                                </div>}

                                                <div
                                                    className="d-flex align-items-center justify-content-between bottom-flags">
                                                    <div className="title">Liveness</div>
                                                    <div className="flag-container">
                                                        <div
                                                            className={classnames("flag", {'success': currentVideo.result}, {'failed': !currentVideo.result})}>{(currentVideo.result) ? 'Pass' : 'Fail'}</div>
                                                    </div>
                                                </div>

                                                {enableSpoof && <div className="d-flex align-items-center justify-content-between bottom-flags">
                                                    <div className="title">Spoof Risk</div>
                                                    <div className="flag-container">
                                                        <div className={classnames("flag", {'success': !currentVideo.spoof_risk}, {'failed': currentVideo.spoof_risk})}>{(currentVideo.spoof_risk) ? 'High Risk': 'Low Risk'}</div>
                                                    </div>
                                                </div>}

                                            </div>
                                        </Col>
                                        <Col sm={8}>
                                            <div className="content-tab-heading">
                                                <h4>Face Match</h4>
                                            </div>
                                            <div className="card-face-container d-flex justify-content-around">
                                                <div className="face-image">
                                                    <p>Face from ID Document</p>
                                                    <img src={images.face} alt=""/>
                                                </div>
                                                {!isAlternate && <div className="face-image face-scan-image">
                                                    <p>Face from Liveness Video</p>
                                                    <img src={face_scan.face} alt=""/>
                                                </div>}
                                            </div>
                                            <div className="d-flex bg-white feature-container">
                                                <div className="d-flex flag-container justify-content-center align-items-center pb-5">
                                                    <div className="title">Face Match</div>
                                                    <div className={classnames("flag", {'success': face_scan.confidence}, {'failed': !face_scan.confidence})}
                                                    >{(face_scan.confidence) ? 'Pass' : 'Fail'}</div>
                                                </div>
                                                <div className="pb-5 pr-5">
                                                    <Features features={face_scan.features}/>
                                                </div>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>

                            </Container>
                        </TabPane>

                        {enableCentrix && <TabPane className="centrix" tabId="4">
                            <Container>
                                <Row>
                                    <div className="col-lg-6 col-md-6">
                                        <h4 className="title c-secondary pull-left">
                                            Smart ID Verified
                                        </h4>
                                        <img
                                            className="pull-right title-status"
                                            src={
                                                smart_id.is_verified
                                                    ? a_success
                                                    : a_warning
                                            }
                                        />
                                        <div className="clearfix"/>
                                        <p className="c-default">
                                            Smart ID Verification rules requires a number of trusted
                                            data sources to have
                                            <br/>
                                            matched. please select the link for further clarification.
                                        </p>
                                        <div className="text-status">
                                            <h4>Name and Date of Birth Verified</h4>
                                            <img
                                                src={
                                                    smart_id.is_name_verified &&
                                                    smart_id.is_dateofbirth_verified
                                                        ? a_success
                                                        : a_warning
                                                }
                                            />
                                        </div>
                                        <div className="text-status">
                                            <h4>Name and Address Verified</h4>
                                            <img
                                                src={
                                                    smart_id.is_name_verified &&
                                                    smart_id.is_address_verified
                                                        ? a_success
                                                        : a_warning
                                                }
                                            />
                                        </div>
                                        <div className="text-status">
                                            <h4>Message</h4>
                                            <div className="data msg c-default">
                                                {smart_id.Message}
                                            </div>
                                        </div>

                                        <table width="100%" className="table-sts">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <h4>Summary of Data Sources Searched</h4>
                                                </td>
                                                <td width="85" className="text-center">
                                                    <h5 className="c-default">Name</h5>
                                                </td>
                                                <td width="85" className="text-center">
                                                    <h5 className="c-default">Date of Birth</h5>
                                                </td>
                                                <td width="85" className="text-center">
                                                    <h5 className="c-default">Address</h5>
                                                </td>
                                            </tr>
                                            {centrixDatas}
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="col-lg-6 col-md-6">
                                        <div className="clearfix">
                                            <h4 className="title c-secondary pull-left">
                                                International Watchlist PEP Status
                                            </h4>
                                            <img
                                                className="pull-right title-status"
                                                src={
                                                    pep_watchlist.is_success
                                                        ? a_success
                                                        : a_warning
                                                }
                                            />
                                        </div>
                                        <div className="clearfix">
                                            <h4 className="title c-secondary pull-left">
                                                {(id_type === 'passport') ?  'Passport' : 'Driver Licence Verified'}
                                            </h4>
                                            {id_type !== 'passport' && <img
                                                className="pull-right title-status"
                                                src={
                                                    driver_licence.is_verified
                                                        ? a_success
                                                        : a_warning
                                                }
                                            />}
                                            {id_type === 'passport' && <img
                                                className="pull-right title-status"
                                                src={
                                                    passport.is_verified
                                                        ? a_success
                                                        : a_warning
                                                }
                                            />}
                                        </div>

                                        <div className="text-status">
                                            <h4>Summary Detail</h4>
                                            <div className="data c-default">
                                                {driver_licence.is_verified_and_matched
                                                    ? 'Match'
                                                    : 'No Match'}
                                            </div>
                                        </div>
                                        {/*<div className="text-status">
                                        <h4>Driver Licence Number and Version</h4>
                                        <img
                                            src={
                                                driver_licence.NumberAndVersion
                                                    ? a_success
                                                    : a_warning
                                            }
                                        />
                                    </div>*/}
                                        <div className="text-status">
                                            <h4>Surname</h4>
                                            <img
                                                src={
                                                    driver_licence.is_lastname_matched
                                                        ? a_success
                                                        : a_warning
                                                }
                                            />
                                        </div>
                                        <div className="text-status">
                                            <h4>First Name</h4>
                                            <img
                                                src={
                                                    driver_licence.is_firstname_matched
                                                        ? a_success
                                                        : a_warning
                                                }
                                            />
                                        </div>
                                        <div className="text-status">
                                            <h4>Middle Name</h4>
                                            <img
                                                src={
                                                    driver_licence.is_middlename_matched
                                                        ? a_success
                                                        : a_warning
                                                }
                                            />
                                        </div>
                                        <div className="text-status">
                                            <h4>Date of Birth</h4>
                                            <img
                                                src={
                                                    driver_licence.is_dateofbirth_matched
                                                        ? a_success
                                                        : a_warning
                                                }
                                            />
                                        </div>
                                    </div>
                                </Row>
                            </Container>
                        </TabPane>}
                    </TabContent>
                </Container>
                <Asf asfItems={asf_check.result} onClose={() => this.setState({showAsf: false})}
                     show={this.state.showAsf}/>
                {this.renderLightBox()}
                {this.renderAssessmentExplanation()}
            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps, null)(TransactionSingle));

function mapStateToProps({user}) {
    return { session: user.session, user: user.info };
}