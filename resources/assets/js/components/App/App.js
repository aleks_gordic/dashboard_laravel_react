import React, { Component } from 'react';
import { connect } from 'react-redux';
import IdleTimer from 'react-idle-timer'
import PropTypes from 'prop-types';
import { ToastContainer } from 'react-toastify';
import UserAction from '@js/store/actions/user';
import Header from '@containers/Header';
import {withRouter} from "react-router-dom";
import axios from "axios";

class App extends Component {

	constructor(props) {
		super(props);
		this.idleTimer = null;
	}

	onIdle = (e) => {
		//console.log("user is idle", e);
		//console.log("last active", this.idleTimer.getLastActiveTime());
		let headers = {
			//'Authorization': ''
		}
		const {match = {}} = this.props;
		const { params = {} }= match;
		const { module = '', workflow = '' } = params
		console.log(module, workflow)

		axios.post('/portal/logout',{headers}).then(res => {
			this.props.setCurrentUser(null);
			localStorage.setItem('logout-event', 'logout' + Math.random());
			//this.props.history.push('/portal/login')
			window.location.href = '/portal/login'
		});
	}

	render(){

		const {style: customStyle,staticContext, setCurrentUser, children, ...others}  = this.props

		return (
			<div style={{...customStyle}} {...others}>
				<Header/>
				{children}
				<ToastContainer
					position="top-right"
					autoClose={2000}
					hideProgressBar
					newestOnTop
					closeOnClick
					rtl={false}
					pauseOnVisibilityChange
					draggable
					pauseOnHover
				/>
				<IdleTimer
					ref={ref => {
						this.idleTimer = ref;
					}}
					element={document}
					onIdle={this.onIdle}
					events={['click']}
					timeout={1000 * 60*15}
				/>
			</div>
		);
	}

}

App.propTypes = {
	children: PropTypes.node.isRequired,
	style: PropTypes.object,
};

App.defaultProps = {
	style: {},
};

export default withRouter(
	connect(
		null,
		mapDispatchToProps
	)(App)
);


function mapDispatchToProps(dispatch) {
	return {
		setCurrentUser: data => dispatch(UserAction.setCurrentUser(data))
	};
}

