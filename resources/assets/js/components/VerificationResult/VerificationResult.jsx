import React from 'react';
import {
    Button,
    Row,
} from 'reactstrap';
import {connect} from "react-redux";
import {withRouter} from 'react-router-dom';
import Trsansaction from '@services/Transaction'
import TransactionSingle from '@components/TransactionSingle';

import './VerificationResult.style.scss';

class VerificationResult extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.setInitialState();
    }

    setInitialState = () => {
        return {
            tId: ''
        }
    }

    componentWillMount() {

        this.setState({
            tId: this.props.location.hash.replace('#','')
        });

    }
    //--------
    redoCheck = ()=> {

        let params = {
            id: this.state.tId,
            action:"redo"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }
        Trsansaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.onActionTaken('redo');
                    success('Details submitted successfully');
                }else{
                    error('Error in updating guided');
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    cancel = ()=> {

        let params = {
            id: this.state.tId,
            action:"cancel"
        }

        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        Trsansaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.onActionTaken('cancel');
                    success('Transaction cancelled successfully');
                }else{
                    error('Error in updating guided');
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }

    submit = () => {
        let params = {
            id: this.state.tId,
            action:"submit"
        }
        let headers = {
            'Authorization': 'Bearer ' + this.props.session
        }

        Trsansaction.update(params, headers)
            .then(({status}) => {
                if (status === 'success') {
                    this.props.onActionTaken('submit');
                    success('Details submitted successfully');
                }else{
                    error('Error in updating guided');
                }
            })
            .catch(err => {
                console.warn(err);
            });
    }
    //--------

    render() {


        return (
            <TransactionSingle guided={true} tId={this.state.tId}>
                <Row>
                    <div className="btn-container">
                        <div className="btn-container-inner">
                            <Button onClick={this.cancel} outline color="primary">Cancel</Button>
                            <Button onClick={this.redoCheck} outline color="primary">Redo Check</Button>
                            <Button onClick={this.submit} color="primary">Submit</Button>
                        </div>
                    </div>
                </Row>
            </TransactionSingle>
        );
    }
}

export default withRouter(connect(mapStateToProps, null)(VerificationResult));
function mapStateToProps({user}) {
    return {session: user.session};
}
