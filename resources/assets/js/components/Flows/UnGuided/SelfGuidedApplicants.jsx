import React, {Component} from 'react';
import {Input, Button, Container} from 'reactstrap';
import {ASSETS_URL} from '@js/config';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom'
import ApplicantItem from '@components/Flows/ApplicantItem'
import RequestSent from '@components/Modals/RequestSent';
import {success, error} from '@js/lib/Tostify'
import Invitation from '@services/Invitation'

import './SelfGuidedApplicants.style.scss';


/**
 * Retail - Identify flow self guided
 */
class SelfGuidedApplicants extends Component {

    constructor(props) {
        super(props);
        this.state = this.setInitialState();
    }

    setInitialState = () => {
        return {
            reference: '',
            module: '',
            workflow: '',
            number: 1,
            applicants: [],
            clearAll: "no",
            showAdditional: false,
            errors: {},
            message_template: 'RETAIL'
        }
    }

    handleChange = (e) => {

        let field = $(e.target).attr('name');

        //----------------------
        if (field === 'number') {
            let applicants = [...Array.from(Array(parseInt(e.target.value)).keys())].map((num, index) => {
                return {
                    email: "",
                    full_name: "",
                    medium: index === 0 ? 'sms' : 'email',
                    mobile: ""
                }
            });

            this.setState({
                applicants
            })
        } else if (field === 'reference') {
            let reg = /^[ A-Za-z0-9_@./#&+-]*$/
            let ef = $(e.target)
            let value = e.target.value;
            if (reg.test(value) === false && value.length > 0) {
                ef.addClass('border-danger').removeClass('border-success')

                let errors = {...this.state.errors}
                errors['reference'] = ''
                this.setState({
                    errors
                })

            } else {
                let errors = {...this.state.errors}
                delete errors['reference']
                this.setState({
                    errors
                })
                ef.addClass('border-success').removeClass('border-danger')
            }
        }
        //----------------------


        this.setState({
            [field]: e.target.value
        })

    }

    componentDidMount() {

        const {match = {}} = this.props
        const {params = {}} = match

        let module = params.module,
            workflow = params.workflow;

        this.setState({
            module,
            workflow
        })
    }

    handleClose = () => {
        let initState = {...this.setInitialState()}
        initState.clearAll = 'yes'
        this.setState(initState);
    }

    renderAdditionalApp = () => {
        return <RequestSent show={this.state.showAdditional} additional={this.state} onClose={this.handleClose}/>
    }

    checkValidation = (param) => {
        const {errors} = param
        let app_errors = []

        if (Object.keys(errors).length > 0) {
            app_errors.push('reference');
        }

        let applicants = param.applicants
        for (let i = 0; i < applicants.length; ++i) {
            let applicant = applicants[i]
            if (applicant.medium === 'sms') {
                let regex = /^[\d{3,4}-]+$/;
                if (!applicant.mobile || (applicant.mobile.match(regex) && applicant.mobile.length < 8)) {
                    app_errors.push('mobile_' + (i + 1));
                }
            } else {
                let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if (!applicant.email || reg.test(applicant.email) === false) {
                    app_errors.push('email_' + (i + 1));
                }
            }

            //--------
            let reg = /^([A-Z '-]+)$/i;
            if (applicant.full_name && reg.test(applicant.full_name) === false) {
                app_errors.push('full_name_' + (i + 1));
            }

        }

        console.log(app_errors)

        return app_errors.length > 0
    }

    formatInputs = (inputs) => {

        let param = {...inputs}

        delete param.errors;

        param.applicants = param.applicants.map(app => {

            let temp = {...app}

            delete temp.error
            delete temp.country_code_select
            delete temp.formatted_mobile

            if (temp.medium === 'sms') {

                delete temp.email

            } else if (temp.medium === 'email') {

                delete temp.country_code
                delete temp.mobile
            }

            return temp;
        })

        return param;
    }

    sendInvite = (inputs) => {

        if (this.checkValidation(inputs)) {
            error('One or more field are invalid.');
            return
        }

        const {session} = this.props
        let param = this.formatInputs(inputs)

        console.log(param);
        let headers = {
            'Authorization': 'Bearer ' + session
        }

        Invitation.send(param, headers)
            .then((response) => {

                const {status, data, message} = response;

                if (status === 'success') {
                    this.setState({
                        showAdditional: true
                    })
                    success('Invitation sent successfully');

                } else {
                    error('Error in sending invitation')
                }

                console.log('response', response);
            })
            .catch(err => {
                error('Error in sending invitation')
                console.warn(err);
            });
    }

    handleUpdate = (n, data) => {
        n = parseInt(n)
        let applicants = [...this.state.applicants]

        applicants[n - 1] = data;

        this.setState({
            applicants
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        // call ajax to send SMS and Email
        let completeData = {...this.state}
        delete completeData.clearAll
        delete completeData.showAdditional
        this.sendInvite(completeData);
        //console.log('applicants',completeData)
    }

    cancel = () => {
        this.props.history.push(`/portal`);
    }

    render() {

        const { MIX_ENABLE_EMAIL, MIX_REFERENCE_TEXT, MIX_MULTIPLE_APPLICANTS, MIX_MESSAGE_TEMPLATE } = process.env
        const showMultiple = MIX_MULTIPLE_APPLICANTS === 'true'
        const showMessageTemplate = MIX_MESSAGE_TEMPLATE === 'true'

        let number = this.state.number || 1;
        number = parseInt(number);

        let ApplicantItems = [...Array.from(Array(number).keys())].map((num, index) => {

            /*let showEmail = false;
            if (MIX_ENABLE_EMAIL === true) {
                if (index > 0) {
                    showEmail = true
                }
            }
            let medium = showEmail ? 'email' : 'sms'*/

            return <ApplicantItem
                clearAll={this.state.clearAll}
                key={index}
                flow="self_guided"
                n={(num + 1).toString()}
                medium="sms"

                onUpdate={this.handleUpdate}/>
        })

        return (
            <Container className="SelfGuidedApplicants-body">
                { showMultiple && <h4 className="description">Number of Verifications</h4>}

                { showMultiple && <div className="radio-group no-margin small mt-4">
                    <input
                        type="radio"
                        id="number1"
                        name="number"
                        onChange={this.handleChange}
                        checked={parseInt(this.state['number']) === 1}
                        value={1}/><label htmlFor="number1">1</label>
                    <input
                        type="radio"
                        id="number2"
                        name="number"
                        onChange={this.handleChange}
                        checked={parseInt(this.state['number']) === 2}
                        value={2}/><label htmlFor="number2">2</label>
                    <input
                        type="radio"
                        id="number3"
                        name="number"
                        onChange={this.handleChange}
                        checked={parseInt(this.state['number']) === 3}
                        value={3}/><label htmlFor="number3">3</label>
                    <input
                        type="radio"
                        id="number4"
                        name="number"
                        onChange={this.handleChange}
                        checked={parseInt(this.state['number']) === 4}
                        value={4}/><label htmlFor="number4">4</label>
                    <input
                        type="radio"
                        id="number5"
                        name="number"
                        onChange={this.handleChange}
                        checked={parseInt(this.state['number']) === 5}
                        value={5}/><label htmlFor="number5">5</label>
                    <input
                        type="radio"
                        id="number6"
                        name="number"
                        onChange={this.handleChange}
                        checked={parseInt(this.state['number']) === 6}
                        value={6}/><label htmlFor="number6">6</label>
                    <input
                        type="radio"
                        id="number7"
                        name="number"
                        onChange={this.handleChange}
                        checked={parseInt(this.state['number']) === 7}
                        value={7}/><label htmlFor="number7">7</label>
                </div>}

                <h4 className="description mb-4">Verification Details</h4>

                {ApplicantItems}

                <h4 className="description margin-reference">{MIX_REFERENCE_TEXT || 'Reference'}</h4>
                <div className="reference-inputClass">
                    <Input name="reference" placeholder={MIX_REFERENCE_TEXT || 'Reference'} value={this.state.reference} type="text" onChange={this.handleChange}
                           className="reference-input"/>
                </div>

                { showMessageTemplate && <h4 className="description identifyapplicants-margintop">Message Template</h4>}
                { showMessageTemplate && <div className="radio-group no-margin large mt-4">
                    <input
                        type="radio"
                        id="retail"
                        name="message_template"
                        onChange={this.handleChange}
                        checked={this.state['message_template'] === "RETAIL"}
                        value="RETAIL"/><label htmlFor="retail">Retail</label>
                    <input
                        type="radio"
                        id="commercial"
                        name="message_template"
                        onChange={this.handleChange}
                        checked={this.state['message_template'] === "COMMERCIAL"}
                        value="COMMERCIAL"/><label htmlFor="commercial">Commercial</label>
                    <input
                        type="radio"
                        id="broker"
                        name="message_template"
                        onChange={this.handleChange}
                        checked={this.state['message_template'] === "BROKER"}
                        value="BROKER"/><label htmlFor="broker">Broker</label>
                </div>}

                <div className="button-groups">
                    <div className="first">
                        <Button onClick={this.cancel} className="cancel-button">Cancel</Button>
                    </div>
                    <div onClick={this.handleSubmit} className="last">
                        <Button className="next-button sendApplicants-width">Send to applicant</Button>
                    </div>
                </div>
                {this.renderAdditionalApp()}
            </Container>
        );
    };

}


export default withRouter(connect(mapStateToProps, null)(SelfGuidedApplicants));

function mapStateToProps({user}) {
    return {session: user.session};
}

