import React, {Component} from 'react';
import { Input } from 'reactstrap'
import {ASSETS_URL} from '@js/config';
import './ApplicantItem.style.scss'

import countries from '@js/lib/countries.json'
import Select, {components} from 'react-select';

const SingleValue = ({children, ...props}) => {
    return (
        <components.SingleValue {...props}>
            {props.data.dial_code}
        </components.SingleValue>
    )

};

const Menu = (props) => {
    return (
        <components.Menu {...props}>
            <div className="select-options">
                {props.children}
            </div>
        </components.Menu>
    );
};
const Inputs = (props) => {
    return (
        <components.Input {...props} className="select-input">

        </components.Input>
    );
};
const Option = (props) => {
    return (
        <components.Option {...props} className="option">
            {props.children}
        </components.Option>
    );
};


class ApplicantItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            /*full_name:"",
            email:"",
            flow_type:"",
            country_code:"+61",
            mobile:"",
            medium:"sms"
            country_code_select:{value: 'one', label: 'One'}*/
            error:false
        };
    }

    setInitialState = (props) => {

        const {n, medium = 'sms', flow} = props;

        let flow_type = '';
        if (n === '1' && flow === 'guided') {
            flow_type = 'guided';
        } else {
            flow_type = 'self_guided';
        }

        return {
            ["country_code_select" + n]: {"name": "Australia", "dial_code": "+61", "code": "AU"},
            ["country_code" + n]: '+61',
            ["medium" + n]: medium,
            ["email" + n]: '',
            ["mobile" + n]: '',
            ["formatted_mobile" + n]: '',
            ["full_name" + n]: '',
            ["flow_type" + n]: flow_type,
            ["input_classes" + n]: {
                full_name: '',
                email: '',
                formatted_mobile: '',
            }
        }
    }

    handleChangeSelectBox = (value) => {

        const {n} = this.props;
        this.setState({
            ['country_code_select' + n]: value,
            ["country_code" + n]:value.dial_code
        })

        let state = {...this.state}
        state["country_code" + n] = value.dial_code;
        delete state['country_code_select' + n];

        let newState = {}
        Object.keys(state).forEach((key) => {
            let f = key.replace(n,'');
            newState[f] = state[key]
        })

        this.props.onUpdate(this.props.n,newState)
    }

    //--------------------------

    replaceAll =( str, find, replace)=> {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    formatPhoneText = (value) => {
        value = this.replaceAll(value.trim(),"-","");

        if(value.length > 3 && value.length <= 6)
            value = value.slice(0,3) + "-" + value.slice(3);
        else if(value.length > 6)
            value = value.slice(0,3) + "-" + value.slice(3,6) + "-" + value.slice(6);

        return value;
    }
    //--------------------------
    handleChange = async (e) => {

        const {n} = this.props;
        let field = $(e.target).attr('name');
        let value = e.target.value;

        if (field === 'full_name' + n) {
            let reg = /^([a-zA-Z '-]+)$/i;

            let cls = ''
            if (value && reg.test(value) === false) {
                cls = 'border-danger';
            } else {
                cls = 'border-success';
            }

            let inputClasses = {...this.state["input_classes" + n]}
            inputClasses['full_name'] = cls
            this.setState({
                ["input_classes" + n]: inputClasses
            })
        }

        if (field === 'email' + n) {
            //console.log('email')
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            let cls = ''
            if (reg.test(value) === false) {
                cls = 'border-danger';
            } else {
                cls = 'border-success';
            }

            let inputClasses = {...this.state["input_classes" + n]}
            inputClasses['email'] = cls
            this.setState({
                ["input_classes" + n]: inputClasses
            })
        }

        if(field === 'formatted_mobile' + n){
            let ef = $(e.target)
            let regex=/^[\d{3,4}-]+$/;

            let cls = ''
            if (value.match(regex) && value.length <= 13)
            {

                if (value.length < 10) {
                    cls = 'border-danger';
                } else {
                    cls = 'border-success';
                }
                let inputClasses = {...this.state["input_classes" + n]}
                inputClasses['formatted_mobile'] = cls

                value = this.formatPhoneText(value)
                let mob_field = 'mobile'+n

                await this.setState({
                    [field]:value,
                    [mob_field]:value.replace(/-/g, ''),
                    ["input_classes" + n]: inputClasses
                })

            }else if(value.length === 0){
                await this.setState({
                    [field]:''
                })
            }
        }else{
            //this.props.onFieldError(false)
            await this.setState({
                [field]: value
            })
        }

        let state = {...this.state}

        let newState = {}
        Object.keys(state).forEach((key) => {
            let f = key.replace(n, '');
            newState[f] = state[key]
        })

        this.props.onUpdate(n, newState)
    }

    componentWillMount() {
        const {n, medium = 'sms', flow} = this.props;

        let flow_type = '';
        if (n === '1' && flow === 'guided') {
            flow_type = 'guided';
        } else {
            flow_type = 'self_guided';
        }

        this.setState({
            ["country_code_select" + n]: {"name": "Australia", "dial_code": "+61", "code": "AU"},
            ["country_code" + n]: '+61',
            ["medium" + n]: medium,
            ["email" + n]: '',
            ["mobile" + n]: '',
            ["formatted_mobile" + n]: '',
            ["full_name" + n]: '',
            ["flow_type" + n]: flow_type,
            ["input_classes" + n]: {
                full_name: '',
                email: '',
                formatted_mobile: '',
            },
        })

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.clearAll !== this.props.clearAll && nextProps.clearAll === 'yes') {
            let initState = {...this.setInitialState(nextProps)}
            this.setState(initState)
        }
    }

    /**
     * Render the component's markup
     * @return {ReactElement}
     */
    render() {
        const { MIX_PRIMARY, MIX_SHOW_NAME, MIX_ENABLE_EMAIL, MIX_ENABLE_COUNTRY_SELECT } = process.env
        const { showEmail = true } = this.props
        const showName = MIX_SHOW_NAME === "true"
        const enableEmail = MIX_ENABLE_EMAIL === "true" && showEmail
        const enableCountrySelect = !(MIX_ENABLE_COUNTRY_SELECT === "true")

        const {n} = this.props;

        return (
            <div className="applicants-detail">
                {showName && <label className="applicant-label">{n}</label>}
                {showName && <Input
                    onChange={this.handleChange}
                    value={this.state['full_name' + n]}
                    type="text" name={"full_name" + n}
                    id={"Applicant" + n}
                    placeholder="Name"
                    className={`applicant-input ${this.state['input_classes' + n]['full_name']}`}/>}

                {this.state['medium' + n] === 'sms' &&
                <div className=" ">
                    <Select
                        isDisabled={enableCountrySelect}
                        options={countries}
                        onChange={this.handleChangeSelectBox}
                        components={{SingleValue, Menu, Input: Inputs, Option}}
                        value={this.state['country_code_select' + n]}
                        theme={(theme) => ({
                            ...theme,
                            borderRadius: '4px',
                            colors: {
                                ...theme.colors,
                                primary: MIX_PRIMARY,
                            },
                        })}
                        getOptionLabel={option => option.name}
                        getOptionValue={option => option.dial_code}
                        name={"country_code" + n}
                        className="country_code"/>
                </div>}
                {this.state['medium' + n] === 'sms' &&
                <Input onPaste={(e)=>{e.persist(), setTimeout(()=>{ this.handleChange(e)},4)}}
                       name={"formatted_mobile" + n}
                       onChange={this.handleChange}
                       value={this.state["formatted_mobile" + n]}
                       id={"formatted_mobile" + n}
                       placeholder="Mobile"
                       type="text"
                       className={`mobile-input ${this.state['input_classes' + n]['formatted_mobile']}`}/>}

                { enableEmail && this.state['medium' + n] === 'email' &&
                <Input onPaste={(e)=>{e.persist(), setTimeout(()=>{ this.handleChange(e)},4)}}
                       name={"email" + n}
                       onChange={this.handleChange}
                       value={this.state["email" + n]}
                       id={"email" + n}
                       placeholder="Email" type="text"
                       className={`email-input equal-to-mobile ${this.state['input_classes' + n]['email']}`}/>}

                <div className="radio-group">
                    <input
                        type="radio"
                        id={"sms" + n}
                        name={"medium" + n}
                        onChange={this.handleChange}
                        checked={this.state['medium' + n] === "sms"}
                        value="sms"/><label htmlFor={"sms" + n}>SMS</label>

                    { enableEmail && <input
                        type="radio"
                        id={"email" + n}
                        name={"medium" + n}
                        onChange={this.handleChange}
                        checked={this.state['medium' + n] === "email"}
                        value="email"/>}
                    { enableEmail &&  <label htmlFor={"email" + n}>eMail</label>}
                </div>

            </div>
        );
    }
}

export default ApplicantItem;

