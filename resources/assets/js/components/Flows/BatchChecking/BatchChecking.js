import React, {Component} from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Input, Button} from 'reactstrap';
import  './BatchChecking.style.scss';
import BatchMenu from './BatchMenu';
import BatchDescription from './BatchDescription';
import BatchSmsProcess from './BatchSmsProcess';
import BatchEmailProcess from './BatchEmailProcess';
import BatchReview from './BatchReview';
import BatchReviewDetail from './BatchReviewDetail';

/**
 * Batch Checking!
 */
class BatchChecking extends Component {

    constructor(props) {
        super(props);
        this.steps = {
            description: 'description',
            process: 'process',
            review: 'review',
            review_detail: 'review_detail'
        };
        this.state = this.getInitialState();
    }

    getInitialState = () => {
        return {
            batchName: '',
            batchInfo: {},
            step: this.steps.description,
            isSms: true,
            isBatchReviewed: false,
            batchId: 0
        }
    }

    updateBatchName(isSms, batchName, batchId){
        this.setState({isSms: isSms, batchName: batchName, batchId: batchId, step: this.steps.process, isBatchReviewed: false})
    }

    cancelProcess() {
        this.setState({step: this.steps.description, isBatchReviewed: false})
    }

    viewBatchInDetail(batch) {
        console.log(batch);
        this.setState({batchInfo: batch, step: this.steps.review_detail, isBatchReviewed: true})
    }

    goReviewBatch() {
        this.setState({step: this.steps.review, isBatchReviewed: true})
    }

    render(){
        let component;
        if (this.state.step === this.steps.description) 
        {
            component = <BatchDescription callBack={this.updateBatchName.bind(this)}/>;
        }

        else if (this.state.step === this.steps.process) {
            if (this.state.isSms) 
            {
                component = <BatchSmsProcess batchName={this.state.batchName} batchId={this.state.batchId}
                    cancelCallBack={this.cancelProcess.bind(this)} 
                    sendBatchCallBack={this.goReviewBatch.bind(this)}/>;
            } 
            else {
                component = <BatchEmailProcess batchName={this.state.batchName} batchId={this.state.batchId}
                    cancelCallBack={this.cancelProcess.bind(this)} 
                    sendBatchCallBack={this.goReviewBatch.bind(this)}/>;
            }
        }

        else if (this.state.step === this.steps.review) {
            component = <BatchReview viewBatchCallBack={this.viewBatchInDetail.bind(this)}/>;
        } 

        else if (this.state.step === this.steps.review_detail) {
            component = <BatchReviewDetail batchInfo={this.state.batchInfo}/>;
        }

        return (
            <div className="container">
                <div className="batchchecking-body">
                    <BatchMenu isBatchReviewed={this.state.isBatchReviewed}
                                cancelCallBack={this.cancelProcess.bind(this)} 
                                goReviewCallBack={this.goReviewBatch.bind(this)}/>
                    <div className="vl"></div>
                    <div className="description-body">
                        {component}
                    </div>
                </div>
            </div>
            
            );
    }
}

export default withRouter(connect(mapStateToProps, null)(BatchChecking));

function mapStateToProps({user}) {
    return {session: user.session};
}