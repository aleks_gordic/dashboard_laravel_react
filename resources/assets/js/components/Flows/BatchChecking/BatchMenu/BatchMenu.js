import React, {Component} from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Input, Button} from 'reactstrap';
import  './BatchMenu.style.scss';

/**
 * Batch Menu!
 */
class BatchMenu extends Component {

    constructor(props) {
        super(props);
        this.state = this.getInitialState();
    }

    getInitialState = () => {
        return {
            isBatchReviewed: this.props.isBatchReviewed,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({isBatchReviewed: nextProps.isBatchReviewed})
    }

    changeToProcess() {
        this.setState({isBatchReviewed: false})
        this.props.cancelCallBack();
    }

    changeToReview() {
        this.setState({isBatchReviewed: true})
        this.props.goReviewCallBack();
    }

    render(){
        let processLine = this.state.isBatchReviewed ? "menu-line hidden" : "menu-line";
        let processItem = this.state.isBatchReviewed ? "menu-item gray" : "menu-item";
        let reviewLine = this.state.isBatchReviewed ? "menu-line" : "menu-line hidden";
        let reviewItem = this.state.isBatchReviewed ? "menu-item" : "menu-item gray";
        console.log(this.state.isBatchReviewed);
        console.log(reviewItem);

        return (
                <div className="menu-body">
                    <div className="display-flex" onClick={this.changeToProcess.bind(this)}>
                        <div className={processLine}></div>
                        <h4 className={processItem}>Create Batch Checks</h4>
                    </div>
                    <div className="display-flex" onClick={this.changeToReview.bind(this)}>
                        <div className={reviewLine}></div>
                        <h4 className={reviewItem}>Review Batch Checks</h4>
                    </div>
                </div>
            );
        }
}

export default withRouter(connect(mapStateToProps, null)(BatchMenu));

function mapStateToProps({user}) {
    return {session: user.session};
}