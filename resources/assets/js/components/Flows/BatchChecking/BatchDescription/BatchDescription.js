import React, {Component} from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Input, Button} from 'reactstrap';
import Dropzone from 'react-dropzone'
import {ASSETS_URL, API_URL} from '@js/config';
import BatchCheck from '@services/BatchCheck'
import {success, error} from '@js/lib/Tostify'
import  './BatchDescription.style.scss';

/**
 * Batch Description!
 */
class BatchDescription extends Component {

    constructor(props) {
        super(props);
        this.state = this.getInitialState();
    }

    getInitialState = () => {
        return {
            batchName: '',
            isSms: true,
            isFileUPloaded: false,
            uploadedFilePath: ''
        }
    }

    selectEmail() {
        this.setState({isSms : false})
    }

    selectSms() {
        this.setState({isSms : true})
    }

    reset() {

    }

    download() {
        let headers = {
            'Content-Type': 'application/vnd.ms-excel',
            'Authorization': 'Bearer ' + this.props.session,
            
        }
        BatchCheck.download(this.state.isSms, headers)
        .then((data) => {
            const downloadUrl = window.URL.createObjectURL(new Blob([data]));
            const link = document.createElement('a');
            link.href = downloadUrl;
            
            if (this.state.isSms) {
                link.setAttribute('download', 'SmsTemplate.xlsx'); //any other extension
            } else {
                link.setAttribute('download', 'EmailTemplate.xlsx'); //any other extension
            }
            
            document.body.appendChild(link);
            link.click();
            link.remove();
        })
        .catch(err => {
            console.warn(err);
        });
    }

    process() {
        if (this.state.batchName == '') {
            error("Please input the batch name!");
        } 
        else if (!this.state.isFileUPloaded) {
            error("Please upload the batch file!");
        } else {
            let params = {
                filePath: this.state.uploadedFilePath,
                isSms: this.state.isSms,
                batchName: this.state.batchName
            }
    
            let headers = {
                'Authorization': 'Bearer ' + this.props.session
            }

            BatchCheck.processBatchFile(params, headers)
            .then((res) => {
            	if (res['status'] == 'success') {
	                console.log(res['data']['batchId']);
	                this.props.callBack(this.state.isSms, this.state.batchName, res['data']['batchId']);
            	} else {
            		console.log(res['message']);
            	}
            })
            .catch(err => {
                console.warn(err);
            });
        }
    }

    handleChange(e) {
        this.setState({ batchName : e.target.value })
    }

    onDrop(accepted, rejected) {
        if (Object.keys(rejected).length !== 0) {
            error("Please submit valid file type");
        } else {
            
          console.log(accepted[0].preview);
          let headers = {
            'Authorization': 'Bearer ' + this.props.session,
            'Content-Type': 'multipart/form-data'
        }

          BatchCheck.upload(accepted[0], headers).then((data) => {
                console.log(data);
                this.setState({isFileUPloaded : true, uploadedFilePath: data})
            })
            .catch(err => {
                console.warn(err);
            });

        //   var blobPromise = new Promise((resolve, reject) => {
        //     const reader = new window.FileReader();
        //     reader.readAsDataURL(accepted[0]);
        //     reader.onloadend = () => {
        //       const base64data = reader.result;
        //       resolve(base64data);
        //     };
        //   });

        //   blobPromise.then(value => {
        //     // console.log(value);
            
        //   });
        }
      };

    render(){
        let sms_button = this.state.isSms ? "option-button selected-blue" : "option-button deselected-gray";
        let email_button = this.state.isSms ? "option-button deselected-gray" : "option-button selected-blue";

        return (
            <div className="batch-description-body">
                <h4 className="description-item">Batch check allows you to send several identity verification checks at one time.</h4>
                <div className="display-flex margin-bottom margin-top">
                    <div>
                        <h4 className="description-step">1. Method</h4>
                        <h4 className="description-item margin-left">Send link to individual via:</h4>
                    </div>
                    <div className="display-flex position-right">
                        <div className="batchType">
                            <Button className={sms_button} onClick={this.selectSms.bind(this)}>SMS</Button>
                        </div>
                        <div className="batchType">
                            <Button className={email_button} onClick={this.selectEmail.bind(this)}>eMail</Button>
                        </div>
                        
                    </div>
                </div>

                <div className="display-flex margin-bottom">
                    <div>
                        <h4 className="description-step">2. Download template</h4>
                        <h4 className="description-item margin-left">Download this excel template and fill in the details.</h4>
                    </div>
                    <div className="display-flex position-right">
                        <Button onClick={this.download.bind(this)} className="download-button">
                            Download  <img className="right" src={`${ASSETS_URL}/icons/move-layer-down.png`}/></Button>
                    </div>
                </div>

                <div className="display-flex">
                    <div>
                        <h4 className="description-step">3. Upload filled in template</h4>
                        <h4 className="description-item margin-left">After filling in the template with the details, drag the file on the box below or click upload</h4>
                    </div>
                </div>

                <Dropzone 
                    onDrop={(accepted, rejected) => this.onDrop(accepted, rejected)}
                    multiple={false}
                    accept="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">


                    {({getRootProps, getInputProps, acceptedFiles}) => (       
                        <div className="dotted-border margin-bottom margin-left" {...getRootProps()}>
                        <input {...getInputProps()} />
                            <div className="center">
                                {this.state.isFileUPloaded ? 
                                    <div className="display-flex-center">
                                        <h4 className="file-fontsize" hide={(!this.state.isFileUPloaded).toString()}>
                                            {acceptedFiles.length > 0 && acceptedFiles[0].name}
                                        </h4>
                                    </div> :
                                    <div hide={(this.state.isFileUPloaded).toString()}>
                                        <div className="display-flex-center">
                                            <img src={`${ASSETS_URL}/icons/move-layer-up.png`}/>
                                        </div>
                                        <div className="display-flex-center">
                                            <h4 className="file-fontsize-bold">Choose a file</h4>
                                            <h4 className="file-fontsize">or drag it here</h4>
                                        </div>
                                    </div>    
                                }
                            </div>
                        </div>
                    )}
                </Dropzone>

                <div className="display-flex margin-bottom">
                    <div>
                        <h4 className="description-step">4. Batch Name</h4>
                        <h4 className="description-item margin-left">What reference or name should this batch be given:</h4>
                    </div>
                    <div className="position-right margin-right">
                        <input className="batch-input" value={this.state.batchName} placeholder="Batch Name" onChange={this.handleChange.bind(this)}></input>
                    </div>
                </div>

                <div className="button-groups">
                    <div className="first">
                        <Button onClick={this.reset.bind(this)} className="cancel-button">Reset</Button>
                    </div>
                    <div onClick={this.process.bind(this)} className="last">
                        <Button className="next-button">Process</Button>
                    </div>
                
                </div>
            </div>
            );
        }
}

export default withRouter(connect(mapStateToProps, null)(BatchDescription));

function mapStateToProps({user}) {
    return {session: user.session};
}
