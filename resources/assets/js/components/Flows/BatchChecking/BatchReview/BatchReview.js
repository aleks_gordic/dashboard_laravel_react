import React, {Component} from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Input, Button} from 'reactstrap';
import {Table, Container} from 'reactstrap';
import {ASSETS_URL, API_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'
import Pagination from '@components/Pagination'
import BatchCheck from '@services/BatchCheck'
import  './BatchReview.style.scss';

/**
 * Batch Review!
 */
class BatchReview extends Component {

    constructor(props) {
        super(props);
        this.state = this.getInitialState();
    }

    getInitialState = () => {
        return {
            batchName: this.props.batchName,
            query: {
                term: '',
            },
            batchList: []
        }
    }

    delete = (e, id) => {

    }

    viewBatch = (e, id) => {
        this.props.viewBatchCallBack(this.state.batchList.find(batch=>batch.id === id));
    }

    fetchBatches = () => {
        let headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.props.session
        }

        let params = {
            page: this.state.page || 1,
            filter: this.state.filter,
        }

        BatchCheck.getBatches( params, headers )
            .then((res) => {

                const { status, data} = res;

                if (status === 'success') {
                    console.log(res);
                    var batchList = data.map(function(batch) {
                        var transactions = batch['transactions'] || [];
                        var batchStatus = 'COMPLETED';
                        var compCount = 0;
                        var completion = 0;

                        for(var i = 0; i < transactions.length; i++) {
                            if (transactions[i]['status'] != "COMPLETED") {
                                batchStatus = "PENDING";
                            } else {
                                compCount = compCount + 1;
                            }
                        }

                        if (compCount == 0) {
                            completion = 0;
                        } else {
                            completion = compCount * 100 / transactions.length;
                        }
                        
                        var batchDetail = {
                            'id' : batch['id'],
                            'date' : batch['created_at'],
                            'name' : batch['name'],
                            'status' : batchStatus,
                            'method' : batch['method'] == 1 ? 'SMS' : 'EMAIL',
                            'records' : batch['records'], 
                            'completion' : completion + '%',
                            'transactions' : batch['transactions']
                        };
                        return batchDetail;
                    });

                    this.setState({
                        batchList: batchList,
                    });
                } else {
                    error('Error in sending invitation')
                }
                console.log(res);
            })
            .catch(err => {
                console.warn(err);
            });
    }

    handlePageChange = async (currentPage) => {

        await this.setState({
            page: currentPage || 1
        })

        this.fetchBatches()
    }

    render(){
        let batchRows = this.state.batchList.map((batch) => {
            return (
                <tr key={batch.id}>
                    <th scope="row">{batch.date}</th>
                    <td>{batch.name}</td>
                    <td>{batch.status}</td>
                    <td>{batch.method}</td>
                    <td>{batch.records}</td>
                    <td>{batch.completion}</td>
                    
                    <td>
                        <center>
                            <a href="#" onClick={(e) => this.viewBatch(e,batch.id)}> <img className="action-imgs"
                                                                                       style={{ width: '19px' }}
                                                                           src={`${ASSETS_URL}/icons/view.png`}/></a>
                        </center>
                    </td>
                    
                    <td>
                        <center>
                            <a href="#" onClick={(e) => this.delete(e,batch.id)}> <img className="action-imgs"
                                                                                       style={{ width: '19px' }}
                                                                           src={`${ASSETS_URL}/icons/trash.png`}/></a>
                        </center>
                    </td>
                </tr>
            )
        });

        let paginate = this.state.batchList || {}
        paginate = {current_page: paginate.current_page, last_page: paginate.last_page}

        return (
            <div className="batchreview-body">
                <div className="inputGroup">
                    <div className="quick-search">
                        <div className="searchPendingClass">
                            <span className="spanPendingSearch"><img className="searchimage"
                                                                        src={`${ASSETS_URL}/icons/search.png`}/></span>
                            <Input onChange={this.handleChange} type="text" value={this.state.query.term}
                                    className="searcPendinghInput" name="term" id="quicksearch"
                                    placeholder="Quick Search"/>
                        </div>
                    </div>

                    <div className="Status">
                        <div className="selectPendingClass select-container">
                            {/* <select onChange={this.handleChange} value={this.state.filter.status} name="status"
                                    className="selectPendingInput form-control"> */}
                                <select name="status"
                                    className="selectPendingInput form-control">
                                <option value="">All</option>
                                <option value="PENDING">Pending</option>
                                <option value="INPROGRESS">InProgress</option>
                                <option value="EXPIRED">Expired</option>
                                <option value="CANCELLED">Cancelled</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div className="table-part">
                    <Table className="custom-table">
                        <thead>
                        <tr>
                            <th className="th-date">Date</th>
                            <th className="th-name">Name</th>
                            <th className="th-status">Status</th>
                            <th className="th-method">Method</th>
                            <th className="th-records">Records</th>
                            <th className="th-completion">Completion</th>
                            <th className="th-view">View</th>
                            <th className="th-delete">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        {batchRows}
                        </tbody>
                    </Table>
                    <div className="float-right">
                        <Pagination onPageChange={this.handlePageChange} data={paginate}/>
                    </div>
                </div>
            </div>  
            );
        }
}

export default withRouter(connect(mapStateToProps, null)(BatchReview));

function mapStateToProps({user}) {
    return {session: user.session};
}