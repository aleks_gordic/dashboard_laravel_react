import React, {Component} from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom'
import {Input, Button} from 'reactstrap';
import {Table, Container} from 'reactstrap';
import {ASSETS_URL, API_URL} from '@js/config';
import {success, error} from '@js/lib/Tostify'
import { CSVLink, CSVDownload } from "react-csv";
import Pagination from '@components/Pagination'
import  './BatchReviewDetail.style.scss';

class BatchReviewDetail extends Component {

    constructor(props) {
        super(props);
        this.state = this.getInitialState();
    }

    getInitialState = () => {
        return {
            batchInfo: this.props.batchInfo,
            query: {
                term: '',
            },
            batchDetailList: []
            // batchDetailList: [{"id": "1", "date": "06/06/2019 08:18AM", "transaction_id": "217939579", "reference": 
            //"CUSTOMER22312", "status": "completed - Pass", "contact_details": "61474000888"}, 
            //                 {"id": "2", "date": "05/06/2019 08:18AM", "transaction_id": "217939579", "reference": "CUSTOMER22312", "status": "completed - Flagged", "contact_details": "61474000888"}]
        }
    }

    fetchBatchDetails = () => {
        var batchDetailList = this.state.batchInfo['transactions'].map(function(transaction) {
            var batch = {
                'id' : transaction['id'],
                'date' : transaction['created_at'],
                'transaction_id' : transaction['transactionId'],
                'reference' : transaction['reference'],
                'status' : transaction['status'],
                'contact_details' : transaction['region'] + transaction['contact'], 
            };
            return batch;
        });

        this.setState({batchDetailList: batchDetailList});
    }

    handlePageChange = async (currentPage) => {

        await this.setState({
            page: currentPage || 1
        })

        this.fetchBatchDetails()
    }

    render(){
        let batchDetailRows = this.state.batchDetailList.map((batchDetail) => {
            return (
                <tr key={batchDetail.id}>
                    <th scope="row">{batchDetail.date}</th>
                    <td>{batchDetail.transaction_id}</td>
                    <td>{batchDetail.reference}</td>
                    <td>{batchDetail.status}</td>
                    <td>{batchDetail.contact_details}</td>
                    
                    <td>
                        <center>
                            <a href="#" onClick={(e) => this.viewBatch(e,batchDetail.id)}> <img className="action-imgs"
                                                                                       style={{ width: '19px' }}
                                                                           src={`${ASSETS_URL}/icons/view.png`}/></a>
                        </center>
                    </td>
                    
                    <td>
                        <center>
                            <a href="#" onClick={(e) => this.delete(e,batchDetail.id)}> <img className="action-imgs"
                                                                                       style={{ width: '19px' }}
                                                                           src={`${ASSETS_URL}/icons/trash.png`}/></a>
                        </center>
                    </td>
                </tr>
            )
        });

        let paginate = this.state.batchDetailList || {}
        paginate = {current_page: paginate.current_page, last_page: paginate.last_page}

        let batchDetailList = this.state.batchDetailList;
        var csvData = [];
        for(var i = 0; i < batchDetailList.length; i++) {
            if (batchDetailList[i]['status'] != 'COMPLETED') {
                csvData.push(batchDetailList[i]);
            }
        }

        console.log(csvData);

        return (
            <div className="batchreviewdetail-body">

                <div className="batch-info-body">
                    <div className="batch-info column-date">
                        <h4 className="description-item bold">Date</h4>
                        <h4 className="description-item">{this.state.batchInfo.date}</h4>
                    </div>
                    <div className="batch-info column-name">
                        <h4 className="description-item bold">Name</h4>
                        <h4 className="description-item">{this.state.batchInfo.name}</h4>
                    </div>
                    <div className="batch-info column-status">
                        <h4 className="description-item bold">Status</h4>
                        <h4 className="description-item">{this.state.batchInfo.status}</h4>
                    </div>
                    <div className="batch-info column-method">
                        <h4 className="description-item bold">Method</h4>
                        <h4 className="description-item">{this.state.batchInfo.method}</h4>
                    </div>
                    <div className="batch-info column-records">
                        <h4 className="description-item bold">Records</h4>
                        <h4 className="description-item">{this.state.batchInfo.records}</h4>
                    </div>
                    <div className="batch-info column-completion">
                        <h4 className="description-item bold">Completion</h4>
                        <h4 className="description-item">{this.state.batchInfo.completion}</h4>
                    </div>
                </div>

                <div className="inputGroup">
                    <div className="quick-search">
                        <div className="searchPendingClass">
                            <span className="spanPendingSearch"><img className="searchimage"
                                                                        src={`${ASSETS_URL}/icons/search.png`}/></span>
                            <Input onChange={this.handleChange} type="text" value={this.state.query.term}
                                    className="searcPendinghInput" name="term" id="quicksearch"
                                    placeholder="Quick Search"/>
                        </div>
                    </div>

                    <div className="Status">
                        <div className="selectPendingClass select-container">
                            {/* <select onChange={this.handleChange} value={this.state.filter.status} name="status"
                                    className="selectPendingInput form-control"> */}
                                <select name="status"
                                    className="selectPendingInput form-control">
                                <option value="">All</option>
                                <option value="PENDING">Pending</option>
                                <option value="INPROGRESS">InProgress</option>
                                <option value="EXPIRED">Expired</option>
                                <option value="CANCELLED">Cancelled</option>
                            </select>
                        </div>
                    </div>

                    {/* <button className="export-incomplete">EXPORT INCOMPLETE</button> */}
                    {/* <div className="export-incomplete"> */}
                        <CSVLink className="export-incomplete" data={csvData}>EXPORT INCOMPLETE</CSVLink>
                    {/* </div> */}
                    
                </div>

                <div className="table-part">
                    <Table className="custom-table">
                        <thead>
                        <tr>
                            <th className="th-date">Date</th>
                            <th className="th-transactionid">Transaction ID</th>
                            <th className="th-reference">Reference</th>
                            <th className="th-status">Status</th>
                            <th className="th-contactdetails">Contact Details</th>
                            <th className="th-view">View</th>
                            <th className="th-resend">Resend</th>
                        </tr>
                        </thead>
                        <tbody>
                        {batchDetailRows}
                        </tbody>
                    </Table>
                    <div className="float-right">
                        <Pagination onPageChange={this.handlePageChange} data={paginate}/>
                    </div>
                </div>
            </div>  
            );
        }
}

export default withRouter(connect(mapStateToProps, null)(BatchReviewDetail));

function mapStateToProps({user}) {
    return {session: user.session};
}