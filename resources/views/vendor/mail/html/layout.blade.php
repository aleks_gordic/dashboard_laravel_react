<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }

        @font-face {
            font-family: 'Gilroy';
            src: url('{{asset('/static/fonts/Gilroy-SemiBold.otf')}}');
            font-weight: 500;
            font-style: normal;
        }

        .wrapper{
            font-family: "Gilroy", Helvetica, sans-serif;
        }

    </style>
    @php
    $color = env('MIX_HEADER_GRAY') ? '#fff' : env('MIX_PRIMARY');
    @endphp

    <table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table class="content" style="max-width: 993px;width: 90%;" width="100%" cellpadding="0" cellspacing="0">
                    {{ $header ?? '' }}
                    <!-- Email Body -->
                    <tr>
                        <td class="body" style="border: none; background: {{$color}}; height: 320px;text-align: center;padding-bottom: 2rem;" width="100%" cellpadding="0" cellspacing="0">

                            <div style="">
                                <a style="display: block;width: 150px;margin: 0 auto;padding-bottom: 30px;" href="{{\App\Helpers\Helper::getSiteUrl()}}">
                                    <img src="{{asset('/assets-portal/images/logo-cropped.png')}}" width="100%" alt="Logo">
                                </a>
                            </div>

                        </td>
                    </tr>
                    <tr style="background-color: #fcfcfc">
                        <td style="border: none;" class="body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">
                                <!-- Body content -->
                                <tr>
                                    <td class="content-cell" style="position: relative;top: -10rem;background-color: white;padding: 2rem;box-shadow: 0 2px 32px 0 rgba(0,0,0,0.07);border-radius: 7px;">
                                        {{ Illuminate\Mail\Markdown::parse($slot) }}

                                        {{ $subcopy ?? '' }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    {{ $footer ?? '' }}
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
