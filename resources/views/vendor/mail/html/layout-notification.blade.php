<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }

        @font-face {
            font-family: 'Gilroy';
            src: url('{{asset('/static/fonts/Gilroy-Regular.otf')}}');
            font-weight: 400;
            font-style: normal;
        }

        @font-face {
            font-family: 'Gilroy';
            src: url('{{asset('/static/fonts/Gilroy-SemiBold.otf')}}');
            font-weight: 500;
            font-style: normal;
        }

        @font-face {
            font-family: 'Gilroy';
            src: url('{{asset('/static/fonts/Gilroy-Bold.otf')}}');
            font-weight: 600;
            font-style: normal;
        }

        .wrapper{
            font-family: "Gilroy", Helvetica, sans-serif;
        }

        .flag-check{
            width: 200px;
            color: white;
            margin: 1rem;
            padding: 1rem;
            font-size: 1.2rem;
            font-weight: bold;
            font-family: 'Gilroy', Helvetica, sans-serif;
        }

        .flag-check img{
            width: 25px;
            float: right;
            margin-top: 2px;
        }

        .flag-check.success{
            background-color: #5b8f23;
        }

        .flag-check.failed{
            background-color: #FB9303;
        }

    </style>
    @php
    $color = env('MIX_HEADER_GRAY') ? '#fff' : env('MIX_PRIMARY');
    @endphp

    <table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table class="content" style="max-width: 993px;width: 90%;" width="100%" cellpadding="0" cellspacing="0">
                    {{ $header ?? '' }}
                    <!-- Email Body -->
                    <tr>
                        <td class="body" style="border: none; background: {{$color}}; border-bottom: 3px solid {{env('MIX_SECONDARY')}}" width="100%" cellpadding="0" cellspacing="0">

                            <div style="">
                                <a style="display: block;width: 150px;padding: 1rem;margin-left: 1rem;" href="{{\App\Helpers\Helper::getSiteUrl()}}">
                                    <img src="{{asset('/assets-portal/images/logo-cropped.png')}}" width="100%" alt="Logo">
                                </a>
                            </div>

                        </td>
                    </tr>
                    <tr style="background-color: #fff">
                        <td style="border: none;" class="body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="inner-body" style="width: 100%" cellpadding="0" cellspacing="0">
                                <!-- Body content -->
                                <tr>
                                    <td class="content-cell" style="background-color: white;padding: 2rem 2rem 0;">
                                        {{ Illuminate\Mail\Markdown::parse($slot) }}

                                        {{ $subcopy ?? '' }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="background-color: #fff">
                        <td style="padding: 0 2rem;">
                            <h1 style="color: {{env('MIX_PRIMARY')}};margin-top: 1rem;margin-bottom: 2rem; font-size: 1.2rem;">Access the portal from <a target="_blank" style="color: {{env('MIX_PRIMARY')}}" href="{{\App\Helpers\Helper::getSiteUrl().'/portal'}}">{{request()->getHttpHost().'/portal'}}</a></h1>
                            <div style="border-top: 2px solid {{env('MIX_SECONDARY')}}; padding: 2rem 2rem 2rem 0">
                                If you need any support please contact your administrator.
                            </div>
                        </td>
                    </tr>
                    {{ $footer ?? '' }}
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
