@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => \App\Helpers\Helper::getSiteUrl()]) @endcomponent
    @endslot
    {{-- Body --}}
    <div>
        <h1 style="text-align: center; font-size: 1.5rem; font-family: 'Gilroy', Helvetica, sans-serif; font-weight: 500;">Invitation for Identity Verification</h1>

        <p style="text-align: center; font-family: Helvetica, sans-serif; line-height: 2; color: #6D7278;">{!! $msg !!}</p>

        <br/>
        <div style="display: flex; align-items: center; justify-content: center;">
            <a class="btn btn-primary btn-lg" href="{{ $url }}" style="font-size: 20px; color: white; width: 300px;text-decoration: none; text-align:center; background: {{env('MIX_SECONDARY')}}; border: none; padding: 13px 20px; border-radius: 5px; margin: 0 auto;" role="button">Verify</a>
        </div>

        <p style="font-size: 14px;text-align: center;font-family: Helvetica, sans-serif;color: #6D7278;margin-top: 1rem;">
            If you need any support please contact your administrator.
        </p>
    </div>

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer') @endcomponent
    @endslot
@endcomponent
