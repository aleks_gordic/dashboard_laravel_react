@component('mail::layout-notification')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => \App\Helpers\Helper::getSiteUrl()]) @endcomponent
    @endslot
    {{-- Body --}}
    @php
    $effTitle  = 'NA';
    $effSubTitle = 'NA';

    if($type == 'cancelled'){

        $effTitle = 'Verification Cancelled';
        $effSubTitle = 'A customer has cancelled their transaction.';

    }elseif ($type == 'expired'){

        $effTitle = 'Verification Expired';
        $effSubTitle = 'A customer verification has expired.';

    }elseif ($type == 'transaction-pass'){

        $effTitle = 'Verification Completed - Pass';
        $effSubTitle = 'A customer verification has been completed.';

    }elseif ($type == 'transaction-failed'){

        $effTitle = 'Verification Completed - Flagged';
        $effSubTitle = 'A customer has failed the identity check.';

    }elseif ($type == 'transaction-failed-data'){

        $effTitle = 'Verification Completed - Data Flagged';
        $effSubTitle = 'A customer has failed the data check.';

    }

    $thHtml = '';
    $tdHtml = '';
    $count = count($data);
    $width = round(100/$count).'%';
    $i = 0;
    foreach ($data as $k => $v){
        if($i == $count -1){
            $tdHtml .= '<td style="padding: 1rem; width: '.$width.';" align="center">'.$v.'</td>';
        }else{
            $tdHtml .= '<td style="padding: 1rem; width: '.$width.'; border-right: 1px solid #979797;" align="center">'.$v.'</td>';
        }

        $thHtml .= '<th style="padding: 1rem; width: '.$width.';">'.$columns[$k].'</th>';
        ++$i;
    }

    @endphp
    <div>
        <h1 style="font-size: 1.5rem; font-family: 'Gilroy', Helvetica, sans-serif; font-weight: 600;">{{ $effTitle }}</h1>

        <p style="font-family: 'Gilroy', Helvetica, sans-serif; line-height: 2; color: #6D7278;">{{ $effSubTitle }}</p>

        <table width="100%" style="border: 1px solid #979797; font-family: 'Gilroy', Helvetica, sans-serif;" cellspacing="0" cellpadding="0">
            <tr style="background-color: {{env('MIX_SECONDARY')}}; color: white">
                {!! $thHtml !!}
            </tr>
            <tr>
                {!! $tdHtml !!}
            </tr>
        </table>

        @if(isset($flags) && count($flags) > 0)
        <div style="width:80%; margin: 2rem auto 0;display: flex">
            @foreach($flags as $k => $val)
                @php
                  $icon = $val ? 'white-check-only' : 'failed-white';
                @endphp
                <div class="flag-check {{$val ? 'success' : 'failed'}}">{{ $k }} <img src="{{asset('/assets-portal/images/icons/'.$icon.'.png')}}" /></div>
            @endforeach
        </div>
        @endif
    </div>

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer') @endcomponent
    @endslot
@endcomponent
