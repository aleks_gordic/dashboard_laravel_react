<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets-portal/images/favicon.ico') }}"/>
    <title>{{config('app.name')}}</title>

    <style>
        @font-face {
            font-family: 'Varela-Round';
            src: url('/static/fonts/Varela-Round.otf') ;
            font-weight: 400;
            font-style: normal;
        }
        body {
            margin: 0;
            padding: 0;
            font-family: "Varela-Round", Arial, Helvetica, sans-serif;
            color: #000;
            background: #f7f7f7;
            line-height: 22px;
        }
        .header{
            height: 60px;
            background-color: {{env('MIX_PRIMARY')}};
            display: block;
            color: white;
            overflow: hidden;
            padding-left: 1rem;
            z-index: 33;
            box-shadow: 0px 5px 15px 0px rgba(3, 36, 56, 0.15);
        }
        .message{
            padding: 1rem;
            margin-top: 4rem;
            text-align: center;
        }
    </style>

</head>
<body>
<div class="header">
    @if($title == 'cancelled')
        <h4 class="title">Session Cancelled</h4>
    @elseif($title == 'expired')
        <h4 class="title">Session Expired</h4>
    @elseif($title == 'completed')
        <h4 class="title">Session Completed</h4>
    @endif
</div>
<div class="message">
    <div>
        @if($title == 'cancelled')
            {{config('portal.status.cancelled')}}
            <br />
            <br />
            You may close this tab.
        @elseif($title == 'expired')
            {{config('portal.status.expired')}}
            <br />
            <br />
            You may close this tab.
        @elseif($title == 'completed')
            {{config('portal.status.completed')}}
            <br />
            <br />
            You may close this tab.
        @endif
    </div>
</div>

</body>
</html>
