
@php
     use Jenssegers\Agent\Agent;
    use \App\Http\Controllers\App\TokenController;
   $agent = new Agent();

            $info  = TokenController::clientInfo();
            if (!array_key_exists('isp', $info)) {
                $info = ['isp' => 'unknown'];
            }

            $browser  = $agent->browser();
            $device   = $agent->device();
            $platform = $agent->platform();
            if ($platform) {
                $platform .= ' ' . substr($agent->version($platform), 0, 2);
            }

            $info = [
                'device'  => $device ?: 'unknown',
                'os'      => $platform ?: 'unknown',
                'browser' => $browser ?: 'unknown',
                'isp'     => $info['isp'],
                'ip'      => TokenController::ip(),
            ];
@endphp
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{config('app.name')}}</title>
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="shortcut icon" type="image/x-icon" href="/assets-portal/images/favicon.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets-portal/images/favicon.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" id="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1,initial-scale=1,user-scalable=no"/>
    <meta name="format-detection" content="telephone=no"/>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">
    <link href="/static/css/main.css?{{time()}}" rel="stylesheet">
    <script>
        window['_fs_debug'] = false;
        window['_fs_host'] = 'fullstory.com';
        window['_fs_org'] = 'J4ESM';
        window['_fs_namespace'] = 'FS';
        (function(m,n,e,t,l,o,g,y){
            if (e in m) {if(m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].');} return;}
            g=m[e]=function(a,b,s){g.q?g.q.push([a,b,s]):g._api(a,b,s);};g.q=[];
            o=n.createElement(t);o.async=1;o.crossOrigin='anonymous';o.src='https://'+_fs_host+'/s/fs.js';
            y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
            g.identify=function(i,v,s){g(l,{uid:i},s);if(v)g(l,v,s)};g.setUserVars=function(v,s){g(l,v,s)};g.event=function(i,v,s){g('event',{n:i,p:v},s)};
            g.shutdown=function(){g("rec",!1)};g.restart=function(){g("rec",!0)};
            g.log = function(a,b) { g("log", [a,b]) };
            g.consent=function(a){g("consent",!arguments.length||a)};
            g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
            g.clearUserCookie=function(){};
        })(window,document,window['_fs_namespace'],'script','user');

        FS.identify('{{$token}}', {
            t: '{{$token}}',
            device: '{{$info['device']}}',
            os: '{{$info['os']}}',
            browser: '{{$info['browser']}}',
            isp: '{{$info['isp']}}',
            ip: '{{$info['ip']}}',
        });
    </script>
</head>
<body data-phone="{{$phone}}" data-id="{{$token}}">
<div id="root"></div>
<script type="text/javascript" src="/static/js/main.js?{{time()}}"></script>
</body>
</html>
