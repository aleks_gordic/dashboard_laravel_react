<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Http\Models\GreenID\GreenID;
use App\Http\Models\Transactions\Transactions;

// testing
Route::get('/test', function () {
    $result = GreenID::get((object) [
        'transaction'    => null,
        'cardType'       => 'PASSPORT',
        'firstName'      => 'Mathew',
        'middleName'     => 'Fernando',
        'lastName'       => 'Budiman',
        'countryCode'    => 'AU',
        'dateOfBirth'    => '1992-02-12',
        // 'licenceNumber' => '11111111',
        'passportNumber' => '111111',
        'address'        => [
            'city'     => 'Sydney',
            'postcode' => '1111',
            'suburb'   => 'Kellyville',
        ],
    ]);

    // $tr = Transactions::find(1)->first();

    // if ($result['success']) {
    //     GreenID::save($result['data'], $tr);
    // }

});

// Files
Route::middleware('auth.basic')->prefix('files')->group(function () {
    Route::get('images/{crop}/{type}/{token}.{ext}', 'APIs\FilesController@image')->where([
        'crop'  => '(uncropped|cropped)',
        'type'  => '(front|back|face)',
        'token' => '[a-zA-Z0-9-_]+',
        'ext'   => '(jpg|jpeg|png)',
    ]);

    Route::prefix('selfie')->group(function () {
        Route::get('{type}/{token}.jpg', 'APIs\FilesController@selfies')->where([
            'token' => '[a-zA-Z0-9-_]+',
        ]);
    });

    Route::prefix('videos')->group(function () {
        Route::get('face/{token}.jpg', 'APIs\FilesController@face')->where([
            'token' => '[a-zA-Z0-9-_]+',
        ]);
        Route::get('{id}/{token}.{exp}', 'APIs\FilesController@liveness')->where([
            'token' => '[a-zA-Z0-9-_]+',
            'exp'   => '(webm|mp4)',
        ]);
        Route::get('{token}.{exp}', 'APIs\FilesController@video')->where([
            'token' => '[a-zA-Z0-9-_]+',
            'exp'   => '(webm|mp4)',
        ]);
    });
});

//For pipeline build
Route::get('/health-check', function () {
    return '';
});

Route::get('/{status}', function ($status) {
    return view('portal.status')->with(['title' => $status]);
})->where([
    'status' => '(cancelled|completed|expired)',
])->name('mobileStatus');

Route::middleware('api_access')->group(function () {
    Route::get('/', 'App\TokenController@view');
    Route::get('/{token}', 'App\TokenController@check')->where([
        'token' => '^((?!portal).)*$',
    ])->name('checkToken');
});

//==========================================
//------------------portal------------------
//==========================================
Route::prefix('portal')->group(function () {

    Route::get('/email', function () {
        return new App\Mail\UserWelcome();
    });

    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');

    Route::get('/login/{token}', 'Auth\LoginController@loginWithToken')->name('login.token');
    Route::post('/login', 'Auth\LoginController@login')->name('postLogin');
    Route::post('/logout', 'Portal\PortalController@myLogout')->name('myLogout');

    Route::any('/check', 'Portal\PortalController@checkIfLoggedIn')->name('checkIfLoggedIn');
    // Password Reset Routes...
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset/{token}/{email}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.token');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('{module?}/{workflow?}', function () {
        return view('portal.app');
    })->where([
        'module'   => '.*',
        'workflow' => '.*',
    ])->middleware('auth');
});

Route::middleware(['auth'])->prefix('api/portal')->group(function () {

    //----Core----
    //user routes
    Route::group(['prefix' => 'user', 'namespace' => 'Portal\Core'], function () {
        $controller = 'PortalUserController';
        $prefix     = 'user';

        Route::get('/read/{id?}', $controller . '@read')->name($prefix . '.read');
        Route::post('/create', $controller . '@create')->name($prefix . '.create');
        Route::post('/update', $controller . '@update')->name($prefix . '.update');
        Route::post('/delete', $controller . '@delete')->name($prefix . '.delete');
        Route::post('/seen/{tId}', $controller . '@updateSeen')->name($prefix . '.updateSeen');
        Route::post('/toggle-status', $controller . '@toggleStatus')->name($prefix . '.toggleStatus');
        Route::get('/search', $controller . '@search')->name($prefix . '.search');
        Route::get('/permissions/{id?}', $controller . '@getUserPermission')->name($prefix . '.permissions');
        Route::post('/resetPassword', $controller . '@resetPassword')->name($prefix . '.resetPassword');
        Route::post('/resetTwoStep', $controller . '@resetTwoStep')->name($prefix . '.resetTwoStep');
        Route::get('/getCurrentUser', $controller . '@getCurrentUser')->name($prefix . '.getCurrentUser');
        Route::get('/fields', $controller . '@getFields')->name($prefix . '.getFields');
        Route::get('/downloadSample', $controller . '@downloadBatchSample')->name($prefix . '.downloadSample');
        Route::post('/batch-process', $controller . '@processBatchUsers')->name($prefix . '.processBatchUsers');

    });

    //Role Routes
    Route::group(['prefix' => 'role', 'namespace' => 'Portal\Core'], function () {
        $controller = 'RoleController';
        $prefix     = 'role';

        Route::post('/create', $controller . '@create')->name($prefix . '.create');
        Route::get('/read/{id?}', $controller . '@read')->name($prefix . '.read');
        Route::post('/update', $controller . '@update')->name($prefix . '.update');
        Route::post('/delete', $controller . '@delete')->name($prefix . '.delete');
        Route::get('/search', $controller . '@search')->name($prefix . '.search');
        Route::get('/permissions/{id?}', $controller . '@getRolePermission')->name($prefix . '.permissions');

    });

    //Permission Routes
    Route::group(['prefix' => 'permission', 'namespace' => 'Portal\Core'], function () {
        $controller = 'PermissionController';
        $prefix     = 'permission';

        //Route::post('/create', $controller . '@create')->name($prefix . '.create');
        Route::get('/read/{id?}', $controller . '@read')->name($prefix . '.read');
        Route::post('/update', $controller . '@update')->name($prefix . '.update');
        //Route::post('/delete', $controller . '@delete')->name($prefix . '.delete');
        Route::post('/update-bulk', $controller . '@updateBulk')->name($prefix . '.updateBulk');

    });

    //Permission Routes
    Route::group(['prefix' => 'transaction', 'namespace' => 'Portal'], function () {
        $controller = 'PortalController';
        $prefix     = 'transaction';

        Route::get('/completed', $controller . '@getCompletedTransactions')->name($prefix . '.completed');
        Route::get('/pending', $controller . '@getPendingTransactions')->name($prefix . '.pending');

        Route::get('/{id}', $controller . '@transactionDetails')->name($prefix . '.transactionDetails');
        Route::post('/update', $controller . '@updateTransactions')->name($prefix . '.updateTransactions');
        Route::post('/delete/{id}', $controller . '@deleteTransaction')->name($prefix . '.deleteTransaction');

        Route::get('/guided/status/{id}', $controller . '@getUserStatus')->name($prefix . '.userStatus');
        Route::get('/guided/validate/{id}', $controller . '@validateGuided')->name($prefix . '.validateGuided');
        Route::get('/guided/cards/{id}', $controller . '@getCards')->name($prefix . '.getCards');
        Route::post('/guided/update', $controller . '@updateTransactionGuided')->name($prefix . '.updateTransactionGuided');
        Route::post('/guided/cards/approve', $controller . '@approveCards')->name($prefix . '.approveCards');

        Route::post('/download', $controller . '@download')->name('export');
        Route::post('/delete/{id}', $controller . '@deleteTransaction')->name($prefix . '.delete');

    });

    // Invitation
    Route::group(['prefix' => 'invitation', 'namespace' => 'Portal'], function () {
        $controller = 'InvitationController';
        $prefix     = 'invitation';

        Route::post('/send', $controller . '@action')->name($prefix . '.send');
        Route::post('/additional/{id}', $controller . '@sendAdditional')->name($prefix . '.sendAdditional');

    });

     // Dashboard
     Route::group(['prefix' => 'dashboard', 'namespace' => 'Portal'], function () {
        $controller = 'DashboardController';
        $prefix     = 'dashboard';

        Route::get('/sessionReport', $controller . '@sessionReport')->name($prefix . '.sessionReport');
        Route::post('/usageReport', $controller . '@usageReport')->name($prefix . '.usageReport');
        Route::post('/analyticsReport', $controller . '@analyticsReport')->name($prefix . '.analyticsReport');
        Route::post('/incompleteReport', $controller . '@incompleteReport')->name($prefix . '.incompleteReport');

    });

});

Route::prefix('api/portal')->group(function () {
    Route::get('/country', 'Portal\PortalController@getUserCountry')->name('getUserCountry');
    Route::post('/sms/send/simple', 'SMS\SMSController@sendSimple');
    Route::get('/sms/sendAuth', 'SMS\SMSController@sendAuth');
    Route::post('/user/sms-verify', 'Portal\Core\PortalUserController@smsVerify');
    Route::post('/password/set', 'Portal\Core\PortalUserController@setPassword');
    Route::get('user/2fa-auth', 'Portal\Core\PortalUserController@twoFA')->name('user.twoFA');
    Route::post('user/2fa-verify', 'Portal\Core\PortalUserController@twoFaVerify')->name('user.twoFaVerify');
    Route::get('/ipAddress', 'Portal\Core\PortalUserController@getIpAddress')->name('user.getIpAddress');
});
