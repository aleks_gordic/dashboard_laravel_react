<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::prefix('v1')->group(function () {

    // Front end application
    Route::middleware('api_access')->group(function () {

        // Check Token
        Route::get('token/{token}', 'App\TokenController@check')->where([
            'token' => '[a-zA-Z0-9-_]+',
        ])->name('checkToken');

        // Upload files
        Route::prefix('upload')->group(function () {
            Route::post('image', 'App\UploadController@image')->name('uploadImage');
            Route::post('selfie', 'App\UploadController@selfie')->name('uploadSelfie');
            Route::post('video', 'App\UploadController@video')->name('uploadVideo');
        });

        // Get data information
        Route::prefix('data')->group(function () {
            Route::get('cards/{token}', 'App\DataController@cards')->where([
                'token' => '[a-zA-Z0-9-_]+',
            ])->name('cards');
            Route::get('face/{token}', 'App\DataController@face')->where([
                'token' => '[a-zA-Z0-9-_]+',
            ])->name('face');
            Route::post('centrix', 'App\DataController@centrix')->name('centrix');
            Route::get('country', 'App\DataController@country')->name('country');
        });

        // Store
        Route::post('store/details', 'App\DataController@store');

        //check type Eg. guided or self guided
        Route::get('checkType', 'App\DataController@checkType');
        // Update status
        Route::post('update/{status}', 'App\DataController@updateStatus');
        //check if card approved in case of guided
        Route::get('approval', 'App\DataController@checkApproval');

        // URLs
        Route::prefix('urls')->group(function () {
            Route::get('return', 'App\URLsController@return')->name('URLs');
            Route::get('cancel', 'App\URLsController@cancel')->name('URLs');
        });

    });
    // Device
    Route::prefix('device')->group(function () {
        Route::get('check', 'APIs\ApiController@device');
    });

});

//Old API - Backward Compatibility
Route::prefix('biometric')->middleware('oauth')->group(function () {
    Route::post('/storeTransaction', 'APIs\OldApiController@storeTransaction');
    Route::prefix('sms')->group(function () {
        Route::post('/sendSms', 'APIs\OldApiController@apiSendSMS');
    });
    Route::prefix('transactions')->group(function () {
        Route::get('/results/{transactionId}', 'APIs\OldApiController@getClientTransaction');
        Route::delete('/results/{transactionId}', 'APIs\OldApiController@deleteClientTransaction');
    });
});

//  V1 (NZ Projects) Apis
Route::middleware('oauth')->prefix('v1')->group(function () {

    // Session
    Route::prefix('session')->group(function () {
        Route::post('start', 'APIs\ApiControllerV1@create');
        Route::get('check/{token}', 'APIs\ApiControllerV1@check')->where([
            'token' => '[a-zA-Z0-9-_]+',
        ]);
    });

    // Transaction
    Route::prefix('transactions')->group(function () {
        Route::get('{token}', 'APIs\ApiControllerV1@get')->where([
            'token' => '[a-zA-Z0-9-_]+',
        ]);
        Route::delete('{token}', 'APIs\ApiControllerV1@delete')->where([
            'token' => '[a-zA-Z0-9-_]+',
        ]);
    });
});

// Portal 3.1 Apis
Route::middleware('oauth')->prefix('v3.1')->group(function () {
    // SMS
    Route::prefix('sms')->group(function () {
        Route::post('send', 'APIs\ApiControllerV3_1@generate');
    });

    // Session
    Route::prefix('session')->group(function () {
        Route::post('start', 'APIs\ApiControllerV3_1@create');
        Route::get('check/{token}', 'APIs\ApiControllerV3_1@check')->where([
            'token' => '[a-zA-Z0-9-_]+',
        ]);
    });

    // Transaction
    Route::prefix('transactions')->group(function () {
        Route::get('{token}', 'APIs\ApiControllerV3_1@get')->where([
            'token' => '[a-zA-Z0-9-_]+',
        ]);
        Route::delete('{token}', 'APIs\ApiControllerV3_1@delete')->where([
            'token' => '[a-zA-Z0-9-_]+',
        ]);
    });
});


// Batch-Check
Route::group(['prefix' => 'portal/batchcheck', 'namespace' => 'Portal'], function () {
    $controller = 'BatchCheckController';
    $prefix     = 'batchcheck';

    Route::get('/download/{isSms}', $controller . '@getDownload')->name($prefix . '.download');
    Route::get('/get-details', $controller . '@getDetails')->name($prefix . '.getDetails');
    Route::get('/get-batches', $controller . '@getBatches')->name($prefix . '.getBatches');
    Route::post('/upload', $controller . '@upload')->name($prefix . '.upload');
    Route::post('/processfile', $controller . '@processBatchFile')->name($prefix . '.processBatchFile');
});
