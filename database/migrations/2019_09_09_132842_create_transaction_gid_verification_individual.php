<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionGidVerificationIndividual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_gid_verification_individual', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vId')->unsigned();
            $table->foreign('vId')->references('id')->on('transaction_gid_verification_result');
            $table->string('dateVerified', 28)->nullable();
            $table->string('method', 15)->nullable();
            $table->string('name', 64)->nullable();
            $table->string('state', 64)->nullable();
            $table->text('fieldResult')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_gid_verification_individual');
    }
}
