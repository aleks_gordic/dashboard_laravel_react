<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CentrixTable5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_centrix_passport', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->boolean('IsPassportVerified');
            $table->boolean('IsSuccess');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_centrix_passport');
    }
}
