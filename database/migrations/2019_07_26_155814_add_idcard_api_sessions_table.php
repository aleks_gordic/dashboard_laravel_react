<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdcardApiSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `api_sessions` CHANGE `type` `type` ENUM('NZL_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT','VID','SELFIE','ID_CARD') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
");
        DB::statement("ALTER TABLE `transactions` CHANGE `identify_type` `identify_type` ENUM('NZL_DRIVERLICENCE','AUS_ASW_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT','ID_CARD') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PASSPORT'");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `api_sessions` CHANGE `type` `type` ENUM('NZL_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT','VID','SELFIE') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
");
        DB::statement("ALTER TABLE `transactions` CHANGE `identify_type` `identify_type` ENUM('NZL_DRIVERLICENCE','AUS_ASW_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PASSPORT'");

    }
}
