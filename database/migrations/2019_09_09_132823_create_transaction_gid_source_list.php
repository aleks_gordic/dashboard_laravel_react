<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionGidSourceList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_gid_source_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->boolean('available');
            $table->string('name')->nullable();
            $table->boolean('notRequired');
            $table->boolean('oneSourceLeft');
            $table->integer('order');
            $table->boolean('passed');
            $table->string('state', 32)->nullable();
            $table->integer('version');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_gid_source_list');
    }
}
