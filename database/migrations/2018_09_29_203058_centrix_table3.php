<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CentrixTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_centrix_licence', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->boolean('NumberAndVersion');
            $table->boolean('IsDriverLicenceVerified');
            $table->boolean('IsDriverLicenceLastNameMatched');
            $table->boolean('IsDriverLicenceFirstNameMatched');
            $table->boolean('IsDriverLicenceMiddleNameMatched');
            $table->boolean('IsDriverLicenceDateOfBirthMatched');
            $table->boolean('IsSuccess');
            $table->boolean('IsDriverLicenceVerifiedAndMatched');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_centrix_licence');
    }
}
