<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CentrixTable6 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_centrix_residential', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->boolean('IsSuccess');
            $table->string('Message', 64)->nullable();
            $table->boolean('ResidentialVerified');
            $table->string('ThirdPartyMatchType', 64)->nullable();
            $table->string('ThirdPartyMatchTypeDescription', 64)->nullable();
            $table->string('Surname', 64)->nullable();
            $table->string('FirstName', 64)->nullable();
            $table->string('MiddleName', 64)->nullable();
            $table->string('DateOfBirth', 64)->nullable();
            $table->string('ThirdPartyNameMatch', 64)->nullable();
            $table->string('ThirdPartyNameMatchDescription', 64)->nullable();
            $table->boolean('SurnameMatch');
            $table->boolean('FirstNameMatch');
            $table->boolean('FirstNameInitialMatch');
            $table->boolean('MiddleNameMatch');
            $table->boolean('MiddleNameInitialMatch');
            $table->boolean('DateOfBirthMatch');
            $table->string('ThirdPartyAddressMatch', 10)->nullable();
            $table->string('ThirdPartyAddressMatchDescription', 64)->nullable();
            $table->string('AddressLine1', 64)->nullable();
            $table->string('Suburb', 64)->nullable();
            $table->string('State', 64)->nullable();
            $table->string('Postcode', 64)->nullable();
            $table->string('DeliveryPointID', 64)->nullable();
            $table->boolean('UnitNoMatch');
            $table->boolean('StreetNoMatch');
            $table->boolean('StreetNameMatch');
            $table->boolean('StreetTypeMatch');
            $table->boolean('SuburbMatch');
            $table->boolean('StateMatch');
            $table->boolean('PostcodeMatch');
            $table->string('HomePhoneNo', 64)->nullable();
            $table->string('MobilePhoneNo', 64)->nullable();
            $table->boolean('PhoneNumberMatch');
            $table->boolean('MobileNumberMatch');
            $table->string('EmailAddress', 32)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_centrix_pep');
    }
}
