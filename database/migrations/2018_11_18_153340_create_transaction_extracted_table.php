<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionExtractedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_extracted', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->string('cardType', 32)->nullable();
            $table->string('licenceType', 32)->nullable();
            $table->string('firstName', 64)->nullable();
            $table->string('lastName', 64)->nullable();
            $table->string('middleName', 64)->nullable();
            $table->char('dateOfBirth', 10)->nullable();
            $table->char('expiryDate', 10)->nullable();
            $table->string('countryOfIssue', 100)->nullable();
            $table->string('address', 200)->nullable();
            $table->string('street', 64)->nullable();
            $table->string('city', 64)->nullable();
            $table->string('state', 64)->nullable();
            $table->string('zipCode', 10)->nullable();
            $table->string('licenceNumber', 12)->nullable();
            $table->string('licenceVersion', 3)->nullable();
            $table->string('cardNumber', 12)->nullable();
            $table->string('imageQuality', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_extracted');
    }
}
