<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionFaceScanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_face_scan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->string('session', 24)->nullable();
            $table->boolean('confidence')->default(0);
            $table->boolean('liveness')->default(0);
            $table->text('features')->nullable();
            $table->text('liveness_assessments')->nullable();
            $table->string('video', 200)->nullable();
            $table->string('face', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_face_scan');
    }
}
