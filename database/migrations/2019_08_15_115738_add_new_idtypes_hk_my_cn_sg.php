<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewIdtypesHkMyCnSg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE `api_sessions` CHANGE `type` `type` ENUM('NZL_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT','VID','SELFIE','ID_CARD','CHN_IDCARD','HKG_IDCARD','MYS_DLCARD','SGP_IDCARD') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
");

        DB::statement("ALTER TABLE `transactions` CHANGE `identify_type` `identify_type` ENUM('NZL_DRIVERLICENCE','AUS_ASW_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT','ID_CARD','CHN_IDCARD','HKG_IDCARD','MYS_DLCARD','SGP_IDCARD') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PASSPORT'");

        DB::statement("ALTER TABLE `transaction_extracted` CHANGE `licenceNumber` `licenceNumber` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;");

        DB::statement("ALTER TABLE `transaction_extracted` CHANGE `cardNumber` `cardNumber` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;");

        DB::statement("ALTER TABLE `transaction_user_details` ADD `cardNumber` VARCHAR(64) NULL AFTER `passportNumber`;");

        DB::statement("ALTER TABLE `transaction_user_details` CHANGE `lastName` `lastName` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;");

        DB::statement("ALTER TABLE `transactions` CHANGE `country` `country` ENUM('NZ','AU','OTHER','CN','HK','MY','SG') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'AU';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::statement("ALTER TABLE `api_sessions` CHANGE `type` `type` ENUM('NZL_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT','VID','SELFIE','ID_CARD') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
");
        DB::statement("ALTER TABLE `transactions` CHANGE `identify_type` `identify_type` ENUM('NZL_DRIVERLICENCE','AUS_ASW_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT','ID_CARD') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PASSPORT'");

    }
}
