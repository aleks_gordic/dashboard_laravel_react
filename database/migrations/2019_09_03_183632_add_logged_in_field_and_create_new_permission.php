<?php

use App\Http\Models\Portal\Core\Module;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoggedInFieldAndCreateNewPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('last_signin_at')->nullable();
        });

        DB::table('modules')->insert([
            'title'    => 'eMail Alerts',
            'slug'    => 'email-alerts',
            'active'     => true,
        ]);

        $email_alert = Module::where('slug', 'email-alerts')->first();

        DB::table('modules')->insert([
            'parent_id'    => $email_alert->id,
            'title'    => 'Transaction Cancelled',
            'slug'    => 'transaction-cancelled',
            'active'     => true,
        ]);
        DB::table('modules')->insert([
            'parent_id'    => $email_alert->id,
            'title'    => 'Transaction Expired',
            'slug'    => 'transaction-expired',
            'active'     => true,
        ]);
        DB::table('modules')->insert([
            'parent_id'    => $email_alert->id,
            'title'    => 'Pass Transaction',
            'slug'    => 'pass-transaction',
            'active'     => true,
        ]);
        DB::table('modules')->insert([
            'parent_id'    => $email_alert->id,
            'title'    => 'Flagged Transaction',
            'slug'    => 'flagged-transaction',
            'active'     => true,
        ]);
        DB::table('modules')->insert([
            'parent_id'    => $email_alert->id,
            'title'    => 'Flagged Transaction - Data only',
            'slug'    => 'flagged-transaction-data-only',
            'active'     => true,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
