<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tId')->unsigned();
            $table->foreign('tId')->references('id')->on('transactions');
            $table->string('os', 64)->nullable();
            $table->string('device', 64)->nullable();
            $table->string('browser', 64)->nullable();
            $table->string('isp', 120)->nullable();
            $table->string('ip', 64)->nullable();
            $table->string('ip_location', 200)->nullable();
            $table->string('geolocation', 200)->nullable();
            $table->boolean('vpn');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_info');
    }
}
