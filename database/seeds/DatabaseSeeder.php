<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            'id'                     => env('AUTH_ID'),
            'name'                   => 'Orbit Password Grant Client',
            'secret'                 => env('AUTH_SECRET'),
            'redirect'               => 'http://localhost',
            'personal_access_client' => 0,
            'password_client'        => 1,
            'revoked'                => 0,
        ]);
        DB::table('oauth_clients')->insert([
            'id'                     => 214524790,
            'name'                   => 'Orbit Personal Access Client',
            'secret'                 => '5bsfa6QeaBEMVKsbTK3vuSxtT0DdYhwGClGRi8JQ',
            'redirect'               => 'http://localhost',
            'personal_access_client' => 1,
            'password_client'        => 0,
            'revoked'                => 0,
        ]);

        DB::table('oauth_personal_access_clients')->insert([
            'client_id'                   => 214524790,
            'created_at'                 => now(),
            'updated_at'                 => now(),
        ]);
        DB::table('users')->insert([
            'first_name'     => 'Basic',
            'last_name'     => 'account',
            'username'     => env('BASIC_AUTH'),
            'email'         => env('BASIC_AUTH'),
            'role'           => 'admin',
            'mobile'           => '123456',
            'password'      => Hash::make(env('BASIC_AUTH_PASS')),
            'password_created_at'      => now(),
            'mobile_verified_at'      => now(),
            'password_status'      => true,
            'created_by'      => 'api',
            'created_at'      => now(),
        ]);

        DB::table('users')->insert([
            'first_name'     => 'OCR',
            'last_name'     => 'Admin',
            'username'     => 'ocradmin',
            'mobile'           => '123456',
            'email'    => 'ocradmin@ocrrlabs.com',
            'password' => Hash::make('ocr@dmin123#'),
            'role' => 'admin',
            'password_created_at'      => now(),
            'mobile_verified_at'      => now(),
            'password_status'      => true,
            'created_by'      => 'api',
            'created_at'      => now(),
        ]);

        //----------
        DB::table('categories')->insert([
            'title'    => 'Remote Verification',
            'slug'     => 'remote-verification',
            'active'   => true,
        ]);
        DB::table('categories')->insert([
            'title'    => 'Customer Review',
            'slug'     => 'customer-review',
            'active'   => true,
        ]);
        DB::table('categories')->insert([
            'title'    => 'Management',
            'slug'     => 'management',
            'active'   => true,
        ]);

        //--------------
        DB::table('modules')->insert([
            'title'    => 'Identity Flow Self Guided',
            'category_id'    => 1,
            'slug'     => 'identity-flow-self-guided',
            'active'   => true,
        ]);

        DB::table('modules')->insert([
            'title'    => 'Identity flow guided',
            'category_id'    => 1,
            'slug'     => 'identity-flow-guided',
            'active'   => true,
        ]);

        DB::table('modules')->insert([
            'title'    => 'Batch Checking',
            'category_id'    => 1,
            'slug'     => 'batch-checking',
            'active'   => true,
        ]);

        DB::table('modules')->insert([
            'title'    => 'Pending Transactions',
            'category_id'    => 2,
            'slug'     => 'pending-transactions',
            'active'   => true,
        ]);

        DB::table('modules')->insert([
            'title'    => 'Customer repository',
            'category_id'    => 2,
            'slug'     => 'customer-repository',
            'active'   => true,
        ]);

        DB::table('modules')->insert([
            'title'    => 'User Management',
            'category_id'    => 3,
            'slug'     => 'user',
            'active'   => true,
        ]);

        DB::table('modules')->insert([
            'title'    => 'Role Management',
            'category_id'    => 3,
            'slug'    => 'role',
            'active'     => true,
        ]);
        //----Permissions only modules--
        // Sub modules -> Pending transaction
        DB::table('modules')->insert([
            'title'    => 'Delete Records',
            'slug'     => 'delete-records',
            'parent_id' => 4,
            'active'   => true,
        ]);
        DB::table('modules')->insert([
            'title'    => 'Export Records',
            'slug'     => 'export-records',
            'parent_id' => 4,
            'active'   => true,
        ]);

        //Sub modules -> Customer Repository
        DB::table('modules')->insert([
            'title'    => 'Delete Records',
            'slug'     => 'delete-records',
            'parent_id' => 5,
            'active'   => true,
        ]);
        DB::table('modules')->insert([
            'title'    => 'Export Records',
            'slug'     => 'export-records',
            'parent_id' => 5,
            'active'   => true,
        ]);
        DB::table('modules')->insert([
            'title'    => 'View Passed Records',
            'slug'     => 'view-passed-records',
            'parent_id' => 5,
            'active'   => true,
        ]);
        DB::table('modules')->insert([
            'title'    => 'Back/Forward Buttons',
            'slug'     => 'back-forward-buttons',
            'parent_id' => 5,
            'active'   => true,
        ]);

        //--------------
        DB::table('roles')->insert([
            'name'    => 'Administrator',
            'slug'    => 'admin',
        ]);
        DB::table('roles')->insert([
            'name'    => 'Manager',
            'slug'    => 'manager',
        ]);
    }
}
