let mix = require('laravel-mix');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

require('laravel-mix-merge-manifest');

mix.setPublicPath('./public').mergeManifest();

mix.react('resources/assets/js/app.js', 'assets-portal/portal.js')
    .sass('resources/assets/js/styles/app.scss', 'assets-portal/portal.css');

mix.webpackConfig({
    target: 'node',
    resolve: {
        alias: {
            "@components": path.resolve(
                __dirname,
                "resources/assets/js/components"
            ),
            "@containers": path.resolve(
                __dirname,
                "resources/assets/js/containers"
            ),
            "@js": path.resolve(
                __dirname,
                "resources/assets/js"
            ),
            "@services": path.resolve(
                __dirname,
                "resources/assets/js/services"
            ),
        }
    },
    plugins: [
        new BrowserSyncPlugin({
            files: []
        }, {reload: true})
    ],
});

if (mix.inProduction()) {
    mix.version();
}
