<?php

return [
    'api' => [
        'url'         => 'https://dev.api.idkit.io',
        'base_path'   => 'OrbitService/v3.5.6/index.php/orbit_Api',
        'id_types'    => ['NZL_DRIVERLICENCE', 'AUS_ASW_DRIVERLICENCE', 'AUS_AUTO_DRIVERLICENCE', 'PASSPORT', 'ID_CARD','CHN_IDCARD', 'HKG_IDCARD', 'MYS_DLCARD', 'SGP_IDCARD'],
        'images_path' => '/IMG/Serv/',
        'token'       => env('APP_TOKEN'),
        'auth'        => [],
    ],
];
