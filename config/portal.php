<?php

return [

    'interactive_map' => true,
    'has_ip_protection' => false,
    'sms_expiry' => 48, //In Hours
    'address_validation' => false,
    '2fa' => false,
    'enableAddy' => false,
    'cronHourly' => env('CRON_HOURLY', true),
    'status' => [
        'expired' => 'Your ID verification session has been expired.',
        'cancelled' => 'Your ID verification session has been cancelled.',
        'completed' => 'Your identity details have already been captured.',
    ],
    'email_notifications' => true,

];
