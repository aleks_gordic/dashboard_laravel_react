<?php

namespace App\Console;

use App\Console\Commands\CheckExpiredTransactions;
use App\Console\Commands\CheckInactiveUser;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        CheckExpiredTransactions::class,
        CheckInactiveUser::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if(env('APP_ENV') != 'local'){
            if(config('portal.cronHourly')){
                $schedule->command('expiryCheck:transactions')
                    ->hourly();
            }else{
                $schedule->command('expiryCheck:transactions')
                    ->everyTenMinutes();
            }

            $schedule->command('checkUser')
                ->hourly();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
