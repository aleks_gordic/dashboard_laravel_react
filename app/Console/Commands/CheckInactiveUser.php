<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class CheckInactiveUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It regularly checks last user login time. If the user last login 60 days ago. Then He will be deactivated.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $users = User::whereNotIn('username', ['ocradmin', env('BASIC_AUTH')])->get();
        foreach ($users as $user){
            $expiry = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->last_signin_at)->addDays(90);
            if($user && $expiry->lt(now())){
                $user->active = false;
                $user->save();
            }
        }

    }
}
