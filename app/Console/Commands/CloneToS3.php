<?php

namespace App\Console\Commands;

use App\Http\Models\Orbitsdk\AwsS3;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Transactions\Cards;
use App\Http\Models\Transactions\FaceScan;
use App\Http\Models\Transactions\Liveness;
use App\Http\Models\Transactions\Transactions;
use Illuminate\Console\Command;

class CloneToS3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 's3clone {--updateDb=false} {--reset=false}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It will clone all images video from engine to s3 Bucket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $reset = $this->option('reset') == "true";
        if($reset){
            $this->resetEngineLinks();
        }

        $updateDb = $this->option('updateDb') == "true";

        $assets = $this->getAssets();
        $count = count($assets);
        $this->info("Started cloning files for $count transactions");

        foreach ($assets as $item){

            $tId = $item['id'];
            unset($item['id']);

            if(empty($item)) continue;

            foreach ($item as $k =>$v){

                if(empty($v)) continue;

                if($k == 'front'){
                    $clone = AwsS3::storeObjectTerminal($v['url'], $v['path']);

                    if($clone["status"] && $updateDb){
                        Cards::where('tId', $tId)->update([
                            'front_card' => $v['path']
                        ]);
                    }

                } elseif ($k == 'frontFull'){

                    AwsS3::storeObjectTerminal($v['url'], $v['path']);

                } elseif ($k == 'back'){
                    $clone = AwsS3::storeObjectTerminal($v['url'], $v['path']);

                    if($clone["status"] && $updateDb){
                        Cards::where('tId', $tId)->update([
                            'back_card' => $v['path']
                        ]);
                    }

                } elseif ($k == 'backFull'){

                    AwsS3::storeObjectTerminal($v['url'], $v['path']);

                } elseif ($k == 'photo'){
                    $clone = AwsS3::storeObjectTerminal($v['url'], $v['path']);

                    if($clone["status"] && $updateDb){
                        Cards::where('tId', $tId)->update([
                            'face_photo' => $v['path']
                        ]);
                    }

                }elseif ($k == 'videos'){
                    foreach ($v as $id => $data){

                        if(isset($data['video']) && !empty($data['video'])){
                            $video = $data['video'];
                            $clone = AwsS3::storeObjectTerminal($video['url'], $video['path']);

                            if($clone["status"] && $updateDb){
                                FaceScan::where('id', $id)->update([
                                    'video' => $video['path'],
                                ]);
                            }
                        }

                        if(isset($data['face']) && !empty($data['face'])){
                            $face = $data['face'];
                            $clone = AwsS3::storeObjectTerminal($face['url'], $face['path']);

                            if($clone["status"] && $updateDb){
                                FaceScan::where('id', $id)->update([
                                    'face' => $face['path'],
                                ]);
                            }

                        }

                    }
                }elseif ($k == 'liveness'){
                    foreach ($v as $id => $data){

                        if(isset($data['video']) && !empty($data['video'])){
                            $video = $data['video'];
                            $clone = AwsS3::storeObjectTerminal($video['url'], $video['path']);

                            if($clone["status"] && $updateDb){
                                Liveness::where('id', $id)->update([
                                    'video' => $video['path'],
                                ]);
                            }
                        }
                    }
                }

            }
            --$count;
            $this->info("-------------------");
            $this->info("remaining $count");
        }

        $this->info("Completed...:)");
    }

    public function getAssets(){
        $transactions = Transactions::whereHas('cards', function ($query) {

            $query->where('front_card', '!=', null)->orWhere('front_card', '!=', '');

        })->with(['cards:tId,front_card,back_card,face_photo'])->orderBy('created_at','DESC')->get()->toArray();

        $newTrans = [];
        foreach ($transactions as $tr){
            $temp            = [];
            $temp['id']      = $tr['id'];

            if($tr['cards']['front_card']){

                $url = $tr['cards']['front_card'];

                if($this->isEngineUrl($url)){
                    $temp['front']   = $this->parseUrl($url);
                    $temp['frontFull']   = $this->parseUrl($url, true);
                }
            }

            if($tr['cards']['back_card']){

                $url = $tr['cards']['back_card'];

                if($this->isEngineUrl($url)){
                    $temp['back']   = $this->parseUrl($url);
                    $temp['backFull']   = $this->parseUrl($url, true);
                }
            }

            if($tr['cards']['face_photo']){

                $url = $tr['cards']['face_photo'];

                if($this->isEngineUrl($url)){
                    $temp['photo']   = $this->parseUrl($url);
                }
            }


            $frs = FaceScan::where('tId',$tr['id'])->where('video', '!=', null)->get();

            foreach ($frs as $fr){

                if($fr->video){
                    $url = $fr->video;

                    if($this->isEngineUrl($url)){
                        $temp['videos'][$fr->id]['video']   = $this->parseUrl($url);
                    }

                }

                if($fr->face){
                    $url = $fr->face;

                    if($this->isEngineUrl($url)){
                        $temp['videos'][$fr->id]['face']   = $this->parseUrl($url);
                    }
                }

            }

            $liveAttempts = Liveness::where('tId',$tr['id'])->where('video', '!=', null)->get();

            foreach ($liveAttempts as $attempt){
                if($attempt->video){
                    $url = $attempt->video;

                    if($this->isEngineUrl($url)){
                        $temp['liveness'][$attempt->id]['video']   = $this->parseUrl($url);
                    }

                }
            }

            if(!empty($temp)){
                $newTrans[] = $temp;
            }
        }

        return $newTrans;
    }

    public function parseUrl($url, $full = false){

        if($full){
            $url = str_replace('/crop/', '/', $url);
        }

        $parsed = parse_url($url);
        return [
            'url' => $url,
            'path' => str_replace('/IMG','IMG',$parsed['path'])
        ];
    }

    public function isEngineUrl($url){
        return strpos($url, 'api.idkit') !== false || strpos($url, 'api.originid') !== false;
    }

    public function getResetFilePath($path, $addCrop = true){
        $parts = explode('/', $path);

        if($addCrop){
            $url = $parts[count($parts)-3].'/crop/'.basename($path);
        }else{
            $url = $parts[count($parts)-2].'/'.basename($path);
        }
        return $url;
    }

    public function resetEngineLinks(){

        $transactions = Transactions::whereHas('cards', function ($query) {

            $query->where('front_card', '!=', null)->orWhere('front_card', '!=', '');

        })->with(['cards:tId,front_card,back_card,face_photo'])->orderBy('created_at','DESC')->get();
        $this->info("reset started");
        foreach ($transactions as $tr){

            if($tr->cards->front_card && (strpos($tr->cards->front_card, 'IMG') !== false)){
                Cards::where('tId', $tr->id)->update([
                    'front_card' => $this->getResetFilePath($tr->cards->front_card)
                ]);
            }

            if($tr->cards->back_card && (strpos($tr->cards->front_card, 'IMG') !== false)){
                Cards::where('tId', $tr->id)->update([
                    'back_card' => $this->getResetFilePath($tr->cards->back_card)
                ]);
            }

            if($tr->cards->face_photo && (strpos($tr->cards->front_card, 'IMG') !== false)){
                Cards::where('tId', $tr->id)->update([
                    'face_photo' => $this->getResetFilePath($tr->cards->face_photo)
                ]);
            }


            $frs = FaceScan::where('tId',$tr['id'])->where('video', '!=', null)->get();

            foreach ($frs as $fr){

                if($fr->video && (strpos($fr->video, 'IMG') !== false)){
                    $fr->video = $this->getResetFilePath($fr->video, false);
                }

                if($fr->face && (strpos($fr->face, 'IMG') !== false)){
                    $fr->face = $this->getResetFilePath($fr->face, false);
                }

                $fr->save();
            }

        }

        $this->info("reset completed :)");
    }
}
