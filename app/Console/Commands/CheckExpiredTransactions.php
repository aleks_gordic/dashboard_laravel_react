<?php

namespace App\Console\Commands;

use App\Http\Models\Portal\Core\Permission;
use App\Http\Models\Transactions\Transactions;
use App\Notifications\TransactionExpired;
use App\User;
use Illuminate\Console\Command;

class CheckExpiredTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expiryCheck:transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check expired transactions and mark the status as "Expired" at regular interval';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $this->info('Preparing for expiry check...');
            $notStatuses = ['COMPLETED','CANCELLED', 'EXPIRED', 'FAILED'];
            $transactions  =  Transactions::whereNotIn('status', $notStatuses)->whereHas('urls', function ($query) {
                $query->whereDate('expiry','<' ,now());
            })->get();

            foreach ($transactions as $tr){
                $tr->status = 'EXPIRED';
                $tr->save();

                //Notify User
                $user = User::where('username',$tr->username)->first();
                if(config('portal.email_notifications') && env('APP_ENV') != 'local' && $user && Permission::hasPermission('email-alerts','transaction-expired', $user->id)){
                    //
                    $user->notify(new TransactionExpired($tr));
                }

                $this->info('Status updated for transactionID '.$tr->transactionId);
            }

            $this->info('Completed...:)');
        } catch (\Exception $e) {

        }


    }
}
