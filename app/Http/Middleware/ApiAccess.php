<?php

namespace App\Http\Middleware;

use App\Http\Models\Transactions\Statistics;
use App\Http\Models\Transactions\Transactions;
use Closure;

class ApiAccess
{
    protected $except = [
        'createToken',
        'checkToken',
        'URLs',
        'myLogout'
    ];

    protected $transaction;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request['transaction'] = null;

        /*if (env('APP_ENV') == 'local') {
            $request['transaction'] = $this->tests();
            return $next($request);
        }*/


        if (!in_array($request->route()->getName(), $this->except)) {
            if (!session()->has('__token') || empty(session('__token')) || !self::transactionExists()) {

                if ($request->wantsJson()) {
                    return response()->json([
                        'status' => 'error',
                        'msg'    => '401 Unauthorized.',
                    ], 401);
                }

                //redirect to custom error pages
                if(session()->has('__token') && !empty(session('__token'))){


                    if(self::linkExpired()){
                        $status  =  'expired';
                    }else{
                        $tr = self::transaction(session('__token'))->first();

                        if($tr->instruction_type == 'GUIDED' && $tr->user_status == 'completed'){
                            $status = 'completed';
                        }else{
                            $status  =  $tr->status;
                        }

                    }

                    return redirect()->route('mobileStatus', [ 'status' => strtolower($status)]);

                }
                if (env('ERROR_URL')) {
                    return redirect(env('ERROR_URL'));
                }

                return abort(401);
            }

            $request['transaction'] = self::transaction(session('__token'));

        }

        return $next($request);
    }

    protected static function transaction($token)
    {

        $statuses = ['COMPLETED','FAILED', "EXPIRED", 'CANCELLED'];
        $tr = Transactions::where('token', $token)->whereNotIn('status', $statuses)->orderBy('created_at', 'desc')->first();

        if ($tr && !$tr->statistics) {
            $statistics      = new Statistics();
            $statistics->tId = $tr->id;
            $statistics->save();
        }

        return Transactions::where('token', $token);
    }

    protected static function transactionExists()
    {
        $statuses = ['COMPLETED', 'FAILED', "EXPIRED", 'CANCELLED'];
        return self::transaction(session('__token'))
            ->where('user_status','!=', 'completed')
            ->whereNotIn('status', $statuses)
            ->whereHas('urls', function ($query) {
                $query->where('expiry', '>', now());
            })
            ->exists();
    }

    protected static function linkExpired(){
        $tr = self::transaction(session('__token'))
            ->whereHas('urls', function ($query) {
                $query->where('expiry', '>', now());
            })->exists();

        return !$tr ;
    }

    public function tests()
    {
        $token = md5('abcd123456');
        $tr    = self::transaction($token);
        if (!$tr->exists()) {
            $tr = new Transactions();

            $tr->token          = $token;
            $tr->transactionId  = '12345678890';
            $tr->original_token = $token;
            $tr->centrix        = 0;
            $tr->save();
        }

        return $tr;
    }

}
