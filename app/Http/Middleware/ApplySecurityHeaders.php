<?php

namespace App\Http\Middleware;

use Closure;

class ApplySecurityHeaders
{
    /*
    |--------------------------------------------------------------------------
    | ApplySecurityHeaders
    |--------------------------------------------------------------------------
    |
    | Handle an incoming request. This will set the security headers
    | to make the application secure
    | for all HTTP request
    |
    */
    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $response->headers->set('Cache-Control', "no-cache, no-store, must-revalidate");
        $response->headers->set('Pragma', "no-cache");
        $response->headers->set('Expires', "0");

        $response->headers->set('Access-Control-Allow-Origin', env('APP_URL'));
        $response->headers->set('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE");
        $response->headers->set('Referrer-Policy', "no-referrer");
        $response->headers->set('X-XSS-Protection', "1; mode=block");
        //$response->headers->set('Content-Security-Policy', "script-src 'self' https://fullstory.com https://maps.googleapis.com");

        return $response;
    }
}
