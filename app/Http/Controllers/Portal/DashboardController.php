<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

use App\Helpers\Helper;
use App\Http\Models\Centrix\Centrix;
use App\Http\Models\Liveness\Token;
use App\Http\Models\Orbitsdk\AwsS3;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Portal\Core\Permission;
use App\Http\Models\Transactions\Seen;
use App\Http\Models\Transactions\Statistics;
use App\Http\Models\Transactions\Transactions;
use App\Http\Models\Transactions\Info;
use App\Http\Models\Transactions\Extracted;
use App\Http\Models\Portal\Core\Category;
use App\Http\Serialisers\TransactionSerialiser;
use Carbon\Carbon;
use DateTime;

class DashboardController extends Controller
{

  public function sessionReport( Request $request )
  {
    $inputs = $request->all();
    $data = [];
    
    $now = Carbon::now();
    
    $weekStartDate = $now->copy()->startOfWeek();
    $weekEndDate = $now->copy()->endOfWeek();

    $data['sessionsToday'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=', Carbon::today()->copy()->setTime(0, 0, 0)->toDateTimeString())
      ->whereDate('completed_at', '<=', Carbon::today()->copy()->toDateTimeString())
      ->get()
      ->count();

    $data['sessionsYesterDaySameTime'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=', Carbon::yesterday()->copy()->setTime(0, 0, 0)->toDateTimeString())
      ->whereDate('completed_at', '<=', Carbon::yesterday()->copy()->toDateTimeString())
      ->get()
      ->count();

    $data['sessionsYesterDayTotal'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=', Carbon::yesterday()->copy()->setTime(0, 0, 0)->toDateTimeString())
      ->whereDate('completed_at', '<=', Carbon::yesterday()->copy()->setTime(23, 59, 59)->toDateTimeString())
      ->get()
      ->count();

    $data['sessionsThisWeek'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=', $weekStartDate->copy()->toDateTimeString())
      ->whereDate('completed_at', '<=', Carbon::today()->copy()->toDateTimeString())
      ->get()
      ->count();

    $todayMinusOneWeekAgo = $now->copy()->subWeek();
    $lastWeekStartDate = $weekStartDate->copy()->subWeek();

    $data['sessionsLastWeekSameDay'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=', $lastWeekStartDate->toDateTimeString())
      ->whereDate('completed_at', '<=', $todayMinusOneWeekAgo->toDateTimeString())
      ->get()
      ->count();

    $lastWeekEndDate = $lastWeekStartDate->copy()->addDays(6);

    $data['sessionsLastWeekTotal'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=', $lastWeekStartDate->toDateTimeString())
      ->whereDate('completed_at', '<=', $lastWeekEndDate->setTime(23, 59, 59)->toDateTimeString())
      ->get()
      ->count();

    $monthStartDate = $now->copy()->firstOfMonth();
    
    $data['sessionsThisMonth'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=',  $monthStartDate->toDateTimeString())
      ->whereDate('completed_at', '<=', Carbon::today()->copy()->toDateTimeString())
      ->get()
      ->count();

    $todayMinusOneMonthAgo = $now->copy()->subMonth();
    $lastMonthStartDate = $monthStartDate->copy()->subMonth();
    
    $data['sessionsLastMonthSameDay'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=',  $lastMonthStartDate->toDateTimeString())
      ->whereDate('completed_at', '<=', $todayMinusOneMonthAgo->toDateTimeString())
      ->get()
      ->count();

    $lastMonthEndDate = new Carbon('last day of last month');
    $data['sessionsLastMonthTotal'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=',  $lastMonthStartDate->toDateTimeString())
      ->whereDate('completed_at', '<=', $lastMonthEndDate->setTime(23, 59, 59)->toDateTimeString())
      ->get()
      ->count();

    $data['sessionsAllTime'] = Transactions::where('status', 'COMPLETED')
      ->get()
      ->count();

    $quarterStartDate = $now->copy()->firstOfQuarter();
    $data['sessionsThisQuarter'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=',  $quarterStartDate->toDateTimeString())
      ->whereDate('completed_at', '<=', Carbon::today()->copy()->toDateTimeString())
      ->get()
      ->count();

    return response()->json($data);
  }

  public function usageReport( Request $request )
  {
    $inputs = $request->all();
    $data = [];

    $inputs = Helper::xss_patch_input($inputs);
    $validator = Validator::make($inputs, [
      'startOfDate' => 'required',
      'endOfDate' => 'required',
    ]);

    if ($validator->fails())
    {
      return response()->json([
        'status' => 'failed',
        'message' => $validator->getMessageBag()->toArray()
      ]);
    }

    $startOfDate = Carbon::createFromTimestamp($inputs['startOfDate']);
    $endOfDate = Carbon::createFromTimestamp($inputs['endOfDate']);
    $diff_days = $startOfDate->diffInDays($endOfDate);

    $transactions = Transactions::with([
      'contactDetails:tId,phone,email,name',
      'userDetails:tId,firstName,lastName',
      'liveness',
      'centrixSmartId:tId,IsVerified',
      'centrixLicence',
      'centrixPassport',
      'cards:tId,asf',
      'faceScan'
    ]) ->orderBy('completed_at','desc');

    $data['totalCompleted'] = Transactions::where('status', 'COMPLETED')
      ->whereDate('completed_at', '>=',  $startOfDate->toDateTimeString())
      ->whereDate('completed_at', '<=', $endOfDate->toDateTimeString())
      ->get()
      ->count();

    $data['totalIncomplete'] = Transactions::where('status', '!=', 'COMPLETED')
      ->whereDate('completed_at', '>=',  $startOfDate->toDateTimeString())
      ->whereDate('completed_at', '<=', $endOfDate->toDateTimeString())
      ->get()
      ->count();

    $transactions = $transactions->whereDate('completed_at','>=',$startOfDate->toDateTimeString());
    $transactions = $transactions->whereDate('completed_at','<=',$endOfDate->toDateTimeString());
    $transactions = $transactions->where('status','=', 'COMPLETED')->get()->toArray();

    if(sizeof($transactions) == 0) {
      return response()->json([]);
    }

    $completed_flgged = 0;
    $y = 0;
    $m = 0;
    $d = 0;
    $h = 0;
    $min = 0;
    $sec = 0;
    
    $averageHourlyVisitsData = array(
      0 => 0,
      2 => 0,
      4 => 0,
      6 => 0,
      8 => 0,
      10 => 0,
      12 => 0,
      14 => 0,
      16 => 0,
      18 => 0,
      20 => 0,
      22 => 0
    );

    foreach ($transactions as $trans){
      // $trans['Data'] = false;
      $doc = false;
      $live = false;
      $face = false;
      /*
      // If AU cards
      if ($trans['country'] == 'AU') {
        $overall = false;
        if (isset($trans['centrix_licence']) && isset($trans['centrix_licence']['IsDriverLicenceVerified'])) {
            $overall = Centrix::boolean($trans['centrix_licence']['IsDriverLicenceVerified']);
        } else if (isset($trans['centrix_passport']) && isset($trans['centrix_passport']['IsPassportVerified'])) {
            $overall = Centrix::boolean($trans['centrix_passport']['IsPassportVerified']);
        }
        $trans['Data'] = $overall;
      }

      if(Helper::hasCentrix($trans['country'])){
          // If NZ cards : default
          if($trans['centrix_smart_id'] && isset($trans['centrix_smart_id']['IsVerified'])){
              $trans['Data'] = Centrix::boolean($trans['centrix_smart_id']['IsVerified']);
          }
      }else{
          $trans['Data'] = 'NA';
      }
      */
      if($trans['cards'] && isset($trans['cards']['asf'])){
        $asf = json_decode($trans['cards']['asf'], true);
        $doc = isset($asf['overall']) ? Centrix::boolean($asf['overall']) : false;
      }

      if($trans['face_scan'] && isset($trans['face_scan']['confidence'])){
        $face = Centrix::boolean($trans['face_scan']['confidence']);
      }

      if($trans['liveness'] && !empty($trans['liveness'])){
        $live = Centrix::boolean($trans['liveness'][count($trans['liveness']) - 1]['result']);
      }

      if(!($doc && $face && $live)){
        $completed_flgged++;
      }

      $to = Carbon::createFromFormat('Y-m-d H:i:s', $trans['completed_at']);
      $from = Carbon::createFromFormat('Y-m-d H:i:s', $trans['created_at']);

      $diff = $to->diff($from);
      $y += $diff->y;
      $m += $diff->m;
      $d += $diff->d;
      $h += $diff->h;
      $min += $diff->i;
      $sec += $diff->s;
      
      // get average visits hourly
      $completed_hour = Carbon::parse($trans['completed_at'])->format('H');
      for($h = 0 ; $h < 24 ; $h = $h + 2) {
        if($completed_hour >= $h && $completed_hour <= $h + 1) {
          $averageHourlyVisitsData[$h] += 1;
        }
      }
    }

    $data['totalCompletedFlagged'] = $completed_flgged;
    
    $total_diff = $y * 31536000 + $m * 2629746 + $d * 86400 + $h * 3600 + $min * 60 + $sec;
    $per_completed = floor($total_diff / $data['totalCompleted']);

    $dt = Carbon::now();
    $days = $dt->diffInDays($dt->copy()->addSeconds($per_completed));
    $hours = $dt->diffInHours($dt->copy()->addSeconds($per_completed)->subDays($days));
    $minutes = $dt->diffInMinutes($dt->copy()->addSeconds($per_completed)->subDays($days)->subHours($hours));
    $seconds = $dt->diffInMinutes($dt->copy()->addSeconds($per_completed)->subDays($days)->subHours($hours)->subDays($days));
    
    $data['averageDuration']['days'] = $days;
    $data['averageDuration']['hours'] = $hours;
    $data['averageDuration']['minutes'] = $minutes;
    $data['averageDuration']['seconds'] = $seconds;

    // get visits daily
    $dailyVisitData = [];
    $dailyVisitValue = [];
    
    for($i = 0 ; $i < $diff_days + 1 ; $i++){
      $temp_date = $startOfDate->copy()->addDays($i);
      $temp = Transactions::where('status', 'COMPLETED')
        ->whereDate('completed_at', '>=', $temp_date->setTime(0, 0, 0)->toDateTimeString())
        ->whereDate('completed_at', '<=', $temp_date->setTime(23, 59, 59)->toDateTimeString())
        ->get()
        ->count();

      array_push($dailyVisitData, Carbon::parse($temp_date->toDateTimeString())->format('M d'));
      array_push($dailyVisitValue, $temp);
    }

    $data['dailyVisit'] = array(
      'data' => $dailyVisitData,
      'values' => $dailyVisitValue,
    );
    
    $_averageHourlyVisitsData = [];
    $_averageHourlyVisitsValue = [];
    foreach ($averageHourlyVisitsData as $key => $value) {
      array_push($_averageHourlyVisitsData, Carbon::createFromFormat('H:i', $key.':00')->format('H:i'));
      array_push($_averageHourlyVisitsValue, $value);
    }

    $data['averageHourlyVisits'] = array(
      'data' => $_averageHourlyVisitsData,
      'values' => $_averageHourlyVisitsValue,
    );

    return response()->json($data);
  }

  public function prepareTransactionRawStatistics($result){
    //Activate time = (time when user open sms link) - (time when SMS was sent)
    $to = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $result['statistics']['created_at']);
    $from = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $result['created_at']);

    $diff = $to->diff($from)->format('%d day %H hour %I min %S sec');
    $plain_items = explode(" ", $diff);
    $day = $plain_items[0];
    $hour = $plain_items[2];
    $min = $plain_items[4];;
    $sec = $plain_items[6];
    $diff = $day * 86400 + $hour * 3600 + $min * 60 + $sec;    

    $statistics = [
      'timeToActiveSMS' => $diff
    ];
    //-----|-|--|-----------------
    // Time spend on review terms Screen
    $statistics['timeToReviewTerms'] = ceil($result['statistics']['review_terms']);
    //-----|-|--|-----------------
    // Capture time: time to capture details of a card
    $statistics['timeToCaptureID'] = ceil($result['statistics']['capture_id']);
    //-----|-|--|-----------------
    // Review data: time taken on review data screen
    $statistics['timeToReviewData'] = ceil($result['statistics']['review_data']);
    //-----|-|--|-----------------
    //Time taken to capture liveness
    $statistics['timeToCaptureLiveness'] = ceil($result['statistics']['liveness']);
    //-------------------------
    $statistics['totalProcessTime'] = ceil($result['statistics']['review_terms']) +
        ceil($result['statistics']['capture_id']) +
        ceil($result['statistics']['review_data']) +
        ceil($result['statistics']['liveness']);
    //-------------------------

    return $statistics;
  }

  public function analyticsReport( Request $request )
  {
    $inputs = $request->all();
    $data = [];
    
    $inputs = Helper::xss_patch_input($inputs);
    $validator = Validator::make($inputs, [
      'startDate' => 'required',
      'endDate' => 'required',
    ]);

    if ($validator->fails())
    {
      return response()->json([
        'status' => 'failed',
        'message' => $validator->getMessageBag()->toArray()
      ]);
    }
    
    $startOfDate = Carbon::createFromTimestamp($inputs['startDate'])->toDateTimeString();
    $endOfDate = Carbon::createFromTimestamp($inputs['endDate'])->toDateTimeString();

    $data['totalSessionsComplete'] = Transactions::whereDate('completed_at', '>=',  $startOfDate)
    ->whereDate('completed_at', '<=', $endOfDate)->where('status','COMPLETED')->count();
    $data['totalSessionsIncomplete'] = Transactions::whereDate('completed_at', '>=',  $startOfDate)
    ->whereDate('completed_at', '<=', $endOfDate)->where('status','!=', 'COMPLETED')->count();
    if ($data['totalSessionsComplete'] == 0) {
      return response()->json([]);
    }
    $transactions = Transactions::where('status', 'COMPLETED')->whereDate('completed_at', '>=',  $startOfDate)
    ->whereDate('completed_at', '<=', $endOfDate)->with('statistics')->orderBy('completed_at','desc')->get()->toArray();
    // Get averageSessionDurationBreakdown values
    $total_timeToActiveSMS = 0;
    $total_timeToReviewTerms = 0;
    $total_timeToCaptureID = 0;
    $total_timeToReviewData = 0;
    $total_timeToCaptureLiveness = 0;
    $total_totalProcessTime = 0;
    foreach ($transactions as $trans) {
      $statistics = $this->prepareTransactionRawStatistics($trans);
      $total_timeToActiveSMS += $statistics['timeToActiveSMS'];
      $total_timeToReviewTerms += $statistics['timeToReviewTerms'];
      $total_timeToCaptureID += $statistics['timeToCaptureID'];
      $total_timeToReviewData += $statistics['timeToReviewData'];
      $total_timeToCaptureLiveness += $statistics['timeToCaptureLiveness'];
      $total_totalProcessTime += $statistics['totalProcessTime'];
    }
    $averageSessionDurationBreakdown = [ 'timeToActiveSMS' => '', 'timeToReviewTerms' => '', 'timeToCaptureID' => '', 'timeToReviewData' => '', 'timeToCaptureLiveness' => '', 'totalProcessTime' => ''];
    $avg_timeToActiveSMS = floor($total_timeToActiveSMS/$data['totalSessionsComplete']);
    $hours = floor($avg_timeToActiveSMS / 3600 % 60 );
    $mins = floor($avg_timeToActiveSMS / 60 % 60 );
    $secs = floor($avg_timeToActiveSMS % 60 );
    if ($hours) {
      $averageSessionDurationBreakdown['timeToActiveSMS'] .= $hours.' hours ';
    }
    if ($mins) {
      $averageSessionDurationBreakdown['timeToActiveSMS'] .= $mins.' mins ';
    }
    if ($secs) {
      $averageSessionDurationBreakdown['timeToActiveSMS'] .= $secs.' sec';
    }
    $avg_timeToReviewTerms = floor($total_timeToReviewTerms/$data['totalSessionsComplete']);
    $hours = floor($avg_timeToReviewTerms / 3600 % 60 );
    $mins = floor($avg_timeToReviewTerms / 60 % 60 );
    $secs = floor($avg_timeToReviewTerms % 60 );
    if ($hours) {
      $averageSessionDurationBreakdown['timeToReviewTerms'] .= $hours.' hours ';
    }
    if ($mins) {
      $averageSessionDurationBreakdown['timeToReviewTerms'] .= $mins.' mins ';
    }
    if ($secs) {
      $averageSessionDurationBreakdown['timeToReviewTerms'] .= $secs.' sec';
    }
    $avg_timeToCaptureID = floor($total_timeToCaptureID/$data['totalSessionsComplete']);
    $hours = floor($avg_timeToCaptureID / 3600 % 60 );
    $mins = floor($avg_timeToCaptureID / 60 % 60 );
    $secs = floor($avg_timeToCaptureID % 60 );
    if ($hours) {
      $averageSessionDurationBreakdown['timeToCaptureID'] .= $hours.' hours ';
    }
    if ($mins) {
      $averageSessionDurationBreakdown['timeToCaptureID'] .= $mins.' mins ';
    }
    if ($secs) {
      $averageSessionDurationBreakdown['timeToCaptureID'] .= $secs.' sec';
    }
    $avg_timeToReviewData = floor($total_timeToReviewData/$data['totalSessionsComplete']);
    $hours = floor($avg_timeToReviewData / 3600 % 60 );
    $mins = floor($avg_timeToReviewData / 60 % 60 );
    $secs = floor($avg_timeToReviewData % 60 );
    if ($hours) {
      $averageSessionDurationBreakdown['timeToReviewData'] .= $hours.' hours ';
    }
    if ($mins) {
      $averageSessionDurationBreakdown['timeToReviewData'] .= $mins.' mins ';
    }
    if ($secs) {
      $averageSessionDurationBreakdown['timeToReviewData'] .= $secs.'sec';
    }
    $avg_timeToCaptureLiveness = floor($total_timeToCaptureLiveness/$data['totalSessionsComplete']);
    $hours = floor($avg_timeToCaptureLiveness / 3600 % 60 );
    $mins = floor($avg_timeToCaptureLiveness / 60 % 60 );
    $secs = floor($avg_timeToCaptureLiveness % 60 );
    if ($hours) {
      $averageSessionDurationBreakdown['timeToCaptureLiveness'] .= $hours.' hours ';
    }
    if ($mins) {
      $averageSessionDurationBreakdown['timeToCaptureLiveness'] .= $mins.' mins ';
    }
    if ($secs) {
      $averageSessionDurationBreakdown['timeToCaptureLiveness'] .= $secs.' sec';
    }
    $avg_totalProcessTime = floor($total_totalProcessTime/$data['totalSessionsComplete']);
    $hours = floor($avg_totalProcessTime / 3600 % 60 );
    $mins = floor($avg_totalProcessTime / 60 % 60 );
    $secs = floor($avg_totalProcessTime % 60 );
    if ($hours) {
      $averageSessionDurationBreakdown['totalProcessTime'] .= $hours.' hours ';
    }
    if ($mins) {
      $averageSessionDurationBreakdown['totalProcessTime'] .= $mins.' mins ';
    }
    if ($secs) {
      $averageSessionDurationBreakdown['totalProcessTime'] .= $secs.' sec';
    }
    $data['averageSessionDurationBreakdown'] = $averageSessionDurationBreakdown;
    // Getting otherStatistics vaules
    $transactions = Transactions::where('status', 'COMPLETED')->whereDate('completed_at', '>=',  $startOfDate)
    ->whereDate('completed_at', '<=', $endOfDate)->with([
      'contactDetails:tId,phone,email,name',
      'userDetails:tId,firstName,lastName',
      'liveness',
      'centrixSmartId:tId,IsVerified',
      'centrixLicence',
      'centrixPassport',
      'cards',
      'faceScan',
      'info:tId,os',
      'extracted:tId,cardType,countryOfIssue'
    ]) ->orderBy('completed_at','desc')->get();
    
    $total_IDCaptureAttempts = 0;
    $total_FieldsEdited = 0;
    $total_LivenessAttempts = 0;
    $total_android = 0;
    $total_ios = 0;
    $total_NSWDriverLicense = 0;
    $total_VICDriverLicense = 0;
    $total_QLDDriverLicense = 0;
    $total_WADriverLicense = 0;
    $total_australiaPassport = 0;
    $total_medicareCard = 0;
    $total_ACTDriverLicense = 0;
    $completed_flagged = 0;
    $completed_passed = 0;
    $total_doc = 0;
    $total_verify = 0;
    $total_face = 0;
    $total_liveness = 0;
    foreach ($transactions as $trans) {
      if(Helper::hasCentrix($trans->country)){
        if($trans->centrixSmartId && $trans->centrixSmartId->IsVerified){
          $verify = Centrix::boolean($trans->centrixSmartId->IsVerified);
        }
      }else{
        $verify = 'NA';
        $total_verify ++;
      }

      if($trans->cards && isset($trans->cards->asf)){
        $asf = json_decode($trans->cards->asf, true);
        $doc = isset($asf['overall']) ? Centrix::boolean($asf['overall']) : false;
        $id_capture_count = $trans->sessions()->whereIn('type', ['NZL_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT'])->count();
        $total_IDCaptureAttempts += $id_capture_count;
        $edited = $trans->cards->edited;
        $edited = (!empty($edited)) ? explode(',', $edited) : [];
        $total_FieldsEdited += count($edited);
        if (!$doc) $total_doc ++;
      }

      if($trans->faceScan){
        if (isset($trans->faceScan->confidence)) {
          $face = Centrix::boolean($trans->faceScan->confidence); 
        } else {
          $total_face ++;
        }
        $attempts = count($trans->liveness) ?? 1;
        $total_LivenessAttempts += $attempts;
      }

      if($trans->liveness && !empty($trans->liveness)){
        // $count = count($trans->liveness);
        if (count($trans->liveness) > 0) {
          $live = Centrix::boolean($trans->liveness[0]->result);
        } else {
          $live = false;
        }
      } else {
        $total_liveness ++;
      }

      if($trans->info->os && !empty($trans->info->os)){
        if (strpos($trans->info->os, 'Android') !== false) {
          $total_android ++;
        }
        if (strpos($trans->info->os, 'iOS') !== false) {
          $total_ios ++;
        }
      }

      if ($trans->extracted->cardType) {
        if (strpos($trans->extracted->cardType, 'NSW') !== false) {
          $total_NSWDriverLicense ++;
        }
        if (strpos($trans->extracted->cardType, 'VIC') !== false) {
          $total_VICDriverLicense ++;
        }
        if (strpos($trans->extracted->cardType, 'QLD') !== false) {
          $total_QLDDriverLicense ++;
        }
        if (strpos($trans->extracted->cardType, 'WA') !== false) {
          $total_WADriverLicense ++;
        }
        if (strpos($trans->extracted->cardType, 'PASSPORT') !== false) {
          if ($trans->extracted->countryOfIssue == 'Australia') {
            $total_australiaPassport ++;
          }
        }
        if (strpos($trans->extracted->cardType, 'NSW') !== false) {
          $total_medicareCard ++;
        }
        if (strpos($trans->extracted->cardType, 'ACT') !== false) {
          $total_ACTDriverLicense ++;
        }
      }

      if(!($doc && $face && $live)){
        $completed_flagged ++;
      }
      if ($verify && $doc && $face && $live) {
        $completed_passed ++;
      }
    }
    // PUT otherStatistics results
    $percentageAndroid = number_format((float)$total_android/$data['totalSessionsComplete'] * 100, 2, '.', '');
    $percentageIOS = number_format((float)$total_ios/$data['totalSessionsComplete'] * 100, 2, '.', '');
    $otherStatistics = [];
    $otherStatistics['averageLivenessAttempts'] = number_format((float)$total_LivenessAttempts/$data['totalSessionsComplete']*100, 2, '.', '');
    $otherStatistics['averageIDCaptureAttempts'] = number_format((float)$total_IDCaptureAttempts/$data['totalSessionsComplete']*100, 2, '.', '');
    $otherStatistics['averageFieldsEdited'] = number_format((float)$total_FieldsEdited/$data['totalSessionsComplete']*100, 2, '.', '');
    $otherStatistics['percentageAndroid'] = $percentageAndroid;
    $otherStatistics['percentageIOS'] = $percentageIOS;
    $otherStatistics['percentageOther'] = number_format(100 - $percentageAndroid - $percentageIOS, 2, '.', '');
    $data['otherStatistics'] = $otherStatistics;

    $documentTypeBreakdown = [];
    if ($total_NSWDriverLicense > 0) {
      $documentTypeBreakdown['NSWDriverLicense'] = number_format((float)$total_NSWDriverLicense/$data['totalSessionsComplete']*100, 2, '.', '');
    } else {
      $documentTypeBreakdown['NSWDriverLicense'] = 0;
    }
    if ($total_QLDDriverLicense > 0) {
      $documentTypeBreakdown['QLDDriverLicense'] = number_format((float)$total_QLDDriverLicense/$data['totalSessionsComplete']*100, 2, '.', '');
    } else {
      $documentTypeBreakdown['QLDDriverLicense'] = 0;
    }
    if ($total_WADriverLicense > 0) {
      $documentTypeBreakdown['WADriverLicense'] = number_format((float)$total_WADriverLicense/$data['totalSessionsComplete']*100, 2, '.', '');
    } else {
      $documentTypeBreakdown['WADriverLicense'] = 0;
    }
    if ($total_australiaPassport > 0) {
      $documentTypeBreakdown['australiaPassport'] = number_format((float)$total_australiaPassport/$data['totalSessionsComplete']*100, 2, '.', '');
    } else {
      $documentTypeBreakdown['australiaPassport'] = 0;
    }
    if ($total_medicareCard > 0) {
      $documentTypeBreakdown['medicareCard'] = number_format((float)$total_medicareCard/$data['totalSessionsComplete']*100, 2, '.', '');
    } else {
      $documentTypeBreakdown['medicareCard'] = 0;
    }
    if ($total_ACTDriverLicense > 0) {
      $documentTypeBreakdown['ACTDriverLicense'] = number_format((float)$total_ACTDriverLicense/$data['totalSessionsComplete']*100, 2, '.', '');
    } else {
      $documentTypeBreakdown['ACTDriverLicense'] = 0;
    }
    $data['documentTypeBreakdown'] = $documentTypeBreakdown;

    $statusBreakdown = [];
    $statusBreakdown['completedPassed']['times'] = $completed_passed;
    if ($completed_passed > 0) {
      $statusBreakdown['completedPassed']['percentage'] = floor($completed_passed/$data['totalSessionsComplete']*100);  
    } else {
      $statusBreakdown['completedPassed']['percentage'] = 0;
    }
    $statusBreakdown['completedFlagged']['times'] = $completed_flagged;
    $data['totalSessionsFlagged'] = $completed_flagged;
    $statusBreakdown['completedFlagged']['percentage'] = 100 - $statusBreakdown['completedPassed']['percentage'];
    $data['statusBreakdown'] = $statusBreakdown;
    
    $incompleteBreakdown = [];
    $initiated = Transactions::whereDate('created_at', '>=',  $startOfDate)->whereDate('updated_at', '<=', $endOfDate)->where('status', 'PENDING')->count();
    $inProgress = Transactions::whereDate('created_at', '>=',  $startOfDate)->whereDate('updated_at', '<=', $endOfDate)->where('status', 'INPROGRESS')->count();
    $expired = Transactions::whereDate('created_at', '>=',  $startOfDate)->whereDate('updated_at', '<=', $endOfDate)->where('status', 'EXPIRED')->count();
    $terminated = Transactions::whereDate('created_at', '>=',  $startOfDate)->whereDate('updated_at', '<=', $endOfDate)->where('status', 'TERMINATED')->count();
    $total_breakdown = Transactions::where('status', '!=', 'COMPLETED')->count();
    $incompleteBreakdown['initiated']['times'] = $initiated;
    if ($initiated > 0) {
      $incompleteBreakdown['initiated']['percentage'] = number_format((float)$initiated/$total_breakdown*100, 2, '.', '');
    } else {
      $incompleteBreakdown['initiated']['percentage'] = 0;
    }
    $incompleteBreakdown['inProgress']['times'] = $inProgress;
    if ($inProgress > 0) {
      $incompleteBreakdown['inProgress']['percentage'] = number_format((float)$inProgress/$total_breakdown*100, 2, '.', '');
    } else {
      $incompleteBreakdown['inProgress']['percentage'] = 0;
    }
    $incompleteBreakdown['expired']['times'] = $expired;
    if ($expired > 0) {
      $incompleteBreakdown['expired']['percentage'] = number_format((float)$expired/$total_breakdown*100, 2, '.', '');
    } else {
      $incompleteBreakdown['expired']['percentage'] = 0;
    }
    $incompleteBreakdown['terminated']['times'] = $terminated;
    if ($terminated > 0) {
      $incompleteBreakdown['terminated']['percentage'] = number_format((float)$terminated/$total_breakdown*100, 2, '.', '');
    } else {
      $incompleteBreakdown['terminated']['percentage'] = 0;
    }
    $data['incompleteBreakdown'] = $incompleteBreakdown;


    $flaggedBreakdown = [];
    $flaggedBreakdown['documentFraudAnalysis']['times'] = $total_doc;
    if ($total_doc > 0) {
      $flaggedBreakdown['documentFraudAnalysis']['percentage'] = number_format((float)$total_doc/$data['totalSessionsComplete']*100, 2, '.', '');
    } else {
      $flaggedBreakdown['documentFraudAnalysis']['percentage'] = 0;
    }
    $flaggedBreakdown['livenessFlagged']['times'] =$total_liveness;
    if ($total_liveness > 0) {
      $flaggedBreakdown['livenessFlagged']['percentage'] = number_format((float)$total_liveness/$data['totalSessionsComplete']*100, 2, '.', '');  
    } else {
      $flaggedBreakdown['livenessFlagged']['percentage'] = 0;
    }
    $flaggedBreakdown['faceFlagged']['times'] = $total_face;
    if ($total_face > 0) {
      $flaggedBreakdown['faceFlagged']['percentage'] = number_format((float)$total_face/$data['totalSessionsComplete']*100, 2, '.', '');
    } else {
      $flaggedBreakdown['faceFlagged']['percentage'] = 0;
    }
    $flaggedBreakdown['dataFail']['times'] = $total_verify;
    if ($total_verify > 0) {
      $flaggedBreakdown['dataFail']['percentage'] = number_format((float)$total_verify/$data['totalSessionsComplete']*100, 2, '.', '');
    } else {
      $flaggedBreakdown['dataFail']['percentage'] = 0;
    }
    $data['flaggedBreakdown'] = $flaggedBreakdown;
    
    return response()->json($data);
  }

  public function incompleteReport( Request $request )
  {
    $inputs = $request->all();
    $data = [];

    $inputs = Helper::xss_patch_input($inputs);
    $validator = Validator::make($inputs, [
      'startOfDate' => 'required',
      'endOfDate' => 'required',
    ]);

    if ($validator->fails())
    {
      return response()->json([
        'status' => 'failed',
        'message' => $validator->getMessageBag()->toArray()
      ]);
    }

    $startOfDate = Carbon::createFromTimestamp($inputs['startOfDate'])->toDateTimeString();
    $endOfDate = Carbon::createFromTimestamp($inputs['endOfDate'])->toDateTimeString();

    $transactions = Transactions::with([
      'contactDetails',
      'userDetails',
      'liveness',
      'centrixSmartId',
      'centrixLicence',
      'centrixPassport',
      'cards:tId,asf',
      'faceScan',
      'statistics',
      'info',
    ])
      ->whereDate('completed_at', '>=',  $startOfDate)
      ->whereDate('completed_at', '<=', $endOfDate)
      ->orderBy('created_at','desc')
      ->get()
      ->toArray();

    $data['totalSessionsIncomplete'] = Transactions::whereDate('completed_at', '>=',  $startOfDate)
      ->whereDate('completed_at', '<=', $endOfDate)
      ->where('status', '!=', 'COMPLETED')
      ->get()
      ->count();

    $data['totalSessionsIgnored'] = Transactions::whereDate('completed_at', '>=',  $startOfDate)
      ->whereDate('completed_at', '<=', $endOfDate)
      ->where('status', 'EXPIRED')->orWhere('status', 'PENDING')
      ->get()
      ->count();
      
    $_totalSessionAttemptedNotCompleted = 0;
    foreach ($transactions as $value) {
      if( $value['status'] == 'INPROGRESS' || $value['status'] == 'CANCELLED' || ($value['status'] == 'EXPIRED' && $value['statistics']['created_at']))
        $_totalSessionAttemptedNotCompleted++;
    }

    $data['totalSessionAttemptedNotCompleted'] = $_totalSessionAttemptedNotCompleted;
    
    $incompleteTypeBreakdown = [];
    $incompleteTypeBreakdown['ignoredExpired']['times'] = $data['totalSessionsIgnored'];
    $incompleteTypeBreakdown['ignoredExpired']['percentage'] = $data['totalSessionsIncomplete'] > 0 ? ceil(($incompleteTypeBreakdown['ignoredExpired']['times'] / $data['totalSessionsIncomplete'] * 100) * 10 ) / 10 : 0;
    
    $_timeoutDuringFlow = 0;
    foreach ($transactions as $value) {
      if( $value['status'] == 'INPROGRESS' || ($value['status'] == 'EXPIRED' && $value['statistics']['created_at'] ))
        $_timeoutDuringFlow++;
    }

    $incompleteTypeBreakdown['timeoutDuringFlow']['times'] =  $_timeoutDuringFlow;
    $incompleteTypeBreakdown['timeoutDuringFlow']['percentage'] = $data['totalSessionsIncomplete'] > 0 ? ceil(($incompleteTypeBreakdown['timeoutDuringFlow']['times'] / $data['totalSessionsIncomplete'] * 100) * 10 ) / 10 : 0;
    
    $incompleteTypeBreakdown['exitedDuringFlow']['times'] =  Transactions::whereDate('completed_at', '>=',  $startOfDate)
      ->whereDate('completed_at', '<=', $endOfDate)
      ->where('status', 'CANCELLED')
      ->get()
      ->count();
    $incompleteTypeBreakdown['exitedDuringFlow']['percentage'] = $data['totalSessionsIncomplete'] > 0 ? ceil(($incompleteTypeBreakdown['exitedDuringFlow']['times'] / $data['totalSessionsIncomplete'] * 100) * 10 ) / 10 : 0;
    
    $incompleteTypeBreakdown['deviceNotCompatible']['times'] =  0;// not used yet
    $incompleteTypeBreakdown['deviceNotCompatible']['percentage'] = 0;// not used yet
    $incompleteTypeBreakdown['total']['times'] = $data['totalSessionsIncomplete'];
    $incompleteTypeBreakdown['total']['percentage'] = 100;
    $data['incompleteTypeBreakdown'] = $incompleteTypeBreakdown;
    
    $timedOutBreakdown = [];
    
    $_termsScreen = 0;
    foreach ($transactions as $value) {
      if( $value['status'] == 'EXPIRED' && $value['statistics']['created_at'] && $value['statistics']['review_terms'])
        $_termsScreen++;
    }

    $timedOutBreakdown['termsScreen']['times'] = $_termsScreen;
    $timedOutBreakdown['termsScreen']['percentage'] = $data['totalSessionsIncomplete'] > 0 ? ceil(($timedOutBreakdown['termsScreen']['times'] / $data['totalSessionsIncomplete'] * 100) * 10 ) / 10 : 0;
    
    $timedOutBreakdown['overviewScreen']['times'] =  0; // not used yet
    $timedOutBreakdown['overviewScreen']['percentage'] = 0; // not used yet

    $_idSelection = 0;
    foreach ($transactions as $value) {
      if( $value['status'] == 'EXPIRED' && $value['statistics']['created_at'] && $value['statistics']['capture_id'])
        $_idSelection++;
    }

    $timedOutBreakdown['idSelection']['times'] = $_idSelection;
    $timedOutBreakdown['idSelection']['percentage'] = $data['totalSessionsIncomplete'] > 0 ? ceil(($timedOutBreakdown['idSelection']['times'] / $data['totalSessionsIncomplete'] * 100) * 10 ) / 10 : 0;

    $_informationReview = 0;
    foreach ($transactions as $value) {
      if( $value['status'] == 'EXPIRED' && $value['statistics']['created_at'] && $value['statistics']['review_data'])
        $_informationReview++;
    }
    $timedOutBreakdown['informationReview']['times'] =  $_informationReview;
    $timedOutBreakdown['informationReview']['percentage'] = $data['totalSessionsIncomplete'] > 0 ? ceil(($timedOutBreakdown['informationReview']['times'] / $data['totalSessionsIncomplete'] * 100) * 10 ) / 10 : 0;

    $timedOutBreakdown['addressScreen']['times'] = 0; // not used yet
    $timedOutBreakdown['addressScreen']['percentage'] = 0; // not used yet

    $_faceCapture = 0;
    foreach ($transactions as $value) {
      if( $value['status'] == 'EXPIRED' && $value['statistics']['created_at'] && $value['statistics']['liveness'])
        $_faceCapture++;
    }

    $timedOutBreakdown['faceCapture']['times'] = $_faceCapture;
    $timedOutBreakdown['faceCapture']['percentage'] = $data['totalSessionsIncomplete'] > 0 ? ceil(($timedOutBreakdown['faceCapture']['times'] / $data['totalSessionsIncomplete'] * 100) * 10 ) / 10 : 0;
    $timedOutBreakdown['total']['times'] = $data['totalSessionsIncomplete'];
    $timedOutBreakdown['total']['percentage'] = 100;
    $data['timedOutBreakdown'] = $timedOutBreakdown;

    $exitedBreakdown = [];

    $_termsScreen_ = 0;
    foreach ($transactions as $value) {
      if( $value['status'] == 'CANCELLED' && $value['statistics']['created_at'] && $value['statistics']['review_terms'])
        $_termsScreen_++;
    }
    $exitedBreakdown['termsScreen']['times'] = $_termsScreen_;
    $exitedBreakdown['termsScreen']['percentage'] = $data['totalSessionsIncomplete'] > 0 ? ceil(($exitedBreakdown['termsScreen']['times'] / $data['totalSessionsIncomplete'] * 100) * 10 ) / 10 : 0;
    
    $exitedBreakdown['overviewScreen']['times'] =  0; // not used yet
    $exitedBreakdown['overviewScreen']['percentage'] = 0; // not used yet
    
    $_informationReview_ = 0;
    foreach ($transactions as $value) {
      if( $value['status'] == 'CANCELLED' && $value['statistics']['created_at'] && $value['statistics']['review_data'])
        $_informationReview_++;
    }

    $exitedBreakdown['informationReview']['times'] = $_informationReview_;
    $exitedBreakdown['informationReview']['percentage'] = $data['totalSessionsIncomplete'] > 0 ? ceil(($exitedBreakdown['informationReview']['times'] / $data['totalSessionsIncomplete'] * 100) * 10 ) / 10 : 0;

    $exitedBreakdown['addressScreen']['times'] =  0; // not used yet
    $exitedBreakdown['addressScreen']['percentage'] = 0; // not used yet
    
    $exitedBreakdown['total']['times'] = $data['totalSessionsIncomplete'];
    $exitedBreakdown['total']['percentage'] = 100;
    $data['exitedBreakdown'] = $exitedBreakdown;

    $neverInitiatedBreakdown = [];
    $neverInitiatedBreakdown['smsSession']['times'] = 0; // not used yet
    $neverInitiatedBreakdown['smsSession']['percentage'] = 0; // not used yet
    $neverInitiatedBreakdown['emailSession']['times'] = 0; // not used yet
    $neverInitiatedBreakdown['emailSession']['percentage'] = 0; // not used yet
    $neverInitiatedBreakdown['total']['times'] = $data['totalSessionsIgnored'];
    $neverInitiatedBreakdown['total']['percentage'] = 100;
    $data['neverInitiatedBreakdown'] = $neverInitiatedBreakdown;

    return response()->json($data);
  }
}
