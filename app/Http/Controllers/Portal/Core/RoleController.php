<?php

namespace App\Http\Controllers\Portal\Core;

use App\Helpers\Helper;
use App\Http\Models\Portal\Core\Module;
use App\Http\Models\Portal\Core\Permission;
use App\Http\Models\Portal\Core\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use stdClass;
use Validator;

class RoleController extends Controller
{
    public $data;

    public $settings;

    public function __construct()
    {
        $this->data = new stdClass();
        $this->settings = Role::getSettings();
        $this->data->input = (object)request()->all();
        $this->data->input = Helper::xss_patch_input($this->data->input);

        $this->data->model = $this->settings->model;
        $this->data->rows = $this->settings->rows;
        $this->data->settings = $this->settings;

        if(Permission::hasPermission('role')){
            $response['status'] = 'error';
            $response['message'] = 'Permission Denied';
            return response()->json($response);
        }
    }

    //------------------------------------------------------
    function create() {

        /*if (!Permission::check('create-'.$this->settings->prefix)) {
            $error_message = "You don't have permission create";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;

        $response = $model::store();
        return response()->json($response);

    }

    //------------------------------------------------------
    function read($id=null) {

        /*if (!Permission::check('read-'.$this->settings->prefix)) {
            $error_message = "You don't have permission read";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;
        if($id){
            $item = $model::where('id', $id)->first();
        }else{

            $option = request()->input('option');
            if($option && $option === 'all'){
                $item = $model::get();
            }else{
                $item = $model::paginate($this->data->rows);
            }
        }

        if ($item) {
            $response['status'] = 'success';
            $response['data'] = $item;
        } else {
            $response['status'] = 'error';
            $response['messages'] = 'Not found';
        }

        return response()->json($response);
    }

    //------------------------------------------------------
    function update()
    {
        /*if (!Permission::check('update-'.$this->settings->prefix)) {
            $error_message = "You don't have permission update";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        $model = $this->data->model;
        $inputs = Helper::xss_patch_input(request()->all());

        try {

            $permissions = $inputs['permissions'];
            $id = $inputs['id'];
            $item = $model::where('id',$inputs['id'])->first();
            unset($inputs['id']);
            unset($inputs['permissions']);


            foreach ($inputs as $k => $v){
                $item->{$k} = $v;
            }

            $item->save();

            if(!empty($permissions)){
                foreach ($permissions as $permission){
                    $exists = Permission::where('module_id',$permission['module_id'])->where('role_id',$id);

                    if($exists->first()){
                        $exists->update([
                            'access' => $permission['access'] == "1" ? 1 : 0
                        ]);
                        $response = [
                            'status' => 'success',
                            'data' => $exists->first()
                        ];
                    }else{
                        $response = Permission::store([
                            'module_id' => $permission['module_id'],
                            'role_id'   => $id,
                            'access'    => $permission['access'] == "1" ? 1 : 0
                        ]);
                    }

                    if($response['status'] != 'success'){
                        return response()->json($response);
                        break;
                    }


                }

            }


            $response['status'] = 'success';
            $response['data'] = $item;

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);
    }

    //------------------------------------------------------
    function delete()
    {
        /*if (!Permission::check('delete-'.$this->settings->prefix)) {
            $error_message = "You don't have permission delete";
            if (request()->ajax()) {
                $response['status'] = 'error';
                $response['message'] = $error_message;
                return response()->json($response);
            } else {
                return abort(403,'Permission denied');
            }
        }*/

        try{
            $model = $this->data->model;
            $item = $model::where('id',$this->data->input->pk)->first();
            $users = User::where('role', $item->slug)->count();
            if($users > 0){
                return response()->json(['status'=>'error','message' => 'Error while deleting a record. Please reassign the user role.']);
            }
            $model::where('id',$this->data->input->pk)->delete($this->data->input->pk);
            return response()->json(['status'=>'success']);

        }catch (\Exception $e){
            return response()->json(['status'=>'error','message' => 'Error while deleting a record']);
        }

    }

    //------------------------------------------------------
    function search()
    {
        $model = $this->data->model;
        $list = $model::orderBy("created_at", "ASC");
        $term = trim($this->data->input->q);
        $list->where('name', 'LIKE', '%' . $term . '%');

        $fields = config('database.user_meta_fields');
        if(count($fields) > 0){
            foreach ($fields as $field) {
                $list->where(str_slug($field['name']), 'LIKE', '%' . $term . '%');
            }
        }

        $result = $list->paginate($this->data->rows);
        return $result;
    }


    //--------------------------
    public function getRolePermission($id, $jsonResponse = true)
    {

        try {

            $role = Role::where('id', $id)->orWhere('slug', $id)->first();
            $modules = Module::where('active', true)->whereNull('parent_id')->with('category')->get()->toArray();
            $modules = array_map(function ($module) use ($role) {
                $sub = Module::where('parent_id', $module['id'])->get()->toArray();
                if(!empty($sub)){
                    $sub = Permission::formatPermissions($role, $sub);
                    $module['sub_permissions'] = $sub;
                }
                return $module;
            }, $modules);

            $permissions =  Permission::formatPermissions($role, $modules, false);

            if(!$jsonResponse){
                return $permissions;
            }

            $response['status'] = 'success';
            $response['data'] = $permissions;

        } catch (\Exception $e) {
            $response['status'] = 'failed';
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);

    }
}
