<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Http\Models\Portal\Core\Permission;
use App\Http\Models\Transactions\Transactions;
use App\Jobs\InformUser;
use App\Notifications\TransactionCancelled;
use App\User;
use \Illuminate\Http\Request;

class URLsController extends Controller
{
    /**
     * @var string
     */
    public $transaction;

    /**
     * @var string
     */
    public $return;

    /**
     * @var string
     */
    public $cancel;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->transaction = Transactions::where('token', session('__token'))->orderBy('created_at','desc')->first();
            if (!isset($this->transaction->urls)) {
                return abort(404);
            }

            $urls = $this->transaction->urls()->first();

            $this->return = $this->converURLs($urls->return_url);
            $this->cancel = $this->converURLs($urls->cancel_url);

            return $next($request);
        });
    }

    /**
     * Handle the return URL.
     *
     * @return Response
     */
    function return () {

        /*$this->transaction->update([
            'status' => 'CANCELLED',
            'user_status' => 'cancelled'
        ]);*/

        return redirect($this->return);
    }

    /**
     * Handle the return URL.
     *
     * @return Response
     */
    public function cancel()
    {
        $this->transaction->update([
            'status' => 'CANCELLED',
            'user_status' => 'cancelled'
        ]);

        //Notify User
        $user = User::where('username',$this->transaction->username)->first();
        if(config('portal.email_notifications') && env('APP_ENV') != 'local' && $user && Permission::hasPermission('email-alerts','transaction-cancelled', $user->id)){
            //
            $user->notify(new TransactionCancelled($this->transaction));
        }
        return redirect($this->cancel);
    }

    public function converURLs($url)
    {
        return preg_replace('#{TOKEN}|{sessionId}|{token}#i', $this->transaction->original_token, $url);
    }
}
