<?php

namespace App\Http\Controllers\APIs;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Models\Centrix\Centrix;
use App\Http\Models\Liveness\Token as LivenessToken;
use App\Http\Models\Orbitsdk\AwsS3;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Transactions\Transactions;
use App\Http\Requests\Token\TokenV1 as Token;
use App\User;
use DateTime;
use Illuminate\Http\Request;

class ApiControllerV1 extends Controller
{
    /**
     * Check the trsansaction ID
     *
     * @param Token $request
     * @return \Illuminate\Http\Request
     */
    public function create(Token $request)
    {
        $token = Helper::generateToken();

        $transaction = new Transactions();

        $transaction->category  = 'remote-verification';
        $transaction->module    = 'identity-flow-self-guided';
        $transaction->reference = $request->reference ?? null;
        $transaction->username  = User::where('active', true)->where('role', 'admin')->first()->username ?? null;

        $transaction->transactionId  = Helper::generateTransactionId();
        $transaction->token          = $token;
        $transaction->original_token = $request->token;

        $transaction->user_status = 'waiting';

        if (isset($request->subcode) && !empty($request->subcode)) {
            $transaction->subcode = $request->subcode;
        }

        if (isset($request->ruleid) && !empty($request->ruleid)) {
            $transaction->ruleid = $request->ruleid;
        }

        $transaction->save();

        //$liveness = new LivenessToken();
        //$liveness->createSession( $token);

        //Store
        $url_inputs = [
            'return_url' => $request->return_url ?? route('mobileStatus', ['status' => 'completed']),
            'cancel_url' => $request->cancel_url ?? route('mobileStatus', ['status' => 'cancelled']),
        ];

        $expiry = config('portal.sms_expiry');

        if ($expiry < 1) {
            $expiry               = ceil(60 * $expiry);
            $url_inputs['expiry'] = now()->addMinutes($expiry);
        } else {
            $url_inputs['expiry'] = now()->addHours($expiry);
        }

        $transaction->urls()->create($url_inputs);

        //Store contact details
        if (isset($request->mobile_number) && !empty($request->mobile_number)) {
            $transaction->contactDetails()->create([
                'phone' => $request->mobile_number,
            ]);
        }

        return response()->json([
            'status'  => 'success',
            'session' => $token,
        ]);
    }

    /**
     * Check the trsansaction ID
     *
     * @param string $token
     * @return \Illuminate\Http\Request
     */
    public function check($token)
    {
        $session = Transactions::where([
            'token' => $token,
        ])->first();

        if (!$session) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Not found.',
            ], 404);
        }

        if ($session->status !== 'PENDING') {
            return response()->json([
                'status' => 'error',
                'msg'    => 'In progress.',
            ], 422);
        }

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Finds transaction by token
     *
     * @param string $token
     * @return Request
     */
    public function get($token)
    {
        $tr = Transactions::where('token', $token)->first();
        if (!$tr) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Transaction not found.',
            ], 404);
        }

        $total_time = $tr->created_at->diff($tr->updated_at)->i . 'min ' . $tr->created_at->diff($tr->updated_at)->s . 'sec';

        $result = [
            'status'     => $tr->status,
            'token'      => $tr->original_token,
            'flow_type'  => $tr->flow_type,
            'id_type'    => $tr->identify_type !== 'PASSPORT' ? 'driver_licence' : 'passport',
            'created_at' => [
                'UTC' => $tr->created_at->format('Y-m-d H:i:s'),
                'NZD' => $tr->created_at->timezone('Pacific/Auckland')->format('Y-m-d H:i:s'),
            ],
            'details'    => [
                'card'      => [],
                'personal'  => [],
                'extracted' => [],
                'contact'   => [],
                'face_scan' => [],
                'device'    => [],
            ],
        ];

        if ($tr->cards) {
            $asf = json_decode($tr->cards->asf, true);

            $result['details']['card'] = [
                'asf_check' => [
                    'document_authenticity' => isset($asf) && isset($asf['overall']) ? $asf['overall'] : false,
                    'result'                => $asf,
                ],
            ];

            if (env('APP_ENV') == 'local' || !config('aws.s3.enable')) {

                $result['details']['card']['images'] = [
                    'front'     => Utils::getFullURL($tr->token, 'images/cropped/front', $tr->cards->front_card),
                    'back'      => !empty($tr->cards->back_card) ? Utils::getFullURL($tr->token, 'images/cropped/back', $tr->cards->back_card) : null,
                    'face'      => Utils::getFullURL($tr->token, 'images/cropped/face', $tr->cards->face_photo),
                    'uncropped' => [
                        'front' => Utils::getFullURL($tr->token, 'images/uncropped/front', $tr->cards->front_card),
                        'back'  => !empty($tr->cards->back_card) ? Utils::getFullURL($tr->token, 'images/uncropped/back', $tr->cards->back_card) : null,
                    ],
                ];

            } else {

                $front = $tr->cards->front_card;
                $back  = $tr->cards->back_card;
                $face  = $tr->cards->face_photo;

                $frontFull = str_replace('/crop/', '/', $front);
                $backFull  = ($back) ? str_replace('/crop/', '/', $back) : null;

                $result['details']['card']['images'] = [
                    'front'     => strpos($front, 'IMG') !== false ? AwsS3::getObject($front) : Utils::getFullURL($tr->token, 'images/cropped/front', $front),
                    'face'      => strpos($face, 'IMG') !== false ? AwsS3::getObject($face) : Utils::getFullURL($tr->token, 'images/cropped/face', $face),
                    'uncropped' => [
                        'front' => strpos($front, 'IMG') !== false ? AwsS3::getObject($frontFull) : Utils::getFullURL($tr->token, 'images/uncropped/front', $front),
                    ],
                ];

                if (!empty($tr->cards->back_card)) {
                    $result['details']['card']['images']['back']              = strpos($back, 'IMG') !== false ? AwsS3::getObject($back) : Utils::getFullURL($tr->token, 'images/cropped/back', $back);
                    $result['details']['card']['images']['uncropped']['back'] = strpos($back, 'IMG') !== false ? AwsS3::getObject($backFull) : Utils::getFullURL($tr->token, 'images/uncropped/back', $back);
                } else {
                    $result['details']['card']['images']['back']              = null;
                    $result['details']['card']['images']['uncropped']['back'] = null;
                }
            }
        }

        if ($tr->extracted) {
            $result['details']['extracted'] = $tr->extracted;

            $ed = $result['details']['extracted'];
            if (isset($ed['dateOfBirth']) && !empty($ed['dateOfBirth'])) {
                $result['details']['extracted']['dateOfBirth'] = date('Y-m-d', strtotime($ed['dateOfBirth']));
            }

            if (isset($ed['expiryDate']) && !empty($ed['expiryDate'])) {
                $result['details']['extracted']['expiryDate'] = date('Y-m-d', strtotime($ed['expiryDate']));
            }

        }

        if ($tr->userDetails) {
            $result['details']['personal'] = $tr->userDetails;

            $expired = false;
            if (!empty($result['details']['personal']['dateOfExpiry'])) {
                $now     = new DateTime();
                $date    = new DateTime($result['details']['personal']['dateOfExpiry']);
                $expired = $now > $date;
            }

            $result['details']['personal']['document_expired'] = $expired;

            $age = 'unknown';
            if (!empty($result['details']['personal']['dateOfBirth'])) {
                $now  = new DateTime();
                $date = new DateTime($result['details']['personal']['dateOfBirth']);
                $age  = $now->diff($date)->y;
            }
            $result['details']['personal']['age'] = $age;
        }

        if ($tr->contactDetails) {
            $result['details']['contact'] = $tr->contactDetails;

            if (Helper::isJson($result['details']['contact']['residential_address'])) {
                $result['details']['contact']['residential_address'] = json_decode($result['details']['contact']['residential_address'], true)['fullAddress'];
            }
        }

        if ($tr->info) {
            $result['details']['device']        = $tr->info;
            $result['details']['device']['vpn'] = Centrix::boolean($result['details']['device']['vpn']);
        }

        if ($tr->faceScan) {
            if ($tr->flow_type !== 'MAIN') {
                $result['details']['face_scan'] = [
                    'confidence' => Centrix::boolean($tr->faceScan->confidence),
                    'features'   => json_decode($tr->faceScan->features, true),
                ];

                if (env('APP_ENV') == 'local' || !config('aws.s3.enable')) {

                    $result['details']['face_scan']['selfies'] = [
                        'face'      => Utils::getFullURL($tr->token, 'selfie/face', $tr->faceScan->video),
                        'turn_head' => Utils::getFullURL($tr->token, 'selfie/turn_head', $tr->faceScan->face),
                    ];

                } else {
                    $result['details']['face_scan']['selfies'] = [
                        'face'      => strpos($tr->faceScan->video, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->video) : Utils::getFullURL($tr->token, 'selfie/face', $tr->faceScan->video),
                        'turn_head' => strpos($tr->faceScan->face, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->face) : Utils::getFullURL($tr->token, 'selfie/turn_head', $tr->faceScan->face),
                    ];
                }

            } else {
                $liveness = $tr->liveness()->limit(2)->get();
                if (count($liveness)) {
                    $liveness = [
                        'result' => Centrix::boolean($liveness[count($liveness) - 1]->result),
                    ];

                    foreach ($tr->liveness()->limit(2)->get() as $i => $live) {
                        $liveness['attempt_' . ($i + 1)]['result']     = Centrix::boolean($live->result);
                        $liveness['attempt_' . ($i + 1)]['spoof_risk'] = Centrix::boolean($live->spoof_risk);
                        $liveness['attempt_' . ($i + 1)]['actions']    = $live->actions;

                        if (env('APP_ENV') == 'local' || !config('aws.s3.enable')) {
                            $liveness['attempt_' . ($i + 1)]['video'] = Utils::getFullURL($tr->token, 'videos/' . $live->id, $live->video);
                        } else {
                            $liveness['attempt_' . ($i + 1)]['video'] = strpos($live->video, 'IMG') !== false ? AwsS3::getObject($live->video) : Utils::getFullURL($tr->token, 'videos/' . $live->id, $live->video);
                        }
                    }
                } else {
                    $liveness = [];
                }

                $result['details']['face_scan'] = [
                    'confidence' => Centrix::boolean($tr->faceScan->confidence),
                    'liveness'   => $liveness,
                    'features'   => json_decode($tr->faceScan->features, true),
                ];

                if (env('APP_ENV') == 'local' || !config('aws.s3.enable')) {
                    $result['details']['face_scan']['video'] = Utils::getFullURL($tr->token, 'videos', $tr->faceScan->video);
                    $result['details']['face_scan']['face']  = Utils::getFullURL($tr->token, 'videos/face', $tr->faceScan->face);
                } else {
                    $result['details']['face_scan']['video'] = strpos($tr->faceScan->video, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->video) : Utils::getFullURL($tr->token, 'videos', $tr->faceScan->video);
                    $result['details']['face_scan']['face']  = strpos($tr->faceScan->face, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->face) : Utils::getFullURL($tr->token, 'videos/face', $tr->faceScan->face);
                }
            }
        }

        if (config('app.verification_api') == 'centrix') {

            if (Helper::hasCentrix($tr->country)) {
                $result['details']['centrix'] = [
                    'provider'  => 'centrix',
                    'type'      => $tr->country == 'AU' ? 'IDDocumentAU' : 'SmartID',
                    'reference' => $token,
                    'data_sets' => [],
                ];

                $dataSets = $tr->centrixDataSets;

                if ($tr->country == 'AU') {
                    $ResponseDetails                                  = strtolower(preg_replace('/([a-z]+)([A-Z]+)/', '$1_$2', $tr->centrixPep->ResponseDetails));
                    $result['details']['centrix']['response_details'] = json_decode($ResponseDetails, true);
                    $result['details']['centrix']['data_sets']        = [
                        'consumer_file'                            => Centrix::boolean($dataSets->consumer_file),
                        'driver_licence_verification'              => Centrix::boolean($dataSets->driver_licence_verification),
                        'known_names'                              => Centrix::boolean($dataSets->known_names),
                        'known_addresses'                          => Centrix::boolean($dataSets->known_addresses),
                        'previous_enquiries'                       => Centrix::boolean($dataSets->previous_enquiries),
                        'defaults'                                 => Centrix::boolean($dataSets->defaults),
                        'judgments'                                => Centrix::boolean($dataSets->judgments),
                        'insolvencies'                             => Centrix::boolean($dataSets->insolvencies),
                        'company_affiliations'                     => Centrix::boolean($dataSets->company_affiliations),
                        'file_notes'                               => Centrix::boolean($dataSets->file_notes),
                        'fines_data_list'                          => Centrix::boolean($dataSets->fines_data_list),
                        'name_only_insolvencies'                   => Centrix::boolean($dataSets->name_only_insolvencies),
                        'payment_history'                          => Centrix::boolean($dataSets->payment_history),
                        'red_arrears_month_list'                   => Centrix::boolean($dataSets->red_arrears_month_list),
                        'property_ownership'                       => Centrix::boolean($dataSets->property_ownership),
                        'client_keys'                              => Centrix::boolean($dataSets->client_keys),
                        'identity_verification_data'               => Centrix::boolean($dataSets->identity_verification_data),
                        'bureau_header_data_identity_verification' => Centrix::boolean($dataSets->bureau_header_data_identity_verification),
                        'summary_items'                            => Centrix::boolean($dataSets->summary_items),
                        'extra_data_items'                         => Centrix::boolean($dataSets->extra_data_items),
                        'application_decision'                     => Centrix::boolean($dataSets->application_decision),
                        'international_idau'                       => [
                            'document_au_data' => [],
                        ],
                    ];
                } else {
                    $result['details']['centrix']['data_sets'] = [];
                }
            }

            if ($tr->centrixLicence && Helper::hasCentrix($tr->country)) {
                if ($tr->country == 'AU') {
                    $result['details']['centrix']['data_sets']['international_idau']['document_au_data'] = [
                        'driver_licence_verification_au' => [
                            'is_success'              => Centrix::boolean($tr->centrixLicence->IsSuccess),
                            'driver_licence_verified' => Centrix::boolean($tr->centrixLicence->IsDriverLicenceVerified),
                        ],
                    ];
                } else {
                    $result['details']['centrix']['data_sets']['driver_licence'] = [
                        'is_success'              => Centrix::boolean($tr->centrixLicence->IsSuccess),
                        'driver_licence_verified' => Centrix::boolean($tr->centrixLicence->IsDriverLicenceVerified),
                        'is_firstname_matched'    => Centrix::boolean($tr->centrixLicence->IsDriverLicenceFirstNameMatched),
                        'is_lastname_matched'     => Centrix::boolean($tr->centrixLicence->IsDriverLicenceLastNameMatched),
                        'is_middlename_matched'   => Centrix::boolean($tr->centrixLicence->IsDriverLicenceMiddleNameMatched),
                        'is_dateofbirth_matched'  => Centrix::boolean($tr->centrixLicence->IsDriverLicenceDateOfBirthMatched),
                        'is_verified_and_matched' => Centrix::boolean($tr->centrixLicence->IsDriverLicenceVerifiedAndMatched),
                    ];
                }
            }

            if ($tr->centrixPassport && Helper::hasCentrix($tr->country)) {
                if ($tr->country == 'AU') {
                    $result['details']['centrix']['data_sets']['international_idau']['document_au_data'] = [
                        'passport_verification_au' => [
                            'is_success'        => Centrix::boolean($tr->centrixPassport->IsSuccess),
                            'passport_verified' => Centrix::boolean($tr->centrixPassport->IsPassportVerified),
                        ],
                    ];
                } else {
                    $result['details']['centrix']['data_sets']['passport'] = [
                        'is_verified' => Centrix::boolean($tr->centrixPassport->IsPassportVerified),
                        'is_success'  => Centrix::boolean($tr->centrixPassport->IsSuccess),
                    ];
                }
            }

            if ($tr->centrixPep && Helper::hasCentrix($tr->country)) {
                $result['details']['centrix']['data_sets']['pep_watchlist'] = [
                    'is_success'                       => Centrix::boolean($tr->centrixPep->IsSuccess),
                    'international_watchlist_is_clear' => Centrix::boolean($tr->centrixPep->InternationalWatchlistIsClear),
                ];
            }

            if ($tr->centrixSmartId && $tr->centrixSmartId->ResponseDetails && Helper::hasCentrix($tr->country) && config('centrix.centrixId')) {
                $result['details']['centrix']['ResponseMeta'] = json_decode($tr->centrixSmartId->ResponseDetails, true);
            }

            if ($tr->centrixSmartId && Helper::hasCentrix($tr->country)) {
                $result['details']['centrix']['overall']               = Centrix::boolean($tr->centrixSmartId->IsVerified);
                $result['details']['centrix']['data_sets']['smart_id'] = [
                    'is_verified'             => Centrix::boolean($tr->centrixSmartId->IsVerified),
                    'is_name_verified'        => Centrix::boolean($tr->centrixSmartId->IsNameVerified),
                    'is_dateofbirth_verified' => Centrix::boolean($tr->centrixSmartId->IsDateOfBirthVerified),
                    'is_address_verified'     => Centrix::boolean($tr->centrixSmartId->IsAddressVerified),
                ];
            }

            if ($tr->centrixSmartId && Helper::hasCentrix($tr->country) && $tr->centrixSmartId->first()->data) {
                foreach ($tr->centrixSmartId()->first()->data as $data) {
                    $result['details']['centrix']['data_sets']['smart_id_data'][] = [
                        'name'                     => $data->DataSourceName,
                        'description'              => $data->DataSourceDescription,
                        'name_match_status'        => $data->NameMatchStatus,
                        'dateofbirth_match_status' => $data->DateOfBirthMatchStatus,
                        'address_match_status'     => $data->AddressMatchStatus,
                    ];
                }
            }

            if ($tr->centrixResidential && Helper::hasCentrix($tr->country)) {
                $IDR = $tr->centrixResidential()->first();

                $result['details']['centrix']['data_sets']['residential'] = [
                    'is_success'                            => Centrix::boolean($IDR['IsSuccess']),
                    'message'                               => $IDR['Message'],
                    'residential_verified'                  => Centrix::boolean($IDR['ResidentialVerified']),
                    'third_party_match_type'                => $IDR['ThirdPartyMatchType'],
                    'third_party_match_type_description'    => $IDR['ThirdPartyMatchTypeDescription'],
                    'surname'                               => $IDR['Surname'],
                    'first_name'                            => $IDR['FirstName'],
                    'middle_name'                           => $IDR['MiddleName'],
                    'date_of_birth'                         => $IDR['DateOfBirth'],
                    'third_party_name_match'                => $IDR['ThirdPartyNameMatch'],
                    'third_party_name_match_description'    => $IDR['ThirdPartyNameMatchDescription'],
                    'surname_match'                         => Centrix::boolean($IDR['SurnameMatch']),
                    'first_name_match'                      => Centrix::boolean($IDR['FirstNameMatch']),
                    'first_name_initial_match'              => Centrix::boolean($IDR['FirstNameInitialMatch']),
                    'middle_name_match'                     => Centrix::boolean($IDR['MiddleNameMatch']),
                    'middle_name_initial_match'             => Centrix::boolean($IDR['MiddleNameInitialMatch']),
                    'date_of_birth_match'                   => Centrix::boolean($IDR['DateOfBirthMatch']),
                    'third_party_address_match'             => $IDR['ThirdPartyAddressMatch'],
                    'third_party_address_match_description' => $IDR['ThirdPartyAddressMatchDescription'],
                    'address_line1'                         => $IDR['AddressLine1'],
                    'suburb'                                => $IDR['Suburb'],
                    'state'                                 => $IDR['State'],
                    'postcode'                              => $IDR['Postcode'],
                    'delivery_point_id'                     => $IDR['DeliveryPointID'],
                    'unit_no_match'                         => Centrix::boolean($IDR['UnitNoMatch']),
                    'street_no_match'                       => Centrix::boolean($IDR['StreetNoMatch']),
                    'street_name_match'                     => Centrix::boolean($IDR['StreetNameMatch']),
                    'street_type_match'                     => Centrix::boolean($IDR['StreetTypeMatch']),
                    'suburb_match'                          => Centrix::boolean($IDR['SuburbMatch']),
                    'state_match'                           => Centrix::boolean($IDR['StateMatch']),
                    'postcode_match'                        => Centrix::boolean($IDR['PostcodeMatch']),
                    'home_phone_no'                         => $IDR['HomePhoneNo'],
                    'mobile_phone_no'                       => $IDR['MobilePhoneNo'],
                    'phone_number_match'                    => Centrix::boolean($IDR['PhoneNumberMatch']),
                    'mobile_number_match'                   => Centrix::boolean($IDR['MobileNumberMatch']),
                    'email_address'                         => $IDR['EmailAddress'],
                ];
            }
        }

        if (config('app.verification_api') == 'greenid') {
            $result['details']['greenid'] = [
                'registrationDetails' => [],
                'sourceList'          => [
                    'source' => [],
                ],
                'verificationResult'  => [],
            ];

            if ($tr->gIDRegistrationDetails) {
                $rDetails = $tr->gIDRegistrationDetails;
                $dob      = $rDetails->dob ?? '0000-0-00';
                if ($dob) {
                    $dob = explode('-', $dob);
                }
                $result['details']['greenid']['registrationDetails'] = [
                    'currentResidentialAddress' => [
                        'city'       => $rDetails->city,
                        'country'    => $rDetails->country,
                        'streetType' => $rDetails->streetType,
                        'suburb'     => $rDetails->suburb,
                    ],
                    'dateCreated'               => $rDetails->dateCreated,
                    'dob'                       => [
                        'day'   => $dob[2],
                        'month' => $dob[1],
                        'year'  => $dob[0],
                    ],
                    'name'                      => [
                        'givenName'   => $rDetails->givenName,
                        'middleNames' => $rDetails->middleNames,
                        'surname'     => $rDetails->surname,
                    ],
                ];
            }

            if ($tr->gIDSourceList) {
                $sourceList                                           = $tr->gIDSourceList;
                $result['details']['greenid']['sourceList']['source'] = $sourceList;
            }

            if ($tr->gIDVerificationResult) {
                $vResult = $tr->gIDVerificationResult;

                $result['details']['greenid']['verificationResult'] = [
                    'individualResult'          => [],
                    'overallVerificationStatus' => $vResult->overallVerificationStatus,
                    'ruleId'                    => $vResult->ruleId,
                    'verificationId'            => $vResult->verificationId,
                ];

                if ($tr->gIDVerificationResult->individual) {
                    $individual = $tr->gIDVerificationResult->individual;

                    $result['details']['greenid']['verificationResult']['individualResult'] = $individual;
                }
            }
        }

        return response()->json($result);
    }

    /**
     * Delete transaction by token
     *
     * @param string $token
     * @return Request
     */
    public function delete($token)
    {
        $tr = Transactions::where('token', $token)->first();
        if (!$tr) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Transaction not found.',
            ], 404);
        }

        $tr->forceDelete();
    }
}
