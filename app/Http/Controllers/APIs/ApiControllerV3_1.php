<?php

namespace App\Http\Controllers\APIs;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SMS\SMSController;
use App\Http\Models\Centrix\Centrix;
use App\Http\Models\Orbitsdk\AwsS3;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Transactions\Transactions;
use App\Http\Requests\Token\TokenV3_1 as Token;
use App\Http\Models\Liveness\Token as LivenessToken;
use App\Http\Requests\SMS\SMS as SMSRequest;

use App\User;
use DateTime;
use Illuminate\Http\Request;

class ApiControllerV3_1 extends Controller
{
    /**
     * Send an SMS message.
     *
     * @param SMSRequest $request
     * @return array
     */
    public function generate(SMSRequest $request)
    {
        $transactionId = $request->transactionId;
        //check if the token already exists
        if (Helper::originalTokenExists($transactionId)) {
            return response()->json([
                'status' => 'error',
                'session' => 'This Transaction Id already exists.',
            ]);
        }

        $code = str_replace('+', '', $request->phone_code);
        $number = preg_replace('/\s/i', '', $request->phone_number);
        $token = Helper::generateToken();

        // Remove zero from mobile number
        if (preg_match('/^0/', $number)) {
            $number = substr($number, 1, strlen($number));
        }

        $transaction = new Transactions();
        $transaction->category = 'remote-verification';
        $transaction->module = 'identity-flow-self-guided';
        $transaction->reference = $request->reference ?? null;
        $transaction->username = User::where('active', true)->where('role', 'admin')->first()->username ?? null;
        $transaction->transactionId = Helper::generateTransactionId();
        $transaction->token = $token;
        $transaction->original_token = $transactionId;
        $transaction->user_status = 'waiting';

        if (isset($request->subcode) && !empty($request->subcode)) {
            $transaction->subcode = $request->subcode;
        }

        $transaction->save();

        //$liveness = new LivenessToken();
        //$liveness->createSession( $token);

        //Store URL
        $url_inputs = [
            'return_url' => $request->return_url ?? route('mobileStatus', [ 'status' => 'completed']),
            'cancel_url' => $request->cancel_url ?? route('mobileStatus', [ 'status' => 'cancelled']),
        ];

        $expiry = config('portal.sms_expiry');

        if($expiry < 1){
            $expiry = ceil(60 * $expiry);
            $url_inputs['expiry'] = now()->addMinutes($expiry);
        }else{
            $url_inputs['expiry'] = now()->addHours($expiry);
        }

        $transaction->urls()->create($url_inputs);

        $fullNumber = $code . $number;

        //Store contact details
        $transaction->contactDetails()->create([
            'name' => $request->name ?? null,
            'phone' => $fullNumber ?? null,
        ]);

        // Finally send the message
        $url = secure_url('/') . "/" . $token;
        $smController = new SMSController();
        $msg = $smController->smsText($url, $request->name, $request->reference);

        $res = [
            'status' => ($smController->send($msg, $fullNumber)) ? 'success' : 'error',
        ];

        if(env('APP_ENV') === 'local'){
            $res['debug'] = $url;
        }

        return $res;

    }

    /**
     * Check the trsansaction ID
     *
     * @param Token $request
     * @return \Illuminate\Http\Request
     */
    public function create(Token $request)
    {
        $transactionId = $request->transactionId;
        if(Helper::transactionIdExists($transactionId)){
            return response()->json([
                'status'  => 'error',
                'session' => 'This Transaction Id already exists.',
            ]);
        }
        $token = Helper::generateToken();

        $transaction                 = new Transactions();

        $transaction->category  = 'remote-verification';
        $transaction->module  = 'identity-flow-self-guided';
        $transaction->reference = $request->reference ?? null;
        $transaction->username = User::where('active', true )->where('role', 'admin')->first()->username ?? null;


        $transaction->transactionId  = Helper::generateTransactionId();
        $transaction->token          = $token;
        $transaction->original_token = $transactionId;

        $transaction->user_status = 'waiting';
        if (isset($request->subcode) && !empty($request->subcode)) {
            $transaction->subcode = $request->subcode;
        }

        $transaction->save();

        //$liveness = new LivenessToken();
        //$liveness->createSession( $transaction->token);
        //Store
        $url_inputs = [
            'return_url' => $request->return_url ?? route('mobileStatus', [ 'status' => 'completed']),
            'cancel_url' => $request->cancel_url ?? route('mobileStatus', [ 'status' => 'cancelled']),
        ];

        $expiry = config('portal.sms_expiry');

        if($expiry < 1){
            $expiry = ceil(60 * $expiry);
            $url_inputs['expiry'] = now()->addMinutes($expiry);
        }else{
            $url_inputs['expiry'] = now()->addHours($expiry);
        }

        $transaction->urls()->create($url_inputs);

        //Store contact details
        if($request->name){
            $transaction->contactDetails()->create([
                'name' => $request->name ?? null,
            ]);
        }
        return response()->json([
            'status'  => 'success',
            'session' => $token,
        ]);
    }

    /**
     * Check the trsansaction ID
     *
     * @param string $token
     * @return \Illuminate\Http\Request
     */
    public function check($token)
    {
        $session = Transactions::where([
            'token' => $token,
        ])->orderBy('created_at','desc')->first();

        if (!$session) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Not found.',
            ], 404);
        }

        if ($session->status !== 'PENDING') {
            return response()->json([
                'status' => 'error',
                'msg'    => 'In progress.',
            ], 422);
        }

        return response()->json([
            'status' => 'success',
        ]);
    }

    /**
     * Finds transaction by token
     *
     * @param string $token
     * @return Request
     */
    public function get($token)
    {
        $tr = Transactions::where('original_token', $token)->orderBy('created_at','desc')->first();
        if (!$tr) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Transaction not found.',
            ], 404);
        }

        $total_time = $tr->created_at->diff($tr->updated_at)->i . 'min ' . $tr->created_at->diff($tr->updated_at)->s . 'sec';

        $result = [
            'status'     => $tr->status,
            'token'      => $tr->original_token,
            'flow_type'  => $tr->flow_type,
            'id_type'    => $tr->identify_type !== 'PASSPORT' ? 'driver_licence' : 'passport',
            'created_at' => $tr->created_at->format('d/m/Y H:i A'),
            'completed_at' =>  $tr->completed_at ? $tr->completed_at->format('d/m/Y H:i A') : null,
            'details'    => [
                'card'      => [],
                'personal'  => [],
                'extracted' => [],
                'contact'   => [],
                'face_scan' => [],
                'device'    => [],
            ],
        ];

        if(Helper::hasCentrix($tr->country)){
            $result['details']['centrix'] = [
                'provider'  => 'centrix',
                'type'      => $tr->country == 'AU' ? 'IDDocumentAU' : 'SmartID',
                'overall'   => false,
                'reference' => $token,
                'result'    => [],
            ];
        }


        if ($tr->cards) {
            $asf                       = json_decode($tr->cards->asf, true);

            $Asf = [];
            if(isset($asf) && isset($asf['overall'])){
                $n = 0;
                foreach ($asf as $k => $v){
                    if($k != 'overall'){
                        $Asf['check_'.$n] = $v;
                    }else{
                        $Asf[$k] = $v;
                    }

                    $n++;
                }
            }

            $result['details']['card'] = [
                'asf_check' => [
                    'document_authenticity' => isset($asf) && isset($asf['overall']) ? $asf['overall'] : false,
                    'result'                => $Asf,
                ],
                'edited' => (!empty($edited)) ? explode(',', $edited) : [],
                'id_capture_count' => $tr->sessions()->whereIn('type', ['NZL_DRIVERLICENCE','AUS_AUTO_DRIVERLICENCE','PASSPORT'])->count()
            ];

            if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){

                $result['details']['card']['images'] = [
                    'front'     => Utils::getFullURL($tr->token, 'images/cropped/front', $tr->cards->front_card),
                    'back'      => !empty($tr->cards->back_card) ? Utils::getFullURL($tr->token, 'images/cropped/back', $tr->cards->back_card) : null,
                    'face'      => Utils::getFullURL($tr->token, 'images/cropped/face', $tr->cards->face_photo),
                    'uncropped' => [
                        'front' => Utils::getFullURL($tr->token, 'images/uncropped/front', $tr->cards->front_card),
                        'back'  => !empty($tr->cards->back_card) ? Utils::getFullURL($tr->token, 'images/uncropped/back', $tr->cards->back_card) : null,
                    ],
                ];

            }else{

                $front = $tr->cards->front_card;
                $back  = $tr->cards->back_card;
                $face = $tr->cards->face_photo;

                $frontFull = str_replace('/crop/','/', $front);
                $backFull = ($back) ? str_replace('/crop/','/', $back): null;

                $result['details']['card']['images'] = [
                    'front'     => strpos($front, 'IMG') !== false ? AwsS3::getObject($front) : Utils::getFullURL($tr->token, 'images/cropped/front', $front),
                    'face'      => strpos($face, 'IMG') !== false ? AwsS3::getObject($face) : Utils::getFullURL($tr->token, 'images/cropped/face', $face),
                    'uncropped' => [
                        'front' => strpos($front, 'IMG') !== false ? AwsS3::getObject($frontFull) : Utils::getFullURL($tr->token, 'images/uncropped/front', $front),
                    ],
                ];

                if(!empty($tr->cards->back_card)){
                    $result['details']['card']['images']['back'] = strpos($back, 'IMG') !== false ? AwsS3::getObject($back) : Utils::getFullURL($tr->token, 'images/cropped/back', $back);
                    $result['details']['card']['images']['uncropped']['back'] = strpos($back, 'IMG') !== false ? AwsS3::getObject($backFull) : Utils::getFullURL($tr->token, 'images/uncropped/back', $back);
                }else{
                    $result['details']['card']['images']['back'] = null;
                    $result['details']['card']['images']['uncropped']['back'] = null;
                }
            }

        }

        if ($tr->extracted) {
            $result['details']['extracted'] = $tr->extracted;

            $ed = $result['details']['extracted'];
            if (isset($ed['dateOfBirth']) && !empty($ed['dateOfBirth'])) {
                $result['details']['extracted']['dateOfBirth'] = date('Y-m-d', strtotime($ed['dateOfBirth']));
            }

            if (isset($ed['expiryDate']) && !empty($ed['expiryDate'])) {
                $result['details']['extracted']['expiryDate'] = date('Y-m-d', strtotime($ed['expiryDate']));
            }

        }

        if ($tr->userDetails) {
            $result['details']['personal'] = $tr->userDetails;

            $expired = false;
            if (!empty($result['details']['personal']['dateOfExpiry'])) {
                $now     = new DateTime();
                $date    = new DateTime($result['details']['personal']['dateOfExpiry']);
                $expired = $now > $date;
            }

            $result['details']['personal']['document_expired'] = $expired;

            $age = 'unknown';
            if (!empty($result['details']['personal']['dateOfBirth'])) {
                $now  = new DateTime();
                $date = new DateTime($result['details']['personal']['dateOfBirth']);
                $age  = $now->diff($date)->y;
            }
            $result['details']['personal']['age'] = $age;
        }

        if ($tr->contactDetails) {
            $result['details']['contact'] = $tr->contactDetails;

            if(Helper::isJson($result['details']['contact']['residential_address'])){
                $result['details']['contact']['residential_address'] = json_decode($result['details']['contact']['residential_address'], true)['fullAddress'];
            }
        }

        if ($tr->info) {
            $result['details']['device']        = $tr->info;
            $result['details']['device']['vpn'] = Centrix::boolean($result['details']['device']['vpn']);
        }

        if ($tr->faceScan) {
            if ($tr->flow_type !== 'MAIN') {
                $result['details']['face_scan'] = [
                    'confidence' => Centrix::boolean($tr->faceScan->confidence),
                    'features'   => json_decode($tr->faceScan->features, true),
                ];

                if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){

                    $result['details']['face_scan']['selfies'] = [
                        'face'      => Utils::getFullURL($tr->token, 'selfie/face', $tr->faceScan->video),
                        'turn_head' => Utils::getFullURL($tr->token, 'selfie/turn_head', $tr->faceScan->face),
                    ];

                }else{
                    $result['details']['face_scan']['selfies'] = [
                        'face'      => strpos($tr->faceScan->video, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->video) : Utils::getFullURL($tr->token, 'selfie/face', $tr->faceScan->video),
                        'turn_head' => strpos($tr->faceScan->face, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->face) : Utils::getFullURL($tr->token, 'selfie/turn_head', $tr->faceScan->face),
                    ];
                }


            } else {
                $liveness = $tr->liveness()->limit(2)->get();
                $liveness = [
                    'result' => Centrix::boolean($liveness[count($liveness) - 1]->result),
                ];

                foreach ($tr->liveness()->limit(2)->get() as $i => $live) {
                    $liveness['attempt_' . ($i + 1)]['result']     = Centrix::boolean($live->result);
                    $liveness['attempt_' . ($i + 1)]['spoof_risk'] = Centrix::boolean($live->spoof_risk);
                    $liveness['attempt_' . ($i + 1)]['actions']    = $live->actions;

                    if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){
                        $liveness['attempt_' . ($i + 1)]['video'] = Utils::getFullURL($tr->token, 'videos/' . $live->id, $live->video);
                    }else{
                        $liveness['attempt_' . ($i + 1)]['video']  = strpos($live->video, 'IMG') !== false ? AwsS3::getObject($live->video) : Utils::getFullURL($tr->token, 'videos/' . $live->id, $live->video);
                    }

                }

                $result['details']['face_scan'] = [
                    'confidence' => Centrix::boolean($tr->faceScan->confidence),
                    'liveness'   => $liveness,
                    'features'   => json_decode($tr->faceScan->features, true),
                ];

                if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){
                    $result['details']['face_scan']['video'] = Utils::getFullURL($tr->token, 'videos', $tr->faceScan->video);
                    $result['details']['face_scan']['face'] = Utils::getFullURL($tr->token, 'videos/face', $tr->faceScan->face);
                }else{
                    $result['details']['face_scan']['video'] = strpos($tr->faceScan->video, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->video) : Utils::getFullURL($tr->token, 'videos', $tr->faceScan->video);
                    $result['details']['face_scan']['face'] = strpos($tr->faceScan->face, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->face) : Utils::getFullURL($tr->token, 'videos/face', $tr->faceScan->face);
                }

            }
        }

        if ($tr->country == 'AU' && Helper::hasCentrix($tr->country)) {
            $overall = false;
            if ($tr->centrixLicence) {
                $overall = Centrix::boolean($tr->centrixLicence->IsDriverLicenceVerified);
            } else if ($tr->centrixPassport) {
                $overall = Centrix::boolean($tr->centrixPassport->IsPassportVerified);
            }
            $result['details']['centrix']['overall'] = $overall;
        }

        if ($tr->centrixLicence && Helper::hasCentrix($tr->country)) {
            $result['details']['centrix']['result']['driver_licence'] = [
                'is_verified'             => Centrix::boolean($tr->centrixLicence->IsDriverLicenceVerified),
                'is_success'              => Centrix::boolean($tr->centrixLicence->IsSuccess),
                'is_firstname_matched'    => Centrix::boolean($tr->centrixLicence->IsDriverLicenceFirstNameMatched),
                'is_lastname_matched'     => Centrix::boolean($tr->centrixLicence->IsDriverLicenceLastNameMatched),
                'is_middlename_matched'   => Centrix::boolean($tr->centrixLicence->IsDriverLicenceMiddleNameMatched),
                'is_dateofbirth_matched'  => Centrix::boolean($tr->centrixLicence->IsDriverLicenceDateOfBirthMatched),
                'is_verified_and_matched' => Centrix::boolean($tr->centrixLicence->IsDriverLicenceVerifiedAndMatched),
            ];
        }

        if ($tr->centrixPassport && Helper::hasCentrix($tr->country)) {
            $result['details']['centrix']['result']['passport'] = [
                'is_verified' => Centrix::boolean($tr->centrixPassport->IsPassportVerified),
                'is_success'  => Centrix::boolean($tr->centrixPassport->IsSuccess),
            ];
        }

        if ($tr->centrixPep && Helper::hasCentrix($tr->country)) {
            $result['details']['centrix']['result']['pep_watchlist'] = [
                'is_success'                       => Centrix::boolean($tr->centrixPep->IsSuccess),
                'international_watchlist_is_clear' => Centrix::boolean($tr->centrixPep->InternationalWatchlistIsClear),
            ];
        }

        if ($tr->centrixPep && $tr->centrixPep->ResponseDetails && Helper::hasCentrix($tr->country) && config('centrix.centrixId')) {
            $result['details']['centrix']['ResponseMeta'] = json_decode($tr->centrixPep->ResponseDetails, true);
        }

        if ($tr->centrixSmartId && Helper::hasCentrix($tr->country)) {
            $result['details']['centrix']['overall']            = Centrix::boolean($tr->centrixSmartId->IsVerified);
            $result['details']['centrix']['result']['smart_id'] = [
                'is_verified'             => Centrix::boolean($tr->centrixSmartId->IsVerified),
                'is_name_verified'        => Centrix::boolean($tr->centrixSmartId->IsNameVerified),
                'is_dateofbirth_verified' => Centrix::boolean($tr->centrixSmartId->IsDateOfBirthVerified),
                'is_address_verified'     => Centrix::boolean($tr->centrixSmartId->IsAddressVerified),
            ];
        }

        if ($tr->centrixSmartId && Helper::hasCentrix($tr->country) && $tr->centrixSmartId->first()->data) {
            foreach ($tr->centrixSmartId()->first()->data as $data) {
                $result['details']['centrix']['result']['smart_id_data'][] = [
                    'name'                     => $data->DataSourceName,
                    'description'              => $data->DataSourceDescription,
                    'name_match_status'        => $data->NameMatchStatus,
                    'dateofbirth_match_status' => $data->DateOfBirthMatchStatus,
                    'address_match_status'     => $data->AddressMatchStatus,
                ];
            }
        }

        if ($tr->centrixResidential && Helper::hasCentrix($tr->country)) {
            $IDR = $tr->centrixResidential()->first();

            $result['details']['centrix']['result']['residential'] = [
                'is_success'                            => Centrix::boolean($IDR['IsSuccess']),
                'message'                               => $IDR['Message'],
                'residential_verified'                  => Centrix::boolean($IDR['ResidentialVerified']),
                'third_party_match_type'                => $IDR['ThirdPartyMatchType'],
                'third_party_match_type_description'    => $IDR['ThirdPartyMatchTypeDescription'],
                'surname'                               => $IDR['Surname'],
                'first_name'                            => $IDR['FirstName'],
                'middle_name'                           => $IDR['MiddleName'],
                'date_of_birth'                         => $IDR['DateOfBirth'],
                'third_party_name_match'                => $IDR['ThirdPartyNameMatch'],
                'third_party_name_match_description'    => $IDR['ThirdPartyNameMatchDescription'],
                'surname_match'                         => Centrix::boolean($IDR['SurnameMatch']),
                'first_name_match'                      => Centrix::boolean($IDR['FirstNameMatch']),
                'first_name_initial_match'              => Centrix::boolean($IDR['FirstNameInitialMatch']),
                'middle_name_match'                     => Centrix::boolean($IDR['MiddleNameMatch']),
                'middle_name_initial_match'             => Centrix::boolean($IDR['MiddleNameInitialMatch']),
                'date_of_birth_match'                   => Centrix::boolean($IDR['DateOfBirthMatch']),
                'third_party_address_match'             => $IDR['ThirdPartyAddressMatch'],
                'third_party_address_match_description' => $IDR['ThirdPartyAddressMatchDescription'],
                'address_line1'                         => $IDR['AddressLine1'],
                'suburb'                                => $IDR['Suburb'],
                'state'                                 => $IDR['State'],
                'postcode'                              => $IDR['Postcode'],
                'delivery_point_id'                     => $IDR['DeliveryPointID'],
                'unit_no_match'                         => Centrix::boolean($IDR['UnitNoMatch']),
                'street_no_match'                       => Centrix::boolean($IDR['StreetNoMatch']),
                'street_name_match'                     => Centrix::boolean($IDR['StreetNameMatch']),
                'street_type_match'                     => Centrix::boolean($IDR['StreetTypeMatch']),
                'suburb_match'                          => Centrix::boolean($IDR['SuburbMatch']),
                'state_match'                           => Centrix::boolean($IDR['StateMatch']),
                'postcode_match'                        => Centrix::boolean($IDR['PostcodeMatch']),
                'home_phone_no'                         => $IDR['HomePhoneNo'],
                'mobile_phone_no'                       => $IDR['MobilePhoneNo'],
                'phone_number_match'                    => Centrix::boolean($IDR['PhoneNumberMatch']),
                'mobile_number_match'                   => Centrix::boolean($IDR['MobileNumberMatch']),
                'email_address'                         => $IDR['EmailAddress'],
            ];
        }

        return response()->json($result);
    }

    /**
     * Delete transaction by token
     *
     * @param string $token
     * @return Request
     */
    public function delete($token)
    {
        $tr = Transactions::where('original_token', $token)->first();
        if (!$tr) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Transaction not found.',
            ], 404);
        }

        $tr->forceDelete();
    }
}