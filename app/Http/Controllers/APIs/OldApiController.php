<?php

namespace App\Http\Controllers\APIs;

use App\Helpers\Helper;
use App\Http\Controllers\App\TokenController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Portal\PortalController;
use App\Http\Models\Centrix\Centrix;
use App\Http\Models\Liveness\Token;
use App\Http\Models\Orbitsdk\AwsS3;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Transactions\Transactions;
use App\Http\Controllers\SMS\SMSController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OldApiController extends Controller
{

    /*{
    "customerNumber": "+610404368868",
    "session": "a-ktUUjxQdoFpRzCruy4QP0cwP-yLeTIsO-Pwp5Bgw8",
    "transactionId": 393779667,
    "transactionType": "AUS_NSW_DRIVERLICENCE",
    "card": {
    "data": {
    "firstName": "Tina",
    "lastName": "Nguyen",
    "middleName": "",
    "dateOfBirth": "1987-06-26",
    "dateOfIssue": null,
    "dateOfExpiry": "2029-09-11",
    "licenceNumber": "14829020",
    "licenceVersion": null,
    "passportNumber": null,
    "address": "46 Woodburn St Colebee Nsw 2761",
    "city": "COLEBEE",
    "postcode": null,
    "over18": "YES",
    "photo": "https://api.idkit.io/IMG/Serv/ORBIT_ID_ANZ/AUS_AUTO_DRIVERLICENCE/Obfba7b36bc16add327b29b44668c77aes/2tOR80047nHGy/crop/cardface.jpg",
    "mobile": "+610404368868"
    },
    "imageFront": "https://api.idkit.io/IMG/Serv/ORBIT_ID_ANZ/AUS_AUTO_DRIVERLICENCE/Obfba7b36bc16add327b29b44668c77aes/2tOR80047nHGy/crop/F_R_1536x2048.jpg",
            "imageBack": ""
        },
        "fr": {
        "data": {
            "confidence": "Pass",
                "liveness": 1,
                "video": "https://api.idkit.io/IMG/Serv/ORBIT_FR/FR/Obfba7b36bc16add327b29b44668c77aes/emy5128397Ay9f/B.webm",
                "features": {
                "leftEye": 77,
                    "rightEye": 86,
                    "leftBrow": 79,
                    "rightBrow": 82,
                    "forehead": 85,
                    "middleForehead": 81,
                    "nose": 84,
                    "philtrum": 78,
                    "mouth": 83,
                    "jaw": 84,
                    "leftCheek": 77,
                    "rightCheek": 80
                }
            }
        },
        "asfCheck": {
        "Result": [
                {
                    "Key": "Overall",
                    "Value": "Pass",
                    "Msg": "Successed"
                },
                {
                    "Key": "CardCorner",
                    "Value": "Pass"
                },
                {
                    "Key": "PhotoCover",
                    "Value": "Pass"
                },
                {
                    "Key": "Resolution",
                    "Value": "Pass"
                },
                {
                    "Key": "CardColor",
                    "Value": "Pass"
                },
                {
                    "Key": "PhotoMatch",
                    "Value": "Pass"
                },
                {
                    "Key": "TextModification",
                    "Value": "Pass"
                },
                {
                    "Key": "Hologram",
                    "Value": "Fail"
                },
                {
                    "Key": "Alignment",
                    "Value": "Pass"
                },
                {
                    "Key": "Font",
                    "Value": "Pass"
                }
            ]
        },
        "metadata": {
        "info": {
            "method": "Mobile Site",
                "ipAddress": "120.154.1.64",
                "devise": "iOS, iOS 12.2",
                "browser": "Safari",
                "location": "-33.8675,151.207",
                "vpn": 0,
                "network": "Telstra Internet"
            },
            "statistics": {
            "activate_time": "03 min 19 sec",
                "review_terms": "1 sec",
                "capture_id": "1 sec",
                "review_data": "1 sec",
                "liveness": "1 sec",
                "total": "4 sec"
            }
        }
    }*/

    //
    public function apiSendSMS(Request $request){

        $inputs = $request->all();
        $rules = [
            'phone_code'   => 'regex:/^([0-9+]+)$/u|max:3',
            'phone_number' => 'required|regex:/^([0-9 ]+)$/u|max:25',
            'transactionId' => 'required|regex:/^([a-zA-Z0-9-_]+)$/u|min:10|max:250',
            'return_url'    => 'nullable|url|max:250',
            'cancel_url'    => 'nullable|url|max:250',
            'name'          => 'nullable|regex:/^([a-zA-Z ]+)$/u|max:25',
            'subcode'       => 'nullable',
        ];
        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            $response['success'] = false;
            $response['messages'] = $validator->errors();
            return response()->json($response);
        }

        $smsController = new SMSController();

        //check if the token already exists
        if (Helper::originalTokenExists($request->transactionId)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Transaction id already exists.',
            ]);
        }

        $code = ($request->phone_code) ? str_replace('+', '', $request->phone_code): '61';
        $number = preg_replace('/\s/i', '', $request->phone_number);
        $token = Helper::generateToken();

        // Remove zero from mobile number
        if (preg_match('/^0/', $number)) {
            $number = substr($number, 1, strlen($number));
        }

        $transactionId = $request->transactionId;

        $transaction = new Transactions();
        $transaction->category = 'remote-verification';
        $transaction->module = 'identity-flow-self-guided';
        $transaction->reference = $request->reference ?? null;
        $transaction->username = User::where('active', true)->where('role', 'admin')->first()->username ?? null;
        $transaction->transactionId = Helper::generateTransactionId();
        $transaction->token = $token;
        $transaction->original_token = $transactionId;
        $transaction->user_status = 'waiting';

        //$liveness = new Token();
        //$liveness->createSession( $token);

        if (isset($request->subcode) && !empty($request->subcode)) {
            $transaction->subcode = $request->subcode;
        }

        $transaction->save();

        //Store URL
        $url_inputs = [
            'return_url' => $request->return_url ?? route('mobileStatus', [ 'status' => 'completed']),
            'cancel_url' => $request->cancel_url ?? route('mobileStatus', [ 'status' => 'cancelled']),
        ];

        $expiry = config('portal.sms_expiry');

        if($expiry < 1){
            $expiry = ceil(60 * $expiry);
            $url_inputs['expiry'] = now()->addMinutes($expiry);
        }else{
            $url_inputs['expiry'] = now()->addHours($expiry);
        }

        $transaction->urls()->create($url_inputs);

        $fullNumber = $code . $number;

        //Store contact details
        $transaction->contactDetails()->create([
            'name' => $request->name ?? null,
            'phone' => $fullNumber ?? null,
        ]);

        // Finally send the message
        $url = secure_url('/') . "/" . $token;
        $msg = $smsController->smsText($url, $request->name, $request->reference);

        $res = [
            'status' => ($smsController->send($msg, $fullNumber)),
        ];

        if(env('APP_ENV') == 'local'){
            $res['url'] = $url;
        }

        return $res;

    }

    public function storeTransaction(Request $request){

        $inputs = $request->all();
        $rules = [
            'transactionId' => 'required|regex:/^([a-zA-Z0-9-_]+)$/u|min:10|max:250',
            'customerNumber'=> 'required',
            'return_url'    => 'nullable|url|max:250',
            'cancel_url'    => 'nullable|url|max:250',
            'name'          => 'nullable|regex:/^([a-zA-Z ]+)$/u|max:25',
        ];
        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            $response['status'] = 'error';
            $response['messages'] = $validator->errors();
            return response()->json($response);
        }

        if(Helper::originalTokenExists($request->transactionId)){
            return response()->json([
                'status'  => 'error',
                'message' => 'Transaction id already exists.',
            ]);
        }
        //$token = self::generateToken();
        $transactionId  = $request->transactionId;
        //$token = md5(sha1($transactionId));
        $token = $transactionId;

        $transaction                 = new Transactions();

        $transaction->category  = 'remote-verification';
        $transaction->module  = 'identity-flow-self-guided';
        $transaction->reference = $request->reference ?? null;
        $transaction->username = User::where('active', true )->where('role', 'admin')->first()->username ?? null;


        $transaction->transactionId  = Helper::generateTransactionId();
        $transaction->token          = $token;
        $transaction->original_token = $request->transactionId;

        $transaction->user_status = 'waiting';

        $transaction->save();

        //$liveness = new Token();
        //$liveness->createSession( $token);

        //Store
        $url_inputs = [
            'return_url' => $request->return_url ?? route('mobileStatus', [ 'status' => 'completed']),
            'cancel_url' => $request->cancel_url ?? route('mobileStatus', [ 'status' => 'cancelled']),
        ];

        $expiry = config('portal.sms_expiry');

        if($expiry < 1){
            $expiry = ceil(60 * $expiry);
            $url_inputs['expiry'] = now()->addMinutes($expiry);
        }else{
            $url_inputs['expiry'] = now()->addHours($expiry);
        }

        $transaction->urls()->create($url_inputs);

        //Store contact details
        $transaction->contactDetails()->create([
            'name' => $request->name ?? null,
            'phone' => $request->customerNumber ?? null,
        ]);

        return [
            'status'  => 'success',
        ];
    }

    public function getClientTransaction($transactionId){

        $tr = Transactions::where('original_token', $transactionId)->orderBy('created_at','desc')->first();
        if (!$tr) {
            return response()->json([
                'status' => 'error',
                'msg'    => 'Transaction not found.',
            ], 404);
        }

        $portalController =  new PortalController();

        $result= [
            'customerNumber' => null,
            'session' => $tr->token,
            'transactionId' => $transactionId,
            'transactionType' => $tr->identify_type,
            'flow_type'  => $tr->flow_type,
            'card' =>[
                'data'=>null,
                'imageFront' => null,
                'imageBack' => null,
            ],
            'fr' =>[
                'data'=>null,
            ],
            'asfCheck' => [
                "Result" => null
            ],
            'metadata' => [
                'info' => null,
                'statistics' => null
            ]
        ];

        if($tr->cards){

            $asf = json_decode($tr->cards->asf, true);
            $newsf = [];

            if(!empty($asf)){
                foreach ($asf as $k => $v){
                    $newsf[] = [
                        'Key' => ucfirst($k),
                        'Value' => $v ? 'Pass' : 'Fail'
                    ];
                }
            }

            $result['asfCheck']['Result'] =  $newsf;
            if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){
                $result['card']['imageFront'] =  Utils::getFullURL($tr->token, 'images/cropped/front', $tr->cards->front_card);
                $result['card']['imageBack'] =  !empty($tr->cards->back_card) ? Utils::getFullURL($tr->token, 'images/cropped/front', $tr->cards->back_card) : null;
            }else{
                $front = $tr->cards->front_card;
                $back  = $tr->cards->back_card;
                $result['card']['imageFront'] =  strpos($front, 'IMG') !== false ? AwsS3::getObject($front) : Utils::getFullURL($tr->token, 'images/cropped/front', $front);
                if(!empty($tr->cards->back_card)){
                    $result['card']['imageBack']  =  strpos($back, 'IMG') !== false ? AwsS3::getObject($back) : Utils::getFullURL($tr->token, 'images/cropped/back', $back);
                }else{
                    $result['card']['imageBack'] = null;
                }
            }
        }

        if($tr->faceScan && $tr->liveness){
            $liveness = $tr->liveness->toArray();
            $result['fr']['data']['liveness'] = !empty($liveness) ? $liveness[count($liveness) - 1]['result'] : 0;
        }

        if($tr->faceScan){
            $result['fr']['data']['features'] = json_decode($tr->faceScan->features, true);
            $result['fr']['data']['confidence'] = Centrix::boolean($tr->faceScan->confidence) ? 'Pass' : 'Fail';

            if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){
                $result['fr']['data']['video'] = Utils::getFullURL($tr->token, 'videos', $tr->faceScan->video);
            }else{
                $result['fr']['data']['video'] = strpos($tr->faceScan->video, 'IMG') !== false ? AwsS3::getObject($tr->faceScan->video) : Utils::getFullURL($tr->token, 'videos', $tr->faceScan->video);
            }

        }

        if($tr->contactDetails){
            $result['customerNumber'] = $tr->contactDetails->phone;
        }

        if($tr->info){
            $info = $tr->info;

            $geolocation    = $info['geolocation'];
            $location = null;
            if(!empty($geolocation)){
                if(Helper::isJson($geolocation)){
                    $geolocation = json_decode($geolocation, true);
                    $location = $geolocation['coordinates'];
                }else{
                    $location = $geolocation;
                }
            }

            $result['metadata']['info'] = [
                'method'    => 'Mobile Site',
                'ipAddress' => $info['ip'],
                'devise'    => $info['device']. ', '.$info['os'],
                'browser'   => $info['browser'],
                'location'  => $location,
                'vpn'       => $info['vpn'],
                'network'   => $info['isp'],
            ];
        }

        if($tr->statistics){
            $result['metadata']['statistics'] = $portalController->prepareTransactionRawStatistics($tr);
        }

        if($tr->userDetails){
            $cardData = $tr->userDetails->toArray();
            $cardData['address'] = $tr->contactDetails->residential_address;

            if(Helper::isJson($cardData['address'])){
                $cardData['address'] = json_decode($cardData['address'], true)['fullAddress'];
            }

            $cardData['mobile'] = $tr->contactDetails->phone;

            $face = $tr->cards->face_photo;
            if(env('APP_ENV') == 'local' || !config('aws.s3.enable')){
                $cardData['photo'] = Utils::getFullURL($tr->token, 'images/cropped/face', $tr->cards->face_photo);
            }else{
                $cardData['photo'] = strpos($face, 'IMG') !== false ? AwsS3::getObject($face) : Utils::getFullURL($tr->token, 'images/cropped/face', $face);
            }

            $result['card']['data'] = $cardData;
        }

        return response()->json($result);

    }

    public function deleteClientTransaction($transactionId){
        try {
            $apiController = new ApiController();
            $tr = Transactions::where('original_token', $transactionId)->first();
            if($tr){
                $apiController->delete($tr->token);
                $response['status'] = 'success';
            }else{
                $response['status'] = 'error';
                $response['message'] = 'Transaction not found.';
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }
        return $response;

    }
}