<?php

namespace App\Http\Controllers\APIs;

use App\Http\Controllers\Controller;
use App\Http\Models\Orbitsdk\Utils;
use App\Http\Models\Transactions\Transactions;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    /**
     * Get image file
     *
     * @param string $type
     * @param string $token
     */
    public function image($crop, $type, $token)
    {
        header('Content-Type: image/jpeg');
        $transaction = Transactions::where('token', $token)->orderBy('created_at','desc')->first();
        if ($transaction && $transaction->cards) {
            $cards = $transaction->cards()->first();
            $image = Utils::getFileUrl($transaction->identify_type, $cards->front_card);
            if ($type == 'back') {
                $image = Utils::getFileUrl($transaction->identify_type, $cards->back_card);
            } else if ($type == 'face') {
                $image = Utils::getFileUrl($transaction->identify_type, $cards->face_photo);
            }

            if ($crop == 'uncropped') {
                $image = str_replace('/crop', '', $image);
            }

            $client = new Client(['http_errors' => false]);
            $image  = $client->request('GET', $image)->getBody();
            die($image);
        }

        return abort(404);
    }

    /**
     * Get video face
     *
     * @param string $token
     * @param string $exp
     */
    public function face($token)
    {
        //header('Content-Type: image/jpeg');
        $transaction = Transactions::where('token', $token)->orderBy('created_at','desc')->first();
        if ($transaction && $transaction->faceScan) {
            $faceScan = $transaction->faceScan()->first();
            $image    = Utils::getFileUrl('VID', $faceScan->face);
            $client   = new Client(['http_errors' => false]);
            $image    = $client->request('GET', $image)->getBody();
            die($image);
        }

        return abort(404);
    }

    /**
     * Get video face
     *
     * @param string $token
     * @param string $exp
     */
    public function selfies($type, $token)
    {
        header('Content-Type: image/jpeg');
        $transaction = Transactions::where('token', $token)->orderBy('created_at','desc')->first();
        if ($transaction && $transaction->faceScan) {
            $faceScan = $transaction->faceScan()->first();
            if ($type == 'face') {
                $image = Utils::getFileUrl('VID', $faceScan->video);
            } else {
                $image = Utils::getFileUrl('VID', $faceScan->face);
            }
            $client = new Client(['http_errors' => false]);
            $image  = $client->request('GET', $image)->getBody();
            die($image);
        }

        return abort(404);
    }

    /**
     * Get video file
     *
     * @param string $token
     * @param string $exp
     */
    public function video($token, $exp)
    {
        header('Content-Type: video/' . $exp);
        $transaction = Transactions::where('token', $token)->orderBy('created_at','desc')->first();
        if ($transaction && $transaction->faceScan) {
            $facescan = $transaction->faceScan()->first();
            $video    = Utils::getFileUrl('VID', $facescan->video);

            $client = new Client(['http_errors' => false]);
            $res    = $client->request('GET', $video)->getBody();
            die($res);
        }

        return abort(404);
    }

    /**
     * Get liveness video file
     *
     * @param string $token
     * @param string $exp
     */
    public function liveness($id, $token, $exp)
    {
        header('Content-Type: video/' . $exp);
        $transaction = Transactions::with(['liveness' => function ($query) use ($id) {
            $query->where('id', $id);
        }])->where([
            'token' => $token,
        ])->orderBy('created_at','desc')->first()->toArray();

        if ($transaction && count($transaction['liveness'])) {
            $liveness = $transaction['liveness'][0];
            $video    = Utils::getFileUrl('VID', $liveness['video']);

            $client = new Client(['http_errors' => false]);
            $res    = $client->request('GET', $video)->getBody();
            die($res);
        }

        return abort(404);
    }
}
