<?php

namespace App\Http\Controllers\SMS;

use App\Http\Controllers\Controller;
use App\Notifications\IdentityVerification;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SMSController extends Controller
{

    /**
     * Send an SMS message.
     *
     * @param string $msg
     * @param string $number
     * @return boolean
     */
    public function send($msg, $number)
    {
        if (env('APP_ENV') == 'local') {
            return true;
        }
        $api = new TransmitsmsAPI(config('sms.transmitsms.key'), config('sms.transmitsms.secret'));
        $result = $api->sendSms($msg, $number, config('app.name'));
        return $result->error->code == 'SUCCESS';
    }


    /**
     * Create SMS Text.
     *
     * @param string $url
     * @param string $name
     * @param string $reference
     * @return string
     */
    public function smsText($url, $name = null, $reference =null)
    {
        $msg = '';
        $msg .= ($name) ? "Hi $name," : 'Hi User,';
        $msg .= ' thanks for placing an order with OCR Labs';
        $msg .= ($reference) ? " with reference number: $reference." : ".";
        $msg .= " Please head to $url within the next 48 hours to complete the verification process. Any questions please visit support.ocrlabs.com .";
        //return 'OCR Labs here. Please visit ' . $url . ' to complete your verification. Any questions please visit support.ocrlabs.com.';
        return $msg;
    }

    /**
     * Send OTP For Authentication.
     *
     * @param Request $request
     * @return string
     */
    public function sendSimple(Request $request)
    {
        $input = $request->all();
        \Log::info($input['number']);
        $six_digit_random_number = mt_rand(100000, 999999);
        $user = Auth::user();
        $user['sms_token'] = $six_digit_random_number;
        $user->save();
        $this->send($six_digit_random_number . ' is your one-time authentication code for IDKit. It will expire in 5 minutes.', $input['number']);
        $res = [
            'status' => 'success'
        ];
        if(env('APP_ENV') === 'local'){
            $res['debug'] = [
                'otp' => $six_digit_random_number
            ];
        }
        return response()->json($res);
    }

    /**
     * Create Email body Content
     *
     * @param String $url
     * @param String $name
     * @param String $reference
     * @return string
     */
    public function emailBody($url, $name = null, $reference = null){
        $msg = '';
        $msg .= ($name) ? "Hi <strong>$name</strong>," : 'Hi <strong>User</strong>,';
        $msg .= ' thanks for placing an order with OCR Labs';
        $msg .= ($reference) ? " with reference number: <strong>$reference</strong>." : ".";
        $msg .= " Please head to this <a href='$url'><strong>Link</strong></a> within the next 48 hours to complete the verification process. Any questions please visit support.ocrlabs.com .";
        return $msg;
    }

    /**
     * Send Email
     *
     * @param String $url
     * @param String $email
     * @param String $name
     * @param String $reference
     * @return string
     */
    public function sendEmail($url, $email, $name = null, $reference = null)
    {
        try {
            $msg = $this->emailBody($url, $name, $reference);

            $invitedUser = new User();
            $invitedUser->email = $email;

            $params = new \stdClass();
            $params->msg = $msg;
            $params->url = $url;
            $invitedUser->notify(new IdentityVerification($params));
            return true;

        } catch (\Exception $e) {
            /*$response['status'] = 'error';
            $response['message'] = $e->getMessage();
            echo '<pre>';
            print_r($response);
            echo '</pre>';
            die('<hr/>here');*/
            return false;
        }


    }

    /**
     * Send OTP For Authentication
     * @return string
     */
    public function sendAuth()
    {
        $user = Auth::user();
        $mobile = $user['mobile'];
        $six_digit_random_number = mt_rand(100000, 999999);
        $user['sms_token'] = $six_digit_random_number;
        $user->save();

        $this->send($six_digit_random_number . ' is your one-time authentication code for IDKit. It will expire in 5 minutes.', $mobile);
    }
}
