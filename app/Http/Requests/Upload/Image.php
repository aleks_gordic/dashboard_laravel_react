<?php

namespace App\Http\Requests\Upload;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class Image extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'idType'    => [
                'required',
                Rule::in(config('orbitsdk.api.id_types')),
            ],
            'frontFile' => 'required|image',
            'backFile'  => ($request->idType === 'CHN_IDCARD' || $request->idType === 'HKG_IDCARD' || $request->idType === 'MYS_DLCARD' || $request->idType === 'SGP_IDCARD' || $request->idType == 'PASSPORT' || preg_match('/AUS/i', $request->idType)) ? 'image' : 'required|image',
        ];
    }
}
