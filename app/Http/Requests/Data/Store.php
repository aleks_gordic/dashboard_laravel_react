<?php

namespace App\Http\Requests\Data;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'cardType'     => 'required|regex:/^([A-Z_]+)$/u|max:32',
            'firstName'    => 'required|regex:/^([a-z0-9A-Z,\'`‘’\- ]+)$/u|min:1|max:30',
            'lastName'     => 'required|regex:/^([a-z0-9A-Z,\'`‘’\- ]+)$/u|min:1|max:50',
            'middleName'   => 'nullable|regex:/^([a-z0-9A-Z,\'`‘’\- ]+)$/u|min:1|max:40',
            'dateOfBirth'  => 'required|date_format:d-m-Y',
            'expiryDate'   => 'required|date_format:d-m-Y',
            'homeAddress'  => 'nullable|max:200',
            'prevAddress'  => 'nullable|max:200',

            'addressLine1' => 'nullable|max:100',
            'addressLine2' => 'nullable|max:100',
            'city'         => 'nullable|max:100',
            'suburb'       => 'nullable|max:100',
            'postcode'     => 'nullable|regex:/^([a-z0-9A-Z]+)$/u|min:1|max:10',
            'country'      => 'nullable|regex:/^([a-z0-9A-Z,\- ]+)$/u|min:1|max:50',
            'countryCode'  => 'nullable|regex:/^([A-Z]+)$/u|min:2|max:2',
            'location'     => 'nullable|regex:/^[a-z0-9,.-]+$/u|max:250',
        ];

        if ($request->cardType !== 'PASSPORT' && !preg_match('/IDCARD/i', $request->cardType)) {
            $rules['licenceNumber'] = 'required|regex:/^[A-Za-z0-9]+$/u|min:4|max:64';
            if ($request->countryCode === 'NZ') {
                $rules['licenceNumber'] = 'required|regex:/^[A-Za-z0-9]+$/u|min:4|max:10';
                $rules['licenceVersion'] = 'required|regex:/^([0-9]+)/u|max:5';
            }

        } elseif (preg_match('/IDCARD/i', $request->cardType)){
            $rules['cardNumber'] = 'required|regex:/^[A-Za-z0-9]+$/u|min:4|max:64';

            if ($request->countryCode === 'CN') {
                $rules['firstName']  = 'required';
                $rules['lastName']   = 'nullable';
                $rules['middleName'] = 'nullable';
            }

        } else {
            $rules['passportNumber'] = 'required|regex:/^[A-Za-z0-9]+$/u|min:4|max:10';
        }

        return $rules;
    }
}
