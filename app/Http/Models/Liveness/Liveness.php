<?php

namespace App\Http\Models\Liveness;

use GuzzleHttp\Client;

class Liveness
{
    /**
     * Create a http client
     * @var \GuzzleHttp\Client
     */
    protected $http;

    /**
     * OAuth bearer
     * @var string
     */
    protected $accessToken;

    /**
     * Liveness token
     * @var string
     */
    protected $token;

    public function __construct($token)
    {
        $this->token = $token;
        $this->http  = new Client([
            'http_errors' => false,
            'verify'      => false,
            'base_uri'    => config('liveness.api.url') . '/' . config('liveness.api.base_path') . '/',
        ]);

        $response = $this->http->post(config('liveness.api.url') . '/oauth/token', [
            'body'    => json_encode(config('liveness.api.auth')),
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        $result            = json_decode((string) $response->getBody(), true);
        $this->accessToken = $result['access_token'];
    }

    /**
     * Get livness video.
     *
     * @return array
     */
    public function getVideo()
    {
        $session = $this->http->request('GET', 'video/' . $this->token, [
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer ' . $this->accessToken,
            ],
        ]);

        return json_decode($session->getBody()->getContents(), true);
    }

    /**
     * Get livness data.
     *
     * @return array
     */
    public function get()
    {
        $session = $this->http->request('GET', 'session/' . $this->token, [
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer ' . $this->accessToken,
            ],
        ]);

        return json_decode($session->getBody(), true);
    }

    /**
     * Get livness data.
     *
     * @return void
     */
    public function delete()
    {
        $session = $this->http->request('DELETE', 'session/' . $this->token, [
            'headers' => [
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer ' . $this->accessToken,
            ],
        ]);
    }
}
