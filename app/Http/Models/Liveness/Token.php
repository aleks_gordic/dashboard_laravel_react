<?php

namespace App\Http\Models\Liveness;

use GuzzleHttp\Client;

class Token
{
    /**
     * Create a http client
     * @var \GuzzleHttp\Client
     */
    protected $http;

    /**
     * OAuth bearer
     * @var string
     */
    protected $accessToken;

    /**
     * Liveness token
     * @var string
     */
    protected $token;

    public function __construct()
    {

    }

    public function createSession($session)
    {
        $http  = new Client([
            'http_errors' => false,
            'verify'      => false,
        ]);

        $response = $http->post(config('liveness.api.url') . '/oauth/token', [
            'body'    => json_encode(config('liveness.api.auth')),
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        $result            = json_decode((string) $response->getBody(), true);
        $accessToken = $result['access_token'];

        $res= $http->post(config('liveness.api.url') . '/api/start', [
            'form_params' => [
                'session' => $session
            ],
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);

        return $res->getStatusCode();
    }
}
