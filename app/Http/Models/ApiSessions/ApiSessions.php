<?php

namespace App\Http\Models\ApiSessions;

use Illuminate\Database\Eloquent\Model;

class ApiSessions extends Model
{
    protected $table    = 'api_sessions';
}
