<?php

namespace App\Http\Models\GreenID;

use App\Http\Models\Transactions\Transactions;
use SimpleXMLElement;
use \Illuminate\Http\Request;

class Save
{
    public static function registrationDetail($data, $tr)
    {
        if (isset($data->registrationDetails)) {
            // Reset
            $tr->gIDRegistrationDetails()->delete();

            $details = $data->registrationDetails;
            $values  = [
                'dateCreated' => $details->dateCreated,
            ];

            // Address
            if (isset($details->currentResidentialAddress)) {
                $address = $details->currentResidentialAddress;

                $values['city']       = $address->city;
                $values['country']    = $address->country;
                $values['streetType'] = $address->streetType;
                $values['suburb']     = $address->suburb;
            }

            // Date of birth
            if (isset($details->dob)) {
                $dob           = $details->dob;
                $values['dob'] = $dob->year . '-' . $dob->month . '-' . $dob->day;
            }

            // Name
            if (isset($details->name)) {
                $name                  = $details->name;
                $values['givenName']   = $name->givenName;
                $values['middleNames'] = $name->middleNames;
                $values['surname']     = $name->surname;
            }

            if ($tr) {
                $tr->gIDRegistrationDetails()->create($values);
            }
        }
    }

    public static function sourceList($data, $tr)
    {
        if (isset($data->sourceList) && isset($data->sourceList->source)) {
            // Reset
            $tr->gIDSourceList()->delete();

            $sourceList = $data->sourceList->source;
            $values     = [];
            foreach ($sourceList as $src) {
                $values[] = [
                    'tId'           => $tr->id,
                    'available'     => $src->available == 'true',
                    'name'          => $src->name,
                    'notRequired'   => $src->notRequired == 'true',
                    'oneSourceLeft' => $src->oneSourceLeft == 'true',
                    'order'         => (int) $src->order,
                    'passed'        => $src->passed == 'true',
                    'state'         => $src->state,
                    'version'       => (int) $src->version,
                ];
            }

            $tr->gIDSourceList()->insert($values);
        }
    }

    public static function verificationResult($data, $tr)
    {
        if (isset($data->verificationResult)) {

            // Reset
            $gIDVerificationResult = $tr->gIDVerificationResult()->first();
            if ($gIDVerificationResult) {
                $gIDVerificationResult->individual()->delete();
                $gIDVerificationResult->delete();
            }

            $result = $data->verificationResult;

            $tr->gIDVerificationResult()->create([
                'overallVerificationStatus' => $result->overallVerificationStatus,
                'ruleId'                    => $result->ruleId,
                'verificationId'            => $result->verificationId,
            ]);

            self::verificationIndividual($data, $tr);
        }
    }

    private static function verificationIndividual($data, $tr)
    {
        if (isset($data->verificationResult)) {
            $result = $data->verificationResult->individualResult;
            $values = [];
            foreach ($result as $r) {
                $val = [
                    'vId'          => $tr->gIDVerificationResult->id,
                    'dateVerified' => $r->dateVerified,
                    'method'       => $r->method,
                    'name'         => $r->name,
                    'state'        => $r->state,
                    'fieldResult'  => null,
                ];

                if (isset($r->fieldResult)) {
                    $r2                 = (array) $r;
                    $fieldResult        = json_encode($r2['fieldResult']);
                    $val['fieldResult'] = $fieldResult;
                }

                $values[] = $val;

            }
            if (count($values)) {
                $gIDVerificationResult = $tr->gIDVerificationResult()->first();
                $gIDVerificationResult->individual()->insert($values);
            }
        }
    }
}
