<?php

namespace App\Http\Models\Orbitsdk;

use Aws\S3\S3Client;
use Illuminate\Support\Facades\Storage;

class AwsS3
{

    protected static $s3;
    protected static $bucket;
    protected static $api_url;
    /**
     * AwsS3 init.
     */
    public static function init()
    {
        self::$s3 = new S3Client([
            'version' => config('aws.s3.version'),
            'region'  => config('aws.s3.region')
        ]);

        self::$bucket = config('aws.s3.bucket');
        self::$api_url = config('orbitsdk.api.url');
    }

    /**
     * Store object to s3 bucket
     *
     * @param string $uri
     * @param string $full
     * @return string
     */
    public static function storeObject($uri, $full = false){

        self::init();
        $uri =  str_replace('../../', '', $uri);
        if($full){
            $uri = str_replace('/crop/', '/', $uri);
        }

        $content = file_get_contents(self::$api_url.'/'.$uri);

        try {
            if(config('filesystems.default') == 's3'){

                $response = Storage::disk('s3')->put($uri, $content);

            }else{
                $response = self::$s3->putObject([
                    'Bucket' => self::$bucket,
                    'Key'    => $uri,
                    'Body'   => $content,
                ]);
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    /**
     * get object from s3 bucket
     *
     * @param string $filename
     * @return string
     */
    public static function getObject($filename){

        self::init();
        try {

            if(config('filesystems.default') == 's3'){

                $response = Storage::disk('s3')->temporaryUrl($filename, now()->addMinutes(60));

            }else{
                $cmd = self::$s3->getCommand('GetObject',[
                    'Bucket' => self::$bucket,
                    'Key'    => $filename
                ]);


                $request = self::$s3->createPresignedRequest($cmd, '+60 minutes');

                $response = (string)$request->getUri();
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }
        return $response;

    }
    /**
     * delete object from s3 bucket
     *
     * @param string $filename
     * @return string
     */
    public static function deleteObject($filename){
        self::init();
        try {
            if(config('filesystems.default') == 's3'){
                $response = Storage::disk('s3')->delete($filename);
            }else{
                $response = self::$s3->deleteObject([
                    'Bucket' => self::$bucket,
                    'Key'    => $filename
                ]);
            }

        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }
        return $response;

    }


}