<?php

namespace App\Http\Models\Orbitsdk;

class Response
{
    /**
     * Adjust card information response
     *
     * @return array
     */
    public static function cardInfo($data)
    {
        $result = [
            'status' => 'success',
            'data'   => [],
            'asf'    => [],
        ];

        // Clean up `CardData`
        if (count($data['CardInfo']['CardData'])) {
            $cardData = $data['CardInfo']['CardData'];
            foreach ($cardData as $card) {
                if ($card !== "N/A" && array_key_exists('Key', $card)) {

                    if ($card['Key'] == "DOB") {
                        $card['Key'] = 'dateOfBirth';
                    }

                    if ($card['Key'] == "CardNumber") {
                        $card['Key'] = 'passportNumber';
                    }

                    if (strtolower($card['Key']) == "dateofexpiry") {
                        $card['Key'] = 'expiryDate';
                    }

                    if ($card['Key'] == "Sex") {
                        $card['Key'] = 'Gender';
                    }

                    if ($card['Key'] == "Address") {
                        $card['Value'] = ucwords(strtolower($card['Value']));
                        $card['Value'] = preg_replace('#P O Box|Po Box#i', 'P.O Box', $card['Value']);
                    }

                    $key   = preg_replace('/\s/i', '', lcfirst($card['Key']));
                    $value = preg_replace(
                        [
                            '#N/A#i',
                            '#^([0-9]+)\/([0-9]+)\/([0-9]+)$#i',
                        ],
                        [
                            '',
                            '$1-$2-$3',
                        ], trim($card['Value']));
                    $result['data'][$key] = $value;
                }
            }


            //Engine issue fix
            if(isset($result['data']['cardType']) && $result['data']['cardType'] == 'MRZ'){
                $result['data']['documentType'] = 'PASSPORT';
            }

            // for IDcard of HK, SG, MY, CN
            if(empty($result['data']['firstName']) && !empty($result['data']['name'])){
                $result['data']['firstName'] = $result['data']['name'];
            }
            if($result['data']['cardType'] === 'CHN_IDCARD' || $result['data']['cardType'] === 'HKG_IDCARD' || $result['data']['cardType'] === 'SGP_IDCARD'){
                $result['data']['cardNumber']     = $result['data']['licenceNumber'];
                unset($result['data']['licenceNumber']);
            }

            //If firstName is empty then throw error
            if (!isset($result['data']['firstName']) ||  empty($result['data']['firstName'])) {
                return [
                    'status'      => 'error',
                    'data'        => [],
                    'asf'         => [],
                    'status_code' => '210',
                ];
            }

            if (!isset($result['data']['dateOfBirth']) || $result['data']['dateOfBirth'] == "N/A") {
                $result['data']['dateOfBirth'] = '';
            } else if (!empty($result['data']['dateOfBirth']) && (!preg_match('#([0-9\/]+)#i', $result['data']['dateOfBirth']) || strlen($result['data']['dateOfBirth']) > 10)) {
                $result['data']['dateOfBirth'] = '';
            }

            if (!isset($result['data']['expiryDate']) || $result['data']['expiryDate'] == "N/A") {
                $result['data']['expiryDate'] = '';
            } else if (!empty($result['data']['expiryDate']) && (!preg_match('#([0-9\/]+)#i', $result['data']['expiryDate']) || strlen($result['data']['expiryDate']) > 10)) {
                $result['data']['expiryDate'] = '';
            }

            if (isset($result['data']['documentType'])) {
                $result['data']['cardType'] = $result['data']['documentType'];
                unset($result['data']['documentType']);
            }

            if (isset($result['data']['documentNumber'])) {
                $result['data']['passportNumber'] = $result['data']['documentNumber'];
                $result['data']['cardNumber']     = $result['data']['documentNumber'];
                unset($result['data']['documentNumber']);
            }
        }

        // Clean up asf check
        if ($data['CardInfo'] && array_key_exists('AsfCheck', $data['CardInfo']) && count($data['CardInfo']['AsfCheck'])) {
            $asf = $data['CardInfo']['AsfCheck'];
            foreach ($asf as $card) {
                $key                 = preg_replace('/\s/i', '', lcfirst($card['Key']));
                $value               = $card['Value'];
                $result['asf'][$key] = $value == 'Pass' ? true : false;
            }
        }

        $result['status']      = $data && array_key_exists('status', $data) ? $data['status'] : ($data['CardInfo']['StatusCode'] ? 'success' : 'error');
        $result['status_code'] = $data['CardInfo']['StatusCode'];

        return $result;
    }

    /**
     * Adjust face scan response
     *
     * @return array
     */
    public static function faceScan($data)
    {
        $result = [
            'status'             => $data['status'],
            'msg'                => isset($data['msg']) ? $data['msg'] : '',
            'confidence'         => $data['Confidence'] == 'Pass',
            //'liveness'         => $data['Liveness'] ,
            'features'           => [],
            'liveness_assessments' => [],
        ];

        // Clean up Features
        if ($data['Features']) {
            foreach ($data['Features'] as $key => $value) {
                $key                      = preg_replace('/\s/i', '', lcfirst($key));
                $result['features'][$key] = (int) $value;
            }
        }
        // Clean up LivenessAssessment
        if ($data['LivenessAssessment']) {
            foreach ($data['LivenessAssessment'] as $key => $value) {
                $key                      = preg_replace('/\s/i', '', lcfirst($key));
                $result['liveness_assessments'][$key] = $value;
            }
        }

        return $result;
    }
}
