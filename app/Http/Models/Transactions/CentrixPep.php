<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class CentrixPep extends Model
{
    protected $table    = 'transaction_centrix_pep';
    protected $hidden   = ['id', 'tId', 'created_at', 'updated_at'];
    protected $fillable = [
        'IsSuccess',
        'InternationalWatchlistIsClear',
        'ResponseDetails'
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
