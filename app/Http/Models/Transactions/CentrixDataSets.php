<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class CentrixDataSets extends Model
{
    protected $table    = 'transaction_centrix_data_sets';
    protected $hidden   = ['id', 'tId', 'created_at', 'updated_at'];
    protected $fillable = [
        'consumer_file',
        'driver_licence_verification',
        'known_names',
        'known_addresses',
        'previous_enquiries',
        'defaults',
        'judgments',
        'insolvencies',
        'company_affiliations',
        'file_notes',
        'fines_data_list',
        'name_only_insolvencies',
        'payment_history',
        'red_arrears_month_list',
        'property_ownership',
        'client_keys',
        'identity_verification_data',
        'bureau_header_data_identity_verification',
        'summary_items',
        'extra_data_items',
        'application_decision',
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
