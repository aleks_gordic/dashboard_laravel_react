<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $table    = 'transaction_info';
    protected $fillable = ['device', 'vpn', 'os', 'ip', 'isp', 'ip_location','geolocation', 'browser'];
    protected $hidden   = ['id', 'tId', 'created_at', 'updated_at'];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
