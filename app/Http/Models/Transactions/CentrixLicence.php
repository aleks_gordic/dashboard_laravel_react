<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class Centrixlicence extends Model
{
    protected $table    = 'transaction_centrix_licence';
    protected $hidden   = ['id', 'tId', 'created_at', 'updated_at'];
    protected $fillable = [
        'NumberAndVersion',
        'IsDriverLicenceVerified',
        'IsDriverLicenceLastNameMatched',
        'IsDriverLicenceFirstNameMatched',
        'IsDriverLicenceMiddleNameMatched',
        'IsDriverLicenceDateOfBirthMatched',
        'IsSuccess',
        'IsDriverLicenceVerifiedAndMatched',
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
