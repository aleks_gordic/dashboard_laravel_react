<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class FaceScan extends Model
{
    protected $table    = 'transaction_face_scan';
    protected $hidden   = ['id', 'tId', 'created_at', 'updated_at'];
    protected $fillable = [
        'session',
        'confidence',
        'liveness',
        'features',
        'video',
        'face',
    ];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
