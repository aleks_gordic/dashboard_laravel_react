<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class URLs extends Model
{
    protected $table    = 'transaction_urls';
    protected $fillable = ['return_url', 'cancel_url', 'expiry'];
    protected $hidden   = ['tId', 'created_at', 'updated_at'];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
