<?php

namespace App\Http\Models\Transactions;

use App\Http\Models\Orbitsdk\Orbitsdk;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $fillable = [
        'category',
        'module',
        'token',
        'transactionId',
        'original_token',
        'reference',
        'username',
        'joint_session',
        'user_status',
        'status',
        'seen_at',
        'activated_at',
        'completed_at',
    ];

    protected $dates = ['activated_at', 'completed_at', 'created_at'];
    /**
     * Transaction document cards
     * @return Illuminate\Database\Eloquent\Model
     */
    public function cards()
    {
        return $this->hasOne('App\Http\Models\Transactions\Cards', 'tId');
    }

    /**
     * Transaction contact details
     * @return Illuminate\Database\Eloquent\Model
     */
    public function contactDetails()
    {
        return $this->hasOne('App\Http\Models\Transactions\ContactDetails', 'tId');
    }

    /**
     * Transaction face scan data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function faceScan()
    {
        return $this->hasOne('App\Http\Models\Transactions\FaceScan', 'tId');
    }

    /**
     * Transaction user details
     * @return Illuminate\Database\Eloquent\Model
     */
    public function userDetails()
    {
        return $this->hasOne('App\Http\Models\Transactions\UserDetails', 'tId');
    }

    /**
     * Centrix smart ID
     * @return Illuminate\Database\Eloquent\Model
     */
    public function centrixSmartId()
    {
        return $this->hasOne('App\Http\Models\Transactions\CentrixSmartId', 'tId');
    }

    /**
     * Centrix driver licence
     * @return Illuminate\Database\Eloquent\Model
     */
    public function centrixLicence()
    {
        return $this->hasOne('App\Http\Models\Transactions\CentrixLicence', 'tId');
    }

    /**
     * Centrix driver licence
     * @return Illuminate\Database\Eloquent\Model
     */
    public function centrixPassport()
    {
        return $this->hasOne('App\Http\Models\Transactions\CentrixPassport', 'tId');
    }

    /**
     * Centrix PEP
     * @return Illuminate\Database\Eloquent\Model
     */
    public function centrixPep()
    {
        return $this->hasOne('App\Http\Models\Transactions\CentrixPep', 'tId');
    }

    /**
     * Centrix Residential
     * @return Illuminate\Database\Eloquent\Model
     */
    public function centrixResidential()
    {
        return $this->hasOne('App\Http\Models\Transactions\CentrixResidential', 'tId');
    }

    /**
     * Centrix Data Sets
     * @return Illuminate\Database\Eloquent\Model
     */
    public function centrixDataSets()
    {
        return $this->hasOne('App\Http\Models\Transactions\CentrixDataSets', 'tId');
    }

    /**
     * Transaction information
     * @return Illuminate\Database\Eloquent\Model
     */
    public function info()
    {
        return $this->hasOne('App\Http\Models\Transactions\Info', 'tId');
    }

    /**
     * Transaction liveness
     * @return Illuminate\Database\Eloquent\Model
     */
    public function liveness()
    {
        return $this->hasMany('App\Http\Models\Transactions\Liveness', 'tId');
    }

    /**
     * Transaction extracted
     * @return Illuminate\Database\Eloquent\Model
     */
    public function extracted()
    {
        return $this->hasOne('App\Http\Models\Transactions\Extracted', 'tId');
    }

    /**
     * Transaction statistics
     * @return Illuminate\Database\Eloquent\Model
     */
    public function statistics()
    {
        return $this->hasOne('App\Http\Models\Transactions\Statistics', 'tId');
    }

    /**
     * Transaction URLs
     * @return Illuminate\Database\Eloquent\Model
     */
    public function urls()
    {
        return $this->hasOne('App\Http\Models\Transactions\URLs', 'tId');
    }

    /**
     * Transactions seen by user
     * @return Illuminate\Database\Eloquent\Model
     */
    public function userSeens()
    {
        return $this->hasMany('App\Http\Models\Transactions\Seen', 'tId');
    }

    /**
     * Transaction api sessions
     * @return Illuminate\Database\Eloquent\Model
     */
    public function sessions()
    {
        return $this->hasMany('App\Http\Models\ApiSessions\ApiSessions', 'tId');
    }

    /**
     * @return Illuminate\Database\Eloquent\Model
     */
    public function gIDRegistrationDetails()
    {
        return $this->hasOne('App\Http\Models\Transactions\GIDRegistrationDetails', 'tId');
    }

    /**
     * @return Illuminate\Database\Eloquent\Model
     */
    public function gIDSourceList()
    {
        return $this->hasMany('App\Http\Models\Transactions\GIDSourceList', 'tId');
    }

    /**
     * @return Illuminate\Database\Eloquent\Model
     */
    public function gIDVerificationResult()
    {
        return $this->hasOne('App\Http\Models\Transactions\GIDVerificationResult', 'tId');
    }

    public function forceDelete()
    {
        $api = new Orbitsdk();

        // Delete ID session
        if ($this->cards) {

            $cards   = $this->cards()->first();
            $tr      = $this->first();
            $sdktype = $tr->type == 'PASSPORT' ? 'ORBIT_MRZ' : 'ORBIT_ID_ANZ';
            $api->deleteSession($cards->session, $sdktype);
        }

        // Delete FR session
        if ($this->faceScan) {

            $faceScan = $this->faceScan()->first();
            $api->deleteSession($faceScan->session, 'ORBIT_FR');
        }

        // Delete transaction
        if ($this->centrixSmartId) {
            $this->centrixSmartId->deleteAll();
        }
        $this->centrixPep()->delete();
        $this->centrixResidential()->delete();
        $this->centrixLicence()->delete();
        $this->centrixPassport()->delete();
        $this->centrixDataSets()->delete();
        $this->userDetails()->delete();
        $this->statistics()->delete();
        $this->liveness()->delete();
        $this->extracted()->delete();
        $this->urls()->delete();
        $this->contactDetails()->delete();
        $this->info()->delete();
        $this->cards()->delete();
        $this->faceScan()->delete();
        $this->userSeens()->delete();
        $this->sessions()->delete();
        $this->delete();
    }
}
