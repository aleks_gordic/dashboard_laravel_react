<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    protected $table    = 'transaction_user_details';
    protected $fillable = [
        'title',
        'gender',
        'firstName',
        'lastName',
        'middleName',
        'dateOfBirth',
        'dateOfExpiry',
        'licenceNumber',
        'versionNumber',
        'passportNumber',
        'cardNumber',
    ];
    protected $hidden = ['id', 'tId', 'created_at', 'updated_at'];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
