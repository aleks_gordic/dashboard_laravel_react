<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class CentrixSmartIdData extends Model
{
    protected $table    = 'transaction_centrix_smart_id_data';
    protected $hidden   = ['id', 'sId', 'created_at', 'updated_at'];
    protected $fillable = [
        'DataSourceName',
        'DataSourceDescription',
        'NameMatchStatus',
        'DateOfBirthMatchStatus',
        'AddressMatchStatus',
    ];

    /**
     * Centrix smart ID
     * @return Illuminate\Database\Eloquent\Model
     */
    public function CentrixSmartId()
    {
        return $this->belongsTo('App\Http\Models\Transactions\CentrixSmartId');
    }
}
