<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class Cards extends Model
{
    protected $table    = 'transaction_cards';
    protected $fillable = [
        'session',
        'front_card',
        'back_card',
        'face_photo',
        'asf',
        'edited',
        'approved'
    ];

    protected $casts = [
        //'asf' => 'json',
    ];


    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
