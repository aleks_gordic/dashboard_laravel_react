<?php

namespace App\Http\Models\Transactions;

use Illuminate\Database\Eloquent\Model;

class Liveness extends Model
{
    protected $table    = 'transaction_liveness';
    protected $fillable = ['token', 'result', 'spoof_risk', 'actions', 'video'];
    protected $hidden   = ['tId', 'token', 'created_at', 'updated_at'];

    /**
     * Transactions data
     * @return Illuminate\Database\Eloquent\Model
     */
    public function Transactions()
    {
        return $this->belongsTo('App\Http\Models\Transactions\Transactions');
    }
}
