<?php

namespace App\Http\Models\Portal\Core;

use App\Helpers\Helper;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use stdClass;
use Validator;

class PortalUser extends User
{
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
        'mobile',
        'role',
        'avatar',
        'active',
        'google_secret',
        'sms_token',
        'account_token',
        'password_status',
        'password_created_at',
        'last_signin_at',
        'created_by',
    ];

    protected $casts =[
        'password_status' => 'boolean',
        'active' => 'boolean',
    ];

    protected $table = 'users';
    public static $table_name = 'users';
    public static $model = 'App\Http\Models\Portal\Core\PortalUser';
    public static $rows = 10;

    /**
     * PortalUser constructor.
     * @param array $fillable
     */
    public function __construct()
    {
        $fields = config('database.user_meta_fields');
        if(count($fields) > 0){
            foreach ($fields as $field) {
                $this->fillable[] = str_slug($field['name']);
            }
        }
    }


    public static function getSettings()
    {
        $settings = new stdClass();
        $settings->table = self::$table_name;
        $settings->model = self::$model;
        $settings->rows = self::$rows;
        return $settings;
    }
    //------------------------------------------------
    public static function create_rules()
    {
        return [
            'first_name' => 'required|regex:/^([A-Z \'-]+)$/i',
            'last_name' => 'required|regex:/^([A-Z \'-]+)$/i',
            'username' => 'string|unique:users|regex:/(^([a-zA-Z.]+)(\d+)?$)/u',
            'email' => 'required|email|unique:users',
            'password' => '',
            'mobile' => 'numeric|digits_between:9,10',
            'role' => '',
            'avatar' => 'url',
            'active' => 'boolean',
        ];
    }
    //------------------------------------------------
    public static function update_rules()
    {
        return [
            'id' => 'required',
            'email' => 'email',
            'mobile' => 'numeric|digits_between:9,10',
        ];
    }
    //------------------------------------------------
    public static function store($input = NULL)
    {
        $model = self::$model;
        $settings = $model::getSettings();
        $model = $settings->model;
        if ($input == NULL) {
            $input = request()->all();
        }

        //------------------
        $input = Helper::xss_patch_input($input);
        //------------------

        foreach ($input as $k =>$v){
            if($v=='true' || $v=='false'){
                $input[$k] = filter_var($v, FILTER_VALIDATE_BOOLEAN);
            }
        }
        //------------------

        if (!is_object($input)) {
            $input = (object)$input;
        }


        //if id is provided then find and update
        if (isset($input->id) && !empty($input->id)) {
            $validator = Validator::make((array)$input, $model::update_rules());
            if ($validator->fails()) {
                $response['status'] = 'error';
                $response['messages'] =$validator->errors();
                return $response;
            }
            $item = $model::where('id', $input->id)->first();


        }
        //if slug is provided then find and update
        if (isset($input->slug) && !empty($input->slug)) {
            $validator = Validator::make((array)$input, $model::update_rules());
            if ($validator->fails()) {
                $response['status'] = 'error';
                $response['messages'] =$validator->errors();
                return $response;
            }
            $item = $model::where('slug', '=', $input->slug)->first();
        }
        //if neither id nor slug are provided then create new
        if (!isset($item)) {
            $validator = Validator::make((array)$input, $model::create_rules());
            if ($validator->fails()) {
                $response['status'] = 'error';
                $response['messages'] =$validator->errors();
                return $response;
            }
            $item = new $model();
        }
        $columns = Schema::getColumnListing($settings->table);
        $input_array = (array)$input;
        foreach ($columns as $key) {
            if (array_key_exists($key, $input_array)) {
                //column and input field name are same
                if ($key == 'name') {
                    $item->$key = ucwords($input_array[$key]);
                }elseif ($key == 'password') {
                    $item->$key = Hash::make($input_array[$key]);
                }elseif ($key == 'username' && empty($input_array[$key])) {
                    $parts = explode('@', $input_array['email']);
                    $item->$key = $parts[0];
                } else {
                    $item->$key = $input_array[$key];
                }
            } else if (isset($input_array['name']) && $key == 'slug' && !array_key_exists($key, $input_array)) {
                //column name is not same as input name
                $item->$key = str_slug($input_array['name']);
            } else if (!array_key_exists($key, $input_array) && $key == 'created_by') {

                $item->$key = Auth::user()->username;
            }
        }
        try {
            $item->save();

            $response['status'] = 'success';
            $response['data'] = $item;
        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['messages'] = $e->getMessage();
        }
        return $response;
    }
}
