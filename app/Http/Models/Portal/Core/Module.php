<?php

namespace App\Http\Models\Portal\Core;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use stdClass;
use Validator;

class Module extends Model
{
    protected $fillable = [
        'parent_id',
        'title',
        'category_id',
        'slug',
        'active',
    ];

    public static $table_name = 'modules';
    public static $model = 'App\Http\Models\Portal\Core\Module';
    public static $rows = 10;

    public static function getSettings()
    {
        $settings = new stdClass();
        $settings->table = self::$table_name;
        $settings->model = self::$model;
        $settings->rows = self::$rows;
        return $settings;
    }
    //------------------------------------------------
    public static function create_rules()
    {
        return [
            'parent_id' => 'integer',
            'title' => 'required',
            'category_id' => 'required|integer',
            'slug' => '',
            'active' => 'boolean',
        ];
    }
    //------------------------------------------------
    public static function update_rules()
    {
        return [
            'id' => 'required',
        ];
    }
    //------------------------------------------------
    public static function store($input = NULL)
    {
        $model = self::$model;
        $settings = $model::getSettings();
        $model = $settings->model;
        if ($input == NULL) {
            $input = request()->all();
        }

        //------------------
        $input = Helper::xss_patch_input($input);
        //------------------

        foreach ($input as $k =>$v){
            if($v=='true' || $v=='false'){
                $input[$k] = filter_var($v, FILTER_VALIDATE_BOOLEAN);
            }
        }
        //------------------

        if (!is_object($input)) {
            $input = (object)$input;
        }


        //if id is provided then find and update
        if (isset($input->id) && !empty($input->id)) {
            $validator = Validator::make((array)$input, $model::update_rules());
            if ($validator->fails()) {
                $response['status'] = 'error';
                $response['messages'] =$validator->errors();
                return $response;
            }
            $item = $model::where('id', $input->id)->first();


        }
        //if slug is provided then find and update
        if (isset($input->slug) && !empty($input->slug)) {
            $validator = Validator::make((array)$input, $model::update_rules());
            if ($validator->fails()) {
                $response['status'] = 'error';
                $response['messages'] =$validator->errors();
                return $response;
            }
            $item = $model::where('slug', '=', $input->slug)->first();
        }
        //if neither id nor slug are provided then create new
        if (!isset($item)) {
            $validator = Validator::make((array)$input, $model::create_rules());
            if ($validator->fails()) {
                $response['status'] = 'error';
                $response['messages'] =$validator->errors();
                return $response;
            }
            $item = new $model();
        }
        $columns = Schema::getColumnListing($settings->table);
        $input_array = (array)$input;
        foreach ($columns as $key) {
            if (array_key_exists($key, $input_array)) {
                //column and input field name are same
                if ($key == 'name') {
                    $item->$key = ucwords($input_array[$key]);
                } else {
                    $item->$key = $input_array[$key];
                }
            } else if (isset($input_array['title']) && $key == 'slug' && !array_key_exists($key, $input_array)) {
                //column name is not same as input name
                $item->$key = str_slug($input_array['title']);
            }
        }
        try {
            $item->save();

            $response['status'] = 'success';
            $response['data'] = $item;
        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['messages'] = $e->getMessage();
        }
        return $response;
    }

    //--------------------------------------
    public function category()
    {
        return $this->belongsTo('App\Http\Models\Portal\Core\Category', 'category_id', 'id');
    }
}
