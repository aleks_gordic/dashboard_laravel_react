<?php

namespace App\Http\Models\Portal\Core;

use App\Helpers\Helper;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use stdClass;
use Validator;

class Permission extends Model
{

    protected $casts = [
        'access' => 'boolean'
    ];

    protected $fillable = [
        'role_id',
        'module_id',
        'access'
    ];

    public static $table_name = 'permissions';
    public static $model = 'App\Http\Models\Portal\Core\Permission';
    public static $rows = 10;


    public static function getSettings()
    {
        $settings = new stdClass();
        $settings->table = self::$table_name;
        $settings->model = self::$model;
        $settings->rows = self::$rows;
        return $settings;
    }
    //------------------------------------------------
    public static function create_rules()
    {
        return [
            'role_id' => 'required',
            'module_id' => 'required',
            'access' => 'boolean',
        ];
    }
    //------------------------------------------------
    public static function update_rules()
    {
        return [
            'id' => 'required',
        ];
    }
    //------------------------------------------------
    public static function store($input = NULL)
    {
        $model = self::$model;
        $settings = $model::getSettings();
        $model = $settings->model;
        if ($input == NULL) {
            $input = request()->all();
        }

        //------------------
        $input = Helper::xss_patch_input($input);
        //------------------

        foreach ($input as $k =>$v){
            if($v=='true' || $v=='false'){
                $input[$k] = filter_var($v, FILTER_VALIDATE_BOOLEAN);
            }
        }
        //------------------

        if (!is_object($input)) {
            $input = (object)$input;
        }


        //if id is provided then find and update
        if (isset($input->id) && !empty($input->id)) {
            $validator = Validator::make((array)$input, $model::update_rules());
            if ($validator->fails()) {
                $response['status'] = 'error';
                $response['messages'] =$validator->errors();
                return $response;
            }
            $item = $model::where('id', $input->id)->first();


        }
        //if slug is provided then find and update
        if (isset($input->slug) && !empty($input->slug)) {
            $validator = Validator::make((array)$input, $model::update_rules());
            if ($validator->fails()) {
                $response['status'] = 'error';
                $response['messages'] =$validator->errors();
                return $response;
            }
            $item = $model::where('slug', '=', $input->slug)->first();
        }
        //if neither id nor slug are provided then create new
        if (!isset($item)) {
            $validator = Validator::make((array)$input, $model::create_rules());
            if ($validator->fails()) {
                $response['status'] = 'error';
                $response['messages'] =$validator->errors();
                return $response;
            }

            //-----check if role_id exist for a module_id
            $count = $model::where('role_id',$input->role_id)->where('module_id',$input->module_id)->count();
            if($count >1){
                $model::where('role_id',$input->role_id)->where('module_id',$input->module_id)->delete();
            }

            $item = $model::where('role_id',$input->role_id)->where('module_id',$input->module_id)->first();
            if(!$item){
                $item = new $model();
            }

        }
        $columns = Schema::getColumnListing($settings->table);
        $input_array = (array)$input;
        foreach ($columns as $key) {
            if (array_key_exists($key, $input_array)) {
                //column and input field name are same
                if ($key == 'name') {
                    $item->$key = ucwords($input_array[$key]);
                } else {
                    $item->$key = $input_array[$key];
                }
            } else if (isset($input_array['name']) && $key == 'slug' && !array_key_exists($key, $input_array)) {
                //column name is not same as input name
                $item->$key = str_slug($input_array['name']);
            }
        }
        try {
            $item->save();

            $response['status'] = 'success';
            $response['data'] = $item;
        } catch (\Exception $e) {
            $response['status'] = 'error';
            $response['messages'] = $e->getMessage();
        }
        return $response;
    }
    //------------------------
    public static function formatPermissions($role, $modules, $override = true){

        if ($role->slug === 'admin') {

            $permissions = array_map(function ($module) use ($override) {

                $temp['id']     = $module['id'];
                $temp['title']  = $module['title'];
                $temp['slug']   = $module['slug'];

                if(!$override){
                    $temp['category']['id']  = $module['category']['id'];
                    $temp['category']['title']  = $module['category']['title'];
                    $temp['category']['slug']   = $module['category']['slug'];
                    if(isset($module['sub_permissions'])){
                        $temp['sub_permissions']   = $module['sub_permissions'];
                    }
                }

                $temp['access'] = true;
                return $temp;
            }, $modules);

        } else {

            $model = self::$model;
            $existing = $model::where('role_id', $role->id)->get()->toArray();

            $permissions = array_map(function ($module) use ($existing, $override) {

                $temp['id']     = $module['id'];
                $temp['title']  = $module['title'];
                $temp['slug']   = $module['slug'];

                if(!$override){
                    $temp['category']['id']  = $module['category']['id'];
                    $temp['category']['title']  = $module['category']['title'];
                    $temp['category']['slug']   = $module['category']['slug'];
                    if(isset($module['sub_permissions'])){
                        $temp['sub_permissions']   = $module['sub_permissions'];
                    }
                }

                $item = null;
                foreach ($existing as $struct) {
                    if ($module['id'] == $struct['module_id']) {
                        $item = $struct;
                        break;
                    }
                }

                if ($item == null) {
                    $temp['access'] = false;
                } else {
                    $temp['access'] = $item['access'] ? true : false;
                }

                return $temp;

            }, $modules);

        }

        return $permissions;
    }
    //------------------------
    public static function getUserPermission($userid = null){
        try {

            if(!$userid){
                $user = Auth::user();
            }else{
                $user = User::find($userid);
            }
            $role = Role::where('slug',$user->role)->first();

            $modules = Module::where('active', true)->whereNull('parent_id')->with('category')->get()->toArray();
            $modules = array_map(function ($module) use ($role) {
                $sub = Module::where('parent_id', $module['id'])->get()->toArray();
                if(!empty($sub)){
                    $sub = self::formatPermissions($role, $sub);
                    $module['sub_permissions'] = $sub;
                }
                return $module;
            }, $modules);

            $permissions =  self::formatPermissions($role, $modules, false);

            $new = [];
            foreach ($permissions as $permission){
                $temp = [];
                if($permission['access']){
                    if(isset($permission['sub_permissions']) && count($permission['sub_permissions']) > 0){
                        foreach ($permission['sub_permissions'] as $sub_permission){
                            if($sub_permission['access']){
                                $temp['parent'] = $permission['slug'];
                                $temp['children'][] = $sub_permission['slug'];
                            }
                        }

                        if(!empty($temp)){
                            $new[] = $temp;
                        }

                    }else{
                        $new[] = $permission['slug'];
                    }
                }
            }

            return $new;

        } catch (\Exception $e) {
            /*$response['status'] = 'error';
            $response['message'] = $e->getMessage();*/
            return [];
        }


    }
    //------------------------
    public static function hasPermission($slug, $child_slug = null, $userid = null){
        $permissions = self::getUserPermission($userid);
        if(empty($permissions)) return false;

        $access = false;

        foreach ($permissions as $permission){
            if($child_slug && is_array($permission)){
                $parent = $permission['parent'];
                $children = $permission['children'];
                foreach ($children as $child){
                    if($parent == $slug && $child == $child_slug){
                        $access = true;
                        break;
                    }

                }
            }else{
                if($permission == $slug){
                    $access = true;
                    break;
                }
            }
        }

        return $access;
    }

}
