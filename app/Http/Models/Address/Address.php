<?php

namespace App\Http\Models\Address;

use GuzzleHttp\Client;

class Address
{
    /**
     * Create a http client
     * @var \GuzzleHttp\Client
     */
    protected $http;

    /**
     * Addressfinder key
     * @var string
     */
    protected $key;

    /**
     * Addressfinder secret
     * @var string
     */
    protected $secret;

    /**
     * @var string
     */
    protected $address;

    protected $country;

    protected $cleanse_path;

    public function __construct($address, $country = 'NZ')
    {
        $this->country = $country;
        $this->address = $address;

        if(config('portal.enableAddy') && $country == 'NZ'){
            $base_uri      = config('addressfinder.addy.url');
            $this->key     = config('addressfinder.addy.credentials.key');
            $this->secret  = config('addressfinder.addy.credentials.secret');
            $this->cleanse_path = 'validation?key=' . $this->key . '&address=' . urlencode($this->address);

        }else{
            $base_uri      = config('addressfinder.af.url');
            $this->key     = config('addressfinder.af.credentials.key');
            $this->secret  = config('addressfinder.af.credentials.secret');
            $this->cleanse_path = strtolower($this->country) . '/address/cleanse?key=' . $this->key . '&secret=' . $this->secret . '&q=' . urlencode($this->address) . '&format=json';
        }



        $this->http    = new Client([
            'http_errors' => false,
            'base_uri'    => $base_uri,
        ]);
    }

    /**
     * Finds address.
     *
     * @return array|boolean
     */
    public function get()
    {
        try {

            $country = $this->country;
            if(strtolower($country) != 'other' ){
                $request = $this->http->request('GET', $this->cleanse_path)->getBody();
                $result  = json_decode($request, true);

                if (config('portal.address_validation')) {

                    if( (config('portal.enableAddy') && empty($result['address'])) || (!config('portal.enableAddy') && empty($result['matched'])) ){
                        return false;
                    }


                }elseif(!config('portal.address_validation')){

                    if( (config('portal.enableAddy') && empty($result['address'])) || (!config('portal.enableAddy') && empty($result['matched'])) ){
                        return [
                            'fullAddress'  => $this->address,
                            'addressLine1' => $this->address,
                            'addressLine2' => '',
                            'city'         => '',
                            'suburb'       => '',
                            'postcode'     => '',
                        ];
                    }
                }

                if (isset($result['address']) && $country == 'AU') {
                    return [
                        'fullAddress'  => $result['address']['full_address'],
                        'addressLine1' => $result['address']['address_line_1'],
                        'addressLine2' => $result['address']['address_line_2'],
                        'city'         => '',
                        'suburb'       => $result['address']['locality_name'],
                        'postcode'     => $result['address']['postcode'],
                    ];
                } elseif (config('portal.enableAddy') && $result['address'] && $country == 'NZ'){

                    $add = $result['address'];

                    return [
                        'fullAddress'  => htmlentities($this->address),
                        'addressLine1' => $add['address1'],
                        'addressLine2' => $add['address2'],
                        'city'         => $add['city'],
                        'suburb'       => $add['suburb'],
                        'postcode'     => $add['postcode'],
                    ];

                } else {

                    return [
                        'fullAddress'  => htmlentities($this->address),
                        'addressLine1' => $result['postal_line_2'],
                        'addressLine2' => $result['postal_line_1'],
                        'city'         => $result['city'],
                        'suburb'       => $result['suburb'],
                        'postcode'     => $result['postcode'],
                    ];
                }



            }else{
                //just return as it is
                return [
                    'fullAddress'  => $this->address,
                    'addressLine1' => $this->address,
                    'addressLine2' => '',
                    'city'         => '',
                    'suburb'       => '',
                    'postcode'     => '',
                ];
            }
        } catch (\Exception $e) {
            return [
                'fullAddress'  => $this->address,
                'addressLine1' => $this->address,
                'addressLine2' => '',
                'city'         => '',
                'suburb'       => '',
                'postcode'     => '',
            ];
        }
    }
}