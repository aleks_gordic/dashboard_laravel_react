<?php

namespace App\Http\Models\BatchCheck;

use Illuminate\Database\Eloquent\Model;

class BatchSms extends Model
{
    protected $table    = 'batch_sms';
    protected $fillable = [
        'id',
        'bid',
        'tid',
        'region',
        'contact',
        'reference'
    ];
}