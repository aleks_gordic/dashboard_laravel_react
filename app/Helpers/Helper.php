<?php
/**
 * Created by PhpStorm.
 * User: xantosh
 * Date: 16/11/18
 * Time: 12:16 PM
 */

namespace App\Helpers;


use App\Http\Models\Transactions\Transactions;
use Illuminate\Support\Facades\Storage;

class Helper
{

    /*
    |--------------------------------------------------------------------------
    | Transaction Controller
    |--------------------------------------------------------------------------
    |
    | List of common static helper function which can be used throughout the project
    |
    */

    /**
     * Generate a random 9 digit transactionId
     * @return int
     */
    public static function generateTransactionId()
    {
        $number = mt_rand(100000000, 999999999);

        if (self::transactionIdExists($number)) {
            return self::generateTransactionId();
        }

        return $number;
    }

    /**
     * Checks if the transactionId exists in the database
     * @param $number
     * @return mixed
     */
    public static function transactionIdExists($number)
    {
        return Transactions::where('transactionId', $number)->exists();
    }


    /**
     * Checks if the originalToken exists in the database
     * @param $number
     * @return mixed
     */
    public static function originalTokenExists($number)
    {
        return Transactions::where('original_token', $number)->exists();
    }

    /**
     * This generate unique session for "transactions" table
     * @param int $length
     * @return string
     */
    public static function generateSessionCode($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        /*if(self::sessionCodeExists($randomString)){
            $randomString = '';
            $randomString .= self::generateSessionCode();
        }*/
        return $randomString;
    }

    /**
     * Generate a hash token
     *
     * @return string
     */
    public static function generateToken()
    {
        $hash = md5(sha1(mt_rand(100000000, 999999999)));

        if (self::tokenExists($hash)) {
            return self::generateToken();
        }

        return $hash;
    }

    /**
     * Check if token exists
     *
     * @return boolean
     */
    public static function tokenExists($token)
    {
        return Transactions::where('token', $token)->exists();
    }
    /**
     * validates location co-ordinates
     * @param $loc
     * @return boolean
     */

    public static function validateLoc($loc)
    {
        return preg_match('/^[-]?((([0-8]?[0-9])(\.(\d+))?)|(90(\.0+)?)),[-]?((((1[0-7][0-9])|([0-9]?[0-9]))(\.(\d+))?)|180(\.0+)?)$/', $loc);
    }

    /**
     * Adds XSS patches to Inputs in Pen testing
     * @param $inputs
     * @return array|string
     */
    public static function xss_patch_input($inputs)
    {

        if (is_array($inputs)) {
            $temp = [];

            foreach ($inputs as $k => $v) {

                if (is_object($v) || is_array($v)) {
                    $temp[$k] = self::xss_patch_input($v);
                } else {
                    $temp[$k] = htmlentities($v);
                }

            }

            return $temp;

        } elseif (is_object($inputs)) {
            $temp = [];
            $inputs = json_decode(json_encode($inputs), true);

            foreach ($inputs as $k => $v) {

                if (is_object($v) || is_array($v)) {
                    $temp[$k] = self::xss_patch_input($v);
                } else {
                    $temp[$k] = htmlentities($v);
                }

            }

            return json_decode(json_encode($temp));

        } elseif (is_string($inputs)) {

            return htmlentities($inputs);

        } else {

            return $inputs;

        }
    }

    /**
     * check json
     * @param $string
     * @return boolean
     */
    public static function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * get site url
     * @return string
     */
    public static function getSiteUrl(){
        if(env('APP_ENV') == 'local'){
            return url('/');
        }

        return secure_url('/');
    }

    /**
     * check if centrix enabled
     * @return string
     */
    public static function hasCentrix($iso){
        return config('centrix.isEnabled') && in_array($iso, config('centrix.countries'));
    }

}