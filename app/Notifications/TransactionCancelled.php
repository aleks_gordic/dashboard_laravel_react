<?php

namespace App\Notifications;

use App\Http\Models\Transactions\Transactions;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TransactionCancelled extends Notification implements ShouldQueue
{
    use Queueable;

    protected $columns;
    protected $data;
    protected $type;
    protected $subject;
    /**
     * @param  Transactions  $tr
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($tr)
    {
        // Column
        $this->columns = [
            'transactionId' => 'Transaction ID',
            'reference'     => 'Reference',
            'name'          => 'Customer Name',
        ];

        // Data
        $this->data = [
            'transactionId' => $tr->transactionId,
            'reference'     => $tr->reference ? $tr->reference : 'NA',
            'name'          => (!$tr->contactDetails->name || is_numeric($tr->contactDetails->name)) ? 'NA' : $tr->contactDetails->name,
        ];

        $this->subject = ($tr->reference) ? 'Verification Cancelled - '.$tr->reference : 'Verification Cancelled';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Verification Cancelled")
            ->markdown('emails.notifications.transaction-status', ['type' => 'cancelled','columns' => $this->columns,'data' => $this->data]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
