<?php

namespace App\Notifications;

use App\Helpers\Helper;
use App\Http\Models\Centrix\Centrix;
use App\Http\Models\Portal\Core\Permission;
use App\Http\Models\Transactions\Transactions;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TransactionCompleted extends Notification implements ShouldQueue
{
    use Queueable;

    protected $columns;
    protected $data;
    protected $flags;
    protected $type;
    protected $subject;
    /**
     * @param  array  $columns
     * @param  array  $data
     * @param  array  $flags
     * @param  string  $type
     * @param  string  $ref
     *
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($type, $columns, $data, $flags, $ref = null)
    {

        $this->type = $type;
        $this->data = $data;
        $this->flags = $flags;
        $this->columns = $columns;

        if( $this->type == 'transaction-pass' ){
            $this->subject = ($ref) ? 'Verification ID Passed - '.$ref : 'Verification ID Passed';
        }elseif ( $this->type == 'transaction-failed-data' ){
            $this->subject = ($ref) ? 'Verification Data Flagged - '.$ref : 'Verification Data Flagged';
        } else {
            $this->subject = ($ref) ? 'Verification ID Flagged - '.$ref : 'Verification ID Flagged';
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject($this->subject)
            ->markdown('emails.notifications.transaction-status', ['type' => $this->type ,'columns' => $this->columns,'data' => $this->data, 'flags' => $this->flags]);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
