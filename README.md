# react-laravel-with-jwt-authentication
FantasyLab Company site using reactjs (16.6.3), redux-thunk, sass, laravel (5.8), Browser sync, mysql with jwt-authentication.

# usage :
1. Rename .env.example to .env
2. Update DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD, and Social IDs in .env file to yours.
3. In command line : 
   - composer update
   - Php artisan key:generate
   - php artisan migrate
4. npm install
5. npm run dev
6. php artisan serve

# To-Do list :
- User Authentication
- Babel Webpack 
- Dashboard (Graph, Chart)

# Project Live link
[a relative link] https://staging.ausdemo.idkit.co/portal